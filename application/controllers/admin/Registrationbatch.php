<?php

class Registrationbatch extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("regbatch_model");
    }

    public function index() { 
        if (!$this->rbac->hasPrivilege('reg_batches', 'can_view')) {
            access_denied();
        }
        $this->session->set_userdata('top_menu', 'Student Information');
        $this->session->set_userdata('sub_menu', 'admin/registrationbatch');
        $data['title'] = 'Add School House';
        $data["name"] = "";
        $data["description"] = ""; 
        $regbatchlist = $this->regbatch_model->get();
        $data["regbatchlist"] = $regbatchlist;
        $this->load->view('layout/header', $data); 
        $this->load->view('admin/regbatch/regbatchlist', $data);
        $this->load->view('layout/footer', $data);
    }

    function create() {
        if (!$this->rbac->hasPrivilege('reg_batches', 'can_add')) {
            access_denied();
        }
        $data['title'] = 'Add School House';
        $regbatchlist = $this->regbatch_model->get();
        $data["regbatchlist"] = $regbatchlist;
        $data["house_name"] = "";
        $data["description"] = "";
        $this->form_validation->set_rules('name', $this->lang->line('name'), 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layout/header', $data);
            $this->load->view('admin/regbatch/regbatchlist', $data);
            $this->load->view('layout/footer', $data);
        } else {
            $data = array(
                'name' => $this->input->post('name'),
                'is_active' => 'yes',
                'description' => $this->input->post('description')
            );
            $this->regbatch_model->add($data);

            $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('success_message') . '</div>');
            redirect('admin/registrationbatch/index');
        }
    }

    function edit($id) {
        if (!$this->rbac->hasPrivilege('reg_batches', 'can_edit')) {
            access_denied();
        }
        $data['title'] = 'Edit School House';
        $regbatchlist = $this->regbatch_model->get();
        $data["regbatchlist"] = $regbatchlist;
        $data['id'] = $id;
        $house = $this->regbatch_model->get($id);
        $data["house"] = $house;
        $data["name"] = $house["name"];
        $data["description"] = $house["description"];
        $this->form_validation->set_rules('name', $this->lang->line('name'), 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layout/header', $data);
            $this->load->view('admin/regbatch/regbatchlist', $data);
            $this->load->view('layout/footer', $data);
        } else {  
            $data = array(
                'id' => $id,
                'name' => $this->input->post('name'),
                'is_active' => 'yes',
                'description' => $this->input->post('description')
            );
            $this->regbatch_model->add($data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('update_message') . '</div>');
            redirect('admin/registrationbatch');
        }
    }

    function delete($id) {
        if (!$this->rbac->hasPrivilege('reg_batches', 'can_delete')) {
            access_denied();
        }
        if (!empty($id)) {

            $this->regbatch_model->delete($id);
            $this->session->set_flashdata('msgdelete', '<div class="alert alert-success text-left">' . $this->lang->line('delete_message') . '</div>');
        }
        redirect('admin/registrationbatch/');
    }

}

?>