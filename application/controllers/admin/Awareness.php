<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Awareness extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');

        // $this->load->model("visitors_purpose_model");
        $this->load->model("awareness_model"); 
    }

    function index() {
        if (!$this->rbac->hasPrivilege('setup_font_office', 'can_view')) {
            access_denied();
        }

        $this->session->set_userdata('top_menu', 'front_office');
        $this->session->set_userdata('sub_menu', 'admin/visitorspurpose');
        $this->form_validation->set_rules('awareness', $this->lang->line('awareness'), 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['awareness_list'] = $this->awareness_model->awareness_list();
 
            $this->load->view('layout/header'); 
            // $this->load->view('admin/frontoffice/visitorspurposeview', $data);
            $this->load->view('admin/frontoffice/awarenesstype', $data);
            $this->load->view('layout/footer');
        } else { 

            $awareness = array(
                'awareness' => $this->input->post('awareness'),
                'description' => $this->input->post('description')
            );
            $this->awareness_model->add($awareness);
            $this->session->set_flashdata('msg', '<div class="alert alert-success">' . $this->lang->line('success_message') . '</div>');
            redirect('admin/awareness');
        }
    }

    function edit($awareness_id) {
        if (!$this->rbac->hasPrivilege('setup_font_office', 'can_edit')) {
            access_denied(); 
        }
        $this->form_validation->set_rules('awareness', $this->lang->line('awareness'), 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['awareness_list'] = $this->awareness_model->awareness_list();
            $data['awareness_data'] = $this->awareness_model->awareness_list($awareness_id);
            $this->load->view('layout/header');
            $this->load->view('admin/frontoffice/awarenesseditview', $data);
            $this->load->view('layout/footer');
        } else {

           $awareness = array(
                'awareness' => $this->input->post('awareness'),
                'description' => $this->input->post('description')
            );
            $this->awareness_model->update($awareness_id, $awareness);
            $this->session->set_flashdata('msg', '<div class="alert alert-success">' . $this->lang->line('update_message') . '</div>');
            redirect('admin/awareness');
        }
    }

    function delete($id) {
        if (!$this->rbac->hasPrivilege('setup_font_office', 'can_delete')) {
            access_denied();
        }
        $this->awareness_model->delete($id);
        $this->session->set_flashdata('msg', '<div class="alert alert-success">' . $this->lang->line('delete_message') . '</div>');
        redirect('admin/awareness');
    }

}
