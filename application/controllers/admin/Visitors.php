<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Visitors extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model("Visitors_model");
        $this->load->model("Class_model");
    }

    function index() {

        if (!$this->rbac->hasPrivilege('visitor_book', 'can_view')) {
            access_denied();
        }
        $this->session->set_userdata('top_menu', 'front_office');
        $this->session->set_userdata('sub_menu', 'admin/visitors');
        $this->form_validation->set_rules('purpose', $this->lang->line('purpose'), 'required');
        $this->form_validation->set_rules('name', $this->lang->line('name'), 'required');
        $this->form_validation->set_rules('date', $this->lang->line('date'), 'required');
        $this->form_validation->set_rules('file', $this->lang->line('file'), 'callback_handle_upload[file]');

        if ($this->form_validation->run() == FALSE) {
            $data['visitor_list'] = $this->Visitors_model->visitors_list();
            $data['Purpose'] = $this->Visitors_model->getPurpose();
            $this->load->view('layout/header');
            $this->load->view('admin/frontoffice/visitorview', $data);
            $this->load->view('layout/footer');
        } else {
            $visitors = array(
                'purpose' => $this->input->post('purpose'),
                'name' => $this->input->post('name'),
                'contact' => $this->input->post('contact'),
                'id_proof' => $this->input->post('id_proof'),
                'no_of_pepple' => $this->input->post('pepples'),
                'date' => date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('date'))),
                'in_time' => $this->input->post('time'),
                'out_time' => $this->input->post('out_time'),
                'note' => $this->input->post('note')
            );

            $visitor_id = $this->Visitors_model->add($visitors);

            if (isset($_FILES["file"]) && !empty($_FILES['file']['name'])) {
                $fileInfo = pathinfo($_FILES["file"]["name"]);
                $img_name = 'id' . $visitor_id . '.' . $fileInfo['extension'];
                move_uploaded_file($_FILES["file"]["tmp_name"], "./uploads/front_office/visitors/" . $img_name);
                $this->Visitors_model->image_add($visitor_id, $img_name);
            }

            $this->session->set_flashdata('msg', '<div class="alert alert-success">' . $this->lang->line('success_message') . '</div>');
            redirect('admin/visitors');
        }
    }

    public function delete($id) {
        if (!$this->rbac->hasPrivilege('visitor_book', 'can_delete')) {
            access_denied();
        }

        $this->Visitors_model->delete($id);
    }
public function delete_parent_visit($id) {
       

        $this->Visitors_model->deleteParentVisit($id);
        redirect('admin/visitors/parent_visit');
    }
    public function delete_social_awareness($id) {
       

        $this->Visitors_model->deletesocialawareness($id);
        redirect('admin/visitors/social_awareness');
    }
    public function delete_spot_situation($id) {
       

        $this->Visitors_model->deleteSpotSituation($id);
        redirect('admin/visitors/spot_situation');
    }
    public function edit($id) {
        if (!$this->rbac->hasPrivilege('visitor_book', 'can_edit')) {
            access_denied();
        }

        $this->form_validation->set_rules('purpose', $this->lang->line('purpose'), 'required');

        $this->form_validation->set_rules('name', $this->lang->line('name'), 'required');

        $this->form_validation->set_rules('file', $this->lang->line('file'), 'callback_handle_upload[file]');
        if ($this->form_validation->run() == FALSE) {

            $data['Purpose'] = $this->Visitors_model->getPurpose();
            $data['visitor_list'] = $this->Visitors_model->visitors_list();
            $data['visitor_data'] = $this->Visitors_model->visitors_list($id);
            $this->load->view('layout/header');
            $this->load->view('admin/frontoffice/visitoreditview', $data);
            $this->load->view('layout/footer');
        } else {

            $visitors = array(
                'purpose' => $this->input->post('purpose'),
                'name' => $this->input->post('name'),
                'contact' => $this->input->post('contact'),
                'id_proof' => $this->input->post('id_proof'),
                'no_of_pepple' => $this->input->post('pepples'),
                'date' => date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('date'))),
                'in_time' => $this->input->post('time'),
                'out_time' => $this->input->post('out_time'),
                'note' => $this->input->post('note')
            );
            if (isset($_FILES["file"]) && !empty($_FILES['file']['name'])) {
                $fileInfo = pathinfo($_FILES["file"]["name"]);

                $img_name = 'id' . $id . '.' . $fileInfo['extension'];
                move_uploaded_file($_FILES["file"]["tmp_name"], "./uploads/front_office/visitors/" . $img_name);
                $this->Visitors_model->image_update($id, $img_name);
            }
            $this->Visitors_model->update($id, $visitors);
            redirect('admin/visitors');
        }
    }

    public function details($id) {
        if (!$this->rbac->hasPrivilege('visitor_book', 'can_view')) {
            access_denied();
        }

        $data['data'] = $this->Visitors_model->visitors_list($id);
        $this->load->view('admin/frontoffice/Visitormodelview', $data);
    }

    public function download($documents) {
        $this->load->helper('download');
        $filepath = "./uploads/front_office/visitors/" . $documents;
        $data = file_get_contents($filepath);
        $name = $documents;
        force_download($name, $data);
    }

    public function imagedelete($id, $image) {
        if (!$this->rbac->hasPrivilege('visitor_book', 'can_delete')) {
            access_denied();
        }
        $this->Visitors_model->image_delete($id, $image);
    }

    public function check_default($post_string) {
        return $post_string == "" ? FALSE : TRUE;
    }

    public function handle_upload($str,$var)
    {

        $image_validate = $this->config->item('file_validate');
        $result = $this->filetype_model->get();
        if (isset($_FILES[$var]) && !empty($_FILES[$var]['name'])) {

            $file_type         = $_FILES[$var]['type'];
            $file_size         = $_FILES[$var]["size"];
            $file_name         = $_FILES[$var]["name"];

            $allowed_extension = array_map('trim', array_map('strtolower', explode(',', $result->file_extension)));
            $allowed_mime_type = array_map('trim', array_map('strtolower', explode(',', $result->file_mime)));
            $ext               = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
            
            if ($files = filesize($_FILES[$var]['tmp_name'])) {

                if (!in_array($file_type, $allowed_mime_type)) {
                    $this->form_validation->set_message('handle_upload', 'File Type Not Allowed');
                    return false;
                }

                if (!in_array($ext, $allowed_extension) || !in_array($file_type, $allowed_mime_type)) {
                    $this->form_validation->set_message('handle_upload', 'Extension Not Allowed');
                    return false;
                }
                if ($file_size > $result->file_size) {
                    $this->form_validation->set_message('handle_upload', $this->lang->line('file_size_shoud_be_less_than') . number_format($image_validate['upload_size'] / 1048576, 2) . " MB");
                    return false;
                }

            } else {
                $this->form_validation->set_message('handle_upload', "File Type / Extension Error Uploading  Image");
                return false;
            }

            return true;
        }
        return true;

    } 



     function parent_visit() {

        if (!$this->rbac->hasPrivilege('parent_visit', 'can_view')) {
            access_denied();
        }
        $this->session->set_userdata('top_menu', 'front_office');
        $this->session->set_userdata('sub_menu', 'admin/visitors/parent_visit');

        $this->form_validation->set_rules('std_id', $this->lang->line('student_name'), 'required');
        $this->form_validation->set_rules('date', $this->lang->line('date'), 'required');
        $this->form_validation->set_rules('class_id', $this->lang->line('class_id'), 'required');
         $data['classlist']       = $this->class_model->get(); 
        if ($this->form_validation->run() == FALSE) { 
            $visitor_list = $this->Visitors_model->parent_visitors_list();
            $result = array();  
            if(!empty($visitor_list)){
                foreach($visitor_list as $row){
                    $class_id = $row['class_id']; 
                    $std_id = $row['std_id']; 
                    $class_row = $this->Class_model->getAll($class_id);
                    $std_row = $this->student_model->getstudentbyid($std_id);
                    $std_name = $std_row->firstname.' '.$std_row->middlename.' '.$std_row->lastname;
                    $result[] = array(
                        'id'=>$row['id'],
                        'date'=>$row['date'],
                        'class'=> isset($class_row['class']) ? $class_row['class'] : '',
                        'std_name'=> isset($std_name) ? $std_name : '',
                        'relative_relation'=>$row['relative_relation'],
                        'reason'=>$row['reason'],
                    );
                }
            }
            $data['visitor_list'] = $result;
            $this->load->view('layout/header');
            $this->load->view('admin/frontoffice/parentvisitorview', $data); 
            $this->load->view('layout/footer');
        } else {
            $visitors = array(
                'date' => date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('date'))),
                'class_id' => $this->input->post('class_id'),
                'std_id' => $this->input->post('std_id'),
                'relative_relation' => $this->input->post('relative_relation'),
                'reason' => $this->input->post('reason')
            );
 
            $visitor_id = $this->Visitors_model->addParentVisitor($visitors);

            $this->session->set_flashdata('msg', '<div class="alert alert-success">' . $this->lang->line('success_message') . '</div>');
            redirect('admin/visitors/parent_visit');
        }
    }
function social_awareness() {

        if (!$this->rbac->hasPrivilege('social_awareness', 'can_view')) {
            access_denied();
        }
        $this->session->set_userdata('top_menu', 'front_office');
        $this->session->set_userdata('sub_menu', 'admin/visitors/social_awareness');

        $this->form_validation->set_rules('date', $this->lang->line('date'), 'required');
        $this->form_validation->set_rules('class_id', $this->lang->line('class_id'), 'required');
         $data['classlist']       = $this->class_model->get(); 
        if ($this->form_validation->run() == FALSE) { 
            $visitor_list = $this->Visitors_model->social_awarenessors_list();
            $data['awareness_types'] = $this->Visitors_model->getawarenesstypes();
            $result = array(); 
            if(!empty($visitor_list)){
                foreach($visitor_list as $row){
                    $class_id = $row['class_id']; 
                    $awareness_type = $row['awareness_type']; 
                    $class_row = $this->Class_model->getAll($class_id);
                    $awr_type_row = $this->Visitors_model->getawarenesstypes($awareness_type);
                    $result[] = array(
                        'id'=>$row['id'],
                        'date'=>$row['date'],
                        'class'=> isset($class_row['class']) ? $class_row['class'] : '',
                        'awareness_type'=> isset($awr_type_row['awareness']) ? $awr_type_row['awareness'] : '',
                        'awareness_place'=>$row['awareness_place'],
                        'comments'=>$row['comments'], 
                    );
                }
            }
            $data['visitor_list'] = $result;
            $this->load->view('layout/header');
            $this->load->view('admin/frontoffice/socialawareness', $data);   
            $this->load->view('layout/footer');
        } else {
            $aware = array(
                'date' => date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('date'))),
                'class_id' => $this->input->post('class_id'),
                'awareness_type' => $this->input->post('awareness_type'),
                'awareness_place' => $this->input->post('awareness_place'),
                'comments' => $this->input->post('comment')
            );
 
            $visitor_id = $this->Visitors_model->addAwareness($aware);

            $this->session->set_flashdata('msg', '<div class="alert alert-success">' . $this->lang->line('success_message') . '</div>');
            redirect('admin/visitors/social_awareness');
        }
    }

    function spot_situation() {

        if (!$this->rbac->hasPrivilege('spot_situation', 'can_view')) {
            access_denied();
        }
        $this->session->set_userdata('top_menu', 'front_office');
        $this->session->set_userdata('sub_menu', 'admin/visitors/spot_situation');

        $this->form_validation->set_rules('std_id', $this->lang->line('student_name'), 'required');
        $this->form_validation->set_rules('date', $this->lang->line('date'), 'required');
        $this->form_validation->set_rules('class_id', $this->lang->line('class_id'), 'required');
         $data['classlist']       = $this->class_model->get(); 
        if ($this->form_validation->run() == FALSE) { 
            $visitor_list = $this->Visitors_model->spot_situation_list();
            $result = array();
            if(!empty($visitor_list)){
                foreach($visitor_list as $row){
                    $class_id = $row['class_id']; 
                    $std_id = $row['std_id']; 
                    $class_row = $this->Class_model->getAll($class_id);
                    $std_row = $this->student_model->getstudentbyid($std_id);
                    $std_name = $std_row->firstname.' '.$std_row->middlename.' '.$std_row->lastname;
                    $result[] = array(
                        'id'=>$row['id'],
                        'date'=>$row['date'],
                        'class'=> isset($class_row['class']) ? $class_row['class'] : '',
                        'std_name'=> isset($std_name) ? $std_name : '',
                        'situation'=>$row['situation'],
                        'transfering_party'=>$row['transfering_party'],
                        'procedure'=>$row['procedure'],
                    );
                }
            }
            $data['visitor_list'] = $result; 
            $this->load->view('layout/header');
            $this->load->view('admin/frontoffice/spotsituationview', $data); 
            $this->load->view('layout/footer');
        } else {
            $visitors = array(
                'date' => date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('date'))),
                'class_id' => $this->input->post('class_id'),
                'std_id' => $this->input->post('std_id'),
                'transfering_party' => $this->input->post('transfering_party'),
                'situation' => $this->input->post('situation'),
                'procedure' => $this->input->post('procedure')
            );
 
            $visitor_id = $this->Visitors_model->addSpotSituation($visitors);

            $this->session->set_flashdata('msg', '<div class="alert alert-success">' . $this->lang->line('success_message') . '</div>');
            redirect('admin/visitors/spot_situation');
        } 
    }
    public function getclasswisestd(){
        $class_id = $this->input->post('class_id'); 
        $result = $this->student_model->getStudentsByClassId($class_id);
         $html = '<option selected >Select Student</option>';
            if(!empty($result)){
                foreach($result as $row){
                    $html .= '<option value="'.$row['id'].'" >'.$row['firstname'].' '.$row['middlename'].' '.$row['middlename'].'</option>';
                }
            }
            echo $html; exit();
    }

}
