<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subject extends Admin_Controller {
public $sch_setting_detail = array();
    function __construct() {
        parent::__construct();
        $this->load->helper('file');
        $this->sch_setting_detail = $this->setting_model->getSetting();
    }

    function index() {
        if (!$this->rbac->hasPrivilege('subject', 'can_view')) {
            access_denied();
        }
        $this->session->set_userdata('top_menu', 'Academics');
        $this->session->set_userdata('sub_menu', 'Academics/subject');
        $data['title'] = 'Add subject';
        $subject_result = $this->subject_model->get();
        $data['subjectlist'] = $subject_result;
        $data['subject_types'] = $this->customlib->subjectType();
        $this->form_validation->set_rules('name', $this->lang->line('subject_name'), 'trim|required|xss_clean|callback__check_name_exists');
        $this->form_validation->set_rules('type', $this->lang->line('type'), 'trim|required|xss_clean');
        if ($this->input->post('code')) {
            $this->form_validation->set_rules('code', $this->lang->line('code'), 'trim|required|callback__check_code_exists');
        }
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layout/header', $data);
            $this->load->view('admin/subject/subjectList', $data);
            $this->load->view('layout/footer', $data);
        } else {
            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'type' => $this->input->post('type'),
            );
            $this->subject_model->add($data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('success_message') . '</div>');
            redirect('admin/subject/index');
        }
    } 

    public function assign_subject() {
        // if (!$this->rbac->hasPrivilege('subject', 'can_view')) {
        //     access_denied();
        // }
        $this->session->set_userdata('top_menu', 'Academics');
        $this->session->set_userdata('sub_menu', 'Academics/assign_subject');
        $data['title'] = 'Assign Course';
        $subject_result = $this->subject_model->get();
        $data['subjectlist'] = $subject_result;
        $data['subject_types'] = $this->customlib->subjectType();
         $data['faculties']          = $this->student_model->getFaculties();
         $data['sessionlist']        = $this->session_model->get();
          $data['sch_setting']     = $this->sch_setting_detail;
         $data['active_session_id'] = $data['sch_setting']->session_id;
         $class                      = $this->class_model->get('', $classteacher = 'yes');
        $data['classlist']          = $class;

        $assignedtlist            = $this->subject_model->getassignedlist();
         // echo "<pre>"; print_r($assignedtlist); exit();
        $data['assignedtlist']    = $assignedtlist;

        $staff_list         = $this->staff_model->getEmployee('2');
            $data['staff_list'] = $staff_list;
            $is_admin           = true;
        $this->form_validation->set_rules('faculty_id', $this->lang->line('faculty_id'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('academic_year', $this->lang->line('academic_year'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('class_id', $this->lang->line('class_id'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('subject_id', $this->lang->line('subject_id'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('teacher', $this->lang->line('teacher'), 'trim|required|xss_clean');
        
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layout/header', $data);  
            $this->load->view('admin/subject/assignSubject', $data);
            $this->load->view('layout/footer', $data);
        } else {
            
            $data = array(
                'faculty_id' => $this->input->post('faculty_id'),
                'academic_year' => $this->input->post('academic_year'),
                'class_id' => $this->input->post('class_id'),
                'subject_id' => $this->input->post('subject_id'),
                'teacher' => $this->input->post('teacher'),
            );
            $this->subject_model->assignSubject($data); 
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('success_message') . '</div>');
            redirect('admin/subject/assign_subject');
        }
    }

    function view($id) {
        if (!$this->rbac->hasPrivilege('subject', 'can_view')) {
            access_denied();
        }
        $data['title'] = 'Subject List';
        $subject = $this->subject_model->get($id);
        $data['subject'] = $subject;
        $this->load->view('layout/header', $data);
        $this->load->view('admin/subject/subjectShow', $data);
        $this->load->view('layout/footer', $data);
    }

    function delete($id) {
        if (!$this->rbac->hasPrivilege('subject', 'can_delete')) {
            access_denied();
        }
        $data['title'] = 'Subject List';
        $this->subject_model->remove($id);
        redirect('admin/subject/index');
    }
    function deleteassign($id) {
        if (!$this->rbac->hasPrivilege('subject', 'can_delete')) {
            access_denied();
        }
        $data['title'] = 'Course Assign';
        $this->subject_model->removeassigned($id);
        redirect('admin/subject/assign_subject');
    }
    function _check_name_exists() {
        $data['name'] = $this->security->xss_clean($this->input->post('name'));
        if ($this->subject_model->check_data_exists($data)) {
            $this->form_validation->set_message('_check_name_exists', $this->lang->line('name_already_exists'));
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function _check_code_exists() {
        $data['code'] = $this->security->xss_clean($this->input->post('code'));
        if ($this->subject_model->check_code_exists($data)) {
            $this->form_validation->set_message('_check_code_exists', $this->lang->line('code_already_exists'));
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function edit($id) {
        if (!$this->rbac->hasPrivilege('subject', 'can_edit')) {
            access_denied();
        }
        $subject_result = $this->subject_model->get();
        $data['subjectlist'] = $subject_result;
        $data['title'] = 'Edit Subject';
        $data['id'] = $id;
        $subject = $this->subject_model->get($id);
        $data['subject'] = $subject;
        $data['subject_types'] = $this->customlib->subjectType();
        $this->form_validation->set_rules('name', $this->lang->line('subject'), 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layout/header', $data);
            $this->load->view('admin/subject/subjectEdit', $data);
            $this->load->view('layout/footer', $data);
        } else {
            $data = array(
                'id' => $id,
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'type' => $this->input->post('type'),
            );
            $this->subject_model->add($data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('success_message') . '</div>');
            redirect('admin/subject/index');
        }
    }


    function editassign($id) {
        if (!$this->rbac->hasPrivilege('subject', 'can_edit')) {
            access_denied();
        }
          $subject_result = $this->subject_model->get();
        $data['subjectlist'] = $subject_result;
        $data['subject_types'] = $this->customlib->subjectType();
         $data['faculties']          = $this->student_model->getFaculties();
         $data['sessionlist']        = $this->session_model->get();
          $data['sch_setting']     = $this->sch_setting_detail;
         $data['active_session_id'] = $data['sch_setting']->session_id;
         $class                      = $this->class_model->get('', $classteacher = 'yes');
        $data['classlist']          = $class;

        $assignedtlist            = $this->subject_model->getassignedlist();
         // echo "<pre>"; print_r($assignedtlist); exit();
        $data['assignedtlist']    = $assignedtlist;

        $staff_list         = $this->staff_model->getEmployee('2');
            $data['staff_list'] = $staff_list;
            $is_admin           = true;
            //////////////////////////////
        $data['title'] = 'Edit Assigned Course';
        $data['id'] = $id;
        $assigned = $this->subject_model->getassign($id);
        $data['assigned'] = $assigned;
        $data['subject_types'] = $this->customlib->subjectType();
          $this->form_validation->set_rules('faculty_id', $this->lang->line('faculty_id'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('academic_year', $this->lang->line('academic_year'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('class_id', $this->lang->line('class_id'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('subject_id', $this->lang->line('subject_id'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('teacher', $this->lang->line('teacher'), 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layout/header', $data);
            $this->load->view('admin/subject/courseAssignEdit', $data); 
            $this->load->view('layout/footer', $data);
        } else {
            $data = array(
                'id' => $id,
                'faculty_id' => $this->input->post('faculty_id'),
                'academic_year' => $this->input->post('academic_year'),
                'class_id' => $this->input->post('class_id'),
                'subject_id' => $this->input->post('subject_id'),
                'teacher' => $this->input->post('teacher'),
            );
            $this->subject_model->updateassign($data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('success_message') . '</div>');
            redirect('admin/subject/assign_subject');
        }
    }

    function getSubjctByClassandSection() {
        $class_id = $this->input->post('class_id');
        $section_id = $this->input->post('section_id');
        $date = $this->teachersubject_model->getSubjectByClsandSection($class_id, $section_id);
        echo json_encode($data);
    }

}

?>