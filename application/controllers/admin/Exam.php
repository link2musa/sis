<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// include_once (dirname(__FILE__) . "/courses.php");

class Exams extends Admin_Controller
{
public $sch_setting_detail = array();
    function __construct()
    {  
        parent::__construct();
          $this->load->helper('file');
          $this->attendence_exam     = $this->config->item('attendence_exam');
          $this->exam_type = $this->config->item('exam_type');
        // admin_is_logged();
        // isValidUser(array(1, 2, 3));
$this->sch_setting_detail = $this->setting_model->getSetting();
        $this->table = array(
            'exams' => 'table_exams',
            'courses' => 'courses',
            'faculty' => 'faculty',
            'sub_exams' => 'table_sub_exams',
            'std_group' => 'tbl_std_group',
            'tbl_group' => 'tbl_group',
            'track_exam' => 'limu_track_exam',
            'user_group' => 'tbl_user_group',
            'exam_result' => 'table_subexams_std_result',
            'std_category' => 'std_category',
            'std_prev_edu' => 'std_previous_education',
            'guardian_file' => 'guardian_file',
            'year_semester' => 'year_semester',
            'batches_course' => 'batches_course',
            'student_detail' => 'student_detail',
            'sub_exams_eval' => 'sub_exams_evaluate',
            'tbl_std_prev_fac_batch_year' => 'tbl_std_prev_fac_batch_year',
        );

        ////////////***** Breadcrumbs *****\\\\\\\\\\\
        // $this->breadcrumbs->unshift(1, lang('examination'), 'admin/exams');

        //////////***** Load Controller *****\\\\\\\\\
        // $this->load->library('../controllers/admin/courses');
        ////////////***** Load Model *****\\\\\\\\\\\\
        // $this->load->model('admin/Hr_model');
        // $this->load->model('admin/Courses_model');
        // $this->load->model('admin/Batches_model');
        $this->load->model('Exams_model');
        $this->load->model('Section_model');
        $this->load->model('Subject_model');
        // $this->load->model('Common_model');
        // $this->load->model('admin/Temp_tbl_std_assigned_courses');
        // $this->load->model('admin/Student_model');
    }

    public function index()
    {
        // if (!$this->rbac->hasPrivilege('subject', 'can_view')) {
        //     access_denied();
        // }
       $this->session->set_userdata('top_menu', 'Examinations');
        $this->session->set_userdata('sub_menu', 'Examinations/examination');
        $data['title'] = 'Assign Course';
        $subject_result = $this->subject_model->get();
        $data['subjectlist'] = $subject_result;
        $data['subject_types'] = $this->customlib->subjectType();
         $data['faculties']          = $this->student_model->getFaculties();
         $data['sessionlist']        = $this->session_model->get();
          $data['sch_setting']     = $this->sch_setting_detail;
         $data['active_session_id'] = $data['sch_setting']->session_id;
         $class                      = $this->class_model->get('', $classteacher = 'yes');
        $data['classlist']          = $class;

        $assignedtlist            = $this->subject_model->getassignedlist();
         // echo "<pre>"; print_r($assignedtlist); exit();
        $data['assignedtlist']    = $assignedtlist;

        $staff_list         = $this->staff_model->getEmployee('2');
            $data['staff_list'] = $staff_list;  
            $is_admin           = true;
        $this->form_validation->set_rules('faculty_id', $this->lang->line('faculty_id'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('academic_year', $this->lang->line('academic_year'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('class_id', $this->lang->line('class_id'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('subject_id', $this->lang->line('subject_id'), 'trim|required|xss_clean');
          
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layout/header', $data);  
            $this->load->view('admin/exams/index', $data);
            $this->load->view('layout/footer', $data); 
        } else {  
             
            $data = array(
                'faculty_id' => $this->input->post('faculty_id'),
                'academic_year' => $this->input->post('academic_year'),
                'class_id' => $this->input->post('class_id'),
                'subject_id' => $this->input->post('subject_id'),
            );
            $this->subject_model->assignSubject($data); 
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('success_message') . '</div>');
            redirect('admin/subject/assign_subject'); 
        } 
    }  
public function subjectstudent()
    {
        $this->form_validation->set_error_delimiters('<p>', '</p>');
        $this->form_validation->set_rules('class_id', $this->lang->line('class'), 'required|trim|xss_clean');
        $this->form_validation->set_rules('section_id', $this->lang->line('section'), 'required|trim|xss_clean');
        $this->form_validation->set_rules('subject_id', $this->lang->line('subject'), 'required|trim|xss_clean');
        $this->form_validation->set_rules('session_id', $this->lang->line('session'), 'required|trim|xss_clean');
        $userdata = $this->customlib->getUserData();
        $role_id  = $userdata["role_id"];
        $can_edit = 1;
        if (isset($role_id) && ($userdata["role_id"] == 2) && ($userdata["class_teacher"] == "yes")) {
            $myclasssubjects = $this->subjecttimetable_model->canAddExamMarks($userdata["id"], $this->input->post('class_id'), $this->input->post('section_id'), $this->input->post('teachersubject_id'));
            $can_edit        = $myclasssubjects;
        }

        if ($this->form_validation->run() == false) {
            $data = array(
                'class_id'   => form_error('class_id'),
                'section_id' => form_error('section_id'),
                'session_id' => form_error('session_id'),
                'subject_id' => form_error('subject_id'),
            );
            $array = array('status' => 0, 'error' => $data);
            echo json_encode($array);
        } elseif ($can_edit == 0) {
            $msg   = array('lesson' => $this->lang->line('not_authoried'));
            $array = array('status' => 0, 'error' => $msg);
            echo json_encode($array);
        } else {
            $exam_subject_id                                = $this->input->post('subject_id');
            $data['exam_group_class_batch_exam_subject_id'] = $exam_subject_id;
            $class_id                                       = $this->input->post('class_id');
            $section_id                                     = $this->input->post('section_id');
            $session_id                                     = $this->input->post('session_id');
            $data['class_id']                               = $this->input->post('class_id');
            $data['section_id']                             = $this->input->post('section_id');
            $data['session_id']                              = $this->input->post('session_id'); 
            $resultlist                                     = $this->Exams_model->examGroupSubjectResult($exam_subject_id, $class_id, $section_id, $session_id);
            $subject_detail = $this->Exams_model->getExamSubject($exam_subject_id);

            $data['subject_detail']  = $subject_detail;
            $data['attendence_exam'] = $this->attendence_exam;
            $data['resultlist']      = $resultlist;
            $data['sch_setting']     = $this->sch_setting_detail; 
            $student_exam_page       = $this->load->view('admin/exams/_partialstudentmarkEntry', $data, true);

            $array = array('status' => '1', 'error' => '', 'page' => $student_exam_page);
            echo json_encode($array);
        }
    }
    public function get_index_data()
    {

        $total = 0;
        $batch_id = $this->input->post('academic_year');
         $group_id = $this->input->post('section_id');
        $course_id = $this->input->post('subject_id');
        $faculty_id = $this->input->post('faculty_id');
        $class_id = $this->input->post('class_id');
        $section_id = $this->input->post('section_id');

        $params = array(
            'year_id' => $class_id,
            'batch_id' => $batch_id,
            'course_id' => $course_id,
            'faculty_id' => $faculty_id,
            'section_id' => $section_id
        );

        $exams = $this->Exams_model->get_exam(0, $params);
       
        $params['group_id'] = $group_id;
        $output = '<table class="table table-striped table-hover table-bordered m-b-0" id="exam_datatable">
                            <thead>
                                <tr>
                                    <th width="1%">' . lang('no') . '</th>
                                    <th class="text-nowrap">' . lang('exam_name') . '</th>
                                    <th class="text-nowrap">' . lang('exam_marks') . '</th>
                                    <th class="text-nowrap">' . lang('publish') . '</th>
                                    <th class="text-nowrap">' . lang('action') . '</th>
                                </tr>
                            </thead><tbody>';
        //q();
        foreach ($exams as $key => $value) :
            //pre($value);
            $marks = ($value['sub_exams'] == 'Y') ? (float) $value['max_weightage'] : (float) $value['max_marks'];
            $total += $marks;
            $checked = ($value['isPublish'] == 'Y') ? 'checked' : '';

            $output .= '<tr id="tbl_row_' . $value['id'] . '">
                    <td class="f-s-600 text-inverse">' . ($key + 1) . '</td>
                    <td>' . htmlspecialchars($value['exam_name'], ENT_QUOTES, 'UTF-8') . '</td>
                    <td class="text-center">' . htmlspecialchars($marks, ENT_QUOTES, 'UTF-8') . '</td>
                    <td class="text-center"><input type="checkbox" class="chg-status" data-toggle="toggle" data-size="mini" data-on="Yes" data-off="No" id="' . $value['id'] . '" value="' . $value['isPublish'] . '" ' . $checked . ' data-value="exam" /></td>
                    <td class="p-t-0 pb-8">
                        <a data-exam="'.$value['id'].'" href="#"  class="btn btn-info btn-xs m-t-8 mr-5 p-5 examEditModalButton"  data-title="' . lang('edit') . '"><i class="fa fa-pencil"></i></a>
                        <a href="' . base_url('admin/exams/delete_exam/' . $value['id']) . '"  class="btn btn-danger btn-xs  m-t-8 mr-5 p-5" data-toggle="tooltip" data-title="' . lang('delete') . '" data-id="' . $value['id'] . '" data-value="Exam"><i class="fa fa-trash"></i></a>';

            if ($value['sub_exams'] == 'Y')
                $output .= '<a href="' . base_url('admin/exams/sub_exams/' . $value['id'] . '/' . $group_id . '/' . base64_encode(json_encode($params))) . '" class="btn btn-success btn-xs m-t-8 mr-5 p-5" data-toggle="tooltip" data-title="' . lang('sub_exam') . '" target="_blank"><i class="fa fa-clipboard"></i></a>';
            else
                $output .= '<a href="' . base_url('admin/exams/std_result_entry/' . $value['id'] . '/' . $group_id) . '" class="btn btn-success btn-xs m-t-8 mr-5 p-5" data-toggle="tooltip" data-title="' . lang('sub_exam') . '" target="_blank"><i class="fa fa-newspaper-o"></i></a>';
                // $output .= '<button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#subjectModal" data-examid="'.$value['id'].'" data-calss="'.$value['year_id'].'" data-section_id="'.$value['section_id'].'" data- data-subject_name="'.$value['exam_name'].'" data-subject_id="'.$value['course_id'].'" data-session_id="'.$value['batch_id'].'" data-teachersubject_id="#" ><i class="fa fa-newspaper-o" aria-hidden="true"></i></button>';

            $output .= '</td></tr>';
        endforeach;

        if ($exams)
            $output .= '<tr><td class="f-s-600 text-inverse">' . (count($exams) + 1) . '</td><td>Total</td><td class="text-center">' . $total . '</td><td></td><td></td></tr>';

        $output .= '</tbody></table>';
        echo $output;
    }

    public function get_exam_data(){
        $id = $this->input->get('exam');
        $row = $this->Exams_model->getexamdetail($id);
        echo json_encode($row);
    }

    public function publish_hide_exam($value = '')
    {
        $id = $this->input->post('id');
        $value = $this->input->post('value');

        if ($id and $value) {
            $params = array(
                'isPublish' => $value,
                'date_updated' => date('Y-m-d H:i:s'),
                'updateUser_id' => $_SESSION['AdminId'],
            );

            $result = $this->Exams_model->update($this->table['exams'], $params, array('id' => $id));

            ///////***** Updating Exams Marks *****\\\\\\\
            if ($result and $value == 'Y') {
                $this->Exams_model->update_subexam_wise_result($id);
            }

            echo $result ? 1 : 0;
        } else
            echo 0;
    }

    public function get_exam_dropdowns($faculty_id = 0, $call_for = '', $selected = 0)
    {
        $return = '';

        if (!$call_for)
            $call_for = $this->input->post('call_for');

        if (!$faculty_id)
            $faculty_id = $this->input->post('faculty_id');

        $exam_id = $this->input->post('exam_id');
        $batch_id = $this->input->post('batch_id');
        $course_id = $this->input->post('course_id');
        $semester_id = $this->input->post('semester_id');
        $type = $this->input->post('typ');

        if ($faculty_id) {
            if ($call_for == 'academic') {
                $result = $this->Batches_model->get_batches(0, array('b.faculty_id' => $faculty_id), 'b.batch_id, b.batch_name');

                if ($result)
                    $return .= '<option value="" selected disabled>Select Academic Year</option>';
                if ($type == 'all')
                    $return .= '<option value="all">All</option>';

                foreach ($result as $key => $value) {
                    $check = ($value['batch_id'] == $selected) ? 'selected' : '';
                    $return .= '<option value="' . $value['batch_id'] . '" ' . $check . '>' . $value['batch_name'] . '</option>';
                }
            } else if ($call_for == 'semester') {
                $where_in = get_year_ids();
                $query = "SELECT semester_id, year_semester FROM " . $this->table['year_semester'] . " WHERE faculty_id = $faculty_id AND isActive = 'Y' AND isVissible = 'Y'";

                if ($where_in)
                    $query .= " AND semester_id IN ($where_in)";

                $result = $this->Exams_model->raw_query($query);

                // if ($result)
                    $return = '<option value="-1">All</option>';
               $ref = $_SERVER['HTTP_REFERER'];
                if(preg_match("/student_performance/i", $ref)){
                    $i= 0;
                        foreach ($result as $key => $value) {
                            $return .= '<option value="' . $i . '" >' . $value['year_semester'] . '</option>';
                            $i++;
                        }

                }else{
                        foreach ($result as $key => $value) {
                            $check = ($value['semester_id'] == $selected) ? 'selected' : '';
                            $return .= '<option value="' . $value['semester_id'] . '" ' . $check . '>' . $value['year_semester'] . '</option>';
                           
                        }

                }     
                    
            } else if ($call_for == 'courses') {
                $result = $this->Exams_model->get_exam_courses($faculty_id, $semester_id, $batch_id);

                //if($result)
                $return .= '<option value="">Select Course</option>';

                foreach ($result as $key => $value) {
                    $check = ($value['course_id'] == $selected) ? 'selected' : '';
                    $return .= '<option value="' . $value['course_id'] . '" ' . $check . '>' . $value['course_name'] . '</option>';
                }
            } else if ($call_for == 'group') {
                echo $this->Exams_model->groupadmin($faculty_id, $semester_id, $batch_id, $course_id, '', 0, 'new');
            } else if ($call_for == 'exams') {
                $where = array('faculty_id' => $faculty_id, 'batch_id' => $batch_id, 'year_id' => $semester_id, 'course_id' => $course_id);
                $result = $this->Exams_model->get_exam(0, $where, 'id, exam_name');

                if ($result)
                    $return .= '<option value="">Select Exams</option>';

                foreach ($result as $key => $value) {
                    $check = ($value['id'] == $selected) ? 'selected' : '';
                    $return .= '<option value="' . $value['id'] . '" ' . $check . '>' . $value['exam_name'] . '</option>';
                }
            } else if ($call_for == 'sub_exams') {
                $result = $this->Exams_model->get_sub_exams(0, array('exam_id' => $exam_id), 'sub_exams_id, sub_exam_name');

                if ($result)
                    $return .= '<option value="">Select Sub Exams</option>';

                foreach ($result as $key => $value) {
                    $check = ($value['sub_exams_id'] == $selected) ? 'selected' : '';
                    $return .= '<option value="' . $value['sub_exams_id'] . '" ' . $check . '>' . $value['sub_exam_name'] . '</option>';
                }
            }
        }

        echo $return;
    }

    public function getMajors()
    {
        $faculty_id = $this->input->post('faculty_id');
        $return = '';
        if ($faculty_id) {
            $result = $this->Student_model->get_speciality(0, 'faculty_id, faculty_name');
            if ($result)
                $return .= '<option selected disabled>Select Major</option>';

            foreach ($result as $key => $value) {
                $return .= '<option value="' . $value['faculty_id'] . '">' . $value['faculty_name'] . '</option>';
            }
        }
        echo $return;
    }

    public function exam_admins()
    {
        ////////***** Title & Breadcrumbs *****\\\\\\\
        $this->page_title->push(lang('examination'));
        // $this->breadcrumbs->unshift(1, lang('examination'), 'admin/exams/exam_admins');
        $this->breadcrumbs->unshift(2, lang('exam_admins'), 'admin/exams/exam_admins');
        $this->data['pagetitle'] = $this->page_title->show();
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        ///////////******* View Data *******\\\\\\\\\\
        $this->data['faculty'] = $this->Hr_model->get_faculty(0, 'faculty_id, faculty_name');

        ///////////******* Load View *******\\\\\\\\\\
        $data['contents'] = $this->load->view('admin/exams/exam-admins', $this->data, TRUE);
        $this->load->view('admin/template', $data);
    }

    public function sub_exams($exam_id = 0, $group_id = 1, $params = '')
    {
        ////////***** Title & Breadcrumbs *****\\\\\\\
        $this->page_title->push(lang('examination'));
        $this->breadcrumbs->unshift(2, lang('sub_exam'), 'admin/exams/sub_exams');
        $this->data['pagetitle'] = $this->page_title->show();
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        ///////////******* View Data *******\\\\\\\\\\
        $this->data['params'] = json_decode(base64_decode($params), TRUE);
        $this->data['exam_id'] = $exam_id;
        $this->data['group_id'] = $group_id;
        $this->data['exam_name'] = $this->Exams_model->get_exam($exam_id, '', 'exam_name')['exam_name'];
        // $this->data['sub_exams']    = $this->Exams_model->get_sub_exams(0, array('exam_id' => $exam_id));
        $this->data['sub_exams'] = $this->Exams_model->raw_query("SELECT (SELECT SUM(e.max_marks) FROM " . $this->table['sub_exams_eval'] . " e WHERE e.sub_exams_id = s.sub_exams_id) as marks, s.* FROM " . $this->table['sub_exams'] . " s WHERE s.exam_id = $exam_id");

        ///////////******* Load View *******\\\\\\\\\\
        $data['contents'] = $this->load->view('admin/exams/sub-exams', $this->data, TRUE);
        $this->load->view('admin/template', $data);
    }

    public function add()
    {
         $this->session->set_userdata('top_menu', 'Examinations');
        $this->session->set_userdata('sub_menu', 'Examinations/examination');
        $data['title'] = 'Assign Course';
        $subject_result = $this->subject_model->get();
        $data['subjectlist'] = $subject_result;
        $data['subject_types'] = $this->customlib->subjectType();
         $data['faculties']          = $this->student_model->getFaculties();
         $data['sessionlist']        = $this->session_model->get();
          $data['sch_setting']     = $this->sch_setting_detail;
         $data['active_session_id'] = $data['sch_setting']->session_id;
         $class                      = $this->class_model->get('', $classteacher = 'yes');
        $data['classlist']          = $class;

        $assignedtlist            = $this->subject_model->getassignedlist();
         // echo "<pre>"; print_r($assignedtlist); exit();
        $data['assignedtlist']    = $assignedtlist;

        $staff_list         = $this->staff_model->getEmployee('2');
            $data['staff_list'] = $staff_list; 
            $is_admin           = true;
        $this->form_validation->set_rules('faculty_id', $this->lang->line('faculty_id'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('academic_year', $this->lang->line('academic_year'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('class_id', $this->lang->line('class_id'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('subject_id', $this->lang->line('subject_id'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('exam_name', $this->lang->line('exam_name'), 'trim|required|xss_clean');

        $this->form_validation->set_rules('sub_exams', 'Sub Exams', 'trim');
        $sub_exams = $this->input->post('sub_exams');
        $main_exams_status = FALSE; 
        $sub_exams_status = FALSE;
        $insert['exam_start_date'] = $this->input->post('exam_start_date');
            // $insert['exam_end_date'] = $this->input->post('exam_end_date');
            $insert['max_marks'] = $this->input->post('max_marks');
            $insert['min_marks'] = $this->input->post('min_marks');
            $insert['sub_exams'] = 'N';
            $main_exams_status = TRUE;

        if ($this->form_validation->run() == TRUE) {
            $insert['faculty_id'] = $this->input->post('faculty_id');
            $insert['year_id'] = $this->input->post('class_id');
            $insert['batch_id'] = $this->input->post('academic_year');
            $insert['course_id'] = $this->input->post('subject_id');
            $insert['exam_name'] = $this->input->post('exam_name');
            $insert['section_id'] = $this->input->post('section_id');
            $insert['is_fail'] = $this->input->post('is_fail');
            $insert['sub_exam_admin'] = $this->input->post('sub_exam_admin');
            $exam_id = $this->Exams_model->insert('table_exams', $insert);
            //$reset_exam_id = $this->Exams_model->insert('table_reset_exams', $insert);

            if ($sub_exams_status == TRUE) {
                $sub_insert['exam_id'] = $exam_id;

                //  $this->Exams_model->insert('table_sub_exams',$sub_insert);
                //  $this->Exams_model->insert('table_sub_reset_exams',$sub_insert);
            }
            if ($main_exams_status == TRUE) {
                $main_insert['exam_id'] = $exam_id;
                //$this->Exams_model->insert('table_main_exams',$main_insert);
            }
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('success_message') . '</div>');
            redirect('admin/exams/index');
        }
         
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layout/header', $data);  
            $this->load->view('admin/exams/add', $data);  
            $this->load->view('layout/footer', $data); 
        } 
    }

    public function update_exam($id = 0)
    {
        // echo "<pre>"; print_r($_POST); exit();
        $id = $this->input->post('exam_id');
        $exam_data = $this->Exams_model->get_exam($id);
        $data['exam']  = $exam_data;
        $data['exam_id'] = $exam_data['id'];
        $data['active_course_data'] = $this->Subject_model->get($exam_data['course_id']);
        if (!$data['exam']) {
            show_error('The Exam you are trying to update does not exist.');
            exit();
        }
        // echo "<pre>"; print_r($data['exam']); exit();
         $this->session->set_userdata('top_menu', 'Examinations');
        $this->session->set_userdata('sub_menu', 'Examinations/examination');
        $data['title'] = 'Assign Course';
        $subject_result = $this->subject_model->get();
        $data['subjectlist'] = $subject_result;
        $data['subject_types'] = $this->customlib->subjectType();
         $data['faculties']          = $this->student_model->getFaculties();
         $data['sessionlist']        = $this->session_model->get();
          $data['sch_setting']     = $this->sch_setting_detail;
         $data['active_session_id'] = $data['sch_setting']->session_id;
         $class                      = $this->class_model->get('', $classteacher = 'yes');
        $data['classlist']          = $class;

        $assignedtlist            = $this->subject_model->getassignedlist();
         // echo "<pre>"; print_r($assignedtlist); exit();
        $data['assignedtlist']    = $assignedtlist;

        $staff_list         = $this->staff_model->getEmployee('2');
            $data['staff_list'] = $staff_list; 
            $is_admin           = true;
       
        $this->form_validation->set_rules('exam_name', $this->lang->line('exam_name'), 'trim|required|xss_clean');

        $this->form_validation->set_rules('sub_exams', 'Sub Exams', 'trim');
        $sub_exams = $this->input->post('sub_exams');
        $main_exams_status = FALSE; 
        $sub_exams_status = FALSE;
         $sub_exams = $data['exam']['sub_exams'];

        if ($sub_exams == 'Y') {
            // $this->form_validation->set_rules('max_weightage', 'Sub Exam Maximum Marks', 'trim|required|max_length[5]');

            $params = array(
                // 'max_marks' => $this->input->post('max_weightage'),
                // 'min_marks' => $this->input->post('min_weightage'),
                'max_weightage' => $this->input->post('max_weightage'),
                'min_weightage' => $this->input->post('min_weightage'),
            );
        } else {
            // $this->form_validation->set_rules('exam_start_date', 'Exam Start Date', 'trim|required');
            // $this->form_validation->set_rules('exam_end_date', 'Exam End Date', 'trim|required');
            // $this->form_validation->set_rules('max_marks', 'Maximum Marks', 'trim|required|max_length[5]');

            $params = array(
                'max_marks' => $this->input->post('max_marks'),
                'min_marks' => $this->input->post('min_marks'),
                // 'exam_end_date' => date('Y-m-d H:i', strtotime($this->input->post('exam_end_date'))),
                'exam_start_date' => date('Y-m-d H:i', strtotime($this->input->post('exam_start_date'))),
            );
             // echo "<pre>"; print_r($params); exit();
        }

        if ($this->form_validation->run()) {
            $params['exam_name'] = $this->input->post('exam_name');
            $params['is_fail'] = $this->input->post('is_fail');
            $params['date_updated'] = date('Y-m-d H:i:s');
            // $params['updateUser_id'] = $_SESSION['AdminId'];
            $params['updateUser_id'] = $this->session->userdata()['admin']['id']; 

            /////////***** Updation *****\\\\\\\\\
            $updated = $this->Exams_model->update($this->table['exams'], $params, array('id' => $id));

            /////////***** Load View *****\\\\\\\\
            if ($updated)
                redirect('admin/exams/index', 'refresh');
            else
                redirect('admin/exams/index/' . $id, 'refresh');
        } else {
            /////////***** Load Data *****\\\\\\\\
            $this->data['update_page'] = TRUE;

            $this->load->view('layout/header', $data);  
            $this->load->view('admin/exams/edit', $data);  
            $this->load->view('layout/footer', $data); 
        }
         

        
    }

    public function delete_exam($exam_id)
    {
        $id = $exam_id;
        if (!$id)
            exit('0');

        // $m = $this->Exams_model->counttotal($this->table['exam_result'], "exam_id = '" . $id . "'");
        // $r = $this->Exams_model->counttotal($this->table['sub_exams'], "exam_id = '" . $id . "'");
        $r = 0;
        $m = 0;
        if ($r > 0 || $m > 0)
            echo 'This Exam has sub exams or student result. Please delete sub exams first';
        else {
            $exam = $this->Exams_model->get_exam($id);

            if ($exam) {
                $params = array(
                    'deleted' => 'Y',
                    'exam_id' => $exam['id'],
                    'year_id' => $exam['year_id'],
                    'batch_id' => $exam['batch_id'],
                    'course_id' => $exam['course_id'],
                    'max_marks' => $exam['max_marks'],
                    'min_marks' => $exam['min_marks'],
                    'sub_exams' => $exam['sub_exams'],
                    'isPublish' => $exam['isPublish'],
                    'faculty_id' => $exam['faculty_id'],
                    'absent_warn' => $exam['absent_warn'],
                    'fail_absent' => $exam['fail_absent'],
                    'created_date' => date('Y-m-d H:i:s'),
                    // 'exam_end_date' => $exam['exam_end_date'],
                    'exam_start_date' => $exam['exam_start_date'],
                );

                // $inserted = $this->Exams_model->insert($this->table['track_exam'], $params);

                // if ($inserted)
                    $this->Exams_model->delete($this->table['exams'], array('id' => $id));
                // else
                    redirect('admin/exams/index', 'refresh');
            } 
        }
    }

    public function delete_sub_exam()
    {
        $id = $this->input->post('id');

        $r = $this->Exams_model->counttotal('sub_exams_evaluate', "sub_exams_id='" . $sid . "'");

        if ($r > 0) {
            $_SESSION['msg_error'] = 'This Exam has sub exams evaluations, Please delete subexams evaluations first';
        } else {
            $this->db->delete('table_sub_exams', array('sub_exams_id' => $sid));
            //$this->db->delete('table_sub_reset_exams', array('sub_exams_id' => $sid));

            $this->db->delete('sub_exams_evaluate', array('sub_exams_id' => $sid));
            // $this->db->delete('sub_reset_exams_evaluate', array('sub_exams_id' => $sid));

            $_SESSION['msg'] = 'Sub Exams has been deleted successfully.';
        }
        redirect('admin/exams/sub_exams/' . $eid . "/" . $cid);
    }

    public function delete_sub_exam_eval()
    {
        $id = $this->input->post('id');

        $r = $this->Exams_model->counttotal('table_subexams_std_result', "sub_exam_eval_id='" . $eval_id . "'");

        if ($r > 0) {
            $_SESSION['msg_error'] = 'This Exam has studerts marks, You can\'t delete.';
        } else {
            $this->db->delete('sub_exams_evaluate', array('id' => $eval_id));
            //$this->db->delete('sub_reset_exams_evaluate', array('id' => $eval_id));

            $_SESSION['msg'] = 'Sub Exams Evaluation has been deleted successfully.';
        }
        redirect('admin/exams/sub_exams_evaluate/' . $exam_id . "/" . $sub_id . "/" . $course_id);
    }

    public function add_sub_exam($exam_id = 0)
    {
        ////////***** Title & Breadcrumbs *****\\\\\\\
        $this->page_title->push(lang('examination'));
        $this->breadcrumbs->unshift(2, lang('sub_exam'), 'admin/exams/sub_exams/' . $exam_id);
        $this->breadcrumbs->unshift(3, lang('add'), 'admin/exams/sub_exams/' . $exam_id . '/add_sub_exam');
        $this->data['pagetitle'] = $this->page_title->show();
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        ///////////******* Validation *******\\\\\\\\\
        $this->form_validation->set_rules('name', 'Name', 'trim|required');

        if ($this->form_validation->run()) {
            $params = array(
                'user_id' => $_SESSION['AdminId'],
                'exam_id' => $exam_id,
                'absent_warn' => $this->input->post('absent_warn'),
                'fail_absent' => $this->input->post('fail_absent'),
                'sub_exam_name' => $this->input->post('sub_exam_name'),
            );

            ///////////***** Insertion *****\\\\\\\\\\
            $inserted_id = $this->Exams_model->insert($this->table['sub_exams'], $params);

            ///////////***** Load View *****\\\\\\\\\\
            if ($inserted_id)
                redirect('admin/exams/sub_exams/' . $exam_id, 'refresh');
            else
                redirect('admin/exams/add_sub_exam/' . $exam_id, 'refresh');
        } else {
            ///////////***** Load Data *****\\\\\\\\\\
            $this->data['exam_id'] = $exam_id;

            ///////////***** Load View *****\\\\\\\\\\
            $data['contents'] = $this->load->view('admin/exams/add-sub-exam', $this->data, TRUE);
            $this->load->view('admin/template', $data);
        }
    }

    public function update_sub_exam($id = 0, $exam_id = 0)
    {
        ////////***** Title & Breadcrumbs *****\\\\\\\
        $this->page_title->push(lang('examination'));
        $this->breadcrumbs->unshift(2, lang('sub_exam'), 'admin/exams/sub_exams/' . $exam_id);
        $this->breadcrumbs->unshift(3, lang('update'), 'admin/exams/sub_exams/' . $exam_id . '/update_sub_exam');
        $this->data['pagetitle'] = $this->page_title->show();
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $this->data['sub_exam'] = $this->Exams_model->get_sub_exams($id);

        if (isset($this->data['sub_exam']['sub_exams_id'])) {
            /////////******* Validation *******\\\\\\\
            $this->form_validation->set_rules('name', 'Name', 'trim|required');

            if ($this->form_validation->run()) {
                $params = array(
                    'absent_warn' => $this->input->post('absent_warn'),
                    'fail_absent' => $this->input->post('fail_absent'),
                    'date_updated' => date('Y-m-d H:i:s'),
                    'sub_exam_name' => $this->input->post('sub_exam_name'),
                    'updateUser_id' => $_SESSION['AdminId'],
                );

                ///////////***** Updation *****\\\\\\\\\\
                $updated = $this->Exams_model->update($this->table['sub_exams'], $params, array('sub_exams_id' => $id));

                ///////////***** Load View *****\\\\\\\\\\
                if ($updated)
                    redirect('admin/exams/sub_exams/' . $exam_id, 'refresh');
                else
                    redirect('admin/exams/update_sub_exams/' . $id . '/' . $exam_id, 'refresh');
            } else {
                ///////////***** Load Data *****\\\\\\\\\\
                $this->data['exam_id'] = $exam_id;

                ///////////***** Load View *****\\\\\\\\\\
                $data['contents'] = $this->load->view('admin/exams/add-sub-exam', $this->data, TRUE);
                $this->load->view('admin/template', $data);
            }
        } else
            show_error("The Sub Exam you are trying to update doesn't exist.");
    }

    public function connect_sub_exams($exam_id = 0, $group_id = 1, $params = '')
    {
        $this->data['exam'] = $this->Exams_model->get_exam($exam_id);

        if (!$this->data['exam'])
            redirect('admin/dashboard', 'refresh');

        ////////***** Title & Breadcrumbs *****\\\\\\\
        $this->page_title->push(lang('examination'));
        $this->breadcrumbs->unshift(2, lang('sub_exam'), 'admin/exams/sub_exams/' . $exam_id . '/' . $group_id . '/' . $params);
        $this->breadcrumbs->unshift(3, lang('connect_sub_exams'), 'admin/exams/sub_exams');
        $this->data['pagetitle'] = $this->page_title->show();
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        ///////////******* View Data *******\\\\\\\\\\
        $this->data['group_id'] = $group_id;
        $this->data['sub_exams'] = []; // $this->Exams_model->get_connect_subexam($exam_id, $this->data['exam']['course_id']);
        ///////////******* Load View *******\\\\\\\\\\
        $data['contents'] = $this->load->view('admin/exams/connect-sub-exam', $this->data, TRUE);
        $this->load->view('admin/template', $data);
    }

    public function connect_sub_exams_old($id = 0, $course_id = 0, $group_id = 0)
    {
        $data = array();
        $row = $this->Exams_model->getexamdetail($id);

        $data['exam_name'] = $row['exam_name'];
        $data['faculty_id'] = $row['faculty_id'];
        $data['batch_id'] = $row['batch_id'];
        $data['year_id'] = $row['year_id'];
        $data['course_id'] = $row['course_id'];
        $data['max_weightage'] = $row['max_weightage'];
        $data['exam_id'] = $id;
        $data['course_id'] = $course_id;
        $data['group_id'] = $group_id;
        $html = '';

        $sub_exam_admin = getSubExamsAdmin($data['faculty_id']);

        $results = $this->Exams_model->SqlExec("SELECT
                                            tbl_sub_connect_exmas.id,
                                            table_sub_exams.sub_exam_name,
                                            tbl_sub_connect_exmas.sub_exam_id,
                                            tbl_sub_connect_exmas.weightage
                                        FROM
                                            tbl_sub_connect_exmas
                                            INNER JOIN table_sub_exams 
                                                ON (tbl_sub_connect_exmas.sub_exam_id = table_sub_exams.sub_exams_id)
                                            
                                            and tbl_sub_connect_exmas.exam_id='$id' and 
                                            tbl_sub_connect_exmas.course_id='$course_id'");
        if ($results->num_rows() > 0) {
            $row = $results->result_array();
            //shaz commented this line due to error
            ///$c_sub_exam_ids=[];
            $html = '';

            foreach ($results->result_array() as $row) {
                $c_sub_exam_ids[] = $row['sub_exam_id'];
                $html .= ' <tr>
                                <td class="center" >' . $row['sub_exam_name'] . '</td>
                                <td class="center" >
                                        <input type="hidden" name="sub_connect_id[]" id="sub_connect_id[]" value="' . $row['id'] . '" />
                                        <input type="hidden" name="sub_exams_id[]" value="' . $row['sub_exam_id'] . '" />
                                        <input type="text" name="weightage[]"  value="' . $row['weightage'] . '" />
                                </td>
                </tr>';
            }
            if (count($c_sub_exam_ids) > 0) {
                $notInIds = implode(",", $c_sub_exam_ids);
                $results = $this->Exams_model->getsubexams($id, $sub_exam_admin, $notInIds);
                foreach ($results->result_array() as $row) {

                    $html .= ' <tr>
                            <td class="center" >' . $row['sub_exam_name'] . '</td>
                            <td class="center" >
                                <input type="hidden" name="sub_connect_id[]" id="sub_connect_id[]" value="0" />
                                <input type="text" name="weightage[]" value="0" />
                                <input type="hidden" name="sub_exams_id[]" value="' . $row['sub_exams_id'] . '" />
                             </td>
                        </tr>';
                }
            }

            $html .= '<tr class="stdform">
                                     <td class="right"></td>
                                     <td class="left">
                                      <input type="button"  value="Update Connect Exams" name="submit" onclick="update_me();" />
                                      <input type="reset" value="Reset" name="reset" /></td>
                                    </td>
                                </tr>';
        } else {

            $results = $this->Exams_model->getsubexams($id, $sub_exam_admin);

            if ($results->num_rows() > 0) {
                foreach ($results->result_array() as $row) {

                    $html .= ' <tr>
                            <td class="center" >' . $row['sub_exam_name'] . '</td>
                            <td class="center" >
                                <input type="text" name="sub_exams[]" value="0" />
                                <input type="hidden" name="sub_exams_id[]" value="' . $row['sub_exams_id'] . '" />
                             </td>
                        </tr>';
                }

                $html .= '<tr class="stdform">
                                     <td class="right"></td>
                                     <td class="left">
                                      <input type="button" value="Save Connect Exams" name="submit" onclick="click_me();" />
                                      <input type="reset" value="Reset" name="reset" /></td>
                                    </td>
                            </tr>';
            } else {
                $html .= '<tr> <td colspan="2"> No Record Found </td> </tr>';
            }
        }

        $data['html'] = $html;
        $data['contents'] = $this->load->view('admin/exams/connect_sub_exams', $data, true);
        $this->load->view('admin/template', $data);
    }

    public function sub_exams_evaluate($sub_exam_id = 0, $group_id = 1, $params = '')
    {
        ///////////******* View Data *******\\\\\\\\\\
        $sub_exam = $this->Exams_model->get_sub_exams($sub_exam_id);
        $this->data['params'] = json_decode(base64_decode($params), TRUE);
        $this->data['exam_id'] = $sub_exam ? $sub_exam['exam_id'] : 0;
        $this->data['group_id'] = $group_id;
        $this->data['sub_exam_id'] = $sub_exam_id;
        $this->data['sub_exam_eval'] = $this->Exams_model->get_sub_exam_eval(0, array('sub_exams_id' => $sub_exam_id));
        $this->data['sub_exam_name'] = $sub_exam['sub_exam_name'];

        ////////***** Title & Breadcrumbs *****\\\\\\\
        $this->page_title->push(lang('examination'));
        $this->breadcrumbs->unshift(2, lang('sub_exam'), 'admin/exams/sub_exams/' . $this->data['exam_id'] . '/' . $group_id . '/' . $params);
        $this->breadcrumbs->unshift(3, 'Sub Exam Evaluation', 'admin/exams/sub_exams');
        $this->data['pagetitle'] = $this->page_title->show();
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        ///////////******* Load View *******\\\\\\\\\\
        $data['contents'] = $this->load->view('admin/exams/sub-exams-evaluate', $this->data, TRUE);
        $this->load->view('admin/template', $data);
    }

    public function add_sub_exams_evaluate($sub_exam_id = 0)
    {
        $sub_exam = $this->Exams_model->get_sub_exams($sub_exam_id, '', 'exam_id');

        if (!$sub_exam)
            redirect('admin/dashboard', 'refresh');

        $exam_id = $sub_exam['exam_id'];

        ////////***** Title & Breadcrumbs *****\\\\\\\
        $this->page_title->push(lang('examination'));
        $this->breadcrumbs->unshift(2, lang('sub_exam'), 'admin/exams/sub_exams/' . $exam_id);
        $this->breadcrumbs->unshift(3, lang('sub_exam_eval'), 'admin/exams/sub_exams/' . $exam_id . '/sub_exam_eval/' . $sub_exam_id);
        $this->breadcrumbs->unshift(4, lang('add'), 'admin/exams/sub_exams/' . $exam_id . '/sub_exam_eval/' . $sub_exam_id . '/add');
        $this->data['pagetitle'] = $this->page_title->show();
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        ///////////******* Validation *******\\\\\\\\\
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('exams_start_date', 'Start Date', 'trim|required');
        $this->form_validation->set_rules('exams_end_date', 'End Date', 'trim|required');
        $this->form_validation->set_rules('max_marks', 'Maximum Marks', 'trim|required|max_length[5]');

        if ($this->form_validation->run()) {
            $params = array(
                'user_id' => $_SESSION['AdminId'],
                'max_marks' => $this->input->post('max_marks'),
                'exam_name' => $this->input->post('exam_name'),
                'sub_exams_id' => $sub_exam_id,
                'exam_end_date' => date('Y-m-d h:i A', strtotime($this->input->post('exam_end_date'))),
                'exam_start_date' => date('Y-m-d h:i A', strtotime($this->input->post('exam_start_date'))),
            ); 

            ///////////***** Insertion *****\\\\\\\\\\
            $inserted_id = $this->Exams_model->insert($this->table['sub_exams_eval'], $params);

            ///////////***** Load View *****\\\\\\\\\\
            if ($inserted_id)
                redirect('admin/exams/sub_exams_evaluate/' . $sub_exam_id, 'refresh');
            else
                redirect('admin/exams/add_sub_exams_evaluate/' . $sub_exam_id, 'refresh');
        } else {
            ///////////***** Load Data *****\\\\\\\\\\
            $this->data['sub_exam_id'] = $sub_exam_id;

            ///////////***** Load View *****\\\\\\\\\\
            $data['contents'] = $this->load->view('admin/exams/sub-exams-evaluate-add', $this->data, TRUE);
            $this->load->view('admin/template', $data);
        }
    }

    public function update_sub_exams_evaluate($id = 0)
    {
        ////////***** Check if id exists *****\\\\\\\\
        $this->data['sub_exam_eval'] = $this->Exams_model->get_sub_exam_eval($id);

        if (!$this->data['sub_exam_eval']) {
            show_error('The exam you are trying to update does not exists');
            exit();
        }

        $sub_exam_id = $this->data['sub_exam_eval']['sub_exams_id'];
        $sub_exam = $this->Exams_model->get_sub_exams($sub_exam_id, '', 'exam_id');

        if (!$sub_exam)
            redirect('admin/dashboard', 'refresh');

        $exam_id = $sub_exam['exam_id'];

        ////////***** Title & Breadcrumbs *****\\\\\\\
        $this->page_title->push(lang('examination'));
        $this->breadcrumbs->unshift(2, lang('sub_exam'), 'admin/exams/sub_exams/' . $exam_id);
        $this->breadcrumbs->unshift(3, lang('sub_exam_eval'), 'admin/exams/sub_exams/' . $exam_id . '/sub_exam_eval/' . $sub_exam_id);
        $this->breadcrumbs->unshift(4, lang('update'), 'admin/exams/sub_exams/' . $exam_id . '/sub_exam_eval/' . $sub_exam_id . '/add');
        $this->data['pagetitle'] = $this->page_title->show();
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /////////******* Validation *******\\\\\\\
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('exams_start_date', 'Exams Start Date', 'trim|required');
        $this->form_validation->set_rules('exams_end_date', 'Exams End Date', 'trim|required');
        $this->form_validation->set_rules('max_marks', 'Maximum Marks', 'trim|required|max_length[5]');

        if ($this->form_validation->run()) {
            $params = array(
                'max_marks' => $this->input->post('max_marks'),
                'exam_name' => $this->input->post('exam_name'),
                'updated_date' => date('Y-m-d H:i:s'),
                'exam_end_date' => date('Y-m-d h:i A', strtotime($this->input->post('exam_end_date'))),
                'exam_start_date' => date('Y-m-d h:i A', strtotime($this->input->post('exam_start_date'))),
                'updated_user_id' => $_SESSION['AdminId'],
            );

            // print_r($params); exit();
            ///////////***** Updation *****\\\\\\\\\\
            $updated = $this->Exams_model->update($this->table['sub_exams_eval'], $params, array('id' => $id));

            ///////////***** Load View *****\\\\\\\\\\
            if ($updated)
                redirect('admin/exams/sub_exams_evaluate/' . $sub_exam_id, 'refresh');
            else
                redirect('admin/exams/update_sub_exams_evaluate/' . $id, 'refresh');
        } else {
            ///////////***** Load Data *****\\\\\\\\\\
            $this->data['sub_exam_id'] = $sub_exam_id;

            ///////////***** Load View *****\\\\\\\\\\
            $data['contents'] = $this->load->view('admin/exams/sub-exams-evaluate-add', $this->data, TRUE);
            $this->load->view('admin/template', $data);
        }
    }

    public function exam_result_entry($exam_id = 0, $group_id = 1)
    {
        $exam = $this->Exams_model->get_exam($exam_id);

        if (!$exam)
            redirect('admin/dashboard', 'refresh');

        ////////***** Title & Breadcrumbs *****\\\\\\\
        $this->page_title->push(lang('examination'));
        $this->breadcrumbs->unshift(2, lang('result_entry'), 'admin/exams/');
        $this->data['pagetitle'] = $this->page_title->show();
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        ///////////******* View Data *******\\\\\\\\\\
        //        $this->data['group_students'] = $this->Exams_model->stdSubExamsResultsGroup($exam_id, $exam['course_id'], 0, $group_id, $exam['faculty_id'], $exam['batch_id'], $exam['year_id']);
        $this->data['group_students'] = $this->Exams_model->stdResultsGroup($exam_id, $exam['course_id'], $group_id, $exam['faculty_id'], $exam['batch_id'], $exam['year_id']); 

        //        echo $this->db->last_query(); die;
        $where = '';
        if ($group_id != 1)
            $where = "INNER JOIN " . $this->table['std_group'] . " g ON (g.std_id = s.std_id and g.group_id='" . $group_id . "'  AND g.faculty_id='" . $exam['faculty_id'] . "'  AND g.batch_id='" . $exam['batch_id'] . "' AND g.semester_id='" . $exam['year_id'] . "'     AND g.course_id='" . $exam['course_id'] . "')";

        $sql = "SELECT s.std_fname, s.std_id FROM " . $this->table['student_detail'] . " s $where LEFT JOIN " . $this->table['exam_result'] . " r ON (s.std_id = r.std_id AND  r.exam_id = '" . $exam_id . "'  AND r.course_id = '" . $exam['course_id'] . "' AND r.sub_exam_eval_id = '0') WHERE r.std_id IS NULL and s.std_faculty_id = '" . $exam['faculty_id'] . "' and s.std_batch_id = '" . $exam['batch_id'] . "' AND s.std_semester_id = '" . $exam['year_id'] . "' AND s.std_active = 'Y' AND  s.isActive = 'Y'

                    UNION

                SELECT s.std_fname, s.std_id FROM " . $this->table['tbl_std_prev_fac_batch_year'] . " p INNER JOIN " . $this->table['student_detail'] . " s ON ( p.std_id = s.std_id) $where LEFT JOIN " . $this->table['exam_result'] . " r ON (s.std_id = r.std_id AND  r.exam_id ='" . $exam_id . "' AND r.course_id ='" . $exam['course_id'] . "' AND r.sub_exam_eval_id = '0') WHERE r.std_id IS NULL AND p.faculty_id = '" . $exam['faculty_id'] . "' AND p.batch_id = '" . $exam['batch_id'] . "' AND p.year_id = '" . $exam['year_id'] . "' AND s.std_active = 'Y' ORDER BY std_id ASC;";

        if ($group_id == 1) { //for group None/All
            $where = '';
        } else {
            $where = "INNER JOIN tbl_std_group
            ON (tbl_std_group.std_id = student_detail.std_id and tbl_std_group.group_id='" . $group_id . "'  AND tbl_std_group.faculty_id='" . $exam['faculty_id'] . "' AND tbl_std_group.batch_id='" . $exam['batch_id'] . "' AND tbl_std_group.course_id='" . $exam['course_id']  . "')";
        }
        $sql = "
            SELECT
                student_detail.std_fname
                , student_detail.std_id
            FROM
                student_detail
            $where
                INNER JOIN tbl_std_assignedcourses
            ON ( tbl_std_assignedcourses.std_id = student_detail.std_id)
                LEFT JOIN table_std_result 
                    ON (student_detail.std_id =table_std_result.std_id AND  table_std_result.exam_id ='" . $exam_id . "'  AND table_std_result.course_id ='" . $exam['course_id'] . "') where table_std_result.std_id IS NULL
                    AND tbl_std_assignedcourses.group_id='" . $group_id . "'  AND  tbl_std_assignedcourses.faculty_id='" . $exam['faculty_id'] . "' AND  tbl_std_assignedcourses.batch_id='" . $exam['batch_id'] . "'  AND  tbl_std_assignedcourses.course_id='" . $exam['course_id'] . "' AND tbl_std_assignedcourses.isActive='Y'
                    order by  student_detail.std_id ASC ;";

        //        echo $sql; die;
        $this->data['prev_students'] = $this->Exams_model->raw_query($sql);
        
        $this->data['year_id'] = $exam['year_id'];
        $this->data['exam_id'] = $exam_id;
        $this->data['batch_id'] = $exam['batch_id'];
        $this->data['group_id'] = $group_id;
        $this->data['course_id'] = $exam['course_id'];
        $this->data['max_marks'] = $exam['max_marks'];
        $this->data['faculty_id'] = $exam['faculty_id'];
        $this->data['sub_exam_id'] = 0;
        $this->data['sub_exam_eval_id'] = 0;
        // echo "<pre>"; print_r($this->data); exit(); 
        ///////////******* Load View *******\\\\\\\\\\
        $data['contents'] = $this->load->view('admin/exams/result-entry', $this->data, TRUE);
        $this->load->view('admin/template', $data);
    }

    public function result_entry($exam_id = 0, $sub_exam_id = 0, $sub_exam_eval_id = 0, $group_id = 1, $params = '')
    {
        $exam = $this->Exams_model->get_exam($exam_id);

        if (!$exam)
            redirect('admin/dashboard', 'refresh');

        ////////***** Title & Breadcrumbs *****\\\\\\\
        $this->page_title->push(lang('examination'));
        $this->breadcrumbs->unshift(2, lang('sub_exam'), 'admin/exams/sub_exams/' . $exam_id . '/' . $group_id . '/' . $params);
        $this->breadcrumbs->unshift(3, 'Sub Exams Evaluation', 'admin/exams/sub_exams_evaluate/' . $sub_exam_id . '/' . $group_id . '/' . $params);
        $this->breadcrumbs->unshift(4, lang('result_entry'), 'admin/exams');
        $this->data['pagetitle'] = $this->page_title->show();
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        ///////////******* View Data *******\\\\\\\\\\
        $this->data['group_students'] = $this->Exams_model->stdSubExamsResultsGroup($exam_id, $exam['course_id'], $sub_exam_eval_id, $group_id, $exam['faculty_id'], $exam['batch_id'], $exam['year_id']);

        $where = '';
        if ($group_id != 1)
            $where = "INNER JOIN " . $this->table['std_group'] . " g ON (g.std_id = s.std_id and g.group_id='" . $group_id . "'  AND g.faculty_id='" . $exam['faculty_id'] . "'  AND g.batch_id='" . $exam['batch_id'] . "' AND g.semester_id='" . $exam['year_id'] . "'     AND g.course_id='" . $exam['course_id'] . "')";

        $sql = "SELECT s.std_fname, s.std_id FROM " . $this->table['student_detail'] . " s $where LEFT JOIN " . $this->table['exam_result'] . " r ON (s.std_id = r.std_id AND  r.exam_id = '" . $exam_id . "'  AND r.course_id = '" . $exam['course_id'] . "' AND r.sub_exam_eval_id = '" . $sub_exam_eval_id . "') WHERE r.std_id IS NULL and s.std_faculty_id = '" . $exam['faculty_id'] . "' and s.std_batch_id = '" . $exam['batch_id'] . "' AND s.std_semester_id = '" . $exam['year_id'] . "' AND s.std_active = 'Y'

                    UNION

                SELECT s.std_fname, s.std_id FROM " . $this->table['tbl_std_prev_fac_batch_year'] . " p INNER JOIN " . $this->table['student_detail'] . " s ON ( p.std_id = s.std_id) $where LEFT JOIN " . $this->table['exam_result'] . " r ON (s.std_id = r.std_id AND  r.exam_id ='" . $exam_id . "' AND r.course_id ='" . $exam['course_id'] . "' AND r.sub_exam_eval_id = '" . $sub_exam_eval_id . "') WHERE r.std_id IS NULL AND p.faculty_id = '" . $exam['faculty_id'] . "' AND p.batch_id = '" . $exam['batch_id'] . "' AND p.year_id = '" . $exam['year_id'] . "' AND s.std_active = 'Y' ORDER BY std_id ASC;";

        $this->data['prev_students'] = $this->Exams_model->raw_query($sql);
        $sub_exam_eval = $this->Exams_model->get_sub_exam_eval($sub_exam_eval_id);

        $this->data['year_id'] = $exam['year_id'];
        $this->data['exam_id'] = $exam_id;
        $this->data['batch_id'] = $exam['batch_id'];
        $this->data['group_id'] = $group_id;
        $this->data['course_id'] = $exam['course_id'];
        $this->data['max_marks'] = $sub_exam_eval['max_marks'];
        $this->data['faculty_id'] = $exam['faculty_id'];
        $this->data['sub_exam_id'] = $sub_exam_id;
        $this->data['sub_exam_eval_id'] = $sub_exam_eval_id;

        ///////////******* Load View *******\\\\\\\\\\
        $data['contents'] = $this->load->view('admin/exams/result-entry', $this->data, TRUE);
        $this->load->view('admin/template', $data);
    }

    function exams_dashboard()
    {
        $data['contents'] = $this->load->view('admin/exams/exams_dashboard', '', true);
        $this->load->view('admin/template', $data);
    }

    function ajax()
    {
        $html = '';
        $action = $this->input->post('action');
        $id = $this->input->post('id');
        switch ($action) {
            case 'fetchYearSemesterUser':
                if ($this->input->post('attr') != '')
                    $attr = $this->input->post('attr');
                else
                    $attr = "onchange=fetchExamCourses(this.value);";
                $where = getYearIds();

                $html = show_dropdown('year_semester', 'year_semester', 'year_semester', 'semester_id', 0, "Select Year/Semester", $attr, "isActive='Y'  and faculty_id='$id'$where");
                break;
            case 'fetchYearSemester':
                if ($this->input->post('attr') != '')
                    $attr = $this->input->post('attr');
                else
                    $attr = "onchange=fetchExamCourses(this.value);";

                $html = show_dropdown('year_semester', 'year_semester', 'year_semester', 'semester_id', 0, "Select Year/Semester", $attr, "isActive='Y' and faculty_id='$id'");
                break;
            case 'fetchBatchesVissible':
                if ($this->input->post('attr') != '')
                    $attr = "";
                else
                    $attr = "";

                $html = show_dropdown('batches', 'batch_name', 'batch_name', 'batch_id', 0, "", $attr, "isActive='Y' and isVissible='Y' and  faculty_id='$id' ORDER BY sort_batch_id DESC");
                break;
            case 'fetchBatches':
                if ($this->input->post('attr') != '')
                    $attr = "";
                else
                    $attr = "";

                $html = show_dropdown('batches', 'batch_name', 'batch_name', 'batch_id', 0, "", $attr, "isActive='Y' and  faculty_id='$id' ORDER BY sort_batch_id DESC");
                break;
            case 'examRegCourses':
                $fid = $this->input->post('faculty_id');
                $bid = $this->input->post('bid');
                $sid = $id;

                $attr = ($this->input->post('attr') != '') ? $this->input->post('attr') : "";
                $html = $this->Exams_model->examRegCourses($fid, $sid, $bid, $attr);

                break;
            case 'examCourses':
                $fid = $this->input->post('faculty_id');
                $bid = $this->input->post('bid');
                $sid = $id;

                $attr = ($this->input->post('attr') != '') ? $this->input->post('attr') : "";
                $html = $this->Exams_model->examCourses($fid, $sid, $bid, $attr);

                break;
            case 'groupadmin':
                $fid = $this->input->post('faculty_id');
                $bid = $this->input->post('bid');
                $sid = $this->input->post('id');
                $courseid = $this->input->post('courseid');
                // shahbaz echo '<pre>';print_r($_POST);exit;

                $attr = ($this->input->post('attr') != '') ? $this->input->post('attr') : "";
                $html = $this->Exams_model->groupadmin($fid, $sid, $bid, $courseid, $attr);

                break;
            case 'examLockCourses':
                $fid = $this->input->post('faculty_id');
                $bid = $this->input->post('bid');
                $sid = $id;

                $attr = ($this->input->post('attr') != '') ? $this->input->post('attr') : "";
                $html = $this->Exams_model->examLockCourses($fid, $sid, $bid, $attr);

                break;

            case 'examsListEval':
                $fid = $this->input->post('fid');
                $sid = $this->input->post('sid');
                $bid = $this->input->post('bid');

                $results = $this->Exams_model->GetDetail('table_exams', "faculty_id='$fid' and batch_id='$bid' and year_id='$sid' and course_id='$id'");

                $resultsConnect = $this->Exams_model->GetDetail('table_connect_exams', "faculty_id='$fid' and batch_id='$bid' and year_id='$sid' and course_id='$id'");
                if ($resultsConnect->num_rows() > 0) {
                    $row = $resultsConnect->result_array();
                    $rows = unserialize($row[0]['weightage']);
                    $ids = array_keys($rows);
                    $html = '<input type="hidden" name="id" id="connect_id" value="' . $row[0]['id'] . '" />';
                    for ($i = 0; $i < sizeof($ids); $i++) {

                        $rec = $this->Exams_model->GetDetail('table_exams', "id='" . $ids[$i] . "'");
                        $rec = $results->result_array();
                        $html .= ' <tr>
                                                <td class="center" >' . $rec[$i]['exam_name'] . '</td>
                                                 <td class="center" >
                                                    <input type="text" name="exam[]" value="' . $rows[$ids[$i]] . '" />
                                                    <input type="hidden" name="exam_id[]" value="' . $rec[$i]['id'] . '" />
                                                    
                                                 </td>
                                         </tr>';
                    }
                    $html .= '
                                <tr class="stdform">
                                     <td class="right"></td>
                                     <td class="left">
                                      <input type="button" value="Update" name="submit" onclick="update_me();" />
                                      <input type="reset" value="Reset" name="reset" /></td>
                                    </td>
                                </tr>';
                } else if ($results->num_rows() > 0) {
                    foreach ($results->result_array() as $row) {

                        $html .= ' <tr>
                                                <td class="center" >' . $row['exam_name'] . '</td>
                                                 <td class="center" >
                                                    <input type="text" name="exam[]" value="0" />
                                                    <input type="hidden" name="exam_id[]" value="' . $row['id'] . '" />
                                                    
                                                 </td>
                                         </tr>';
                    }
                    $html .= '
                                <tr class="stdform">
                                     <td class="right"></td>
                                     <td class="left">
                                      <input type="button" value="Save" name="submit" onclick="click_me();" />
                                      <input type="reset" value="Reset" name="reset" /></td>
                                    </td>
                                </tr>';
                } else {
                    $html .= '<tr> <td colspan="2"> No Record Found </td> </tr>';
                }
                break;


            case 'examsList':

                $data = array();



                $fid = $this->input->post('fid');
                $sid = $this->input->post('sid');
                $bid = $this->input->post('bid');

                $sub_exam_admin = getSubExamsAdmin($fid);

                $course_id = $id;

                if ($sub_exam_admin == 1 || $sub_exam_admin == 2 || $sub_exam_admin == 4) {
                    $where = "faculty_id = '$fid' and batch_id ='$bid' and year_id ='$sid' and course_id = '$course_id'";
                } else {

                    $where = "faculty_id = '$fid' and batch_id ='$bid' and year_id ='$sid' and course_id = '$course_id' and sub_exam_admin = '$sub_exam_admin' ";
                }

                $results = $this->Exams_model->GetDetail('table_exams', $where);
                //pre($results->result_array());
                if ($results->num_rows() > 0) {
                    $total = 0;
                    foreach ($results->result_array() as $row) {

                        if ($row['sub_exams'] == 'Y') {
                            $marks = $row['max_weightage'];
                            $total = $total + $row['max_weightage'];
                        } else {
                            $marks = $row['max_marks'];
                            $total = $total + $row['max_marks'];
                        }
                        $html .= ' <tr>
                                                <td class="center" >' . $row['exam_name'] . '</td>
                                                <td class="center" >' . $marks . '</td>
                                                 <td class="center" >
                                                     <a href="' . site_url() . 'admin/exams/update_exam/' . $row['id'] . '">Edit</a> |
                                                     <a href="' . site_url() . 'admin/exams/delete_exams/' . $row['id'] . '">Delete</a> | ';
                        if ($row['sub_exams'] == 'Y')
                            $html .= ' <a href="' . site_url() . 'admin/exams/sub_exams/' . $row['id'] . '/' . $row['course_id'] . '">Sub Exams</a> ';


                        if ($row['sub_exams'] != 'Y')
                            $html .= ' | <a href="' . site_url() . 'admin/exams/std_result_entry/' . $row['id'] . '">Student Result Entry</a>
                                                 </td>
                                         </tr>';
                    }
                    $html .= '<tr><td class="center">Total</td> <td>' . $total . '</td><td>&nbsp;</td> </tr>';
                } else {
                    $html .= '<tr> <td colspan="3"> No Record Found </td> </tr>';
                }
                break;
            case 'examsListSubGroup':

                $data = array();

                $fid = $this->input->post('fid');
                $sid = $this->input->post('sid');
                $bid = $this->input->post('bid');
                $course_id = $this->input->post('cid');

                $sub_exam_admin = getSubExamsAdmin($fid);


                if ($sub_exam_admin == 1 || $sub_exam_admin == 2 || $sub_exam_admin == 4) {
                    $where = "faculty_id = '$fid' and batch_id ='$bid' and year_id ='$sid' and course_id = '$course_id'";
                } else {

                    $where = "faculty_id = '$fid' and batch_id ='$bid' and year_id ='$sid' and course_id = '$course_id' and sub_exam_admin = '$sub_exam_admin' ";
                }

                $results = $this->Exams_model->GetDetail('table_exams', $where);
                //pre($results->result_array());
                if ($results->num_rows() > 0) {
                    $total = 0;
                    foreach ($results->result_array() as $row) {

                        if ($row['sub_exams'] == 'Y') {
                            $marks = $row['max_weightage'];
                            $total = $total + $row['max_weightage'];
                        } else {
                            $marks = $row['max_marks'];
                            $total = $total + $row['max_marks'];
                        }
                        $html .= ' <tr>
                                                <td class="center" >' . $row['exam_name'] . '</td>
                                                <td class="center" >' . $marks . '</td>
                                                 <td class="center" > ';
                        if ($row['sub_exams'] == 'Y')
                            $html .= ' <a href="' . site_url() . 'admin/group/sub_exams/' . $row['id'] . '/' . $row['course_id'] . '" target="_blank">Sub Exams</a> ';


                        if ($row['sub_exams'] != 'Y')
                            $html .= ' | <a href="' . site_url() . 'admin/group/std_result_entry/' . $row['id'] . '" target="_blank">Assign Exams Group</a>
                                                 </td>
                                         </tr>';
                    }
                    $html .= '<tr><td class="center">Total</td> <td>' . $total . '</td><td>&nbsp;</td> </tr>';
                } else {
                    $html .= '<tr> <td colspan="3"> No Record Found </td> </tr>';
                }
                break;


            case 'examsListGroup':

                $data = array();
                $fid = $this->input->post('fid');
                $sid = $this->input->post('sid');
                $bid = $this->input->post('bid');
                $course_id = $this->input->post('cid');
                $group_id = $this->input->post('gid');
                //echo '<pre>';print_r($_POST);exit;

                $sub_exam_admin = getSubExamsAdmin($fid);

                // $group_id = $gid;
                //shaz temp if($sub_exam_admin == 1 || $sub_exam_admin == 2 || $sub_exam_admin == 4)

                if ($sub_exam_admin <= 12) {
                    $where = "faculty_id = '$fid' and batch_id ='$bid' and year_id ='$sid' and course_id = '$course_id'";
                } else {

                    $where = "faculty_id = '$fid' and batch_id ='$bid' and year_id ='$sid' and course_id = '$course_id' and sub_exam_admin = '$sub_exam_admin' ";
                }

                $results = $this->Exams_model->GetDetail('table_exams', $where);
                //pre($results->result_array());
                if ($results->num_rows() > 0) {
                    $total = 0;
                    foreach ($results->result_array() as $row) {

                        if ($row['sub_exams'] == 'Y') {
                            $marks = $row['max_weightage'];
                            $total = $total + $row['max_weightage'];
                        } else {
                            $marks = $row['max_marks'];
                            $total = $total + $row['max_marks'];
                        }
                        $html .= ' <tr>
                                                <td class="center" >' . $row['exam_name'] . '</td>
                                                <td class="center" >' . $marks . '</td>
                                                 <td class="center" >
                                                     <a href="' . site_url() . 'admin/exams/update_exam/' . $row['id'] . '/' . $group_id . '" target="_blank">Edit</a> |
                                                     <a href="' . site_url() . 'admin/exams/delete_exams/' . $row['id'] . '"  target="_blank">Delete</a> |';
                        if ($row['isPublish'] == 'N') {
                            $html .= '  <a href="' . site_url() . 'admin/exams/publish_exams/' . $row['id'] . '/' . $row['isPublish'] . '"  target="_blank" onclick="return doconfirm();">Publish Result</a> |';
                        } else {
                            $html .= '  <a href="' . site_url() . 'admin/exams/publish_exams/' . $row['id'] . '/' . $row['isPublish'] . '"  target="_blank" onclick="return doconfirm2();">Hide Result</a> |';
                        }
                        if ($row['sub_exams'] == 'Y')
                            $html .= ' <a href="' . site_url() . 'admin/exams/sub_exams/' . $row['id'] . '/' . $row['course_id'] . '/' . $group_id . '"  target="_blank">Sub Exams</a> ';


                        if ($row['sub_exams'] != 'Y')
                            $html .= ' | <a href="' . site_url() . 'admin/exams/std_result_entry/' . $row['id'] . '/' . $group_id . '"  target="_blank">Student Result Entry</a>
                                                 </td>
                                         </tr>';
                    }
                    $html .= '<tr><td class="center">Total</td> <td>' . $total . '</td><td>&nbsp;</td> </tr>';
                } else {
                    $html .= '<tr> <td colspan="3"> No Record Found </td> </tr>';
                }

                break;
            case 'connect_sub_exams':
                $ids = $this->input->post('ids');
                $inputs = $this->input->post('inputs');


                $insert['course_id'] = $this->input->post('cid');
                $insert['exam_id'] = $this->input->post('eid');
                $insert['user_id'] = $_SESSION['AdminId'];


                for ($i = 0; $i < sizeof($ids); $i++) {
                    $insert['sub_exam_id'] = $ids[$i];
                    $insert['weightage'] = $inputs[$i];;

                    $this->Exams_model->insert('tbl_sub_connect_exmas', $insert);
                    //$this->Exams_model->insert('tbl_sub_connect_reset_exmas', $insert);
                }
                $html = "Sub Exam Connected Successfully";

                break;
            case 'update_connect_sub_exams':
                $ids = $this->input->post('ids');
                $inputs = $this->input->post('inputs');
                $sub_exams = $this->input->post('sub_exam');

                $update['course_id'] = $this->input->post('cid');
                $update['exam_id'] = $this->input->post('eid');
                $update['updateUser_id'] = $_SESSION['AdminId'];

                for ($i = 0; $i < sizeof($ids); $i++) {
                    $id = $ids[$i];
                    $update['weightage'] = $inputs[$i];
                    if ($id == 0) {

                        $update['sub_exam_id'] = $sub_exams[$i];
                        $this->Exams_model->insert('tbl_sub_connect_exmas', $update);
                        //$this->Exams_model->insert('tbl_sub_connect_reset_exmas', $update);
                    }

                    $where = array('id' => $id);
                    $this->Exams_model->update('tbl_sub_connect_exmas', $update, $where);
                    //$this->Exams_model->update('tbl_sub_connect_reset_exmas', $update, $where);
                }
                $html = "Sub Exam Group updated Successfully";
                break;
            case 'connect_exams':
                $insert['batch_id'] = $this->input->post('bid');
                $insert['course_id'] = $this->input->post('cid');
                $insert['faculty_id'] = $this->input->post('fid');
                $insert['year_id'] = $this->input->post('sid');
                $ids = $this->input->post('ids');
                $inputs = $this->input->post('inputs');


                for ($i = 0; $i < sizeof($ids); $i++)
                    $test[$ids[$i]] = $inputs[$i];

                $insert['weightage'] = serialize($test);
                //          $un_val = unserialize($s_val);

                $this->Exams_model->insert('table_connect_exams', $insert);
                //$this->Exams_model->insert('table_connect_reset_exams', $insert);

                $html = "Exam Connected Successfully";


                break;
            case 'update_connect_exams':
                $update['batch_id'] = $this->input->post('bid');
                $update['course_id'] = $this->input->post('cid');
                $update['faculty_id'] = $this->input->post('fid');
                $update['year_id'] = $this->input->post('sid');
                $ids = $this->input->post('ids');
                $inputs = $this->input->post('inputs');
                $connect_id = $this->input->post('connect_id');

                for ($i = 0; $i < sizeof($ids); $i++)
                    $test[$ids[$i]] = $inputs[$i];

                $update['weightage'] = serialize($test);
                //          $un_val = unserialize($s_val);

                $where = array('id' => $connect_id);
                $this->Exams_model->update('table_connect_exams', $update, $where);
                //$this->Exams_model->update('table_connect_reset_exams', $update, $where);

                $html = "Exam Connected updated Successfully";

                break;
            case 'std_result_update':

                $exam_id = $this->input->post('exam_id');
                $course_id = $this->input->post('course_id');
                $std_id = $this->input->post('std_id');

                $marks = $this->input->post('marks');
                $absent = $this->input->post('absent');
                $ea = $this->input->post('ea');
                $ids = $this->input->post('ids');


                for ($i = 0; $i < sizeof($std_id); $i++) {
                    $update = array();
                    $update['exam_id'] = $exam_id;
                    $update['course_id'] = $course_id;
                    $update['std_id'] = $std_id[$i];
                    $update['marks'] = $marks[$i];
                    $update['absent'] = $absent[$i];
                    $update['ea'] = $ea[$i];

                    $where = array(
                        'exam_id' => $exam_id,
                        'course_id' => $course_id,
                        'std_id' => $std_id[$i]
                    );

                    $this->Exams_model->update('table_std_result', $update, $where);
                    //$this->Exams_model->update('table_std_reset_result', $update, $where);
                }
                $html = "Students result updated successfully";
                break;
            case 'std_result_add':

                $exam_id = $this->input->post('exam_id');
                $course_id = $this->input->post('course_id');
                $std_id = $this->input->post('std_id');
                $marks = $this->input->post('marks');
                $absent = $this->input->post('absent');
                $ea = $this->input->post('ea');

                for ($i = 0; $i < sizeof($std_id); $i++) {
                    $insert = array();
                    $insert['exam_id'] = $exam_id;
                    $insert['course_id'] = $course_id;
                    $insert['std_id'] = $std_id[$i];
                    $insert['marks'] = $marks[$i];
                    $insert['absent'] = $absent[$i];
                    $insert['ea'] = $ea[$i];
                    $result = $this->Exams_model->insert('table_std_result', $insert);
                    //$this->Exams_model->insert('table_std_reset_result', $insert);
                }

                $sql_res = $this->Exams_model->rawSQL(" SELECT * FROM table_std_result LEFT JOIN table_exams ON table_std_result.exam_id=table_exams.id GROUP by table_std_result.std_id,table_std_result.exam_id,table_std_result.course_id ");
                $max_exam_count = $this->Exams_model->rawSQL(" SELECT * FROM table_std_result ORDER BY exam_id DESC LIMIT 0,1");
                $max_exam_no = $max_exam_count[0]['exam_id'];
                $total_marks = 0;
                if (count($sql_res) > 0) {

                    for ($a = 0; $a < $max_exam_no; $a++) {
                        $total_marks += $sql_res[$a]['max_marks'];
                    }
                    for ($i = 0; $i < count($sql_res); $i += $max_exam_no) {
                        $marks = 0;
                        for ($j = 0; $j < $max_exam_no; $j++) {
                            $marks += $sql_res[$i + $j]['marks'];
                        }
                        $per = ($marks * 100) / $total_marks;
                        if ($per == 0) {
                            $points = 0;
                        } else {
                            $points = ($per * 4) / 100;
                        }
                        $grade = $this->gradeCalculator($points);
                        $update = [
                            'points' => round($points,2),
                            'per' => round($per,2),
                            'grade' => $grade,
                            'status' => 'Reg',
                        ];
                        $where = [
                            'std_id' => $sql_res[$i]['std_id'],
                            'course_id' => $sql_res[$i]['course_id'],
                        ];
                        $this->Student_model->update('tbl_std_assignedcourses', $update, $where);
                    }
                }

                $html = "Students result Added successfully ";

                break;
            case 'subexams_std_result_add':
                $exam_id = $this->input->post('exam_id');
                $course_id = $this->input->post('course_id');
                $sub_exam_id = $this->input->post('sub_exam_id');
                $sub_exam_eval_id = $this->input->post('sub_exam_eval_id');
                $std_id = $this->input->post('std_id');
                $marks = $this->input->post('marks');
                $absent = $this->input->post('absent');
                $ea = $this->input->post('ea');

                $dataToInsert = array();
                for ($i = 0; $i < sizeof($std_id); $i++) {
                    $update = array();
                    $update['exam_id'] = $exam_id;
                    $update['sub_exam_id'] = $sub_exam_id;
                    $update['sub_exam_eval_id'] = $sub_exam_eval_id;
                    $update['course_id'] = $course_id;
                    $update['std_id'] = $std_id[$i];
                    $update['marks'] = $marks[$i];
                    $update['absent'] = $absent[$i];
                    $update['ea'] = $ea[$i];
                    $update['user_id'] = $_SESSION['AdminId'];

                    $dataToInsert[] = $update;
                    $this->Exams_model->insert('table_subexams_std_reset_result', $update);
                }

                $this->Exams_model->bulk_insert('table_subexams_std_result', $dataToInsert);

                $html = "Students result Added successfully";

                break;
            case 'subexams_std_result_update':

                $exam_id = $this->input->post('exam_id');
                $course_id = $this->input->post('course_id');
                $sub_exam_id = $this->input->post('sub_exam_id');
                $sub_exam_eval_id = $this->input->post('sub_exam_eval_id');
                $std_id = $this->input->post('std_id');
                $marks = $this->input->post('marks');
                $absent = $this->input->post('absent');
                $ea = $this->input->post('ea');
                $id = $this->input->post('ids');
                $currentdt = $this->input->post('currentdt');

                $dataToUpdate = array();

                //                pre($where);
                for ($i = 0; $i < sizeof($id); $i++) {
                    $update = array();
                    //$update['exam_id']    =   $exam_id;
                    //$update['sub_exam_id']    =   $sub_exam_id;
                    //$update['sub_exam_eval_id']   =   $sub_exam_eval_id;
                    #$update['course_id'] = $course_id;
                    //$update['std_id']     =   $std_id[$i];
                    $update['marks'] = $marks[$i];
                    $update['absent'] = $absent[$i];
                    $update['ea'] = $ea[$i];
                    //$update['std_id'] = $std_id[$i];

                    //$update['updateUser_id'] = $_SESSION['AdminId'];
                    //$update['date_update'] = $currentdt;

                    //$dataToUpdate[] = $update;
                    //pre($update,0);
                    //pre($where,1);
                    //$this->Exams_model->update('table_subexams_std_result', $update, $where);
                    // $this->Exams_model->update('table_subexams_std_reset_result', $update, $where);
                    $where = array(
                        'exam_id' => $exam_id,
                        'course_id' => $course_id,
                        'id' => $id[$i],
                        #'sub_exam_eval_id' => $sub_exam_eval_id
                    );
                    $this->Exams_model->update('table_std_result', $update, $where);
                    //                    q();
                }

                //$this->Exams_model->bulk_update('table_std_result', $dataToUpdate, $where, 'std_id');
                //redirect('admin/exams/sub_exams/' . $eid . "/" . $cid);
                //$sql_res = $this->Common_model->rawSQL(" SELECT * FROM table_std_result LEFT JOIN table_exams ON table_std_result.exam_id = table_exams.id GROUP by table_std_result.std_id,table_std_result.exam_id,table_std_result.course_id ");
                //$res = $sql_res[1]['max_marks'];

                $sql_res = $this->Common_model->rawSQL(" SELECT * FROM table_std_result LEFT JOIN table_exams ON table_std_result.exam_id=table_exams.id GROUP by table_std_result.std_id,table_std_result.exam_id,table_std_result.course_id ");
                $max_exam_count = $this->Common_model->rawSQL(" SELECT * FROM table_std_result ORDER BY exam_id DESC LIMIT 0,1 ");
                $max_exam_no = $max_exam_count[0]['exam_id'];
                $total_marks = 0;
                if (count($sql_res) > 0) {

                    for ($a = 0; $a < $max_exam_no; $a++) {
                        $total_marks += $sql_res[$a]['max_marks'];
                    }
                    for ($i = 0; $i < count($sql_res); $i += $max_exam_no) {
                        $marks = 0;

                        for ($j = 0; $j < $max_exam_no; $j++) {
                            if(isset($sql_res[$i + $j]['marks'])){
                                $marks += $sql_res[$i + $j]['marks'];
                            }
                            
                        }
                        $per = ($marks * 100) / $total_marks;
                        if ($per == 0) {
                            $points = 0;
                        } else {
                            $points = ($per * 4) / 100;
                        }
                        $grade = $this->gradeCalculator($points);
                        $update = [
                            'points' => round($points,2),
                            'per' => round($per,2),
                            'grade' => $grade,
                            'status' => 'Reg',
                        ];
                        $where = [
                            'std_id' => $sql_res[$i]['std_id'],
                            'course_id' => $sql_res[$i]['course_id'],
                        ];
                        $this->Student_model->update('tbl_std_assignedcourses', $update, $where);
                    }
                }

                $html = "Students result Update successfully";

                //redirect($_SERVER['REQUEST_URI'], 'refresh'); 

                break;
            case 'resetData':
                $fid = $this->input->post('fid');
                $bid = $this->input->post('bid');
                $sid = $this->input->post('sid');
                //              $isSubExam  = $this->Exams_model->isSubExam($id);
                //              if($isSubExam=='Y')
                //                  $html = $this->Exams_model->resetSubExamData($id);
                //              else
                $html = $this->Exams_model->resetExamData($fid, $bid, $sid, $id);
                break;
            case 'resetLockData':
                $fid = $this->input->post('fid');
                $bid = $this->input->post('bid');
                $sid = $this->input->post('sid');
                //              $isSubExam  = $this->Exams_model->isSubExam($id);
                //              if($isSubExam=='Y')
                //                  $html = $this->Exams_model->resetSubExamData($id);
                //              else
                $html = $this->Exams_model->resetExamLockData($fid, $bid, $sid, $id);
                break;
            case 'reset_course':
                $fid = $this->input->post('fid');
                $bid = $this->input->post('bid');
                $yid = $this->input->post('yid');
                $cid = $this->input->post('cid');

                $desc = $this->input->post('desc');
                $reSetArr = $this->input->post('reSetArr');
                $grace_marks = $this->input->post('grace_marks');
                $std_ids = $this->input->post('std_ids');
                $inserted = false;
                for ($i = 0; $i < sizeof($std_ids); $i++) {

                    if ($grace_marks[$i] != 0) {

                        $sql = "Select * from tbl_gracemarks where faculty_id = '$fid' and batch_id = '$bid' and year_id='$yid' and course_id= '$cid' and std_id = '" . $std_ids[$i] . "'";
                        $rs = $this->Exams_model->SqlExec($sql);
                        if ($rs->num_rows() > 0) {
                            $row = $rs->row_array();
                            $where = array('id' => $row['id']);
                            $insert_gm['faculty_id'] = $fid;
                            $insert_gm['batch_id'] = $bid;
                            $insert_gm['year_id'] = $yid;
                            $insert_gm['course_id'] = $cid;
                            $insert_gm['grace_marks'] = $grace_marks[$i];
                            $insert_gm['std_id'] = $std_ids[$i];
                            $this->Exams_model->update('tbl_gracemarks', $insert_gm, $where);
                            //$this->Exams_model->update('tbl_reset_gracemarks', $insert_gm, $where);
                            $inserted = true;
                        } else {

                            $insert_gm['faculty_id'] = $fid;
                            $insert_gm['batch_id'] = $bid;
                            $insert_gm['year_id'] = $yid;
                            $insert_gm['course_id'] = $cid;
                            $insert_gm['grace_marks'] = $grace_marks[$i];
                            $insert_gm['std_id'] = $std_ids[$i];
                            $this->Exams_model->insert('tbl_gracemarks', $insert_gm);
                            // $this->Exams_model->insert('tbl_reset_gracemarks', $insert_gm);
                            $inserted = true;
                        }
                    }

                    if ($reSetArr[$i] == 'Y') {
                        $reset_arr = $this->Exams_model->isStdResetCourse($fid, $bid, $yid, $cid, $std_ids[$i]);


                        if (count($reset_arr) == 0) {
                            $insert_reset['faculty_id'] = $fid;
                            $insert_reset['batch_id'] = $bid;
                            $insert_reset['year_id'] = $yid;
                            $insert_reset['course_id'] = $cid;
                            $insert_reset['desc'] = $desc[$i];
                            $insert_reset['std_id'] = $std_ids[$i];
                            $this->Exams_model->insert('tbl_reset_course', $insert_reset);
                            $inserted = true;
                        } else {
                            $insert_reset['faculty_id'] = $fid;
                            $insert_reset['batch_id'] = $bid;
                            $insert_reset['year_id'] = $yid;
                            $insert_reset['course_id'] = $cid;
                            $insert_reset['desc'] = $desc[$i];
                            $insert_reset['std_id'] = $std_ids[$i];
                            $where = array('id' => $reset_arr['id']);
                            $this->Exams_model->update('tbl_reset_course', $insert_reset, $where);
                            $inserted = true;
                        }
                    }
                    if ($inserted == true) {
                        $html = '<div class="notification msgsuccess">
                                        <a class="close"></a>
                                        <p>Exam Reset Sucessfully...</p>
                                   </div>';
                    } else {
                        $html = '<div class="notification msgerror"><a class="close"></a><p>Some Student Exams is already Reset!</p></div>';
                    }
                }
                break;
            case 'year_transfer_form':
                $fid = $this->input->post('faculty_id');
                $bid = $this->input->post('bid');
                $yid = $id;
                $html = $this->Exams_model->yearTransferForm($fid, $bid, $yid);
                break;



            case 'year_rep_form':
                $fid = $this->input->post('faculty_id');
                $bid = $this->input->post('bid');
                $yid = $id;
                $html = $this->Exams_model->yearRepForm($fid, $bid, $yid);
                break;
            case 'year_rep_un_form':
                $fid = $this->input->post('faculty_id');
                $bid = $this->input->post('bid');
                $yid = $id;
                $html = $this->Exams_model->yearRepUnCreditForm($fid, $bid, $yid);
                break;
            case 'year_rep_un_form2':
                $fid = $this->input->post('faculty_id');
                $bid = $this->input->post('bid');
                $yid = $this->input->post('yid');
                $cid = $this->input->post('cid');
                $html = $this->Exams_model->yearRepUnCreditForm2($fid, $bid, $yid, $cid);
                break;
            case 'year_rep_course_form':
                $fid = $this->input->post('fid');
                $bid = $this->input->post('bid');
                $sid = $this->input->post('sid');
                $cid = $id;
                $html = $this->Exams_model->yearRepCourseForm($fid, $bid, $sid, $cid);
                break;
            case 'rep_single_course_form':
                $fid = $this->input->post('fid');
                $bid = $this->input->post('bid');
                $sid = $this->input->post('sid');
                $cid = $id;
                $html = $this->Exams_model->RepSingleCourseForm($fid, $bid, $sid, $cid);
                break;
            case 'repeater_un_form':
                $fid = $this->input->post('faculty_id');
                $bid = $this->input->post('bid');
                $yid = $this->input->post('yid');
                $cid = $id;

                $sql = "SELECT
                          student_detail.std_fname
                        , student_detail.std_id
                        , tbl_std_rep_uncredit_course.marks
                        , tbl_std_rep_uncredit_course.id
                    FROM
                        tbl_std_rep_uncredit_course
                        INNER JOIN student_detail 
                            ON (tbl_std_rep_uncredit_course.std_id = student_detail.std_id)
                        where 
                        tbl_std_rep_uncredit_course.fac_id = '$fid' and 
                        tbl_std_rep_uncredit_course.batch_id ='$bid' and 
                        tbl_std_rep_uncredit_course.year_id='$yid' and 
                        tbl_std_rep_uncredit_course.course_id ='$cid'
                    ;";
                $query = $this->Exams_model->SqlExec($sql);
                $i = 0;
                $sno = 1;
                $html = '';
                if ($query->num_rows() > 0) {
                    foreach ($query->result_array() as $row) {
                        $html .= "<tr>
                        <input type='hidden' name='std_id[]' id='std_id_$i' value='" . $row['std_id'] . "'>
                        <input type='hidden' name='id[]' id='id_$i' value='" . $row['id'] . "'>
                            <td>$sno</td>
                            <td>" . $row['std_id'] . "</td>
                            <td>" . $row['std_fname'] . "</td>
                            <td><input type='text' name='marks[]' id='marks_$i' value='" . ($row['marks'] != '' ? $row['marks'] : 0) . "'></td>
                        </tr>";
                        $sno++;
                        $i++;
                    }
                    $html .= '
                                    <tr class="stdform">
                                         <td class="right"></td>
                                         <td class="left">
                                          <input type="button" value="Save" name="submit" onclick="formSubmit();" />
                                          <input type="reset" value="Reset" name="reset" /></td>
                                        </td>
                                    </tr>';
                } else {
                    $html = '<tr><td colspan="4"><h3>No Record found</h3></td></tr>';
                }

                break;
            case 'rep_un_update':
                $fid = $this->input->post('fid');
                $bid = $this->input->post('bid');
                $yid = $this->input->post('yid');
                $cid = $this->input->post('course_id');

                $ids = $this->input->post('id');
                $std_ids = $this->input->post('std_ids');
                $marks = $this->input->post('marks');


                for ($i = 0; $i < count($id); $i++) {
                    $this->Exams_model->SqlExec("update tbl_std_rep_uncredit_course set marks = '" . $marks[$i] . "' where std_id = '" . $std_ids[$i] . "' and id='" . $ids[$i] . "'");
                }
                $html = '<div class="notification msgsuccess">
                                        <a class="close"></a>
                                        <p>Repeater Students Marks Updated Sucessfully...</p>
                                   </div>';



                break;
            case 'uncredit_course':
                $fid = $this->input->post('faculty_id');
                $bid = $this->input->post('bid');
                $yid = $id;
                $attr = ($this->input->post('attr') != '') ? $this->input->post('attr') : "";

                $sql = "SELECT courses.*, batches_course.id FROM batches_course INNER JOIN courses ON (batches_course.course_id = courses.course_id) and batches_course.faculty_id = '$fid' and batches_course.batch_id= '$bid' and batches_course.semester_id = '$yid' and `courses`.`type`='uncredit'";

                $html = show_dropdown_bysql($sql, 'course', 'course_id', 'course_name', 'Select Course', '', $attr);



                break;
            case 'uncredit_course_rep':
                $fid = $this->input->post('faculty_id');
                $bid = $this->input->post('bid');
                $yid = $id;
                $attr = ($this->input->post('attr') != '') ? $this->input->post('attr') : "";

                $sql = "SELECT courses.*, batches_course.id FROM batches_course INNER JOIN courses ON (batches_course.course_id = courses.course_id) and batches_course.faculty_id = '$fid' and batches_course.batch_id= '$bid' and batches_course.semester_id = '$yid' and batches_course.type='uncredit' and batches_course.status = 'rep' ";

                $html = show_dropdown_bysql($sql, 'course', 'course_id', 'course_name', 'Select Course', '', $attr);
                break;
            case 'uncredit_course_notrep':
                $fid = $this->input->post('faculty_id');
                $bid = $this->input->post('bid');
                $yid = $id;
                $attr = ($this->input->post('attr') != '') ? $this->input->post('attr') : "";

                $sql = "SELECT courses.*, batches_course.id FROM batches_course INNER JOIN courses ON (batches_course.course_id = courses.course_id) and batches_course.faculty_id = '$fid' and batches_course.batch_id= '$bid' and batches_course.semester_id = '$yid' and `courses`.`type`='uncredit' and batches_course.status = 'reg' ";

                $html = show_dropdown_bysql($sql, 'course', 'course_id', 'course_name', 'Select Course', '', $attr);
                break;
            case 'uncredit_course_notrep2':
                $fid = $this->input->post('faculty_id');
                $bid = $this->input->post('bid');
                $yid = $id;
                $attr = ($this->input->post('attr') != '') ? $this->input->post('attr') : "";

                $sql = "SELECT courses.*, batches_course.id FROM batches_course INNER JOIN courses ON (batches_course.course_id = courses.course_id) where batches_course.faculty_id = '$fid' and batches_course.batch_id= '$bid' and batches_course.semester_id = '$yid' and batches_course.type='uncredit' and batches_course.status = 'rep' ";
                //  echo $sql;
                //   exit;

                $html = show_dropdown_bysql($sql, 'course', 'course_id', 'course_name', 'Select Course', '', $attr);
                break;
            case 'course_notrep':
                $fid = $this->input->post('faculty_id');
                $bid = $this->input->post('bid');
                $yid = $id;
                $attr = ($this->input->post('attr') != '') ? $this->input->post('attr') : "";

                $sql = "SELECT courses.*, batches_course.id FROM batches_course INNER JOIN courses ON (batches_course.course_id = courses.course_id) and batches_course.faculty_id = '$fid' and batches_course.batch_id= '$bid' and batches_course.semester_id = '$yid'  and batches_course.status = 'reg' ";

                $html = show_dropdown_bysql($sql, 'course', 'course_id', 'course_name', 'Select Course', '', $attr);


                break;
            case 'rep_un_add':
                $fid = $this->input->post('fid');
                $yid = $this->input->post('yid');
                $bid = $this->input->post('bid');
                $course_id = $this->input->post('course_id');

                $new_bid = $this->input->post('new_bid');
                $new_sid = $this->input->post('new_sid');
                $repeater_course = $this->input->post('repeater_course');


                $rep_arr = $this->input->post('rep');
                $std_ids_arr = $this->input->post('std_ids');

                $isInserted = false;

                for ($i = 0; $i < sizeof($std_ids_arr); $i++) {
                    if ($rep_arr[$i] == 'Y' && $std_ids_arr[$i] != '') {

                        // $count_already = $this->Exams_model->counttotal('tbl_std_rep_course', "fac_id ='$fid' and batch_id='$bid' and year_id='$yid' and std_id='$std_ids_arr[$i]' and course_id='$course_id'");
                        $count_already = $this->Exams_model->counttotal('tbl_std_rep_course', "fac_id ='$fid' and  std_id='$std_ids_arr[$i]' and course_id='$course_id'");
                        if ($count_already == 0) {
                            $insert_std_rep_course_query = "insert into `tbl_std_rep_course`(`std_id`,`fac_id`,`batch_id`,`year_id`,`course_id`,`user_id`) values ( '" . $std_ids_arr[$i] . "','$fid','$bid','$yid','" . $course_id . "','" . $_SESSION['AdminId'] . "')";
                            $insert_std_rep_un_course_query = "insert into `tbl_std_rep_uncredit_course`(`std_id`,`fac_id`,`batch_id`,`year_id`,`course_id`,`repeater_course`,`user_id`) values ( '" . $std_ids_arr[$i] . "','$fid','$new_bid','$new_sid','" . $course_id . "','" . $repeater_course . "','" . $_SESSION['AdminId'] . "')";
                            //$this->Exams_model->SqlExec("update student_detail set std_batch_id = '$new_bid' , std_semester_id = '$new_sid' where std_id = '".$std_ids_arr[$i]."'");
                            $query = $this->Exams_model->SqlExec($insert_std_rep_course_query);
                            $query = $this->Exams_model->SqlExec($insert_std_rep_un_course_query);
                        }
                    } //end if
                }
                $isInserted = true;

                if ($isInserted == true) {
                    $html = '<div class="notification msgsuccess">
                                        <a class="close"></a>
                                        <p>Repeater Students data added Sucessfully...</p>
                                   </div>';
                } else {
                    $html = '<div class="notification msgerror"><a class="close"></a><p>All Repeater Students data already added!</p></div>';
                }


                break;
            case 'rep_course_add':
                $fid = $this->input->post('fid');
                $yid = $this->input->post('yid');
                $bid = $this->input->post('bid');
                $course_id = $this->input->post('course_id');
                $isregular = $this->input->post('isregular');

                $new_bid = $this->input->post('new_bid');
                $new_sid = $this->input->post('new_sid');
                // $repeater_course = $this->input->post('repeater_course');


                $rep_arr = $this->input->post('rep');
                $std_ids_arr = $this->input->post('std_ids');

                $sql = "SELECT courses.*, batches_course.id FROM batches_course INNER JOIN courses ON (batches_course.course_id = courses.course_id) and batches_course.faculty_id = '$fid' and batches_course.batch_id= '$bid' and batches_course.semester_id = '$yid'";
                $query = $this->Exams_model->SqlExec($sql);
                foreach ($query->result() as $o) {
                    $course_arr[] = $o->course_id;
                    $course_type[] = $o->type;
                }

                $isInserted = false;

                for ($i = 0; $i < sizeof($std_ids_arr); $i++) {
                    if ($rep_arr[$i] == 'Y' && $std_ids_arr[$i] != '') {


                        $count_already = $this->Exams_model->counttotal('tbl_std_rep_course', "fac_id ='$fid' and batch_id='$bid' and year_id='$yid' and std_id='$std_ids_arr[$i]' and course_id='$course_id'");


                        if ($count_already == 0) {

                            for ($j = 0; $j < sizeof($course_arr); $j++) {
                                $repeaterCourseDone = 0;
                                $stdRepeaterCourse = 0;


                                if ($course_id == $course_arr[$j]) {

                                    $count_already2 = $this->Exams_model->counttotal('tbl_std_rep_course', "fac_id ='$fid' and  std_id='$std_ids_arr[$i]' and course_id='$course_id'");
                                    if ($count_already2 == 0) {


                                        $insert_std_rep_course_query = "insert into `tbl_std_rep_course`(`std_id`,`fac_id`,`batch_id`,`year_id`,`course_id`,`isregular`,`user_id`) values ( '" . $std_ids_arr[$i] . "','$fid','$bid','$yid','" . $course_id . "','$isregular','" . $_SESSION['AdminId'] . "')";
                                        $query = $this->Exams_model->SqlExec($insert_std_rep_course_query);
                                    } // end of if ($count_already2 == 0)

                                    $repeaterCourseDone = $this->Exams_model->getRepeaterCourseDone2($fid, $bid, $yid, $std_ids_arr[$i], $course_id);
                                    if ((count($repeaterCourseDone) > 0)) {
                                        $delete_done_course = "delete from `tbl_std_course_done` where `std_id` = '" . $std_ids_arr[$i] . "' and `fac_id`='$fid' and`batch_id`= '$bid' and `year_id` = $yid and `course_id` = '" . $course_id . "'";
                                        $query = $this->Exams_model->SqlExec($delete_done_course);
                                    }
                                }   // end ( $course_id == $course_arr[$j])
                                else { // start else of if($course_arr[$j] == $course_id)
                                    $repeaterCourseDone = $this->Exams_model->getRepeaterCourseDone2($fid, $bid, $yid, $std_ids_arr[$i], $course_arr[$j]);
                                    $stdRepeaterCourse = $this->Exams_model->getStdRepeaterCourse($fid, $bid, $yid, $std_ids_arr[$i], $course_arr[$j]);

                                    if ((count($repeaterCourseDone) > 0) || (count($stdRepeaterCourse) > 0)) {
                                    } else {
                                        $insert_std_rep_course_query = "insert into `tbl_std_course_done`(`std_id`,`fac_id`,`batch_id`,`year_id`,`course_id`,`user_id`) values ( '" . $std_ids_arr[$i] . "','$fid','$bid','$yid','" . $course_arr[$j] . "','" . $_SESSION['AdminId'] . "')";
                                        $query = $this->Exams_model->SqlExec($insert_std_rep_course_query);
                                    }
                                }  // end else of if($course_arr[$j] == $course_id) 
                            } //end for ($j = 0; $j < sizeof($course_arr); $j++) {
                        }  // end if ($count_already == 0)
                    } //end if
                } // end for

                $isInserted = true;

                if ($isInserted == true) {
                    $html = '<div class="notification msgsuccess">
                                        <a class="close"></a>
                                        <p>Repeater Students data added Sucessfully...</p>
                                   </div>';
                } else {
                    $html = '<div class="notification msgerror"><a class="close"></a><p>All Repeater Students data already added!</p></div>';
                }


                break;
            case 'rep_add':
                $bid = $this->input->post('bid');
                $new_bid = $this->input->post('new_bid');
                $option = $this->input->post('option');
                $rep_arr = $this->input->post('rep');
                $std_ids_arr = $this->input->post('std_ids');


                $fid = $this->input->post('fid');
                $yid = $this->input->post('yid');

                $isInserted = false;
                $sql = "SELECT courses.*, batches_course.id FROM batches_course INNER JOIN courses ON (batches_course.course_id = courses.course_id) and batches_course.faculty_id = '$fid' and batches_course.batch_id= '$bid' and batches_course.semester_id = '$yid'";
                $query = $this->Exams_model->SqlExec($sql);
                foreach ($query->result() as $o) {
                    $course_arr[] = $o->course_id;
                    $course_type[] = $o->type;
                }
                if ($option == 'year') {

                    // save std  rep  courses

                    for ($i = 0; $i < sizeof($std_ids_arr); $i++) {
                        if ($rep_arr[$i] == 'Y' && $std_ids_arr[$i] != '') {
                            for ($j = 0; $j < sizeof($course_arr); $j++) {
                                // $count_already = $this->Exams_model->counttotal('tbl_std_rep_course', "fac_id ='$fid' and batch_id='$bid' and year_id='$yid' and std_id='$std_ids_arr[$i]' and course_id='$course_arr[$j]'");
                                $count_already = $this->Exams_model->counttotal('tbl_std_rep_course', "fac_id ='$fid' and  std_id='$std_ids_arr[$i]' and course_id='$course_arr[$j]'");
                                if ($count_already == 0) {
                                    $insert_std_rep_course_query = "insert into `tbl_std_rep_course`(`std_id`,`fac_id`,`batch_id`,`year_id`,`course_id`,`user_id`) values ( '" . $std_ids_arr[$i] . "','$fid','$bid','$yid','" . $course_arr[$j] . "','" . $_SESSION['AdminId'] . "')";
                                    $this->Exams_model->SqlExec("update student_detail set std_batch_id = '$new_bid' where std_id = '" . $std_ids_arr[$i] . "'");
                                    $query = $this->Exams_model->SqlExec($insert_std_rep_course_query);
                                }
                            }
                        } //end if
                    }
                    $isInserted = true;
                } else {
                    for ($i = 0; $i < sizeof($std_ids_arr); $i++) {
                        if ($rep_arr[$i] == 'Y' && $std_ids_arr[$i] != '') {
                            for ($j = 0; $j < sizeof($course_arr); $j++) {
                                //$count_already = $this->Exams_model->counttotal('tbl_std_rep_course', "fac_id ='$fid' and batch_id='$bid' and year_id='$yid' and std_id='$std_ids_arr[$i]' and course_id='$course_arr[$j]'");
                                $count_already = $this->Exams_model->counttotal('tbl_std_rep_course', "fac_id ='$fid' and  std_id='$std_ids_arr[$i]' and course_id='$course_arr[$j]'");
                                if ($count_already == 0) {
                                    if ($course_type[$j] == 'block' || $course_type[$j] == 'gs') {
                                        $insert_std_rep_course_query = "insert into `tbl_std_rep_course`(`std_id`,`fac_id`,`batch_id`,`year_id`,`course_id`,`user_id`) values ( '" . $std_ids_arr[$i] . "','$fid','$bid','$yid','" . $course_arr[$j] . "','" . $_SESSION['AdminId'] . "')";
                                    } else {
                                        $insert_std_rep_course_query = "insert into `tbl_std_course_done`(`std_id`,`fac_id`,`batch_id`,`year_id`,`course_id`,`user_id`) values ( '" . $std_ids_arr[$i] . "','$fid','$bid','$yid','" . $course_arr[$j] . "','" . $_SESSION['AdminId'] . "')";
                                    }


                                    $query = $this->Exams_model->SqlExec($insert_std_rep_course_query);


                                    $this->Exams_model->SqlExec("update student_detail set std_batch_id = '$new_bid' where std_id = '" . $std_ids_arr[$i] . "'");
                                }   //end  if ($count_already == 0)
                            } //end for
                            $insertf['faculty_id'] = $fid;
                            $insertf['batch_id'] = $bid;
                            $insertf['year_id'] = $yid;
                            $insertf['std_id'] = $std_ids_arr[$i];
                            $insert['user_id'] = $_SESSION['AdminId'];

                            $this->Exams_model->insert('tbl_std_prev_fac_batch_year', $insertf);
                        } //end if
                    }
                    $isInserted = true;
                }

                if ($isInserted == true) {
                    $html = '<div class="notification msgsuccess">
                                        <a class="close"></a>
                                        <p>Repeater Students data added Sucessfully...</p>
                                   </div>';
                } else {
                    $html = '<div class="notification msgerror"><a class="close"></a><p>All Repeater Students data already added!</p></div>';
                }

                break;
            case 'eca':
                $fid = $this->input->post('faculty_id');
                $bid = $this->input->post('bid');
                $yid = $id;
                $html = $this->Exams_model->ecaData($fid, $bid, $yid);
                break;
            case 'eca_add':
                $fid = $this->input->post('fid');
                $bid = $this->input->post('bid');
                $yid = $this->input->post('yid');

                $desc = $this->input->post('desc');
                $eca = $this->input->post('eca');
                $std_ids = $this->input->post('std_ids');
                $eca_ids = @$this->input->post('eca_id');
                $inserted = false;

                for ($i = 0; $i < sizeof($std_ids); $i++) {

                    if ($desc[$i] != '' || $eca[$i] != '') {
                        if ($this->Exams_model->isAlreadyStdEca($fid, $bid, $yid, $std_ids[$i]) == false) {
                            $insert_reset['faculty_id'] = $fid;
                            $insert_reset['batch_id'] = $bid;
                            $insert_reset['year_id'] = $yid;
                            $insert_reset['desc'] = $desc[$i];
                            $insert_reset['std_id'] = $std_ids[$i];
                            $insert_reset['eca'] = $eca[$i];
                            $this->Exams_model->insert('tbl_eca', $insert_reset);
                            $this->Exams_model->insert('tbl_reset_eca', $insert_reset);
                            $inserted = true;
                        } else {
                            $update = array();
                            $update['faculty_id'] = $fid;
                            $update['batch_id'] = $bid;
                            $update['year_id'] = $yid;
                            $update['desc'] = $desc[$i];
                            $update['std_id'] = $std_ids[$i];
                            $update['eca'] = $eca[$i];
                            $where = array('id' => $eca_ids[$i]);
                            $this->Exams_model->update('tbl_eca', $update, $where);
                        }
                    }
                }
                if ($inserted == true) {
                    $html = '<div class="notification msgsuccess">
                        <a class="close"></a>
                        <p>Student ECA Added Sucessfully...</p>
                    </div>';
                } else {
                    $html = '<div class="notification msgerror"><a class="close"></a><p>All Student ECA is already Added!</p></div>';
                }



                break;

            case 'std_year_transfer_add':
                $old_fid = $this->input->post('old_fid');
                $old_bid = $this->input->post('old_bid');
                $old_yid = $this->input->post('old_yid');

                $new_fid = $this->input->post('new_fid');
                $new_bid = $this->input->post('new_bid');
                $new_yid = $this->input->post('new_yid');

                $transfer = $this->input->post('transfer');
                $std_ids = $this->input->post('std_ids');
                //pre($transfer,1);
                //pre($transfer,1);
                $inserted = false;
                $n = 0;
                for ($i = 0; $i < sizeof($std_ids); $i++) {

                    if ($transfer[$i] == 'Y') {

                        // insert old record in to new table
                        $insert['faculty_id'] = $old_fid;
                        $insert['batch_id'] = $old_bid;
                        $insert['year_id'] = $old_yid;
                        $insert['std_id'] = $std_ids[$i];
                        $insert['user_id'] = $_SESSION['AdminId'];
                        $this->Exams_model->insert('tbl_std_prev_fac_batch_year', $insert);
                        $this->Exams_model->insert('tbl_std_reset_prev_fac_batch_year', $insert);
                        // update current record

                        $update['std_faculty_id'] = $new_fid;
                        $update['std_batch_id'] = $new_bid;
                        $update['std_semester_id'] = $new_yid;

                        $where = array('std_id' => $std_ids[$i]);
                        $result = $this->Exams_model->update('student_detail', $update, $where);
                        //pre($this->db->last_query());
                        if ($result != 0) {
                            $result_arr = $this->Common_model->rawSQL(" SELECT * FROM tbl_std_assignedcourses WHERE faculty_id = " . $old_fid . " AND batch_id = " . $old_bid . " AND semester_id = " . $old_yid . " AND std_id = " . $std_ids[$i] . "");
                            foreach ($result_arr as $res) {
                                if ($res['grade'] == 'F') {
                                    $params = [
                                        'group_id' => 1,
                                        'faculty_id' => $new_fid,
                                        'batch_id' => $new_bid,
                                        'semester_id' => $new_yid,
                                        'std_semester_id' => $res['std_semester_id'] + 1,
                                        'course_id' => $res['course_id'],
                                        'std_id' => $res['std_id'],
                                        'credit_hours' => $res['credit_hours'],
                                        'user_id' => $res['user_id'],
                                    ];
                                    $this->Temp_tbl_std_assigned_courses->insert('tbl_std_assignedcourses', $params);
                                }
                            }
                        }

                        $inserted = true;

                        $n++;
                    }
                }

                if ($inserted == true) {
                    $html = 'Students Transfered Sucessfully';
                } else {
                    $html = 'All Students already transfered!';
                }

                break;
        }
        echo $html;
    }

    function _checkSubExamsExists($id = 0)
    {
        $num = $this->Exams_model->counttotal('table_sub_exams', "exam_id=$id");
        if ($num > 0)
            return true;
        else
            return false;
    }

    function std_uncredit_result_entry($id = 0)
    {
        $data = array();
        $data['contents'] = $this->load->view('admin/exams/std_uncredit_result_entry', $data, true);
        $this->load->view('admin/template', $data);
    }

    function std_result_entry($id = 0, $group_id = 0)
    {
        $data = array();
        $row = $this->Exams_model->getexamdetail($id);
        // pre($row);
        $data['exam_name'] = $row['exam_name'];
        $data['faculty_id'] = $row['faculty_id'];
        $data['batch_id'] = $row['batch_id'];
        $data['year_id'] = $row['year_id'];
        $data['course_id'] = $row['course_id'];
        $data['max_marks'] = $row['max_marks'];
        $data['exam_id'] = $id;
        $data['group_id'] = $group_id;
        $this->session->set_userdata('top_menu', 'Examinations');
        $this->session->set_userdata('sub_menu', 'Examinations/examination');
        $this->form_validation->set_rules('exam_id', 'Name', 'trim');
        $data['title'] = 'Assign Course';
        if ($this->form_validation->run() == TRUE) {
            $exam_id = $this->input->post('exam_id');
            $course_id = $this->input->post('course_id');
            $std_id = $this->input->post('std_id');
            $marks = $this->input->post('marks');
            $absent = $this->input->post('absent');

            for ($i = 0; $i < sizeof($std_id); $i++) {

                if ($marks[$i] <= $row['max_marks']) {
                    $insert = array();
                    $insert['exam_id'] = $exam_id;
                    $insert['course_id'] = $course_id;
                    $insert['std_id'] = $std_id[$i];
                    $insert['marks'] = $marks[$i];

                    $insert['absent'] = ($this->input->post("absent_$i") == 'Y' ? 'Y' : 'N');;
                    $this->Exams_model->insert('table_std_result', $insert);
                    // $this->Exams_model->insert('table_std_reset_result', $insert);
                }
            }



            $_SESSION['msg'] = "Sub Exam Added Successfully";
        }

        $stdResults = $this->Exams_model->stdResultsGroup($id, $data['course_id'], $data['group_id'], $data['faculty_id'], $data['batch_id'], $data['year_id']);

        //   $results = $this->Exams_model->GetDetail('student_detail', "std_faculty_id='" . $data['faculty_id'] . "' and std_batch_id='" . $data['batch_id'] . "' and std_semester_id='" . $data['year_id'] . "'");

        if ($data['group_id'] == '') { //for group None/All
            $where = '';
        } else {
            $where = "and student_session.section_id='" . $data['group_id'] . "' ";
            $where2  = "and students.section_id='" . $data['group_id'] . "' ";
        }

        /// This sql is for save button and union code is to show also previous faculty student ---- "table_std_result.std_id IS NULL" ka purpose unko exclude krna he jo table_std_result may already hain///

       $sql = " 
                SELECT
                    students.firstname,students.middlename,students.lastname
                    , students.id, students.admission_no
                FROM
                    students
                    
                    LEFT JOIN table_std_result 
                        ON (students.id =table_std_result.std_id AND  table_std_result.exam_id ='" . $id . "'  AND table_std_result.course_id ='" . $data['course_id'] . "') where table_std_result.std_id IS NULL and students.faculty_id='" . $data['faculty_id'] . "' and students.academic_year='" . $data['batch_id'] . "' and students.class_grade_id='" . $data['year_id'] . "'   $where2  and students.status= 'Regular'
                        
                        
                        
                        UNION
                        
                        SELECT 
                students.firstname,students.middlename,students.lastname
                , students.id , students.admission_no
                FROM
                    student_session
                    INNER JOIN students
                    ON ( student_session.student_id = students.id)
                    
                    LEFT JOIN table_std_result 
                        ON (students.id =table_std_result.std_id AND  table_std_result.exam_id ='" . $id . "'  AND table_std_result.course_id ='" . $data['course_id'] . "') where table_std_result.std_id IS NULL
                    and student_session.session_id='" . $data['batch_id'] . "' and student_session.class_id='" . $data['year_id'] . "'  $where
                    
                        
                        
                        ;";

        //query timings check k lye usko echo krwa k phpmyadmin may run kro//
        //echo $sql; exit;
 

        $results = $this->Exams_model->SqlExec($sql);
        // echo "<pre>"; print_r($results->result()); exit();
        $data['stdResults'] = $stdResults; 
        $data['results'] = $results;


         $this->load->view('layout/header', $data);  
            $this->load->view('admin/exams/std_result_entry', $data);  
            $this->load->view('layout/footer', $data); 
    }

    function check_exam_weightage()
    {
        $exam = $this->input->post('inputs');
        sizeof($exam);
        $sum = 0;
        for ($i = 0; $i < sizeof($exam); $i++)
            $sum += $exam[$i];

        if ($sum != 100) {
            $this->form_validation->set_message('check_exam_weightage', 'Sum of the weigtage must be "100 &#37;"');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function connect_exams()
    {
        $data = array();

        $data['contents'] = $this->load->view('admin/exams/connect_exams', $data, true);
        $this->load->view('admin/template', $data);
    }

    function sub_exams_std_result_entry($id = 0, $sid = 0, $eid = 0, $group_id = 0)
    {
        $data = array();
        $row = $this->Exams_model->getexamdetail($id);

        $data['exam_name'] = $row['exam_name'];
        $data['faculty_id'] = $row['faculty_id'];
        $data['batch_id'] = $row['batch_id'];
        $data['year_id'] = $row['year_id'];
        $data['course_id'] = $row['course_id'];

        $data['exam_id'] = $id;
        $data['sub_exam_id'] = $sid;
        $data['group_id'] = $group_id;
        $row = $this->Exams_model->getsubexamdetail($sid);
        $data['sub_exam_name'] = $row['sub_exam_name'];
        $data['sub_exams_evaluate_id'] = $eid;

        $rowsz = $this->Exams_model->getsubexamevaldetail($eid);
        $data['max_marks'] = $rowsz['max_marks'];
        $data['eval_exam_name'] = $rowsz['exam_name'];

        $this->form_validation->set_rules('exam_id', 'Name', 'trim');

        if ($this->form_validation->run() == TRUE) {
            $exam_id = $this->input->post('exam_id');
            $sub_exam_id = $this->input->post('sub_exam_id');
            $sub_exam_eval_id = $this->input->post('sub_exam_eval_id');
            $course_id = $this->input->post('course_id');
            $std_id = $this->input->post('std_id');
            $marks = $this->input->post('marks');
            $absent = $this->input->post('absent');

            for ($i = 0; $i < sizeof($std_id); $i++) {
                $insert = array();
                $insert['exam_id'] = $exam_id;
                $insert['course_id'] = $course_id;
                $insert['sub_exam_id'] = $sub_exam_id;
                $insert['sub_exam_eval_id'] = $sub_exam_eval_id;
                $insert['std_id'] = $std_id[$i];
                $insert['marks'] = $marks[$i];

                $insert['absent'] = ($this->input->post("absent_$i") == 'Y' ? 'Y' : 'N');
                $this->Exams_model->insert('table_subexams_std_result', $insert);
                // $this->Exams_model->insert('table_subexams_std_reset_result', $insert);
            }

            $_SESSION['msg'] = "Sub Exam Added Successfully";
        }

        $stdResults = $this->Exams_model->stdSubExamsResultsGroup($id, $data['course_id'], $eid, $data['group_id'], $data['faculty_id'], $data['batch_id'], $data['year_id']);

        // $results = $this->Exams_model->GetDetail('student_detail', "std_faculty_id='" . $data['faculty_id'] . "' and std_batch_id='" . $data['batch_id'] . "' and std_semester_id='" . $data['year_id'] . "'");

        if ($data['group_id'] == 1) { //for group None/All
            $where = '';
        } else {
            $where = "INNER JOIN tbl_std_group
                ON (tbl_std_group.std_id = student_detail.std_id and tbl_std_group.group_id='" . $data['group_id'] . "'  AND tbl_std_group.faculty_id='" . $data['faculty_id'] . "'  AND tbl_std_group.batch_id='" . $data['batch_id'] . "' AND tbl_std_group.semester_id='" . $data['year_id'] . "'     AND tbl_std_group.course_id='" . $data['course_id'] . "')";
        }

        /// This sql is for save button and union code is to show also previous faculty student ---- "table_subexams_std_result.std_id IS NULL" ka purpose unko exclude krna he jo table_subexams_std_result may already hain///
        $sql = "
                SELECT
                    student_detail.std_fname
                    , student_detail.std_id 
                FROM
                    student_detail
                    $where
                    LEFT JOIN table_subexams_std_result 
                        ON (student_detail.std_id =table_subexams_std_result.std_id AND  table_subexams_std_result.exam_id ='" . $id . "'  AND table_subexams_std_result.course_id ='" . $data['course_id'] . "'  AND  table_subexams_std_result.sub_exam_eval_id='" . $eid . "' ) where table_subexams_std_result.std_id IS NULL and student_detail.std_faculty_id='" . $data['faculty_id'] . "' and student_detail.std_batch_id='" . $data['batch_id'] . "' and student_detail.std_semester_id='" . $data['year_id'] . "' and student_detail.std_active= 'Y'
                        
                        UNION
                        
                        SELECT 
                student_detail.std_fname
                , student_detail.std_id 
                FROM
                    tbl_std_prev_fac_batch_year
                    INNER JOIN student_detail
                    ON ( tbl_std_prev_fac_batch_year.std_id = student_detail.std_id)
                    $where
                    LEFT JOIN table_subexams_std_result 
                        ON (student_detail.std_id =table_subexams_std_result.std_id AND  table_subexams_std_result.exam_id ='" . $id . "'  AND table_subexams_std_result.course_id ='" . $data['course_id'] . "'  AND  table_subexams_std_result.sub_exam_eval_id='" . $eid . "' ) where table_subexams_std_result.std_id IS NULL
                    and tbl_std_prev_fac_batch_year.faculty_id='" . $data['faculty_id'] . "' and tbl_std_prev_fac_batch_year.batch_id='" . $data['batch_id'] . "' and tbl_std_prev_fac_batch_year.year_id='" . $data['year_id'] . "'and student_detail.std_active= 'Y'
                    ORDER BY std_id ASC
                    ;";
        $results = $this->Exams_model->SqlExec($sql);

        $data['stdResults'] = $stdResults;
        $data['results'] = $results;
        $data['contents'] = $this->load->view('admin/exams/subexams_std_result_entry', $data, true);
        $this->load->view('admin/template', $data);
    }

    function reset_decision()
    {
        $data = array();
        $data['contents'] = $this->load->view('admin/exams/reset_exams', $data, true);
        $this->load->view('admin/template', $data);
    }

    function eca()
    {
        $data = array();
        $data['contents'] = $this->load->view('admin/exams/eca', $data, true);
        $this->load->view('admin/template', $data);
    }

    // function publish_exams($id, $publish, $page = 0) {
    //     if ($publish == 'N') {
    //         $update['isPublish'] = 'Y';
    //         $_SESSION['msg'] = 'Exams has been published successfully.';
    //     } else {
    //         $update['isPublish'] = 'N';
    //         $_SESSION['msg'] = 'Exams has been hidden successfully.';
    //     }
    //     $where = array('id' => $id);
    //     $this->Exams_model->update('table_exams', $update, $where);
    //     redirect('admin/exams/index/' . $page);
    // }
    // public function delete_file()
    // {
    //     $id = $this->input->post('id');
    //     if($id)
    //     {
    //         $file = $this->Student_model->get_guardian_file($id, '', 'file_id');
    //         if($file)
    //         {
    //             $students = $this->Student_model->get_students(0, array('s.file_id' => $file['file_id']), 's.id');
    //             if($students)
    //                 echo 'Not Deleted';
    //             else
    //                 echo $this->Student_model->update($this->table['guardian_file'], array('date_deleted' => date('Y-m-d H:i:s'), 'user_id_delete' => $_SESSION['AdminId']), array('id' => $id));
    //         }
    //         else
    //             echo 0;
    //     }
    //     else
    //         echo 0;
    // }

    function report($id = 0)
    {
        $data = [];

        $this->load->library('pdf');

        // create new PDF document
        $pdf = new Pdf('-1', PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('TCPDF Example 006');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->setPrintHeader(false);

        // set default header data
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);


        // set auto page breaks
        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------
        // set font
        $pdf->SetFont('dejavusans', '', 10);

        // add a page
        $pdf->AddPage();
        $data = [];
        $html = $this->load->view('admin/reports/pdf/' . $id . '.php', $data, TRUE);
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->AddPage();
        $data = [];
        $html = $this->load->view('admin/reports/pdf/' . $id . '.php', $data, TRUE);
        $pdf->writeHTML($html, true, false, true, false, '');

        $pdf->Output('LAB_Report.pdf', 'I');
    }

    protected function gradeCalculator($points)
    {
        if ($points >= 3.60 && $points <= 4) {
            $grade = "A+";
        } else if ($points >= 3.40 && $points < 3.60) {
            $grade = "A";
        } else if ($points >= 3.20 && $points < 3.40) {
            $grade = "B+";
        } else if ($points >= 3.00 && $points < 3.20) {
            $grade = "B";
        } else if ($points >= 2.80 && $points < 3.00) {
            $grade = "C+";
        } else if ($points >= 2.60 && $points < 2.80) {
            $grade = "C";
        } else if ($points >= 2.40 && $points < 2.60) {
            $grade = "D+";
        } else if ($points >= 2.00 && $points < 2.40) {
            $grade = "D";
        } else {
            $grade = "F";
        }
        return $grade;
    }

    public function getCourses(){
        $faculty_id = $this->input->get('faculty_id');
        $academic_year = $this->input->get('academic_year');
        $class_id = $this->input->get('class_id'); 
        $data     = $this->Exams_model->getCourses($faculty_id,$academic_year,$class_id);
        echo json_encode($data);
    }
    public function printresult() { 
        if (!$this->rbac->hasPrivilege('print_marksheet', 'can_view')) {
            access_denied();
        }
        $this->session->set_userdata('top_menu', 'Examinations');
        $this->session->set_userdata('sub_menu', 'Examinations/examination/printresult');
        $examgroup_result = $this->examgroup_model->get();
        $data['examgrouplist'] = $examgroup_result;

        $marksheet_result = $this->marksheet_model->get();
        $data['marksheetlist'] = $marksheet_result;

        $class = $this->class_model->get();
        $data['title'] = 'Add Batch';
        $data['title_list'] = 'Recent Batch';
        $data['examType'] = $this->exam_type;
        $data['classlist'] = $class;
        $session = $this->session_model->get();
        $data['sessionlist'] = $session;

         $data['faculties']          = $this->student_model->getFaculties();
         $data['sch_setting']     = $this->sch_setting_detail;
         $data['active_session_id'] = $data['sch_setting']->session_id;
        $this->form_validation->set_rules('printsheet', $this->lang->line('printsheet'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('class_id', $this->lang->line('class'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('section_id', $this->lang->line('section'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('session_id', $this->lang->line('student'), 'trim|required|xss_clean');
       

        if ($this->form_validation->run() == false) {
            
        } else {
            $faculty_id = $this->input->post('faculty_id');
            $session_id = $this->input->post('session_id');
            $class_id = $this->input->post('class_id');
            $section_id = $this->input->post('section_id');
            $title = $this->input->post('title'); 
            $data['faculty_id'] = $faculty_id;
            $data['session_id'] = $session_id;
            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['mark_title'] = $title;
            $printsheet_template = $this->input->post('printsheet');
            $data['printsheet_template'] = $printsheet_template;
  
            $data['studentList'] = $this->Exams_model->searchExamStudents($faculty_id, $class_id, $section_id, $session_id,null);
            // $data['examList'] = $this->examgroup_model->getExamByExamGroup($exam_group_id, true);

           
        }      

        $data['sch_setting'] = $this->sch_setting_detail;
        $this->load->view('layout/header', $data);
        // $this->load->view('admin/examresult/marksheet', $data);
        $this->load->view('admin/exams/printresult', $data); 
        $this->load->view('layout/footer', $data);
    }
    public function printmarksheet1() {
        // echo "<pre>"; print_r($_POST); exit();
        $this->form_validation->set_error_delimiters('', '');
        // $this->form_validation->set_rules('post_exam_id', $this->lang->line('exam'), 'required|trim|xss_clean');
        // $this->form_validation->set_rules('post_exam_group_id', $this->lang->line('exam') . " " . $this->lang->line('group'), 'required|trim|xss_clean');
        $this->form_validation->set_rules('exam_group_class_batch_exam_student_id[]', $this->lang->line('students'), 'required|trim|xss_clean');
        $data = array();

        if ($this->form_validation->run() == false) {
            $data = array(
                // 'post_exam_id' => form_error('post_exam_id'),
                // 'post_exam_group_id' => form_error('post_exam_group_id'),
                'exam_group_class_batch_exam_student_id' => form_error('exam_group_class_batch_exam_student_id'),
            );
            $array = array('status' => 0, 'error' => $data);
            echo json_encode($array); 
        } else {
            $result         = $this->setting_model->getSetting(); 
           // $lg = $this->setting_model->getAdminlogo();               
           $logo_url = base_url().'uploads/school_content/report_logo/'.$result->report_logo;
           $signature =  base_url().'uploads/school_content/signature/'.$result->app_signature; 
           
            // $data['template'] = $this->getprintsheettemplate($this->input->post('printsheet'),$students_array);
             $output = '';
             $mark_title = $this->input->post('mark_title'); 
             $data['mark_title'] = $mark_title;
             $faculty_id = $this->input->post('faculty_id');
             $session_id = $this->input->post('session_id');
             $s_q = " SELECT session FROM sessions WHERE id='".$session_id."' ";
             $s_res = $this->Exams_model->SqlExec($s_q);
             $session_name = $s_res->row()->session;
             $class_id = $this->input->post('class_id');
             $section_id = $this->input->post('section_id');
             $courses_html = '';
             $sql1 = " SELECT * FROM classes WHERE id='".$class_id."'  ";
             $grade_result = $this->Exams_model->SqlExec($sql1);
             $r = $grade_result->row();
             $grade_system = isset($r->grade_system) ? $r->grade_system : '';
             if(!empty($r->class_in_arabic)){
                $class_name = $r->class_in_arabic;
             }else{
                $class_name = $r->class;
             }
           
            $students_array = $this->input->post('exam_group_class_batch_exam_student_id');
            foreach($students_array as $std_id){

                $stdsql = " SELECT eng_name,firstname,middlename,lastname,admission_no FROM students WHERE id='".$std_id."' ";
                $std_res = $this->Exams_model->SqlExec($stdsql);
                $std_data = $std_res->row();
                $admission_no = $std_data->admission_no;
                $std_name = $std_data->firstname.'&nbsp'.$std_data->middlename.'&nbsp'.$std_data->lastname;
                
                 $total_marks = 0;
            $student_total_weightage = 0;
            $failCounter=0;
            $grade = ''; 
            $remarks ="";
            $per=0;
            $failCourses =0;
            $c_marks =0;
            //Get Courses of current class 
            $sql2 = "SELECT assigned_subjects.subject_id,`subjects`.`name`  FROM assigned_subjects INNER JOIN subjects ON(subjects.id = assigned_subjects.subject_id) WHERE assigned_subjects.faculty_id = '".$faculty_id."' AND assigned_subjects.academic_year= '".$session_id."' AND assigned_subjects.class_id = '".$class_id."' ";

            $course_q = $this->Exams_model->SqlExec($sql2);
            if($course_q->num_rows() >0){
                $courses_html = '';
                $c_counter = 1;
                $exams_html = '';
                
                foreach ($course_q->result_array() as $i=>$course_row) {

                    $subject_id[] = $course_row['subject_id'];
                    $subject_name = $course_row['name'];
                    // //Get Exam w.r.t Courses of current class 
                   $exam_max_marks = 0;
                   $exam_min_marks = 0;
                  
                   $total_course_marks = 0;
                   $total_course_min_marks= 0;
                   
                   $course_note ="";
                   $stdFail = "";
                   $c_marks = 0;
                   $isFail = '';

                   //exam start//                           
             $exams_id = array();
             $sql3 = "SELECT * FROM table_exams WHERE faculty_id = '".$faculty_id."' and batch_id= '".$session_id."' and year_id = '".$class_id."'  and course_id ='" . $course_row['subject_id'] . "' ";
              $exam_q = $this->Exams_model->SqlExec($sql3);
             $i = 0; 
               if($exam_q->num_rows() >0){
                foreach ($exam_q->result_array() as $k=>$exam_row) {
                    $exam_ids[$i] = $exam_row['id'];
                    $exam_max_marks = $exam_row['max_marks'];
                    $exam_min_marks = $exam_row['min_marks'];
                    $total_course_marks +=$exam_max_marks;
                    $total_course_min_marks += $exam_min_marks;

                       // $exam_name = $exam_row['exam_name'];
                        //$exams_html .= '<th>'.$exam_name.'</th>';

$i ++;
               }}

                   //Student marks 
                 $marks_html = '';
                       for ($j = 0; $j <2; $j++) {
                                 
                           
                           
                           $sql = "select *from table_std_result where exam_id = '" . $exam_ids[$j] . "' and course_id = '" . $course_row['subject_id'] . "' and std_id='" .  $std_id . "' and isActive = 'Y';";
                             
                       $marks_q = $this->Exams_model->SqlExec($sql);
                       if($marks_q->num_rows() >0)
                       {
                             
                            foreach ($marks_q->result_array() as $stdMarksRow) 
                            {
                            
                                $std_marks = $stdMarksRow['marks'];
                                $marks_html .= '<td>'.$std_marks.'</td>';
                                $c_marks += $stdMarksRow['marks'];
                                
                            }
                        $c_marks = round($c_marks, 2);    

                        }
                        else{
                        $marks_html .= '<td>'."-".'</td>';}
 }
                 
              


              if ($grade_system == "fail")  
                   {
                    if ($c_marks < $total_course_min_marks) 
                    {

                        $stdFail = 'Fail';
                        $failCounter++;
                        $course_note = "راسب"; //fail
                     }
                    }
                                            
                if ($grade_system == "need_care")
                    {
                        if ($c_marks < ($total_course_marks/2)) {  // yani less than 50perc
                        $stdFail = 'Fail';
                        $failCounter++;
                        $course_note = "يرجى الإهتمام"; //fail
                    }
                        
                    }
                   $total_marks = $total_marks + $total_course_marks;     
                   $student_total_weightage = $student_total_weightage + $c_marks;

                   foreach ($exam_q->result_array() as $k=>$row) {
                        $min_marks = $row['min_marks'];
                        $max_marks = $row['max_marks'];
                   }
                   
                   
                        $c_marks = round($c_marks, 2);    
                            
                                
                   $courses_html .= '<tr>
                        <td>'.$c_counter.'</td>
                        <td>'.$subject_name.'</td>
                        <td>'.$total_course_marks.'</td>
                        <td>'.$total_course_min_marks.'</td>
                        '.$marks_html.'
                        <td>'.$c_marks.'</td>
                        <td>'.$course_note.'</td>
                    </tr>';
                    $c_counter++;
            }//courses
        }//course
        if(!empty($total_marks) && $total_marks >0){
           $per = round(($student_total_weightage * 100) / $total_marks, 2); 
       }else{
        $per = round(($student_total_weightage * 100), 2);
       }
        
                            if ((round($per, 0) < 50) || ($failCounter > 0))
                                {$grade = 'F'; $remarks ="يحتاج إلى عناية";}
                            else if ((round($per, 0) >= 65) && (round($per, 0) <= 50))
                                    {$grade = 'C'; $remarks ="جيـــد";}
                            else if ((round($per, 0) >= 75) && (round($per, 0) <= 64))
                                   {$grade = 'B' ; $remarks ="جيـــد جـــدًا";}
                            else if ((round($per, 0) >= 85) && (round($per, 0) <= 75))
                                 {$grade = 'A' ; $remarks ="مـمـتــــاز";} 
                            else
                            $remarks ="مـمـتــــاز";
                        
                                
                            
                        $c_marks = round($c_marks, 2);    

                        
        $output .= '<img src='.$logo_url.' >';
        $output .= '<div class="main_head">
                    <h3>وزارة التعليم – المنطقة التعليمية بنغازي</h3>
                    <h3>مدرســة النابــغة الصغــير للتعلــيم الحــر</h3>
                    <h3>بطاقة تقدير درجات طالب '.$class_name.' ('.$mark_title.') للسنة الدراسية '.$session_name.'</h3>
             </div>';
        $output .= '<div class="std_detail">
                <div class="roll right">
                    <h4>الاســــم: '.$std_name.'</h4>
                </div>
                <div class="curr left">
                    <h4>الرقــــم: '.$admission_no.'</h4>
                </div>
            </div>';
        $output .='<table>';
        $output .= '<thead>
                    <tr>
                        <th>م</th>
                        <th>المواد الـدراسية</th>
                        <th>العظمى</th>
                        <th>الصغرى</th>
                        <th>الفترة الأولى</th>
                        <th>الفترة الثانية</th>
                        <th>المجموع</th>
                        <th>مــلاحظــات</th>
                    </tr>
                </thead>';
        $output .= '<tbody>'; 
        $output .= $courses_html;   //yhan courses ane hain
        $output .='</tbody>';

        $output .='</table>';
        $output .= '<table>
                <tbody>
                    <tr>
                        <th>النتيجة</th>
                        <td>'.$remarks.'</td>
                        <th>المجموع</th>
                        <td>'.$student_total_weightage.' من '.$total_marks.'</td>
                        <th>النسبة</th>
                        <td>'.$per.'%</td>
                    </tr>
                </tbody>
            </table>';
         $output .= '<div class="foot">
                <div class="right">
                    <h4>توقيع ولى الأمر:ــــــــــــــــــــــــــــــــــــــــ </h4>
                </div>
                <div class="left">
                    <h4><span>مديرة المدرسة: </span>: فاطمة ابحيح</h4>
                    <img src='.$signature.' style="width:80px;" >
                </div>
                
            </div>';
         $output .= '<div class="pagebreak"> </div>';      

        }//student
            $data['html_data'] = $output;  
            $data['sch_setting'] = $this->sch_setting_detail;
            $student_exam_page = $this->load->view('admin/exams/_printresult', $data, true); 
            $array = array('status' => '1', 'error' => '', 'page' => $student_exam_page);
            echo json_encode($array); 
        
    }
}


 public function printmarksheet2() {
        // echo "<pre>"; print_r($_POST); exit();
        $this->form_validation->set_error_delimiters('', '');
        // $this->form_validation->set_rules('post_exam_id', $this->lang->line('exam'), 'required|trim|xss_clean');
        // $this->form_validation->set_rules('post_exam_group_id', $this->lang->line('exam') . " " . $this->lang->line('group'), 'required|trim|xss_clean');
        $this->form_validation->set_rules('exam_group_class_batch_exam_student_id[]', $this->lang->line('students'), 'required|trim|xss_clean');
        $data = array();

        if ($this->form_validation->run() == false) {
            $data = array(
                // 'post_exam_id' => form_error('post_exam_id'),
                // 'post_exam_group_id' => form_error('post_exam_group_id'),
                'exam_group_class_batch_exam_student_id' => form_error('exam_group_class_batch_exam_student_id'),
            );
            $array = array('status' => 0, 'error' => $data);
            echo json_encode($array); 
        } else {
            $result         = $this->setting_model->getSetting(); 
           // $lg = $this->setting_model->getAdminlogo();               
           $logo_url = base_url().'uploads/school_content/report_logo/'.$result->report_logo;
           $signature =  base_url().'uploads/school_content/signature/'.$result->app_signature; 
           
            // $data['template'] = $this->getprintsheettemplate($this->input->post('printsheet'),$students_array);
             $output = '';
             $mark_title = $this->input->post('mark_title'); 
             $data['mark_title'] = $mark_title;
             $faculty_id = $this->input->post('faculty_id');
             $session_id = $this->input->post('session_id');
             $s_q = " SELECT session FROM sessions WHERE id='".$session_id."' ";
             $s_res = $this->Exams_model->SqlExec($s_q);
             $session_name = $s_res->row()->session;
             $class_id = $this->input->post('class_id');
             $section_id = $this->input->post('section_id');
             $courses_html = '';
             $sql1 = " SELECT * FROM classes WHERE id='".$class_id."'  ";
             $grade_result = $this->Exams_model->SqlExec($sql1);
             $r = $grade_result->row();
             $grade_system = isset($r->grade_system) ? $r->grade_system : '';
             if(!empty($r->class_in_arabic)){
                $class_name = $r->class_in_arabic;
             }else{
                $class_name = $r->class;
             }
           
            $students_array = $this->input->post('exam_group_class_batch_exam_student_id');
            foreach($students_array as $std_id){

                $stdsql = " SELECT eng_name,firstname,middlename,lastname,admission_no FROM students WHERE id='".$std_id."' ";
                $std_res = $this->Exams_model->SqlExec($stdsql);
                $std_data = $std_res->row();
                $admission_no = $std_data->admission_no;
                $std_name = $std_data->firstname.'&nbsp'.$std_data->middlename.'&nbsp'.$std_data->lastname;
                
                 $total_marks = 0;
            $student_total_weightage = 0;
            $failCounter=0;
            $grade = ''; 
            $remarks ="";
            $per=0;
            $failCourses =0;
            $c_marks =0;
            //Get Courses of current class 
            $sql2 = "SELECT assigned_subjects.subject_id,`subjects`.`name`  FROM assigned_subjects INNER JOIN subjects ON(subjects.id = assigned_subjects.subject_id) WHERE assigned_subjects.faculty_id = '".$faculty_id."' AND assigned_subjects.academic_year= '".$session_id."' AND assigned_subjects.class_id = '".$class_id."' ";

            $course_q = $this->Exams_model->SqlExec($sql2);
            if($course_q->num_rows() >0){
                $courses_html = '';
                $c_counter = 1;
                $exams_html = '';
                
                foreach ($course_q->result_array() as $i=>$course_row) {

                    $subject_id[] = $course_row['subject_id'];
                    $subject_name = $course_row['name'];
                    // //Get Exam w.r.t Courses of current class 
                   $exam_max_marks = 0;
                   $exam_min_marks = 0;
                  
                   $total_course_marks = 0;
                   $total_course_min_marks= 0;
                    
                    
                   
                   $course_note ="";
                   $stdFail = "";
                   $c_marks = 0;
                   $isFail = '';

                   //exam start//                           
             $exam_ids[] = array();
             
            
            
             $sql3 = "SELECT * FROM table_exams WHERE faculty_id = '".$faculty_id."' and batch_id= '".$session_id."' and year_id = '".$class_id."'  and course_id ='" . $course_row['subject_id'] . "' ";
              $exam_q = $this->Exams_model->SqlExec($sql3);
             $i = 0; 
               if($exam_q->num_rows() >0){
                foreach ($exam_q->result_array() as $k=>$exam_row) {
                    $exam_ids[$i] = $exam_row['id'];
                    $exam_max_marks = $exam_row['max_marks'];
                    $exam_min_marks = $exam_row['min_marks'];
                    $total_course_marks +=$exam_max_marks;
                    $total_course_min_marks += $exam_min_marks;
                    
                    

                       // $exam_name = $exam_row['exam_name'];
                        //$exams_html .= '<th>'.$exam_name.'</th>';

$i ++;
               }}
               
               
                  //Exams marks 
                 $maxmarks_html = '';
                       for ($j = 0; $j <2; $j++) {
                                 
                           
                           
                           $sql22 = "select *from table_exams where id = '" . $exam_ids[$j] . "' and course_id = '" . $course_row['subject_id'] . "' ;";
                             
                       $maxmarks_q = $this->Exams_model->SqlExec($sql22);
                        $total_course_maxmarks =0;
                              $max_marks=0;
                       if($maxmarks_q->num_rows() >0)
                       {
                            
                             
                            foreach ($maxmarks_q->result_array() as $maxMarksRow) 
                            {
                            
                                $max_marks = $maxMarksRow['max_marks'];
                                 $maxmarks_html .= '<td>'.$max_marks.'</td>';
                                $total_course_maxmarks += $maxMarksRow['max_marks'];
                                
                            }
                        $total_course_maxmarks = round($total_course_maxmarks, 2);    

                        }
                        else{
                         $maxmarks_html .= '<td>'."-".'</td>';}
 }
 
 

                   //Student marks 
                 $marks_html = '';
                       for ($j = 0; $j <2; $j++) {
                                 
                           
                           
                           $sql = "select *from table_std_result where exam_id = '" . $exam_ids[$j] . "' and course_id = '" . $course_row['subject_id'] . "' and std_id='" .  $std_id . "' and isActive = 'Y';";
                             
                       $marks_q = $this->Exams_model->SqlExec($sql);
                       if($marks_q->num_rows() >0)
                       {
                             
                            foreach ($marks_q->result_array() as $stdMarksRow) 
                            {
                            
                                $std_marks = $stdMarksRow['marks'];
                                $marks_html .= '<td>'.$std_marks.'</td>';
                                $c_marks += $stdMarksRow['marks'];
                                
                            }
                        $c_marks = round($c_marks, 2);    

                        }
                        else{
                        $marks_html .= '<td>'."-".'</td>';}
 }
                 
              


              if ($grade_system == "fail")  
                   {
                    if ($c_marks < $total_course_min_marks) 
                    {

                        $stdFail = 'Fail';
                        $failCounter++;
                        $course_note = "راسب"; //fail
                     }
                    }
                                            
                if ($grade_system == "need_care")
                    {
                        if ($c_marks < ($total_course_marks/2)) {  // yani less than 50perc
                        $stdFail = 'Fail';
                        $failCounter++;
                        $course_note = "يرجى الإهتمام"; //fail
                    }
                        
                    }
                   $total_marks = $total_marks + $total_course_marks;     
                   $student_total_weightage = $student_total_weightage + $c_marks;

                   foreach ($exam_q->result_array() as $k=>$row) {
                        $min_marks = $row['min_marks'];
                        $max_marks = $row['max_marks'];
                   }
                   
                   
                        $c_marks = round($c_marks, 2);    
                            
                                
                   $courses_html .= '<tr>
                        <td>'.$c_counter.'</td>
                        <td>'.$subject_name.'</td>
                        '.$maxmarks_html.'
                        <td>'. $total_course_maxmarks.'</td>
                        '.$marks_html.'
                        <td>'.$c_marks.'</td>
                        <td>'.$course_note.'</td>
                    </tr>';
                    $c_counter++;
            }//courses
        }//course
        if(!empty($total_marks) && $total_marks >0){
           $per = round(($student_total_weightage * 100) / $total_marks, 2); 
       }else{
        $per = round(($student_total_weightage * 100), 2);
       }
        
                            if ((round($per, 0) < 50) || ($failCounter > 0))
                                {$grade = 'F'; $remarks ="يحتاج إلى عناية";}
                            else if ((round($per, 0) >= 65) && (round($per, 0) <= 50))
                                    {$grade = 'C'; $remarks ="جيـــد";}
                            else if ((round($per, 0) >= 75) && (round($per, 0) <= 64))
                                   {$grade = 'B' ; $remarks ="جيـــد جـــدًا";}
                            else if ((round($per, 0) >= 85) && (round($per, 0) <= 75))
                                 {$grade = 'A' ; $remarks ="مـمـتــــاز";} 
                            else
                            $remarks ="مـمـتــــاز";
                        
                                
                            
                        $c_marks = round($c_marks, 2);    

                        
        $output .= '<img src='.$logo_url.' >';
        $output .= '<div class="main_head">
                    <h3>وزارة التعليم – المنطقة التعليمية بنغازي</h3>
                    <h3>مدرســة النابــغة الصغــير للتعلــيم الحــر</h3>
                    <h3>بطاقة تقدير درجات طالب '.$class_name.' ('.$mark_title.') للسنة الدراسية '.$session_name.'</h3>
             </div>';
        $output .= '<div class="std_detail">
                <div class="roll right">
                    <h4>الاســــم: '.$std_name.'</h4>
                </div>
                <div class="curr left">
                    <h4>الرقــــم: '.$admission_no.'</h4>
                </div>
            </div>';
        $output .='<table>';
        $output .= '<thead>
                    <tr>
                        <th>م</th>
                        <th>المواد الـدراسية</th>
                        <th colspan="3">
                            النهاية العظمى
                        </th>
                        <th colspan="3">
                            الفصل الأول
                            
                        </th>
                        <th>ملاحظات</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>عمال</th>
                        <th>امتحان</th>
                        <th>مجموع</th>
                        <th>عمال</th>
                        <th>امتحان</th>
                        <th>مجموع</th>
                        <th></th>
                    </tr>
                </thead>';
        $output .= '<tbody>'; 
        $output .= $courses_html;   //yhan courses ane hain
        $output .='</tbody>';

        $output .='</table>';
        $output .= '<table>
                <tbody>
                    <tr>
                        <th>النتيجة</th>
                        <td>'.$remarks.'</td>
                        <th>المجموع</th>
                        <td>'.$student_total_weightage.' من '.$total_marks.'</td>
                        <th>النسبة</th>
                        <td>'.$per.'%</td>
                    </tr>
                </tbody>
            </table>';
         $output .= '<div class="foot">
                <div class="right">
                    <h4>توقيع ولى الأمر:ــــــــــــــــــــــــــــــــــــــــ </h4>
                </div>
                <div class="left">
                    <h4><span>مديرة المدرسة: </span>: فاطمة ابحيح</h4>
                    <img src='.$signature.' style="width:80px;" >
                </div>
                
            </div>';
         $output .= '<div class="pagebreak"> </div>';      

        }//student
            $data['html_data'] = $output;  
            $data['sch_setting'] = $this->sch_setting_detail;
            $student_exam_page = $this->load->view('admin/exams/_printresult', $data, true); 
            $array = array('status' => '1', 'error' => '', 'page' => $student_exam_page);
            echo json_encode($array); 
        
    }
}

  public function printmarksheet3() {
        $this->form_validation->set_error_delimiters('', '');
        // $this->form_validation->set_rules('post_exam_id', $this->lang->line('exam'), 'required|trim|xss_clean');
        // $this->form_validation->set_rules('post_exam_group_id', $this->lang->line('exam') . " " . $this->lang->line('group'), 'required|trim|xss_clean');
        $this->form_validation->set_rules('exam_group_class_batch_exam_student_id[]', $this->lang->line('students'), 'required|trim|xss_clean');
        $data = array();

        if ($this->form_validation->run() == false) {
            $data = array(
                // 'post_exam_id' => form_error('post_exam_id'),
                // 'post_exam_group_id' => form_error('post_exam_group_id'),
                'exam_group_class_batch_exam_student_id' => form_error('exam_group_class_batch_exam_student_id'),
            );
            $array = array('status' => 0, 'error' => $data);
            echo json_encode($array); 
        } else {
            $result         = $this->setting_model->getSetting(); 
           // $lg = $this->setting_model->getAdminlogo();               
           $logo_url = base_url().'uploads/school_content/report_logo/'.$result->report_logo;
           $signature =  base_url().'uploads/school_content/signature/'.$result->app_signature; 
           
            // $data['template'] = $this->getprintsheettemplate($this->input->post('printsheet'),$students_array);
             $output = '';
             $mark_title = $this->input->post('mark_title'); 
             $data['mark_title'] = $mark_title;
             $faculty_id = $this->input->post('faculty_id');
             $session_id = $this->input->post('session_id');
             $s_q = " SELECT session FROM sessions WHERE id='".$session_id."' ";
             $s_res = $this->Exams_model->SqlExec($s_q);
             $session_name = $s_res->row()->session;
             $class_id = $this->input->post('class_id');
             $section_id = $this->input->post('section_id');
             $courses_html = '';
             
             $sqlsec = " SELECT * FROM sections WHERE id='".$section_id."'  ";
             $section_result = $this->Exams_model->SqlExec($sqlsec);
             $sec = $section_result->row();
             $section_name = $sec->section;
             
             
             
             
             $sql1 = " SELECT * FROM classes WHERE id='".$class_id."'  ";
             $grade_result = $this->Exams_model->SqlExec($sql1);
             $r = $grade_result->row();
             
             $grade_system = isset($r->grade_system) ? $r->grade_system : '';
             if(!empty($r->class_in_arabic)){
                $class_name = $r->class_in_arabic;
             }else{
                $class_name = $r->class;
             }
           
            $students_array = $this->input->post('exam_group_class_batch_exam_student_id');
            foreach($students_array as $std_id){

                $stdsql = " SELECT eng_name,firstname,middlename,lastname,admission_no FROM students WHERE id='".$std_id."' ";
                $std_res = $this->Exams_model->SqlExec($stdsql);
                $std_data = $std_res->row();
                $admission_no = $std_data->admission_no;
                $std_name = $std_data->firstname.'&nbsp'.$std_data->middlename.'&nbsp'.$std_data->lastname;
                
                 $total_marks = 0;
            $student_total_weightage = 0;
            $failCounter=0;
            $grade = ''; 
            $remarks ="";
            $per=0;
            $failCourses =0;
            $c_marks =0;
            //Get Courses of current class 
            $sql2 = "SELECT assigned_subjects.subject_id,`subjects`.`name`  FROM assigned_subjects INNER JOIN subjects ON(subjects.id = assigned_subjects.subject_id) WHERE assigned_subjects.faculty_id = '".$faculty_id."' AND assigned_subjects.academic_year= '".$session_id."' AND assigned_subjects.class_id = '".$class_id."' ";

            $course_q = $this->Exams_model->SqlExec($sql2);
            if($course_q->num_rows() >0){
                $courses_html = '';
                $c_counter = 1;
                $exams_html = '';
                
                foreach ($course_q->result_array() as $i=>$course_row) {

                    $subject_id[] = $course_row['subject_id'];
                    $subject_name = $course_row['name'];
                    // //Get Exam w.r.t Courses of current class 
                   $exam_max_marks = 0;
                   $exam_min_marks = 0;
                  
                   $total_course_marks = 0;
                   $total_course_min_marks= 0;
                   
                   $course_note ="";
                   $stdFail = "";
                   $c_marks = 0;
                   $isFail = '';

                   //exam start//                           
             $exams_id = array();
             $sql3 = "SELECT * FROM table_exams WHERE faculty_id = '".$faculty_id."' and batch_id= '".$session_id."' and year_id = '".$class_id."'  and     section_id = '".$section_id."' and course_id ='" . $course_row['subject_id'] . "' ";
              $exam_q = $this->Exams_model->SqlExec($sql3);
              
            //  echo $ex;am_q; exit;
              
              //  echo "<pre>"; print_r($exam_q); exit();
             $i = 0; 
               if($exam_q->num_rows() >0){
                foreach ($exam_q->result_array() as $k=>$exam_row) {
                    $exam_ids[$i] = $exam_row['id'];
                    $exam_max_marks = $exam_row['max_marks'];
                    $exam_min_marks = $exam_row['min_marks'];
                    $total_course_marks +=$exam_max_marks;
                    $total_course_min_marks += $exam_min_marks;

                       // $exam_name = $exam_row['exam_name'];
                        //$exams_html .= '<th>'.$exam_name.'</th>';

$i ++;
               }}

                   //Student marks 
                 $marks_html = '';
                       for ($j = 0; $j <1; $j++) {
                                 
                           
                           $ex_id = isset($exam_ids[$j]) ? $exam_ids[$j] : '';
                           $cid = $course_row['subject_id'];
                           if(isset($ex_id) && !empty($ex_id)){
                               $sql = "select *from table_std_result where exam_id = '" . $exam_ids[$j] . "' and course_id = '" . $course_row['subject_id'] . "' and std_id='" .  $std_id . "' and isActive = 'Y';";
                           }
                           
                           
                           //echo  $sql; exit;
                             
                       $marks_q = $this->Exams_model->SqlExec($sql);
                       if($marks_q->num_rows() >0)
                       {
                             
                            foreach ($marks_q->result_array() as $stdMarksRow) 
                            {
                            
                                $std_marks = $stdMarksRow['marks'];
                                $ea  = $stdMarksRow['ea'];
                                
                                
                                
                        
                        
                        if( $ea == 'Y')
                        {
                             $marks_html .= '<td>-</td>';
                                $marks_html .= '<td>-</td>';
                                $marks_html .= '<td>-</td>';
                                $total_course_marks = 0;
                               // $c_marks += $stdMarksRow['marks'];
                               
                             
                        }
                    else
                    {
                             $course_per =  ($std_marks *100)/$total_course_marks;
                         if ((round($course_per, 0) < 33) || ($failCounter > 0))
                                {$course_grade = 'F'; $course_remarks ="Try Again";}
                            else if ((round($course_per, 0) >= 33) && (round($course_per, 0) < 40))
                                    {$course_grade = 'E'; $course_remarks ="SATISFACTORY";}
                            else if ((round($course_per, 0) >= 40) && (round($course_per, 0) <50))
                                   {$course_grade = 'D' ; $course_remarks ="FAIR";}
                            else if ((round($course_per, 0) >= 50) && (round($course_per, 0) <60))
                                 {$course_grade = 'C' ; $course_remarks ="Good";} 
                             else if ((round($course_per, 0) >= 60) && (round($course_per, 0) <70))
                                 {$course_grade = 'B' ; $course_remarks ="VERY Good";} 
                             else if ((round($course_per, 0) >= 70) && (round($course_per, 0) <80))
                                 {$course_grade = 'A' ; $course_remarks ="EXCELLENT";} 
                            else
                            {$course_grade = 'A-ONE' ; $course_remarks ="DISTINCTION";} 
                        
                                $marks_html .= '<td>'.$std_marks.'</td>';
                                $marks_html .= '<td>'.$course_grade.' </td>';
                                $marks_html .= '<td>'.$course_remarks.'</td>';
                                $c_marks += $stdMarksRow['marks'];
                                
                            }
                            }
                        $c_marks = round($c_marks, 2);    

                        }
                        else{
                        $marks_html .= '<td>'."-".'</td>';}
 }
                 
              


              if ($grade_system == "fail")  
                   {
                    if ($c_marks < $total_course_min_marks) 
                    {

                        $stdFail = 'Fail';
                        $failCounter++;
                        $course_note = "راسب"; //fail
                     }
                    }
                                            
                if ($grade_system == "need_care")
                    {
                        if ($c_marks < ($total_course_marks/2)) {  // yani less than 50perc
                        $stdFail = 'Fail';
                        $failCounter++;
                        $course_note = "يرجى الإهتمام"; //fail
                    }
                        
                    }
                   $total_marks = $total_marks + $total_course_marks;     
                   $student_total_weightage = $student_total_weightage + $c_marks;

                   foreach ($exam_q->result_array() as $k=>$row) {
                        $min_marks = $row['min_marks'];
                        $max_marks = $row['max_marks'];
                   }
                   
                   
                        $c_marks = round($c_marks, 2);    
                         

if( $total_course_marks == 0)
{ $total_course_marks_label = '-';}
else{
    $total_course_marks_label = $total_course_marks;
}   
                                
                   $courses_html .= '<tr>
                        <td>'.$subject_name.'</td>
                        <td>'.$total_course_marks_label.'</td>
                        '.$marks_html.'
                       
                    </tr>';
                    $c_counter++;
            }//courses
        }//course
        if(!empty($total_marks) && $total_marks >0){
           $per = round(($student_total_weightage * 100) / $total_marks, 2); 
       }else{
        $per = round(($student_total_weightage * 100), 2);
       }
        
        
                            if ((round($per, 0) < 33) || ($failCounter > 0))
                                {$grade = 'F'; $remarks ="Try Again";}
                            else if ((round($per, 0) >= 33) && (round($per, 0) < 40))
                                    {$grade = 'E'; $remarks ="SATISFACTORY";}
                            else if ((round($per, 0) >= 40) && (round($per, 0) <50))
                                   {$grade = 'D' ; $remarks ="FAIR";}
                            else if ((round($per, 0) >= 50) && (round($per, 0) <60))
                                 {$grade = 'C' ; $remarks ="Good";} 
                             else if ((round($per, 0) >= 60) && (round($per, 0) <70))
                                 {$grade = 'B' ; $remarks ="VERY Good";} 
                             else if ((round($per, 0) >= 70) && (round($per, 0) <80))
                                 {$grade = 'A' ; $remarks ="EXCELLENT";} 
                            else
                            {$grade = 'A-ONE' ; $remarks ="DISTINCTION";} 
                        
                                
                            
                        $c_marks = round($c_marks, 2); 
                        
                        
                        $courses_html .= '<tr class="courses_total_r">
                        <td>Total</td>
                        <td>'.$total_marks.'</td>
                        <td>'.$student_total_weightage.'</td> 
                        <td>'.$grade.'</td> 
                         <td>'.$remarks.'</td>                      
                    </tr>';
                    
            

        $output .= '<div class="post-container">                
               <div class="post-thumb"><img src='.$logo_url.' style="width: 105px; height: 105px;"></div>
               <div class="post-title">
                 <h4>PAKISTAN EMBASSY INTERNATIONAL SCHOOL & COLLEGE BENGHAZI – LIBYA</h4>
                 <h5 >'.$mark_title.'</h5>
                 <h6>Session: '.$session_name.'</h6>
               </div>
            </div>';                   
        $output .='<div class="namecard">
                    <div class="parent">
                      <div class="left">
                        <p class="title">Name:<u>'.$std_name.'</u></p>
                      </div>
                      
                      <div class="left">
                        <p class="title">NO. OF STUDENTS IN THE CLASS: ______________</p>
                      </div>
                      <div class="right">
                        <p class="title">CLASS: <u>'.$class_name.' '.$section_name.'</u></p>
                      </div>
                      <div class="right">
                        <p class="title">FILE NO:<u> '.$admission_no.'</u></p>
                      </div>
                    </div>
                </div>
                <br>';
        
        $output .='<table>';
        $output .= '<thead>
                    <tr>
                        <tr>
                            <th>SUBJECT</th>
                            <th>MAXIMUM MARKS</th>
                            <th>MARKS OBTAINED</th>
                            <th>Grade</th>
                            <th>Remarks</th>
                        </tr>
                    </tr>
                </thead>';
        $output .= '<tbody>'; 
        $output .= $courses_html;   //yhan courses ane hain
        $output .='</tbody>';

        $output .='</table>'; 
        
        
        $output .= '<div class="finalresultcard">
                    <div class="footer_left">
                        <p class="title">CONDUCT: <u>Good </u></p>
                      </div>
                      <div class="footer_left">
                        <p class="title">PERCENTAGE: <u>'.$per.'</u></p>
                      </div>
                      <div class="footer_left">
                        <p class="title">GRADE: <u>'.$grade.'</u></p>
                      </div>
                      <div class="footer_left">
                        <p class="title">Position: <u>_________</u></p>
                      </div>
                </div><br>';
         $output .= '<div class="finalresultcard">
                    <div class="footer_left">
                        <p >CLASS TEACHER: Ms. Asma Luta</p>
                      </div>
                      <div class="footer_left">
                        <p class="title">DATE: 13-04-2023</p>
                      </div>
                </div>';
          $output .= '<div class="finalresultcard">
                    <div class="footer_left">
                        <p>VERIFIED BY SECTION HEAD: Mr. Shaiman Frazzz </p>
                      </div>
                </div>';
           $output .='<h3>GRADATION</h3>';
           $output .= '<table class="graduation_table">';
           $output .='<tr>
                        <td>A-ONE</td>
                        <td>80% and above marks</td>
                        <td>DISTINCTION</td>
                    </tr>
                     <tr>
                        <td>A</td>
                        <td>70% and above marks</td>
                        <td>EXCELLENT</td>
                    </tr>
                    <tr>
                        <td>B</td>
                        <td>60% and above marks</td>
                        <td>VERY GOOD</td>
                    </tr>
                    <tr>
                        <td>C</td>
                        <td>50% and above marks</td>
                        <td>GOOD</td>
                    </tr>
                    <tr>
                        <td>D</td>
                        <td>40% and above marks</td>
                        <td>FAIR</td>
                    </tr>
                    <tr>
                        <td>E</td>
                        <td>33% and above marks</td>
                        <td>SATISFACTORY</td>
                    </tr>
                    <tr>
                        <td>F</td>
                        <td>32.9% and low marks</td>
                        <td>FAIL</td>
                    </tr>';
           $output .='</table>';                   
            $output .='<div class="signature">
                    <img src='.$signature.'>
                </div>';
         $output .= '<div class="pagebreak"> </div>';      

        }//student
            $data['html_data'] = $output;   
            $data['sch_setting'] = $this->sch_setting_detail;
            $student_exam_page = $this->load->view('admin/exams/_printannualresult', $data, true);
            // echo "<pre>"; print_r($student_exam_page); exit();
            $array = array('status' => '1', 'error' => '', 'page' => $student_exam_page);
            echo json_encode($array); 
        
    }
}

    public function getprintsheettemplate($id,$students){
        echo "<pre>"; print_r($studerts); exit();

    }
}
