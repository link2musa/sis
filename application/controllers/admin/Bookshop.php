<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Bookshop extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->sch_setting_detail = $this->setting_model->getSetting();
    } 

    public function index() {
        if (!$this->rbac->hasPrivilege('bookshop', 'can_view')) {
            access_denied();
        }

        $this->session->set_userdata('top_menu', 'Library');
        $this->session->set_userdata('sub_menu', 'bookshop/index');
        $data['title'] = 'Bookshop';
        $data['title_list'] = 'Members';
        $studentList  = $this->student_model->getStudentsBybookshop();
        $data['studentList'] = $studentList;
        $data['sch_setting'] = $this->sch_setting_detail;
        $this->load->view('layout/header');
        $this->load->view('admin/librarian/bookshop', $data); 
        $this->load->view('layout/footer');
    }

    public function purchase($id) {
        if (!$this->rbac->hasPrivilege('bookshop', 'can_view')) {
            access_denied();
        }

        $this->session->set_userdata('top_menu', 'Library');
        $this->session->set_userdata('sub_menu', 'bookshop/index');
        $data['title'] = 'bookshop'; 
        $data['title_list'] = 'Members'; 
        $memberList = $this->student_model->get($id); 
        $data['memberList'] = $memberList;
         $data['classlist']       = $this->class_model->get();
        $purchase_list = $this->book_model->getMemberBooks($id);
        $data['purchase_list'] = $purchase_list;
        $bookList = $this->book_model->getBookshop();
        $data['bookList'] = $bookList;
         
        $data['sch_setting'] = $this->sch_setting_detail;
        $this->load->view('layout/header');
        $this->load->view('admin/librarian/purchasebook', $data);
        $this->load->view('layout/footer');
    } 
    public function collect_payment(){
        if($this->input->post('grand_total') && $this->input->post('grand_total') >0){

            $grand_total = $this->input->post('grand_total');
            $student_id = $this->input->post('student_id');
            // $studentSession        = $this->student_model->getStudentSession($student_id);
            $session  = $this->setting_model->getCurrentSession();
            $activesessiondata = $this->book_model->getStudentSessionId($student_id,$session);
            
            $due_date = date('Y-m-d');
                    $student_data = array();
                        $student_array = array();
                        $student_array['student_session_id'] = $activesessiondata['id'];
                        $student_array['amount'] = $grand_total;
                        $student_array['is_system'] = 1;
                        $student_array['fee_session_group_id'] = 0;
                        $student_data[] = $student_array;
                    $student_due_fee = $this->book_model->addPreviousBal($student_data, $due_date);
                    $this->book_model->updatepaymentstatus($student_id);
                  redirect('studentfee/addfee/'.$activesessiondata['id']);  

            
        }else{
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    public function getBooksByClass(){
        $class_id = $this->input->post('class_id');
        $listbook           = $this->book_model->getbooksbyclassid($class_id);
        $book_html = ''; 
        if(!empty($listbook)){
            $book_html = '';
            foreach($listbook as $book){
                $book_html .= "<option value='".$book['id']."'>".$book['book_title']."</option>";
            }
        }
        echo $book_html;

    }

   
    public function purchase_book(){

         $data = array(
                'student_id' => $this->input->post('member_id'),
                'book_id' => $this->input->post('book_id'),
                'class_id' => $this->input->post('class_id'),
                'discount' => $this->input->post('discount'),
                'notes' => $this->input->post('notes'),
                'due_return_date' => $this->input->post('due_return_date'),
            );
            $book_id = $this->input->post('book_id');
            // $checkbookstock = $this->book_model->checkBookStock($book_id);
            $inserted_id = $this->book_model->purchase_book($data); 
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('success_message') . '</div>');
            redirect($_SERVER['HTTP_REFERER']);

    }
    public function getAvailQuantity()
    {

        $book_id   = $this->input->post('book_id');
        $available = 0;
        if ($book_id != "") { 
            $result    = $this->book_model->getAvailQuantity($book_id);
            $bookissueresult    = $this->bookissue_model->getAvailQuantity($book_id);
            $available = $result->qty - $result->total_issue;
            if(isset($bookissueresult) && !empty($bookissueresult->total_issue)){
                $available = ($result->qty - $result->total_issue) - $bookissueresult->total_issue;
            }else{
                $available = $result->qty - $result->total_issue;
            }
        }
        $result_final = array('status' => '1', 'qty' => $available,'price' => $result->perunitcost );
        echo json_encode($result_final);
    }


      public function bookreturn() {

        $this->form_validation->set_rules('id', $this->lang->line('id'), 'required|trim|xss_clean');
        $this->form_validation->set_rules('member_id', $this->lang->line('member_id'), 'required|trim|xss_clean');
        $this->form_validation->set_rules('date', $this->lang->line('date'), 'required|trim|xss_clean');
        if ($this->form_validation->run() == false) {
            $data = array(
                'id' => form_error('id'),
                'member_id' => form_error('member_id'),
                'date' => form_error('date'),
            );
            $array = array('status' => 'fail', 'error' => $data);
            echo json_encode($array);
        } else {
            $id = $this->input->post('id');
            $return_note = $this->input->post('return_note');
            $member_id = $this->input->post('member_id');
            $date = date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('date')));
            $data = array(
                'id' => $id,
                'return_date' => $date,
                'return_note' => $return_note,
                'is_returned' => 1,
            );
            $this->book_model->updatebookshop($data);

            $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('success_message'));
            echo json_encode($array);
        }
    }


}
