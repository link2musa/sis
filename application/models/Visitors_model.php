<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class visitors_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->current_session = $this->setting_model->getCurrentSession();
        $this->current_session_name = $this->setting_model->getCurrentSessionName();
        $this->start_month = $this->setting_model->getStartMonth();
    }

    function add($data) {
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
        //=======================Code Start===========================
        $this->db->insert('visitors_book', $data);
        $query = $this->db->insert_id();
        $message = INSERT_RECORD_CONSTANT . " On  visitors  id " . $query;
        $action = "Insert";
        $record_id = $query;
        $this->log($message, $record_id, $action);
        
        //======================Code End==============================

        $this->db->trans_complete(); # Completing transaction
        /* Optional */

        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            return false;
        } else {


            return $record_id;
        }
    }
 function addParentVisitor($data) {
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
        //=======================Code Start===========================
        $this->db->insert('parent_visit', $data);
        $query = $this->db->insert_id();
        $message = INSERT_RECORD_CONSTANT . " On  parent_visit  id " . $query;
        $action = "Insert";
        $record_id = $query;
        $this->log($message, $record_id, $action);
        
        //======================Code End==============================

        $this->db->trans_complete(); # Completing transaction
        /* Optional */

        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            return false;
        } else {


            return $record_id;
        }
    }
    function addAwareness($data) {
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
        //=======================Code Start===========================
        $this->db->insert('social_awareness', $data);
        $query = $this->db->insert_id();
        $message = INSERT_RECORD_CONSTANT . " On  social_awareness  id " . $query;
        $action = "Insert";
        $record_id = $query;
        $this->log($message, $record_id, $action);
        
        //======================Code End==============================

        $this->db->trans_complete(); # Completing transaction
        /* Optional */

        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            return false;
        } else {


            return $record_id;
        }
    }
     function addSpotSituation($data) {
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
        //=======================Code Start===========================
        $this->db->insert('spot_situation', $data);
        $query = $this->db->insert_id();
        $message = INSERT_RECORD_CONSTANT . " On  spot_situation  id " . $query;
        $action = "Insert";
        $record_id = $query;
        $this->log($message, $record_id, $action);
        
        //======================Code End==============================

        $this->db->trans_complete(); # Completing transaction
        /* Optional */

        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            return false;
        } else {


            return $record_id;
        }
    }
    public function getPurpose() {
        $this->db->select('*');
        $this->db->from('visitors_purpose');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function visitors_list($id = null) {
        $this->db->select()->from('visitors_book');
        if ($id != null) {
            $this->db->where('visitors_book.id', $id);
        } else {
            $this->db->order_by('visitors_book.id', 'desc');
        }
        $query = $this->db->get();
        if ($id != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }
    public function parent_visitors_list($id = null) {
        $this->db->select()->from('parent_visit');
        if ($id != null) {
            $this->db->where('parent_visit.id', $id); 
        } else {
            $this->db->order_by('parent_visit.id', 'desc');
        }
        $query = $this->db->get();
        if ($id != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }
      public function social_awarenessors_list($id = null) {
        $this->db->select()->from('social_awareness');
        if ($id != null) {
            $this->db->where('social_awareness.id', $id);
        } else {
            $this->db->order_by('social_awareness.id', 'desc');
        }
        $query = $this->db->get();
        if ($id != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }
    public function getawarenesstypes($id = null) {
        $this->db->select()->from('awareness');
        if ($id != null) {
            $this->db->where('awareness.id', $id);
        } else {
            $this->db->order_by('awareness.id', 'desc');
        }
        $query = $this->db->get();
        if ($id != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }
    public function spot_situation_list($id = null) {
        $this->db->select()->from('spot_situation');
        if ($id != null) {
            $this->db->where('spot_situation.id', $id);
        } else {
            $this->db->order_by('spot_situation.id', 'desc');
        }
        $query = $this->db->get();
        if ($id != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }
    public function delete($id) {
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
        //=======================Code Start===========================
        $this->db->where('id', $id);
        $this->db->delete('visitors_book');
        $message = DELETE_RECORD_CONSTANT . " On  visitors  id " . $id;
        $action = "Delete";
        $record_id = $id;
        $this->log($message, $record_id, $action);
        //======================Code End==============================
        $this->db->trans_complete(); # Completing transaction
        /* Optional */
        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            return false;
        } else {
            //return $return_value;
        }
        $this->session->set_flashdata('msg', '<div class="alert alert-success">' . $this->lang->line('delete_message') . '</div>');
        redirect('admin/visitors');
    }
public function deleteParentVisit($id) {
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
        //=======================Code Start===========================
        $this->db->where('id', $id);
        $this->db->delete('parent_visit');
        $message = DELETE_RECORD_CONSTANT . " On  parent_visit  id " . $id;
        $action = "Delete";
        $record_id = $id;
        //======================Code End==============================
        $this->db->trans_complete(); # Completing transaction
        /* Optional */
        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            return false;
        } else {
            //return $return_value;
        } 
        $this->session->set_flashdata('msg', '<div class="alert alert-success">' . $this->lang->line('delete_message') . '</div>');
        redirect('admin/visitors');
    }
    public function deletesocialawareness($id) {
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
        //=======================Code Start===========================
        $this->db->where('id', $id);
        $this->db->delete('social_awareness');
        $message = DELETE_RECORD_CONSTANT . " On  social_awareness  id " . $id;
        $action = "Delete";
        $record_id = $id;
        //======================Code End==============================
        $this->db->trans_complete(); # Completing transaction
        /* Optional */
        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            return false;
        } else {
            //return $return_value;
        } 
        $this->session->set_flashdata('msg', '<div class="alert alert-success">' . $this->lang->line('delete_message') . '</div>');
        redirect('admin/visitors');
    }
    public function deleteSpotSituation($id) {
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
        //=======================Code Start===========================
        $this->db->where('id', $id);
        $this->db->delete('spot_situation');
        $message = DELETE_RECORD_CONSTANT . " On  spot_situation  id " . $id;
        $action = "Delete";
        $record_id = $id;
        //======================Code End==============================
        $this->db->trans_complete(); # Completing transaction
        /* Optional */
        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            return false;
        } else {
            //return $return_value;
        }
        $this->session->set_flashdata('msg', '<div class="alert alert-success">' . $this->lang->line('delete_message') . '</div>');
        redirect('admin/visitors');
    }
    public function update($id, $data) {
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
        //=======================Code Start===========================
        $this->db->where('id', $id);
        $this->db->update('visitors_book', $data);

        $message = UPDATE_RECORD_CONSTANT . " On  visitors id " . $id;
        $action = "Update";
        $record_id = $id;
        $this->log($message, $record_id, $action);
        //======================Code End==============================
        $this->db->trans_complete(); # Completing transaction
        /* Optional */
        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            return false;
        } else {
            //return $return_value;
        }
        $this->session->set_flashdata('msg', '<div class="alert alert-success">' . $this->lang->line('update_message') . '</div>');
        redirect('admin/visitors');
    }

    public function image_add($visitor_id, $image) {
        $array = array('id' => $visitor_id);
        $this->db->set('image', $image);
        $this->db->where($array);
        $this->db->update('visitors_book');
        $this->session->set_flashdata('msg', '<div class="alert alert-success">' . $this->lang->line('success_message') . '</div>');
    }

    public function image_update($visitor_id, $image) {
        $array = array('id' => $visitor_id);
        $this->db->set('image', $image);
        $this->db->where($array);
        $this->db->update('visitors_book');
        $this->session->set_flashdata('msg', '<div class="alert alert-success">' . $this->lang->line('success_message') . '</div>');
    }

    public function image_delete($id, $img_name) {
        $file = "./uploads/front_office/visitors/" . $img_name;
        unlink($file);
        $this->db->where('id', $id);
        $this->db->delete('visitors_book');
        $controller_name = $this->uri->segment(2);
        $this->session->set_flashdata('msg', '<div class="alert alert-success"> ' . ucfirst($controller_name) . '' . $this->lang->line('success_message') . '</div>');
        redirect('admin/' . $controller_name);
    }
    public function parent_visit_report($start_date,$end_date){ 
         $condition = " and date_format(pv.date,'%Y-%m-%d') between '" . $start_date . "' and '" . $end_date . "'";

        $sql = "SELECT pv.date,pv.relative_relation,pv.reason,s.firstname,s.middlename,s.lastname,c.class FROM `parent_visit` pv LEFT JOIN `classes` c ON c.id = pv.class_id LEFT JOIN students s on s.id=pv.std_id  WHERE 1 " . $condition;
        $query = $this->db->query($sql);
        return $query->result_array();
    }
     public function social_awareness_report($start_date,$end_date){ 
         $condition = " and date_format(sa.date,'%Y-%m-%d') between '" . $start_date . "' and '" . $end_date . "'";

        $sql = "SELECT sa.date,a.awareness,sa.comments,sa.awareness_place,c.class FROM `social_awareness` sa LEFT JOIN `classes` c ON c.id = sa.class_id LEFT JOIN awareness a on a.id=sa.awareness_type  WHERE 1 " . $condition;
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function spot_situation_report($start_date,$end_date){ 
         $condition = " and date_format(pv.date,'%Y-%m-%d') between '" . $start_date . "' and '" . $end_date . "'";

        $sql = "SELECT pv.date,pv.transfering_party,pv.situation,pv.procedure,s.firstname,s.middlename,s.lastname,c.class FROM `spot_situation` pv LEFT JOIN `classes` c ON c.id = pv.class_id LEFT JOIN students s on s.id=pv.std_id  WHERE 1 " . $condition;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}
