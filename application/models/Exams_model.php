<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Exams_model extends CI_Model {
 
    function __construct() {
        parent::__construct();

        $this->table = array(
            'exams' => 'table_exams',
            'courses' => 'courses',
            'faculty' => 'faculty',
            'sub_exams' => 'table_sub_exams',
            'std_group' => 'tbl_std_group',
            'tbl_group' => 'tbl_group',
            'user_group' => 'tbl_user_group',
            'exam_result' => 'table_subexams_std_result',
            'connect_exmas' => 'tbl_sub_connect_exmas',
            'year_semester' => 'year_semester',
            'batches_course' => 'batches_course',
            'student_detail' => 'student_detail',
            'sub_exams_eval' => 'sub_exams_evaluate',
            'std_course_done' => 'tbl_std_course_done',
            'assigned_courses' => 'tbl_std_assignedcourses',
            'std_subexam_results' => 'limu_std_subexams_results',
            'tbl_std_prev_fac_batch_year' => 'tbl_std_prev_fac_batch_year',
        );
    }

    public function SqlExec($sql) {
        return $this->db->query($sql);
    }

    public function get_index_data($where = array(), $or_where = array(), $cols = 's.*') {
        if ($where AND $or_where) {
            $this->db->select($cols)->from($this->table['tbl_std_prev_fac_batch_year'] . ' t');
            $this->db->join($this->table['student_detail'] . ' s', 't.std_id = s.std_id', 'INNER');

            $this->db->where($where);
            $this->db->or_where($or_where);

            $this->db->where('s.std_active', 'Y');
            $this->db->order_by('s.std_id', 'ASC');
            $this->db->group_by('s.std_id');
            return $this->db->get()->result_array();
        }

        return array();
        // return $this->db->last_query();
    }

    public function get_exam($id = 0, $where = '', $cols = '*') {
        if ($id OR $where) {
            $this->db->select($cols)->from($this->table['exams']);

            if ($where)
                $this->db->where($where);

            if ($id)
                return $this->db->where('id', $id)->get()->row_array();

            $this->db->where('is_Active', 'Y');
            // $this->db->order_by('id', 'DESC');
            return $this->db->get()->result_array();
        }

        return array();
    }

    public function get_exam_courses($faculty_id = 0, $semester_id = 0, $batch_id = 0) {
        if ($faculty_id AND $semester_id AND $batch_id) {
            $this->db->select('c.course_id, c.course_name')->from($this->table['courses'] . ' c');
            $this->db->join($this->table['batches_course'] . ' b', 'b.course_id = c.course_id', 'INNER');
            $this->db->where(array('b.faculty_id' => $faculty_id, 'b.batch_id' => $batch_id, 'b.semester_id' => $semester_id));

            $this->db->where('c.isActive', 'Y');
            // $this->db->order_by('id', 'DESC');
            return $this->db->get()->result_array();
        }

        return array();
    }

    public function get_sub_exams($id = 0, $where = '', $cols = '*') {
        $return = array();
        if ($id OR $where) {
            $this->db->select($cols)->from($this->table['sub_exams']);

            if ($where)
                $this->db->where($where);

            if ($id)
                return $this->db->where('sub_exams_id', $id)->get()->row_array();

            return $this->db->get()->result_array();
        }

        return $return;
    }

    public function get_connect_subexam($exam_id = 0, $course_id = 0) {
        if ($exam_id AND $course_id) {
            $result = $this->db->query("SELECT t.id, s.sub_exam_name, t.sub_exam_id, t.weightage FROM tbl_sub_connect_exmas t INNER JOIN table_sub_exams s ON (t.sub_exam_id = s.sub_exams_id) and t.exam_id = '$exam_id' and t.course_id = '$course_id'");

            if ($results->num_rows() > 0) {
                $row = $results->result_array();

                $c_sub_exam_ids = array();
                foreach ($results->result_array() as $row) {
                    $c_sub_exam_ids[] = $row['sub_exam_id'];
                    $html .= ' <tr>
                                <td class="center" >' . $row['sub_exam_name'] . '</td>
                                <td class="center" >
                                        <input type="hidden" name="sub_connect_id[]" id="sub_connect_id[]" value="' . $row['id'] . '" />
                                        <input type="hidden" name="sub_exams_id[]" value="' . $row['sub_exam_id'] . '" />
                                        <input type="text" name="weightage[]"  value="' . $row['weightage'] . '" />
                                </td>
                </tr>';
                }
                if (count($c_sub_exam_ids) > 0) {
                    $notInIds = implode(",", $c_sub_exam_ids);
                    $results = $this->Exams_model->getsubexams($id, $sub_exam_admin, $notInIds);
                    foreach ($results->result_array() as $row) {

                        $html .= ' <tr>
                            <td class="center" >' . $row['sub_exam_name'] . '</td>
                            <td class="center" >
                                <input type="hidden" name="sub_connect_id[]" id="sub_connect_id[]" value="0" />
                                <input type="text" name="weightage[]" value="0" />
                                <input type="hidden" name="sub_exams_id[]" value="' . $row['sub_exams_id'] . '" />
                             </td>
                        </tr>';
                    }
                }

                $html .= '<tr class="stdform">
                                     <td class="right"></td>
                                     <td class="left">
                                      <input type="button"  value="Update Connect Exams" name="submit" onclick="update_me();" />
                                      <input type="reset" value="Reset" name="reset" /></td>
                                    </td>
                                </tr>';
            } else {

                $results = $this->Exams_model->getsubexams($id, $sub_exam_admin);

                if ($results->num_rows() > 0) {
                    foreach ($results->result_array() as $row) {

                        $html .= ' <tr>
                            <td class="center" >' . $row['sub_exam_name'] . '</td>
                            <td class="center" >
                                <input type="text" name="sub_exams[]" value="0" />
                                <input type="hidden" name="sub_exams_id[]" value="' . $row['sub_exams_id'] . '" />
                             </td>
                        </tr>';
                    }

                    $html .= '<tr class="stdform">
                                     <td class="right"></td>
                                     <td class="left">
                                      <input type="button" value="Save Connect Exams" name="submit" onclick="click_me();" />
                                      <input type="reset" value="Reset" name="reset" /></td>
                                    </td>
                            </tr>';
                } else {
                    $html .= '<tr> <td colspan="2"> No Record Found </td> </tr>';
                }
            }
        }

        return array();
    }

    public function get_sub_exam_eval($id = 0, $where = '', $cols = '*') {
        $return = array();
        if ($id OR $where) {
            $this->db->select($cols)->from($this->table['sub_exams_eval']);

            if ($where)
                $this->db->where($where);

            if ($id)
                return $this->db->where('id', $id)->get()->row_array();

            return $this->db->get()->result_array();
        }

        return $return;
    }

    public function groupadmin($fid = 0, $sid = 0, $bid = 0, $courseid = 0, $attr = '', $selected = 0, $call_for = '') {
        $sub_exam_admin = getSubExamsAdmin($fid);

        if ($sub_exam_admin == 1 || $sub_exam_admin == 2 || $sub_exam_admin == 4 || $sub_exam_admin == 17)
            $sql = "SELECT * FROM " . $this->table['tbl_group'];
        else
            $sql = "SELECT g.*, ug.id FROM " . $this->table['user_group'] . " ug INNER JOIN " . $this->table['tbl_group'] . " g ON (ug.group_id = g.group_id) and ug.faculty_id = '$fid' and ug.batch_id= '$bid' and ug.semester_id = '$sid' and ug.course_id = '$courseid' and ug.group_admin_id = '" . $_SESSION['AdminId'] . "'";

        $output = '';
        $query = $this->SqlExec($sql);

        if ($call_for != 'new')
            $output .= "<select id='group_id' name='group_id' $attr >";

        $output .= "<option value=\"\">Select Group</option>";

        foreach ($query->result() as $o) {
            if ($o->group_id == $selected)
                $output .= "<option selected=\"selected\" value=\"" . $o->group_id . "\" >" . $o->group_name . "</option>";
            else
                $output .= "<option value=\"" . $o->group_id . "\" >" . $o->group_name . "</option>";
        }

        if ($call_for != 'new')
            $output .= '</select>';

        return $output;
    }

    public function stdSubExamsResultsGroup($exid = 0, $cid = 0, $eid = 0, $gid = 1, $fid = 0, $bid = 0, $yid = 0) {
        if ($gid == 1)
            $where = '';
        else
            $where = "INNER JOIN " . $this->table['std_group'] . " g ON (g.std_id = r.std_id and g.group_id = '$gid' AND g.faculty_id = '$fid' AND g.batch_id = '$bid' AND g.semester_id = '$yid' AND g.course_id = '$cid')";

        // $sql = "SELECT table_subexams_std_result.*, s.std_fname FROM ".$this->table['exam_result']." r $where INNER JOIN ".$this->table['student_detail']." s ON (s.std_id = r.std_id and s.std_active = 'Y') WHERE r.exam_id = '$exid' AND r.course_id = '$cid' AND r.sub_exam_eval_id = '$eid' ORDER BY s.std_id ASC";

        $query = "SELECT r.*, s.std_id, s.std_fname FROM " . $this->table['exam_result'] . " r $where INNER JOIN " . $this->table['student_detail'] . " s ON (s.std_id = r.std_id and s.std_active = 'Y') WHERE r.exam_id = '$exid' AND r.course_id = '$cid' AND r.sub_exam_eval_id = '$eid' AND NOT EXISTS (SELECT std_id FROM " . $this->table['std_course_done'] . " cd WHERE cd.fac_id = '$fid' AND cd.course_id = '$cid' AND cd.year_id = '$yid' AND cd.batch_id != '$bid' AND cd.std_id = s.std_id) ORDER BY s.std_id ASC";
        // SELECT r.*, s.std_id, s.std_fname, SUM(CASE WHEN cd.fac_id = '1' AND cd.course_id = '10' AND cd.year_id = '2' AND cd.batch_id != 41 AND cd.std_id = s.std_id THEN 1 else 0 END) as course_done FROM table_subexams_std_result r INNER JOIN student_detail s ON (s.std_id = r.std_id and s.std_active = 'Y') LEFT JOIN tbl_std_course_done cd ON (cd.std_id = s.std_id) WHERE r.exam_id = '1413' AND r.course_id = '10' AND r.sub_exam_eval_id = '4628' ORDER BY s.std_id ASC

        return $this->raw_query($query);
    }

    public function counttotal($tbl, $where) {
        if ($where != '')
            $sql = "SELECT * FROM " . $this->db->dbprefix . $tbl . " WHERE " . $where . " ";
        else
            $sql = "SELECT * FROM " . $this->db->dbprefix . $tbl . " ";

        $result = $this->SqlExec($sql);

        return $result->num_rows();
    }

    public function sub_exam_eval_ids($sub_exam_id = 0) {
        if ($sub_exam_id) {
            $this->db->select('id')->from($this->table['sub_exams_eval'])->where('sub_exams_id', $sub_exam_id);
            return $this->db->get()->result_array();
        }

        return array();
    }

    public function get_assigned_students($where = '', $cols = 'a.*') {
        if ($where) {
            $this->db->select($cols . ', s.std_fname')->from($this->table['assigned_courses'] . ' a');
            $this->db->join($this->table['student_detail'] . ' s', 's.std_id = a.std_id', 'LEFT')->where($where);
            /* return */ $this->db->get()->result_array();
            return $this->db->last_query();
        }

        return array();
    }

    public function update_subexam_wise_result($exam_id = 0) {
        $tbl_exam = $this->table['exams'];
        $tbl_sub_exam = $this->table['sub_exams'];
        $tbl_exam_rst = $this->table['exam_result'];
        $tbl_connect_exmas = $this->table['connect_exmas'];
        $tbl_sub_exam_eval = $this->table['sub_exams_eval'];
        $tbl_calculated_result = $this->table['std_subexam_results'];

        $exam = $this->get_exam($exam_id);

        if ($exam AND $exam_id) {
            $chk = FALSE;

            ////////// New way of getting students told by Shaz
            // $where = array(
            //  'a.batch_id'    => $exam['batch_id'],
            //  'a.isActive'    => 'Y',
            //  'a.isregular'   => 'Reg',
            //  'a.course_id'   => $exam['course_id'],
            //  'a.faculty_id'  => $exam['faculty_id'],
            //  'a.semester_id' => $exam['year_id'],
            // );
            // $students    = $this->get_assigned_students($where, 'a.std_id');
            ////////// Old way of geeting students
            if (isAllowed2(array(16)))
                $whereSupervisor = "and s.sup_id=" . $_SESSION['AdminId'];
            else
                $whereSupervisor = "";

            $std_prev_tbl = $this->table['tbl_std_prev_fac_batch_year'];
            $student_detail_tbl = $this->table['student_detail'];
            $std_course_done_tbl = $this->table['std_course_done'];

            $year_id = $exam['year_id'];
            $batch_id = $exam['batch_id'];
            $course_id = $exam['course_id'];
            $faculty_id = $exam['faculty_id'];

            $sql = "SELECT id FROM $std_prev_tbl WHERE faculty_id = '$faculty_id' AND batch_id = '$batch_id' AND year_id = '$year_id' LIMIT 1";
            $res = $this->SqlExec($sql);

            if ($res->num_rows() > 0)
                $sql = "SELECT s.std_id, s.std_fname FROM $std_prev_tbl p INNER JOIN $student_detail_tbl s ON (p.std_id = s.std_id) AND (p.faculty_id = '$faculty_id' AND p.batch_id = '$batch_id' AND p.year_id = '$year_id') OR (s.std_faculty_id = '$faculty_id' AND s.std_batch_id = '$batch_id' AND s.std_semester_id = '$year_id') WHERE s.std_active = 'Y' AND NOT EXISTS (SELECT std_id FROM $std_course_done_tbl cd WHERE cd.fac_id = '$faculty_id' AND cd.course_id = '$course_id' AND cd.year_id = '$year_id' AND cd.batch_id != '$batch_id' AND cd.std_id = s.std_id) $whereSupervisor GROUP BY s.std_id";
            else
                $sql = "SELECT s.std_id, s.std_fname FROM $student_detail_tbl s WHERE (s.std_faculty_id = '$faculty_id' AND s.std_batch_id = '$batch_id' AND s.std_semester_id = '$year_id') AND NOT EXISTS (SELECT cd.std_id FROM $std_course_done_tbl cd WHERE cd.fac_id = '$faculty_id' AND cd.course_id = '$course_id' AND cd.year_id = '$year_id' AND cd.batch_id != '$batch_id' AND cd.std_id = s.std_id) AND s.std_active = 'Y' $whereSupervisor GROUP BY s.std_id";
            $students = $this->raw_query($sql);

            if ($students)
                $query = "INSERT INTO $tbl_calculated_result (`std_id`, `exam_id`, `sub_exam_id`, `marks`, `total_weightage`, `ea`, `pass`, `absence`, `warning_message`, `isActive`) VALUES ";

            if ($exam['sub_exams'] == 'Y') {
                $sub_exams = $this->get_sub_exams(0, array('exam_id' => $exam_id), 'sub_exams_id, absent_warn, fail_absent');

                foreach ($sub_exams as $key => $value):

                    $sub_exam_id = $value['sub_exams_id'];
                    $sub_exam_eval_ids = $this->sub_exam_eval_ids($sub_exam_id);
                    $sub_exam_eval_ids = ($sub_exam_eval_ids) ? implode(',', array_column($sub_exam_eval_ids, 'id')) : '0';

                    foreach ($students as $k => $val):

                        $q = "SELECT COUNT(r.id) as total_problems, SUM(r.marks) as marks, ROUND((SUM(r.marks) * (SELECT weightage FROM $tbl_connect_exmas WHERE exam_id = $exam_id AND sub_exam_id = $sub_exam_id AND course_id = $course_id LIMIT 1) / (SELECT SUM(max_marks) FROM $tbl_sub_exam_eval WHERE sub_exams_id = $sub_exam_id)), 2) as student_weightage, SUM(CASE WHEN (r.absent = 'Y') THEN 1 ELSE 0 END) as absence FROM $tbl_exam_rst r WHERE r.exam_id = $exam_id AND r.sub_exam_id = $sub_exam_id AND r.course_id = $course_id AND r.isActive = 'Y' AND r.std_id = " . $val['std_id']; // (CASE WHEN absence > 0 AND absence >= ROUND((total_problems * 25 ) / 100.0) THEN 'Warning3' WHEN absence > 0 AND absence >= ROUND((total_problems * 20 ) / 100.0) THEN 'Warning2' WHEN absence > 0 AND absence >= ROUND((total_problems * 10 ) / 100.0) THEN 'Warning1' ELSE '' END) as warning
                        $result = $this->raw_query($q);

                        if ($result) {
                            $params = array(
                                'pass' => 'Y',
                                'marks' => $result[0]['marks'],
                                'std_id' => $val['std_id'],
                                'exam_id' => $exam_id,
                                'absence' => $result[0]['absence'],
                                'sub_exam_id' => $sub_exam_id,
                                'warning_message' => '',
                                'total_weightage' => $result[0]['student_weightage'],
                            );

                            if ($value['fail_absent'] == 'Y' AND $result[0]['absence'] > 0) {
                                $params['pass'] = 'N';
                            } else if ($value['absent_warn'] == 'Y') {
                                $warnings = $this->Reports_model->getAbsentWarning($result[0]['total_problems']);

                                if ($result[0]['absence'] > 0 AND $result[0]['absence'] >= $warnings['warning3']) {
                                    $params['pass'] = 'N';
                                    $params['warning_message'] = 'Warning3';
                                } else if ($result[0]['absence'] > 0 AND $result[0]['absence'] >= $warnings['warning2'])
                                    $params['warning_message'] = 'Warning2';
                                else if ($result[0]['absence'] > 0 AND $result[0]['absence'] >= $warnings['warning1'])
                                    $params['warning_message'] = 'Warning1';
                            }

                            $chk = TRUE;
                            $query .= "(" . intval($params['std_id']) . ", " . intval($params['exam_id']) . ", " . intval($params['sub_exam_id']) . ", " . $params['marks'] . ", " . $params['total_weightage'] . ", 'N', '" . $params['pass'] . "', " . intval($params['absence']) . ", '" . $params['warning_message'] . "', 'Y'),";
                        }

                    endforeach;

                endforeach;

                if ($query AND $chk) {
                    $query = rtrim($query, ", ") . ' ON DUPLICATE KEY UPDATE pass = VALUES(pass), marks = VALUES(marks), absence = VALUES(absence), warning_message = VALUES(warning_message), total_weightage = VALUES(total_weightage)';

                    $this->db->query($query);
                }

                // print_r($query); exit();
            } else {
                foreach ($students as $k => $val):

                    $q = "SELECT marks FROM $tbl_exam_rst WHERE exam_id = $exam_id AND course_id = $course_id AND isActive = 'Y' AND std_id = " . $val['std_id'];
                    $result = $this->raw_query($q);

                    if ($result) {
                        $params = array(
                            'pass' => 'Y',
                            'marks' => $result[0]['marks'],
                            'std_id' => $val['std_id'],
                            'exam_id' => $exam_id,
                        );

                        if ($exam['min_marks'] != 'No' AND $exam['min_marks'] > $params['marks'])
                            $params['pass'] = 'N';

                        $chk = TRUE;
                        $query .= "(" . intval($params['std_id']) . ", " . intval($params['exam_id']) . ", NULL, " . $params['marks'] . ", " . $params['marks'] . ", '" . $params['pass'] . "', 0, '', 'Y'),";
                    }

                endforeach;

                if ($query AND $chk) {
                    $query = rtrim($query, ", ") . ' ON DUPLICATE KEY UPDATE pass = VALUES(pass), marks = VALUES(marks)';

                    $this->db->query($query);
                }
            }

            return 1;
        }

        return 0;
    }

    public function raw_query($query = '') {
        if ($query)
            return $this->db->query($query)->result_array();

        return array();
    }

    public function insert($table = '', $data = '', $batch_op = FALSE) {
        if ($table AND $data) {
            if ($batch_op)
                $this->db->insert_batch($table, $data);
            else
                $this->db->insert($table, $data);

            return $this->db->insert_id();
        }

        return 0;
        // return $this->db->set($data)->get_compiled_insert($table);
    }

    public function update($table = '', $data = '', $where = '') {
        if ($table AND $data AND $where) {
            $this->db->where($where)->update($table, $data);
            return $this->db->affected_rows();
        }

        return 0;
    }

    public function delete($table = '', $where = '') {
        if ($table AND $where)
            return $this->db->where($where)->delete($table);

        return 0;
    }

    // function insert($tblname,$data)
    // {
    //  $this->db->insert($tblname,$data);
    //  return $this->db->insert_id();
    // }
    // function update($tblname,$data,$where)
    // { 
    //  $this->db->update($tblname,$data,$where);
    //  return true;
    // }

    function bulk_update($tblname, $data, $whereData, $whereField) {
        foreach ($whereData as $whereKey => $where) {
            $this->db->where($whereKey, $where);
        }
        $this->db->update_batch($tblname, $data, $whereField);
        return true;
    }

#End of function

    function getwhere_num($tbl, $fld, $val) {
        $this->db->select('*');
        $this->db->from($tbl);
        $result = $this->db->get();
        return $result->num_rows();
    }

#End of function

    function yearCourses($fid = 0, $sid = 0, $bid = 0) {
        $sql = "SELECT
            courses.*, batches_course.id
            FROM
            batches_course
            INNER JOIN courses 
                ON (batches_course.course_id = courses.course_id)
                and batches_course.faculty_id = '$fid' and batches_course.batch_id= '$bid'
                and batches_course.semester_id = '$sid' ";
        $query = $this->SqlExec($sql);
        $arr = array();
        $size = $query->num_rows();
        $i = 1;
        $q = '';
        foreach ($query->result() as $o) {
            if ($i == 1)
                $q .= "course_id='" . $o->course_id . "'";
            else
                $q .= "or course_id='" . $o->course_id . "'";
            $i++;
        }
        return $q;
    }

    function blockCourses($fid = 0, $sid = 0, $bid = 0) {
        $sql = "SELECT
            courses.*, batches_course.id
            FROM
            batches_course
            INNER JOIN courses 
                ON (batches_course.course_id = courses.course_id)
                and batches_course.faculty_id = '$fid' and batches_course.batch_id= '$bid'
                and batches_course.semester_id = '$sid' and (courses.type='block' or courses.type='gs') ";

        $query = $this->SqlExec($sql);
        $arr = array();
        $size = $query->num_rows();
        $i = 1;
        $q = '';
        foreach ($query->result() as $o) {
            if ($i == 1)
                $q .= "course_id='" . $o->course_id . "'";
            else
                $q .= "or course_id='" . $o->course_id . "'";
            $i++;
        }
        return $q;
    }

    function examRegCourses($fid = 0, $sid = 0, $bid = 0, $attr = '', $selected = 0) {
        $sql = "SELECT courses.*, batches_course.id FROM batches_course INNER JOIN courses ON (batches_course.course_id = courses.course_id) and batches_course.faculty_id = '$fid' and batches_course.batch_id= '$bid' and batches_course.semester_id = '$sid' and batches_course.status='reg'";

        $output = '';
        $query = $this->SqlExec($sql);

        $output .= "<select id='courese_id' name='courese_id' $attr >";

        $output .= "<option value=\"\">Select Course</option>";
        //pre($query->result());
        foreach ($query->result() as $o) {

            if ($o->course_id == $selected)
                $output .= "<option selected=\"selected\" value=\"" . $o->course_id . "\" >" . $o->course_name . "</option>";
            else
                $output .= "<option value=\"" . $o->course_id . "\" >" . $o->course_name . "</option>";
        }
        $output .= '</select>';
        return $output;
    }

#End of function

    function examCourses($fid = 0, $sid = 0, $bid = 0, $attr = '', $selected = 0) {
        $sql = "SELECT
            courses.*, batches_course.id
            FROM
            batches_course
            INNER JOIN courses 
                ON (batches_course.course_id = courses.course_id)
                and batches_course.faculty_id = '$fid' and batches_course.batch_id= '$bid'
                and batches_course.semester_id = '$sid' ";

        $output = '';
        $query = $this->SqlExec($sql);

        $output .= "<select id='courese_id' name='courese_id' $attr >";

        $output .= "<option value=\"\">Select Course</option>";
        //pre($query->result());
        foreach ($query->result() as $o) {

            if ($o->course_id == $selected)
                $output .= "<option selected=\"selected\" value=\"" . $o->course_id . "\" >" . $o->course_name . "</option>";
            else
                $output .= "<option value=\"" . $o->course_id . "\" >" . $o->course_name . "</option>";
        }
        $output .= '</select>';
        return $output;
    }

#End of function

    function examLockCourses($fid = 0, $sid = 0, $bid = 0, $attr = '', $selected = 0) {
        $sql = "SELECT
            courses.*, batches_course.id, batches_course.is_lock
            FROM
            batches_course
            INNER JOIN courses 
                ON (batches_course.course_id = courses.course_id)
                and batches_course.faculty_id = '$fid' and batches_course.batch_id= '$bid'
                and batches_course.semester_id = '$sid' 
                and batches_course.is_lock = 'N' ";

        $output = '';
        $query = $this->SqlExec($sql);

        $output .= "<select id='courese_id' name='courese_id' $attr >";

        $output .= "<option value=\"\">Select Course</option>";
        //pre($query->result());
        foreach ($query->result() as $o) {

            if ($o->course_id == $selected)
                $output .= "<option selected=\"selected\" value=\"" . $o->course_id . "\" >" . $o->course_name . "</option>";
            else
                $output .= "<option value=\"" . $o->course_id . "\" >" . $o->course_name . "</option>";
        }
        $output .= '</select>';
        return $output;
    }

#End of function

    function bulk_insert($tblname, $data) {
        $this->db->insert_batch($tblname, $data);
        return true;
    }

#End of function

    function getwhere($tbl, $fld, $val) {
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->where($fld, $val);
        $result = $this->db->get();
        return $result;
    }

#End of function

    function getexamdetail($id) {
        $sql = "SELECT * FROM table_exams WHERE id=" . $id . " ";
        $query = $this->SqlExec($sql);
        $row = $query->row_array();
        return $row;
    }

    function getattenddetail($id) {
        $sql = "SELECT * FROM tbl_attend WHERE id=" . $id . " ";
        $query = $this->SqlExec($sql);
        $row = $query->row_array();
        return $row;
    }

    function getsubexams($id, $sub_exam_admin, $notInIds = '') {
        $role = getUserRole();
        $where = "";

        if ($role[0] != 1 && $sub_exam_admin != 2 && $sub_exam_admin != 4 && $sub_exam_admin != 1) // allow year coordinator, EEC, All to see all subexam
            $where .= "sub_exam_admin = '$sub_exam_admin' and ";

        if ($notInIds != '') {
            $where .= "sub_exams_id NOT IN( " . $notInIds . ") and ";
        }

        $sql = "SELECT * from table_sub_exams where  $where exam_id= '$id'";
        $row = $this->SqlExec($sql);

        return $row;
    }

    function getsubexamdetail($id) {
        $sql = "SELECT * from table_sub_exams where sub_exams_id= '$id'";
        $query = $this->SqlExec($sql);
        $row = $query->row_array();
        return $row;
    }

    function getsubexamevaldetail($id) {
        $sql = "SELECT * from sub_exams_evaluate where id= '$id'";
        $query = $this->SqlExec($sql);
        $row = $query->row_array();
        return $row;
    }

    function GetDetail($tbl, $where) {
        if ($where != '')
            $sql = "SELECT * FROM " . $this->db->dbprefix . $tbl . " WHERE " . $where . " ";
        else
            $sql = "SELECT * FROM " . $this->db->dbprefix . $tbl . " ";

        $result = $this->SqlExec($sql);
        return $result;
    }

    function fetch_main_exams($id) {
        $sql = "SELECT* from table_exams where id = '$id'";
        $result = $this->SqlExec($sql);
        return $result;
    }

    function fetch_sub_exams($id) {
        $sql = "SELECT* from table_sub_exams where sub_exams_id = '$id'";
        $result = $this->SqlExec($sql);
        return $result;
    }

    function stdResults($fld1 = '', $id1 = 0, $fld2 = '', $id2 = 0) {
        $this->db->select('table_std_result.*,student_detail.std_fname');
        $this->db->from('table_std_result');
        $this->db->join('student_detail', 'student_detail.std_id = table_std_result.std_id and student_detail.std_active= "Y"');
        $this->db->order_by("student_detail.std_id", "ASC");
        $this->db->where($fld1, $id1);
        $this->db->where($fld2, $id2);
        $result = $this->db->get();
        return $result;
    }
//
    function stdResultsGroup($exid = 0, $cid = 0, $gid = 0, $fid = 0, $bid = 0, $yid = 0) {
        if ($gid == '') { //for group None/All
            $where = '';
        } else {
            $where = "and students.section_id='$gid'";
        }
        $sql = "SELECT 
            table_std_result.*, students.firstname,students.middlename,students.lastname,students.section_id , students.admission_no
            FROM 
            table_std_result
            
                INNER JOIN students
              ON  (students.id = table_std_result.std_id and students.status= 'Regular'   $where)
                 WHERE table_std_result.exam_id = '$exid' AND table_std_result.course_id = '$cid' GROUP BY students.id
                 ORDER BY students.id ASC";

        $result = $this->SqlExec($sql);
        return $result;
    }

    function stdAttendGroup($exid = 0, $cid = 0, $gid = 0, $fid = 0, $bid = 0, $yid = 0) {
        if ($gid == 1) { //for group None/All
            $where = '';
        } else {
            $where = "INNER JOIN tbl_std_group
                ON (tbl_std_group.std_id = table_std_attend.std_id and tbl_std_group.group_id='$gid'  AND tbl_std_group.faculty_id='$fid'  AND tbl_std_group.batch_id='$bid' AND tbl_std_group.semester_id='$yid'    AND tbl_std_group.course_id='$cid')";
        }
        $sql = "SELECT 
            table_std_attend.*, student_detail.std_fname 
            FROM 
            table_std_attend
              $where
                INNER JOIN student_detail
              ON  (student_detail.std_id = table_std_attend.std_id and student_detail.std_active= 'Y')
                 WHERE table_std_attend.attend_id = '$exid' AND table_std_attend.course_id = '$cid' 
                 ORDER BY student_detail.std_id ASC";
        $result = $this->SqlExec($sql);
        return $result;
    }

    function stdSubExamsResults($fld1 = '', $id1 = 0, $fld2 = '', $id2 = 0, $fld3 = '', $id3 = 0) {
        $this->db->select('table_subexams_std_result.*,student_detail.std_fname');
        $this->db->from('table_subexams_std_result');
        $this->db->join('student_detail', 'student_detail.std_id = table_subexams_std_result.std_id and student_detail.std_active= "Y"');
        $this->db->order_by("student_detail.std_id", "ASC");
        $this->db->where($fld1, $id1);
        $this->db->where($fld2, $id2);
        $this->db->where($fld3, $id3);
        $result = $this->db->get();
        return $result;
    }

    function subExamsEvalMarks($id) {
        $sql = "select sum(max_marks) as marks from sub_exams_evaluate where sub_exams_id='$id';";
        $result = $this->SqlExec($sql);
        $row = $result->row_array();

        return $row['marks'];
    }

    function isSubExam($id) {
        $sql = "select sub_exams from table_exams where id = '$id'";
        $result = $this->SqlExec($sql);
        $row = $result->row_array();
        return $row['sub_exams'];
    }

    function getMaxWeightage($id) {
        $sql = "select max_weightage from table_exams where id = '$id'";
        $result = $this->SqlExec($sql);
        $row = $result->row_array();
        return $row['max_weightage'];
    }

    function getCourseMarks($fid, $bid, $yid, $cid) {
        $sql = "select max_weightage, max_marks, sub_exams from table_exams where faculty_id = '$fid' and batch_id='$bid' and year_id = '$yid' and course_id = '$cid'";
        $result = $this->SqlExec($sql);

        $marks = 0;

        foreach ($result->result() as $row) {

            if ($row->sub_exams == 'Y') {
                $marks = $marks + $row->max_weightage;
            } else {
                $marks = $marks + $row->max_marks;
            }
        }
        return $marks;
    }

    function getExamMarks($cid, $std_id) {
        $marks = 0;
        $sql = "SELECT sum(marks) as marks FROM table_std_result where course_id = '$cid' and std_id='$std_id';";
        $result = $this->SqlExec($sql);
        $row = $result->result_array();
        foreach ($result->result_array() as $row) {
            $marks = $marks + $row['marks'];
        }
        return $marks;
    }

    function getSubExamMarks($cid, $std_id) {
        $marks = 0;
        $sql = "SELECT table_subexams_std_result.*,sum(marks) as marks FROM table_subexams_std_result where exam_id = '" . $exam_ids[$j] . "' and sub_exam_id='" . $rowz['sub_exams_id'] . "' and std_id='" . $row['std_id'] . "' and course_id='" . $course_id . "' and isActive='Y';";
        $stdMarks = $this->reports_model->SqlExec($sql);
        foreach ($stdMarks->result_array() as $stdMarksRow) {
            if ($stdMarksRow['sub_exam_id'] != '') {
                $total_weightage = $this->reports_model->subExamsWeightage($exam_ids[$j], $stdMarksRow['sub_exam_id'], $course_id);
                $total_max = $this->reports_model->getTotalMarksBySubExamId($stdMarksRow['sub_exam_id']);
                $student_weightage = ((float) $stdMarksRow['marks'] * (float) $total_weightage ) / (float) $total_max;
                $student_weightage = round($student_weightage, 2);
            }
        }

        return $marks;
    }

    function getStdGraceMarks($fid, $bid, $yid, $cid, $std_id) {

        $sql = "Select grace_marks from tbl_gracemarks where faculty_id = '$fid' and batch_id = '$bid' and year_id='$yid' and course_id= '$cid' and std_id = '$std_id'";
        $result = $this->SqlExec($sql);
        $grace_marks = 0;
        if ($result->num_rows() > 0) {
            $row = $result->row_array();
            $grace_marks = $row['grace_marks'];
        }
        return $grace_marks;
    }

    function isStdResetCourse($fid, $bid, $yid, $cid, $std_id) {
        $sql = "Select * from tbl_reset_course where faculty_id = '$fid' and batch_id = '$bid' and year_id='$yid' and course_id= '$cid' and std_id = '$std_id'";

        $result = $this->SqlExec($sql);
        $desc = '';
        $row = array();
        if ($result->num_rows() > 0) {
            $row = $result->row_array();
        }
        return $row;
    }

    function resetExamData($fid, $bid, $yid, $cid) {
        $this->load->model('admin/reports_model');
        $sql = "SELECT
                                student_detail.*
                                    FROM
                                      tbl_std_prev_fac_batch_year
                                        INNER JOIN student_detail 
                                            ON (tbl_std_prev_fac_batch_year.std_id = student_detail.std_id)
                                            and     tbl_std_prev_fac_batch_year.faculty_id = '$fid'
                                        and tbl_std_prev_fac_batch_year.batch_id = '$bid'
                                        and tbl_std_prev_fac_batch_year.year_id = '$yid'
                                        
                                        or (student_detail.std_faculty_id = '$fid'
                                    and student_detail.std_batch_id = '$bid'
                                    and student_detail.std_semester_id='$yid')
                                    group by student_detail.std_id order by student_detail.std_id";

        $sql = "SELECT *from tbl_std_prev_fac_batch_year where faculty_id = '$fid'
                                        and batch_id = '$bid'
                                        and year_id = '$yid'";
        $res = $this->reports_model->SqlExec($sql);
        if ($res->num_rows() > 0) {
            $sql = "SELECT
                        student_detail.*
                            FROM
                              tbl_std_prev_fac_batch_year
                                INNER JOIN student_detail 
                                    ON (tbl_std_prev_fac_batch_year.std_id = student_detail.std_id)
                                    and
                                    (
                                        tbl_std_prev_fac_batch_year.faculty_id = '$fid'
                                        and tbl_std_prev_fac_batch_year.batch_id = '$bid'
                                        and tbl_std_prev_fac_batch_year.year_id = '$yid'
                                    )
                                    or 
                                    (   student_detail.std_faculty_id = '$fid'
                                        and student_detail.std_batch_id = '$bid'
                                        and student_detail.std_semester_id='$yid'
                                    )
                                group by student_detail.std_id";
        } else {
            $sql = "SELECT
                        *
                            FROM
                              student_detail
                                where  (student_detail.std_faculty_id = '$fid'
                            and student_detail.std_batch_id = '$bid'
                            and student_detail.std_semester_id='$yid')
                            group by student_detail.std_id
                    ;";
        }

        $result = $this->SqlExec($sql);

        $html = '';
        $courseType = getFieldValueById('courses', 'type', 'course_id', $cid);
        $total_e_marks = 0;
        $i = 0;
        foreach ($result->result_array() as $std_row) {
            ++$i;
            $std_id = $std_row['std_id'];
            $isReset = $this->reports_model->isStdResetMarks($fid, $bid, $yid, $std_id, $cid);

            $exmas_results = $this->reports_model->GetDetail('table_exams', "faculty_id = '$fid' and batch_id='$bid' and year_id='$yid' and course_id='$cid'");
            $total_marks = 0;
            if ($exmas_results->num_rows() > 0) {
                $student_total_weightage = 0;
                foreach ($exmas_results->result_array() as $row) {
                    $c_marks = 0;
                    if ($row['sub_exams'] == 'Y') {
                        $course_marks = $row['max_weightage'];
                        $min_marks = $row['min_weightage'];
                        $subexmas_results = $this->reports_model->GetDetail('table_sub_exams', "exam_id= '" . $row['id'] . "'");
                        foreach ($subexmas_results->result_array() as $rowz) {

                            $sql = "SELECT table_subexams_std_result.*,sum(marks) as marks FROM table_subexams_std_result where exam_id = '" . $row['id'] . "' and sub_exam_id='" . $rowz['sub_exams_id'] . "' and std_id='" . $std_id . "' and course_id='" . $cid . "' and isActive='Y';";

                            $stdMarks = $this->reports_model->SqlExec($sql);
                            $student_weightage = 0;
                            if ($stdMarks->num_rows() > 0) {
                                foreach ($stdMarks->result_array() as $stdMarksRow) {
                                    if ($stdMarksRow['sub_exam_id'] != '') {
                                        $total_weightage = $this->reports_model->subExamsWeightage($row['id'], $stdMarksRow['sub_exam_id'], $cid);
                                        $total_max = $this->reports_model->getTotalMarksBySubExamId($stdMarksRow['sub_exam_id']);
                                        $student_weightage = ((float) $stdMarksRow['marks'] * (float) $total_weightage ) / (float) $total_max;
                                        $student_weightage = round($student_weightage, 2);
                                    }
                                }
                            }
                            $c_marks += round($student_weightage, 2);
                            if ($isReset == true && $student_weightage > 60)
                                $c_marks = 60;
                        }
                    } else {
                        $course_marks = $row['max_marks'];
                        $min_marks = $row['min_marks'];
                        $isReset = $this->reports_model->isStdResetMarks($fid, $bid, $yid, $std_id, $cid);
                        $sql = "select *from table_std_result where exam_id = '" . $row['id'] . "' and course_id = '" . $cid . "' and std_id='" . $std_id . "' and isActive = 'Y';";
                        $stdMarks = $this->reports_model->SqlExec($sql);
                        foreach ($stdMarks->result_array() as $stdMarksRow) {
                            $c_marks += $stdMarksRow['marks'];
                            if ($isReset == true && $c_marks > 60)
                                $c_marks = 60;
                        }
                    }
                    $student_total_weightage += $c_marks;





                    $total_marks = $total_marks + $course_marks;
                }//end foreach

                $per = round(($student_total_weightage * 100) / $total_marks, 2);
                //Grace marks
                $grace_marks = $this->reports_model->getStdGraceMarks($cid, $std_id);

                $total_course = $per + $grace_marks;
                $reset_arr = $this->isStdResetCourse($fid, $bid, $yid, $cid, $std_id);
                $checked = '';
                $desc = '';

                if (count($reset_arr) > 0) {
                    $checked = 'checked="checked"';
                    $desc = $reset_arr['desc'];
                }
                if ($courseType == 'block')
                    $perc = 60;
                else if ($courseType == 'uncredit')
                    $perc = 50;
                else if ($courseType == 'clinical_skills')
                    $perc = 75;
                else if ($courseType == 'gs')
                    $perc = 50;
                else
                    $perc = 1;
                if (($total_course < $perc) || ($total_course < 50))
                    $grade = 'F';
                else if (($total_course >= 50) && ($total_course <= 64))
                    $grade = 'D';
                else if (($total_course >= 65) && ($total_course <= 74))
                    $grade = 'C';
                else if (($total_course >= 75) && ($total_course <= 84))
                    $grade = 'B';
                else
                    $grade = 'A';

                $html .= "
                        <tr>
                            <td>" . $std_id . "</td>
                            <td>" . $std_row['std_fname'] . "</td>
                            <td><input type='text' style='width:30px;' name='grace_marks[]' size='5' value='$grace_marks'> </td>
                            <td>" . (round($per, 0)) . "</td>
                            
                            <td><input type='checkbox' name='reset[]' $checked value='Y'></td>
                            <td>
                                <input type='text' name='desc[]' value='$desc'>
                                <input type='hidden' name='std_id[]' value='" . $std_id . "'>
                             </td>
                        </tr>
                        ";

                if ($i == $result->num_rows() && $course_marks > 0) {
                    $html .= "<tr>
                            
                                        <td colspan='7' align='center'>
                                                        <input type='button' onclick='resetCourse();' value='Save' name='submit' />
                                 
                                         </td>
                                         </tr>    ";
                }
            } else {
                $html = "<tr>
                            
                                        <td colspan='7' align='center'>
                                                    <h3>No Record Found</h3>
                                 
                                         </td>
                                         </tr>    ";
            }
        }
        return $html;
    }

    function resetExamLockData($fid, $bid, $yid, $cid) {
        $this->load->model('admin/reports_model');
        $sql = "SELECT
                                student_detail.*
                                    FROM
                                      tbl_std_prev_fac_batch_year
                                        INNER JOIN student_detail 
                                            ON (tbl_std_prev_fac_batch_year.std_id = student_detail.std_id)
                                            and     tbl_std_prev_fac_batch_year.faculty_id = '$fid'
                                        and tbl_std_prev_fac_batch_year.batch_id = '$bid'
                                        and tbl_std_prev_fac_batch_year.year_id = '$yid'
                                        
                                        or (student_detail.std_faculty_id = '$fid'
                                    and student_detail.std_batch_id = '$bid'
                                    and student_detail.std_semester_id='$yid'
                                     and student_detail.std_active= 'Y')
                                    group by student_detail.std_id order by student_detail.std_id";


        $sql = "SELECT *from tbl_std_prev_fac_batch_year where faculty_id = '$fid'
                                        and batch_id = '$bid'
                                        and year_id = '$yid'";
        $res = $this->reports_model->SqlExec($sql);
        if ($res->num_rows() > 0) {
            $sql = "SELECT
                                student_detail.*
                                    FROM
                                      tbl_std_prev_fac_batch_year
                                        INNER JOIN student_detail 
                                            ON (tbl_std_prev_fac_batch_year.std_id = student_detail.std_id)
                                            and
                                            (
                                                tbl_std_prev_fac_batch_year.faculty_id = '$fid'
                                                and tbl_std_prev_fac_batch_year.batch_id = '$bid'
                                                and tbl_std_prev_fac_batch_year.year_id = '$yid'
                                            )
                                            or 
                                            (   student_detail.std_faculty_id = '$fid'
                                                and student_detail.std_batch_id = '$bid'
                                                and student_detail.std_semester_id='$yid'
                                                 and student_detail.std_active= 'Y'
                                            )
                                        group by student_detail.std_id";
        } else {
            $sql = "SELECT
                                *
                                    FROM
                                      student_detail
                                        where  (student_detail.std_faculty_id = '$fid'
                                    and student_detail.std_batch_id = '$bid'
                                    and student_detail.std_semester_id='$yid'
                                     and student_detail.std_active= 'Y')
                                    group by student_detail.std_id
                            ;";
        }




        $result = $this->SqlExec($sql);

        $html = '';
        $coursetype = getFieldValueById('batches_course', 'type', 'course_id', $cid);


        if ($coursetype == 'block')
            $perc = 60;
        else if ($coursetype == 'uncredit')
            $perc = 50;
        else if ($coursetype == 'clinical_skills')
            $perc = 75;
        else if ($coursetype == 'gs')
            $perc = 50;
        else
            $perc = 1;
        $total_e_marks = 0;
        $i = 0;
        $per = 0;

        foreach ($result->result_array() as $std_row) {
            ++$i;
            $std_id = $std_row['std_id'];
            $isReset = $this->reports_model->isStdResetMarks($fid, $bid, $yid, $std_id, $cid);

            if (count($isReset) > 0) {

                /////////////////// Get Reset Marks ////////////////////////////
                $exmas_results = $this->reports_model->GetDetail('tbl_lock_resetcourse', "faculty_id = '$fid' and batch_id='$bid' and year_id='$yid' and std_id='$std_id' and course_id='$cid'");



                foreach ($exmas_results->result_array() as $exmas_resultsRow) {


                    $per = $exmas_resultsRow['total_marks'];
                }
                if ($per > $perc) {
                    $per = $perc;
                }

                //$repNote = 'Reset';
            } else {

                /////////////////// Get Exam Marks ////////////////////////////
                $exmas_results = $this->reports_model->GetDetail('tbl_lock_course', "faculty_id = '$fid' and batch_id='$bid' and year_id='$yid' and std_id='$std_id' and course_id='$cid'");



                foreach ($exmas_results->result_array() as $exmas_resultsRow) {


                    $per = $exmas_resultsRow['total_marks'];
                }
                // $repNote = '';
            }




            //Grace marks
            $grace_marks = $this->reports_model->getStdGraceMarks($cid, $std_id);

            $total_course = $per + $grace_marks;
            $reset_arr = $this->isStdResetCourse($fid, $bid, $yid, $cid, $std_id);
            $checked = '';
            $desc = '';

            if (count($reset_arr) > 0) {
                $checked = 'checked="checked"';
                $desc = $reset_arr['desc'];
            }


            $html .= "
                        <tr>
                            <td>" . $std_id . "</td>
                            <td>" . $std_row['std_fname'] . "</td>
                            <td><input type='text' style='width:30px;' name='grace_marks[]' size='5' value='$grace_marks'> </td>
                            <td>" . (round($per, 0)) . "</td>
                            
                            <td><input type='checkbox' name='reset[]' $checked value='Y'></td>
                            <td>
                                <input type='text' name='desc[]' value='$desc'>
                                <input type='hidden' name='std_id[]' value='" . $std_id . "'>
                             </td>
                        </tr>
                        ";
        }
        if ($i == $result->num_rows()) {
            $html .= "<tr>
                            
                                        <td colspan='7' align='center'>
                                                        <input type='button' onclick='resetCourse();' value='Save' name='submit' />
                                 
                                         </td>
                                         </tr>    ";
        } else {
            $html = "<tr>
                            
                                        <td colspan='7' align='center'>
                                                    <h3>No Record Found</h3>
                                 
                                         </td>
                                         </tr>    ";
        }

        return $html;
    }

    function isAlreadyStdEca($fid, $bid, $yid, $std_id) {
        $sql = "Select * from tbl_eca where faculty_id= '$fid' and batch_id = '$bid' and year_id ='$yid' and std_id='$std_id'";
        $result = $this->SqlExec($sql);

        if ($result->num_rows() > 0)
            return true;
        else
            return false;
    }

    function ecaData($fid, $bid, $yid) {
        $html = '';
        // $sql = "SELECT
        //      std_id
        //      ,std_fname
        //  FROM
        //  student_detail
        //  where std_faculty_id = '$fid' and std_semester_id='$yid' and std_batch_id = '$bid'
        //  order by std_id;"; 

        $sql = "SELECT  student_detail.*
                                    FROM
                                      tbl_std_prev_fac_batch_year
                                        INNER JOIN student_detail 
                                            ON (tbl_std_prev_fac_batch_year.std_id = student_detail.std_id)
                                            and     tbl_std_prev_fac_batch_year.faculty_id = '$fid'
                                        and tbl_std_prev_fac_batch_year.batch_id = '$bid'
                                        and tbl_std_prev_fac_batch_year.year_id = '$yid'
                                        
                                        or (student_detail.std_faculty_id = '$fid'
                                    and student_detail.std_batch_id = '$bid'
                                    and student_detail.std_semester_id='$yid')
                                    group by student_detail.std_id order by student_detail.std_id";

        $result = $this->SqlExec($sql);
        $i = 0;
        foreach ($result->result_array() as $row) {
            $std_eca = $this->fetchStdEca($fid, $bid, $yid, $row['std_id']);
            if (count($std_eca) > 0) {
                $eca_text = $std_eca['eca'];
                $eca_desc = $std_eca['desc'];
                $eca_id = $std_eca['id'];
            } else {
                $eca_text = '';
                $eca_desc = '';
                $eca_id = '';
            }

            $html .= "
            <tr>
                <td>" . $row['std_id'] . "</td>
                <td>" . $row['std_fname'] . "</td>
                <td><input type='text' name='eca_text[]' value='$eca_text' ></td>
                <td><input type='text' name='desc[]' value='$eca_desc' >
                <input type='hidden' name='std_id[]' value='" . $row['std_id'] . "'>
                <input type='hidden' name='eca_id[]' value='" . $eca_id . "'>
                 </td>
            </tr>
            ";
            $i++;
        }
        if ($i > 0) {
            $html .= "<td colspan='5' align='center'>
                            <input type='button' onclick='ecaSubmit();' value='Save' name='submit' />
     
             </td>
             </tr>    ";
        }
        echo $html;
    }

    function HideData($fid, $bid, $yid) {
        $html = '';
        $checked = '';

        $sql = "SELECT  student_detail.*
                                    FROM
                                      tbl_std_prev_fac_batch_year
                                        INNER JOIN student_detail 
                                            ON (tbl_std_prev_fac_batch_year.std_id = student_detail.std_id)
                                            and     tbl_std_prev_fac_batch_year.faculty_id = '$fid'
                                        and tbl_std_prev_fac_batch_year.batch_id = '$bid'
                                        and tbl_std_prev_fac_batch_year.year_id = '$yid'
                                        
                                        or (student_detail.std_faculty_id = '$fid'
                                    and student_detail.std_batch_id = '$bid'
                                    and student_detail.std_semester_id='$yid')
                                    where student_detail.std_active = 'Y'
                                    group by student_detail.std_id order by student_detail.std_id";

        $result = $this->SqlExec($sql);
        $i = 0;
        foreach ($result->result_array() as $row) {
            $std_eca = $this->fetchStdEca($fid, $bid, $yid, $row['std_id']);
            if ($row['stopresult'] == 'Y') {
                $hide_text = "Hidden";
            } else {
                $hide_text = '';
            }

            $html .= "
                        
            <tr>
                <td><input class='chk' type='checkbox'  name='reset[]' $checked value='Y'></td>
                <td>" . $row['std_id'] . "</td>
                <td>" . $row['std_fname'] . "</td>
                <td>" . $hide_text . "</td>
                <td><input type='text' name='desc[]' value='" . $row['std_hide_note'] . "'> 
                <input type='hidden' name='std_id[]' value='" . $row['std_id'] . "'> 
                </td>
                
            </tr>
            ";
            $i++;
        }
        if ($i > 0) {
            $html .= "<td colspan='5' align='center'>
                            
                            <input type='button' onclick='ecaSubmit();' value='Hide' name='submit' />
     
             </td>
             </tr>    ";
        }
        echo $html;
    }

    function yearTransferForm($fid, $bid, $yid) {
        $html = '';
        $sql = "SELECT
                    std_id
                    ,std_fname
                FROM
                student_detail
                where std_faculty_id = '$fid' and std_semester_id='$yid' and std_batch_id = '$bid'
                order by std_id;";

        $result = $this->SqlExec($sql);
        $i = 0;
        foreach ($result->result_array() as $row) {
            $html .= "
            <tr>
                <td>" . $row['std_id'] . "</td>
                <td>" . $row['std_fname'] . "</td>
                <td><input type='checkbox' name='transfer[]' ></td>
                <td></td>
                <td><input type='hidden' name='std_id[]' value='" . $row['std_id'] . "'>
                 </td>
            </tr>
            ";
            $i++;
        }
        if ($i > 0) {

            $where = getFacultyIds();


            $html .= "
            <tr>
                <td>Select Faculty</td>
                <td>" .
                    show_dropdown('faculty', 'faculty', 'faculty_name', 'faculty_id', set_value('faculty_id'), "Select Faculty", "onchange=fetchExamSemesterBatch(this.value);", "isActive='Y'$where") . "</td>
            </tr>
                    
            <tr>
                <td>Select Batch</td>
                <td id='tbatch'>
                <select>
                    <option value='0'>Select Batch</option>
                </select>
                </td>
            </tr>
            <tr>
                <td>Select Year</td>
                <td id='tyear'>
                <select>
                    <option value='0'>Select Year/Semester</option>
                </select>
                                            
                </td>
            </tr>

            <tr>
                    
            <td colspan='5' align='center'>
                            <input type='button' onclick='doconfirmSave();' value='Save' name='submit' />
     
             </td>
             </tr>    ";
        }
        return $html;
    }

    function isStdRepeater($fid, $bid, $yid, $std_id) {
        $count_already = $this->counttotal('tbl_std_rep_course', "fac_id ='$fid' and batch_id='$bid' and year_id='$yid' and std_id='$std_id'");
        return $count_already;
    }

    function isStdRepeater2($fid, $bid, $yid, $std_id, $cid) {
        $count_already = $this->counttotal('tbl_std_rep_course', "fac_id ='$fid' and batch_id='$bid' and year_id='$yid' and course_id='$cid'and year_id='$yid' and std_id='$std_id'");
        return $count_already;
    }

    function yearRepForm($fid, $bid, $yid) {
        $html = '';
        $sql = "SELECT
                    std_id
                    ,std_fname
                FROM
                student_detail
                where std_faculty_id = '$fid' and std_semester_id='$yid' and std_batch_id = '$bid'
                order by std_id;";

        $result = $this->SqlExec($sql);
        $i = 0;
        foreach ($result->result_array() as $row) {
            $count = $this->isStdRepeater($fid, $bid, $yid, $row['std_id']);
            if ($count > 0)
                $checked = "checked='checked'";
            else
                $checked = "";

            $html .= "
            <tr id='r_" . $row['std_id'] . "'>
                <td>" . $row['std_id'] . "</td>
                <td>" . $row['std_fname'] . "</td>
                <td><input type='checkbox' name='repeat[]' $checked></td>
                <td></td>
                <td><input type='hidden' name='std_id[]' value='" . $row['std_id'] . "'>
                 </td>
            </tr>
            ";
            $i++;
        }
        if ($i > 0) {
            $html .= "
                    
            <tr>
                <td>Repeate this Year With</td>
                <td colspan='4'>" .
                    show_dropdown('batches', 'new_batch_name', 'batch_name', 'batch_id', 0, "Select Batch", '', "isActive='Y' and faculty_id='$fid'")
                    . "</td>
                
                
            </tr>
            <tr>
                    
            <td colspan='5' align='center'>
                            <input type='button' onclick='doconfirmSave();' value='Save' name='submit' />
     
             </td>
             </tr>    ";
        }
        return $html;
    }

    function yearRepUnCreditForm($fid, $bid, $yid) {
        $html = '';
        $sql = "SELECT
                    std_id
                    ,std_fname
                FROM
                student_detail
                where std_faculty_id = '$fid' and std_semester_id='$yid' and std_batch_id = '$bid'
                order by std_id;";

        $result = $this->SqlExec($sql);
        $i = 0;
        foreach ($result->result_array() as $row) {
            $count = $this->isStdRepeater($fid, $bid, $yid, $row['std_id']);
            if ($count > 0)
                $checked = "checked='checked'";
            else
                $checked = "";

            $html .= "
            <tr id='r_" . $row['std_id'] . "'>
                <td>" . $row['std_id'] . "</td>
                <td>" . $row['std_fname'] . "</td>
                <td><input type='checkbox' name='repeat[]' $checked></td>
                <td></td>
                <td><input type='hidden' name='std_id[]' value='" . $row['std_id'] . "'>
                 </td>
            </tr>
            ";
            $i++;
        }
        if ($i > 0) {
            $html .= "
                    <tr>
                        <td colspan='5'>Repeat with</td>
                    </tr>
            <tr>
                <td>Select Batch</td>
                <td colspan='4'>" .
                    show_dropdown('batches', 'new_batch_name', 'batch_name', 'batch_id', 0, "Select Batch", '', "isActive='Y' and faculty_id='$fid'")
                    . "</td>
                
                
            </tr>
            <tr>
                <td>Select Year</td>
                <td colspan='4'>" .
                    show_dropdown('year_semester', 'new_year_semester', 'year_semester', 'semester_id', 0, "Select Year/Semester", 'onchange="getUncreditCourse(this.value)"', "isActive='Y' and faculty_id='$fid'")
                    . "</td>
            </tr>
                    <tr>
                <td>Select Repeater Course</td>
                <td colspan='4' id='uncourse'>
                <select>
                <option value=''> selct course</option>
                </selct>
                </td>
            </tr>
            <tr>
                    
            <td colspan='5' align='center'>
                            <input type='button' onclick='doconfirmSave();' value='Save' name='submit' />
     
             </td>
             </tr>    ";
        }
        return $html;
    }

    function yearRepUnCreditForm2($fid, $bid, $yid, $cid) {
        $html = '';
        $sql = "SELECT
                    std_id
                    ,std_fname
                FROM
                student_detail
                where std_faculty_id = '$fid' and std_semester_id='$yid' and std_batch_id = '$bid'
                order by std_id;";

        $result = $this->SqlExec($sql);
        $i = 0;
        foreach ($result->result_array() as $row) {
            $count = $this->isStdRepeater2($fid, $bid, $yid, $row['std_id'], $cid);
            if ($count > 0)
                $checked = "checked='checked'";
            else
                $checked = "";

            $html .= "
            <tr id='r_" . $row['std_id'] . "'>
                <td>" . $row['std_id'] . "</td>
                <td>" . $row['std_fname'] . "</td>
                <td><input type='checkbox' name='repeat[]' $checked></td>
                <td></td>
                <td><input type='hidden' name='std_id[]' value='" . $row['std_id'] . "'>
                 </td>
            </tr>
            ";
            $i++;
        }
        if ($i > 0) {
            $html .= "
                    <tr>
                        <td colspan='5'>Repeat with</td>
                    </tr>
            <tr>
                <td>Select Batch</td>
                <td colspan='4'>" .
                    show_dropdown('batches', 'new_batch_name', 'batch_name', 'batch_id', 0, "Select Batch", '', "isActive='Y' and faculty_id='$fid'")
                    . "</td>
                
                
            </tr>
            <tr>
                <td>Select Year</td>
                <td colspan='4'>" .
                    show_dropdown('year_semester', 'new_year_semester', 'year_semester', 'semester_id', 0, "Select Year/Semester", 'onchange="getUncreditCourse(this.value)"', "isActive='Y' and faculty_id='$fid'")
                    . "</td>
            </tr>
                    <tr>
                <td>Select Repeater Course</td>
                <td colspan='4' id='uncourse'>
                <select>
                <option value=''> select course</option>
                </selct>
                </td>
            </tr>
            <tr>
                    
            <td colspan='5' align='center'>
                            <input type='button' onclick='doconfirmSave();' value='Save' name='submit' />
     
             </td>
             </tr>    ";
        }
        return $html;
    }

    function yearRepCourseForm($fid, $bid, $yid, $cid) {
        $html = '';
        $sql = "SELECT
                    std_id
                    ,std_fname
                FROM
                student_detail
                where std_faculty_id = '$fid' and std_semester_id='$yid' and std_batch_id = '$bid'
                order by std_id;";

        $result = $this->SqlExec($sql);
        $i = 0;
        foreach ($result->result_array() as $row) {
            $count = $this->isStdRepeater2($fid, $bid, $yid, $row['std_id'], $cid);
            if ($count > 0)
                $checked = "checked='checked'";
            else
                $checked = "";

            $html .= "
            <tr id='r_" . $row['std_id'] . "'>
                <td>" . $row['std_id'] . "</td>
                <td>" . $row['std_fname'] . "</td>
                <td><input type='checkbox' name='repeat[]' $checked></td>
                <td></td>
                <td><input type='hidden' name='std_id[]' value='" . $row['std_id'] . "'>
                 </td>
            </tr>
            ";
            $i++;
        }
        if ($i > 0) {
            $html .= "
                    <tr>
                        <td colspan='5'>Repeat with</td>
                    </tr>
            <tr>
                <td>Select Academic Year</td>
                <td colspan='4'>" .
                    show_dropdown('batches', 'new_batch_name', 'batch_name', 'batch_id', 0, "Select Academic Year", '', "isActive='Y' and faculty_id='$fid'")
                    . "</td>
                
                
            </tr>
            <tr>
                <td>Select Year</td>
                <td colspan='4'>" .
                    show_dropdown('year_semester', 'new_year_semester', 'year_semester', 'semester_id', 0, "Select Year/Semester", 'onchange="getUncreditCourse(this.value)"', "isActive='Y' and faculty_id='$fid'")
                    . "</td>
            </tr>
                    
            <tr>
                    
            <td colspan='5' align='center'>
                            <input type='button' onclick='formSubmit();' value='Save' name='submit' />
     
             </td>
             </tr>    ";
        }
        return $html;
    }

    function RepSingleCourseForm($fid, $bid, $yid, $cid) {
        $html = '';
        $sql = "SELECT
                    std_id
                    ,std_fname
                FROM
                student_detail
                where std_faculty_id = '$fid' and std_semester_id='$yid' and std_batch_id = '$bid'
                order by std_id;";

        $result = $this->SqlExec($sql);
        $i = 0;
        foreach ($result->result_array() as $row) {
            $count = $this->isStdRepeater2($fid, $bid, $yid, $row['std_id'], $cid);
            if ($count > 0)
                $checked = "checked='checked'";
            else
                $checked = "";

            $html .= "
            <tr id='r_" . $row['std_id'] . "'>
                <td>" . $row['std_id'] . "</td>
                <td>" . $row['std_fname'] . "</td>
                <td><input type='checkbox' name='repeat[]' $checked></td>
                <td></td>
                <td><input type='hidden' name='std_id[]' value='" . $row['std_id'] . "'>
                 </td>
            </tr>
            ";
            $i++;
        }
        if ($i > 0) {
            $html .= "
                    <tr>
                        <td colspan='5'>Repeat with</td>
                    </tr>
            <tr>
                <td>Select Batch</td>
                <td colspan='4'>" .
                    show_dropdown('batches', 'new_batch_name', 'batch_name', 'batch_id', 0, "", '', "isActive='Y' and faculty_id='$fid' ORDER BY sort_batch_id DESC")
                    . "</td>
                
                
            </tr>
            <tr>
                <td>Select Year</td>
                <td colspan='4'>" .
                    show_dropdown('year_semester', 'new_year_semester', 'year_semester', 'semester_id', 0, "Select Year/Semester", '', "isActive='Y' and faculty_id='$fid'")
                    . "</td>
            </tr>
                    
            <tr>
                    
            <td colspan='5' align='center'>
                            <input type='button' onclick='doconfirmSave();' value='Save' name='submit' />
     
             </td>
             </tr>    ";
        }
        return $html;
    }

    function fetchStdEca($fid = 0, $bid = 0, $yid = 0, $std_id = 0) {
        $sql = "select *from tbl_eca where faculty_id = '$fid' and batch_id = '$bid' and year_id = '$yid' and std_id = '$std_id'";
        $result = $this->SqlExec($sql);

        $row = array();
        if ($result->num_rows() > 0)
            $row = $result->row_array();

        return $row;
    }

    function getRepeaterCourseDone($fid, $bid, $yid, $std_id, $cid) {

        $sql = "select *from tbl_std_course_done where fac_id = '$fid' and course_id='$cid' and year_id = '$yid' and std_id = '$std_id'  and  batch_id != '$bid' ";
        $result = $this->SqlExec($sql);
        $row = array();

        if ($result->num_rows() > 0)
            $row = $result->row_array();

        return $row;
    }

    function getRepeaterCourseDone2($fid, $bid, $yid, $std_id, $cid) {

        //$sql = "select *from tbl_std_course_done where fac_id = '$fid' and course_id='$cid' and year_id = '$yid' and std_id = '$std_id'  and batch_id = '$bid' ";
        $sql = "select *from tbl_std_course_done where fac_id = '$fid' and course_id='$cid' and year_id = '$yid' and std_id = '$std_id' ";  //shaz removed batch_id bcz it allows to repeat second time any course and other courses wouldnot go to course_done again.
        $result = $this->SqlExec($sql);
        $row = array();

        if ($result->num_rows() > 0)
            $row = $result->row_array();

        return $row;
    }

    function getStdRepeaterCourse($fid, $bid, $yid, $std_id, $cid) {
        $sql = "select *from tbl_std_rep_course where fac_id = '$fid' and course_id='$cid' and year_id = '$yid' and std_id = '$std_id'  and batch_id = '$bid' ";
        $result = $this->SqlExec($sql);
        $row = array();

        if ($result->num_rows() > 0)
            $row = $result->row_array();

        return $row;
    }
    public function getCourses($faculty_id,$academic_year,$class_id){
         $this->db->select('assigned_subjects.subject_id,subjects.name')->from('assigned_subjects');
        $this->db->join('subjects', 'subjects.id = assigned_subjects.subject_id');
        $this->db->where('assigned_subjects.faculty_id',$faculty_id);
        $this->db->where('assigned_subjects.academic_year',$academic_year);
        $this->db->where('assigned_subjects.class_id',$class_id);
        $this->db->order_by('assigned_subjects.id');

        $query = $this->db->get();
        
        return $query->result_array();
    }
    public function getExamSubject($exam_subject_id) {

        $sql = "SELECT exam_group_class_batch_exam_subjects.*,subjects.name as `subject_name`,subjects.code FROM `exam_group_class_batch_exam_subjects` INNER JOIN subjects on subjects.id=exam_group_class_batch_exam_subjects.subject_id WHERE exam_group_class_batch_exam_subjects.id=" . $this->db->escape_str($exam_subject_id);

        $query = $this->db->query($sql);
        return $query->row();
    }

    function rawSQl($sql)
    {
        // print_r($sql);
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function searchExamStudents($faculty_id, $class_id, $section_id, $session_id,$group_id=null) {
       if ($group_id == '') { //for group None/All
            $where = '';
            $where2  = "and students.section_id='" . $section_id . "' ";
        } else {
            $where = "and student_session.section_id='" . $data['group_id'] . "' ";
            $where2  = "and students.section_id='" . $data['group_id'] . "' ";
        }

        /// This sql is for save button and union code is to show also previous faculty student ---- "table_std_result.std_id IS NULL" ka purpose unko exclude krna he jo table_std_result may already hain///

       $sql = " 
                SELECT
                   students.admission_no , students.id as `student_id`, students.roll_no,students.admission_date,students.firstname,students.middlename, students.lastname,students.father_name,students.gender,students.mobileno
                FROM
                    students
                    
                      where  students.faculty_id='" . $faculty_id . "' and students.academic_year='" . $session_id . "' and students.class_grade_id='" . $class_id . "'    $where2  and students.status= 'Regular'
                        
                        UNION
                        
                        SELECT 
               students.admission_no , students.id as `student_id`, students.roll_no,students.admission_date,students.firstname,students.middlename, students.lastname,students.father_name,students.gender,students.mobileno
                FROM
                    student_session
                    INNER JOIN students
                    ON ( student_session.student_id = students.id)
                    
                  
                     WHERE student_session.session_id='" . $session_id . "'  and student_session.class_id='" . $class_id . "'  $where2
                    
                        
                        
                        ;";
        $query = $this->db->query($sql);

        return $query->result();
    }
}
