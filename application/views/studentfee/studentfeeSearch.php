<?php
$currency_symbol = $this->customlib->getSchoolCurrencyFormat();
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-money"></i> <?php echo $this->lang->line('fees_collection'); ?> </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> <?php echo $this->lang->line('select_criteria'); ?></h3>
                    </div>
                    <div class="box-body">
                        <form  action="<?php echo site_url('studentfee/search') ?>" method="post" class="class_search_form">
                                        <?php echo $this->customlib->getCSRF(); ?>
                        <div class="row">
                            <div class="">
                                <div class="row">
                                    
                                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo $this->lang->line('academic_year'); ?> </label>
                                <select id="academic_year" name="academic_year" class="form-control">
                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                    <?php
                                    foreach ($sessionlist as $session) {
                                    ?>
                                        <option value="<?php echo $session['id'] ?>" 
                                        
                                         ><?php echo $session['session'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label><?php echo $this->lang->line('class'); ?>
                                                <select autofocus="" id="class_id" name="class_id" class="form-control" >
                                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                    <?php
                                    foreach ($classlist as $class) {
                                        ?>
                                          <option value="<?php echo $class['id'] ?>" <?php if (set_value('class_id') == $class['id']) {
                                            echo "selected=selected";
                                        }
                                        ?>><?php echo $class['class'] ?></option>
                                                                                            <?php
                                    }
                                    ?>
                                                                                    </select>
                                                 <span class="text-danger" id="error_class_id"></span>
                                            </div>

                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label><?php echo $this->lang->line('section'); ?></label>
                                                <select  id="section_id" name="section_id" class="form-control" >
                                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                </select>
                                                <span class="text-danger"><?php echo form_error('section_id'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo $this->lang->line('session'); ?> </label>
                                <select id="session" name="session" class="form-control">
                                    <option value="" <?php if (set_value('session') == '') echo "selected=selected" ?>><?php echo $this->lang->line('select'); ?></option>
                                    <option value="Morning" <?php if (set_value('session') == 'Morning') echo "selected=selected" ?>>Morning</option>
                                    <option value="Afternoon" <?php if (set_value('session') == 'Afternoon') echo "selected=selected" ?>>Afternoon</option>
                                    <option value="Evening" <?php if (set_value('session') == 'Evening') echo "selected=selected" ?>>Evening</option>
                                </select>
                            </div>
                            </div>
                           <div class="col-md-3">
                                            <div class="form-group">
                                                <label><?php echo $this->lang->line('search_by_keyword'); ?></label>
                                        <input type="text" name="search_text" id="search_text" class="form-control" value="<?php echo set_value('search_text'); ?>"   placeholder="Search By Keyword (Student ID, Roll No, Student Name, Name in English, Father Name)">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>First Name</label>
                                        <input type="text" name="firstname" id="firstname" class="form-control" value="<?php echo set_value('firstname'); ?>"   placeholder="First Name">
                                            </div>
                                        </div>
                                        <div class="col-md-3"> 
                                            <div class="form-group">
                                                <label><?php echo $this->lang->line('middle_name'); ?></label>
                                        <input type="text" name="middlename" id="middlename" class="form-control" value="<?php echo set_value('middlename'); ?>"   placeholder="<?php echo $this->lang->line('middle_name'); ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label><?php echo $this->lang->line('last_name'); ?></label>
                                        <input type="text" name="lastname" id="lastname" class="form-control" value="<?php echo set_value('lastname'); ?>"   placeholder="<?php echo $this->lang->line('last_name'); ?>">
                                            </div>
                                        </div>

                                        <!-- <div class="col-sm-12">
                                            <div class="form-group">

                                                <button type="submit" class="btn btn-primary btn-sm pull-right" name="class_search" data-loading-text="Please wait.." value=""><i class="fa fa-search"></i> <?php echo $this->lang->line('search'); ?></button>
                                            </div>
                                        </div> -->
                                    
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                                <?php if ($sch_setting->category) { ?>
                                <div class="col-sm-3">
                                    <div class="form-group">   
                                        <label><?php echo $this->lang->line('category'); ?></label>
                                        <select  id="category_id" name="category_id" class="form-control" >
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                            <?php
                                            foreach ($categorylist as $category) {
                                                ?>
                                                <option value="<?php echo $category['id'] ?>" <?php if (set_value('category_id') == $category['id']) echo "selected=selected"; ?>><?php echo $category['category'] ?></option>
                                                <?php
                                                $count++;
                                            }
                                            ?>
                                        </select>
                                    </div>   
                                </div>
                            <?php } ?>
                             <div class="col-md-2 col-xs-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('fees') ?> <?php echo $this->lang->line('category') ?></label>
                                                <select class="form-control" rows="3" placeholder="" name="feecategory">
                                                    <option value="-1">Select</option>
                                                    <?php foreach ($feecategory as $k => $cat) {
                                                    ?>
                                                        <option value="<?php echo $cat["id"] ?>"><?php echo $cat["category_name"] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('nationality'); ?></label>

                                            <select id="my-select" class="form-control" name="nationality">
                                                <option value="-1">Select</option>
                                                <?php foreach ($countries as $country) {
                                                        ?>
                                                <option  value="<?php echo $country->country_name; ?>"><?php echo $country->country_name; ?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                            <div class="col-sm-2">
                                <div class="form-group">  
                                    <label><?php echo $this->lang->line('gender'); ?></label>
                                    <select class="form-control" name="gender">
                                        <option value=""><?php echo $this->lang->line('select'); ?></option>
                                        <?php
                                        foreach ($genderList as $key => $value) {
                                            ?>
                                            <option value="<?php echo $key; ?>" <?php if (set_value('gender') == $key) echo "selected"; ?>><?php echo $value; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>  
                            </div>  
                            <?php if ($sch_setting->rte) { ?>
                                <div class="col-sm-2">
                                    <div class="form-group">  
                                        <label><?php echo $this->lang->line('rte'); ?></label>
                                        <select  id="rte" name="rte" class="form-control" >
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                            <?php
                                            foreach ($RTEstatusList as $k => $rte) {
                                                ?>
                                                <option value="<?php echo $k; ?>" <?php if (set_value('rte') == $k) echo "selected"; ?>><?php echo $rte; ?></option>

                                                <?php
                                                $count++;
                                            }
                                            ?>
                                        </select>
                                    </div>   
                                </div>
                            <?php } ?>
                           </div>
                           <div class="row">
                                  <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('admission_date'); ?> <?php echo $this->lang->line('from'); ?></label>
                                        <input id="admission_date" name="admission_date_from" placeholder="" type="text" class="form-control date" value="" />
                                        <span class="text-danger"><?php echo form_error('admission_date'); ?></span>
                                    </div>
                                  </div>
                                  <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('admission_date'); ?> <?php echo $this->lang->line('to'); ?></label>
                                        <input id="admission_date" name="admission_date_to" placeholder="" type="text" class="form-control date" value="" readonly="readonly" />
                                    </div>
                                  </div>
                              </div>
                        <div class="row">
                                   
                                        

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                               <button type="submit" class="btn btn-primary btn-sm pull-right" name="keyword_search" data-loading-text="Please wait.." value=""><i class="fa fa-search"></i> <?php echo $this->lang->line('search'); ?></button>
                                            </div>
                                        </div>
                                  
                                </div>
                    </form>
                    </div>


                        <div class="">
                            <div class="box-header ptbnull"></div>
                            <div class="box-header ptbnull">
                                <h3 class="box-title titlefix"><i class="fa fa-users"></i> <?php echo $this->lang->line('student'); ?> <?php echo $this->lang->line('list'); ?>
                                    <?php echo form_error('student'); ?></h3>
                                <div class="box-tools pull-right"></div>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    
                              
                                <table class="table table-striped table-bordered table-hover student-list" data-export-title="<?php echo $this->lang->line('student')." ".$this->lang->line('list'); ?>">
                                    <thead>

                                        <tr>
                                            <th><?php echo $this->lang->line('admission_no'); ?></th>
                                            <th><?php echo $this->lang->line('student_name'); ?></th>
                                            <th><?php echo $this->lang->line('class'); ?></th>
                                                <th><?php echo $this->lang->line('father_name'); ?></th>
                                                <th><?php echo $this->lang->line('father_phone'); ?></th>
                                                <th><?php echo $this->lang->line('mother_phone'); ?></th>
                                                <th><?php echo $this->lang->line('total_payable'); ?></th>
                                            <th class="text-right noExport"><?php echo $this->lang->line('action'); ?></th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                                  </div>
                            </div><!--./box-body-->
                        </div>
                    </div>

            </div>

        </div>

    </section>
</div>

<script>
$(document).ready(function() {
     emptyDatatable('student-list','fees_data');

});
</script>
<script type="text/javascript">


    $(document).ready(function () {

        var class_id = $('#class_id').val();
        var section_id = '<?php echo set_value('section_id', 0) ?>';
        getSectionByClass(class_id, section_id);
    });

    $(document).on('change', '#class_id', function (e) {
        $('#section_id').html("");
        var class_id = $(this).val();
        getSectionByClass(class_id, 0);
    });

    function getSectionByClass(class_id, section_id) {

        if (class_id != "") {
            $('#section_id').html("");
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            $.ajax({
                type: "GET",
                url: base_url + "sections/getByClass",
                data: {'class_id': class_id},
                dataType: "json",
                beforeSend: function () {
                    $('#section_id').addClass('dropdownloading');
                },
                success: function (data) {
                    $.each(data, function (i, obj)
                    {
                        var sel = "";
                        if (section_id == obj.section_id) {
                            sel = "selected";
                        }
                        div_data += "<option value=" + obj.section_id + " " + sel + ">" + obj.section + "</option>";
                    });
                    $('#section_id').append(div_data);
                },
                complete: function () {
                    $('#section_id').removeClass('dropdownloading');
                }
            });
        }
    }
</script>
<script type="text/javascript">
$(document).ready(function(){ 
$(document).on('submit','.class_search_form',function(e){
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var $this = $(this).find("button[type=submit]:focus");  
    var form = $(this);
    var url = form.attr('action');
    var form_data = form.serializeArray();
    form_data.push({name: 'search_type', value: $this.attr('name')});
     // console.log(form_data); return false;
    $.ajax({
           url: url,
           type: "POST",
           dataType:'JSON',
           data: form_data, // serializes the form's elements.
              beforeSend: function () {
                $('[id^=error]').html("");
                $this.button('loading');
                // resetFields($this.attr('name'));
               },
              success: function(response) { // your success handler
                if(!response.status){
                    $.each(response.error, function(key, value) {
                    $('#error_' + key).html(value);
                });
                }else{
                    initDatatable('student-list','studentfee/ajaxSearch',response.params,[],100);
                }
              },
             error: function() { // your error handler
                 $this.button('reset');
             },
             complete: function() {
             $this.button('reset');
             }
         });

});

    });
    function resetFields(search_type){
        if(search_type == "keyword_search"){
            $('#class_id').prop('selectedIndex',0);
            $('#section_id').find('option').not(':first').remove();
        }else if (search_type == "class_search") {
            
             $('#search_text').val("");
        }
    }
</script>
