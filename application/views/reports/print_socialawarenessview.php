<!DOCTYPE html>
<html lang="ar">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>جدول يوضح التوعية المقدمة من المكتب االجتماعي</title>
    <link
      href="https://fonts.googleapis.com/css2?family=Inter:wght@400;700&display=swap"
      rel="stylesheet"
    />
    <style>
      @media print {
        @page {
          size: A4;
        }
      }
      ul {
        padding: 0;
        margin: 0 0 1rem 0;
        list-style: none;
      }
      body {
        font-family: "Inter", sans-serif;
        margin: 0;

      }
      table {
        width: 100%;
        border-collapse: collapse;
      }
      table,
      table th,
      table td {
        border: 1px solid silver;
      }
      table th,
      table td {
        text-align: right;
        padding: 4px;
      }
      h1,
      h4,
      p {
        margin: 0;
      }

      .container {
        padding: 20px 0;
        width: 1500px;
        max-width: 95%;
        margin: 0 auto;
      }
      .parent_r_main{
        border: 2px solid #222;
        box-shadow: 10px 10px #222;
        padding: 10px;
      }
      .inv-title {
        padding: 10px;
        text-align: center;
        margin-bottom: 30px;
      }

      /* header */
      .inv-header {
        display: flex;
        margin-bottom: 20px;
      }
      .inv-header > :nth-child(1) {
        flex: 2;
      }
      .inv-header > :nth-child(2) {
        flex: 1;
      }
      .inv-header h2 {
        font-size: 20px;
        margin: 0 0 0.3rem 0;
      }
      .inv-header ul li {
        font-size: 15px;
        padding: 3px 0;
      }

      /* body */
      .inv-body table th,
      .inv-body table td {
        text-align: left;
      }
      .inv-body table th{
        font-size: 20px;
      }
      .inv-body table td{
        font-size: 15px;
      }
      .inv-body {
        margin-bottom: 30px;
      }

      /* footer */
      .inv-footer {
        display: flex;
        flex-direction: row;
      }
      .inv-footer > :nth-child(1) {
        flex: 2;
      }
      .inv-footer > :nth-child(2) {
        flex: 1;
      }
      .top_titles{
       font-size: 24px;
      font-weight: bold;
      padding-bottom: 10px;

      }
      .top_titles .left{
        float: left;
      }
      .top_titles .right{
        float: right;
      }
    </style>
  </head>
  <body dir="rtl">
    <div class="container parent_r_main">
      <div class="inv-title">
        <h1>جدول يوضح التوعية المقدمة من المكتب االجتماعي</h1>
      </div>

      <div class="inv-body">
        <table>
          <thead>
            <th>نوع التوعية</th>
            <th>التاريخ</th> 
            <th>الصف</th>
            <th>مكان التوعية</th> 
            <th>مالحظات</th> 
          </thead>
          <tbody>
            <?php if(isset($response) && !empty($response)){ 
              foreach($response as $row){
              ?>
            <tr>
                <td><?php echo isset($row['awareness']) ? $row['awareness'] : ''; ?></td>
              <td><?php echo isset($row['date']) ? $row['date'] : ''; ?></td>
                <td><?php echo isset($row['class']) ? $row['class'] : ''; ?></td>
                <td><?php echo isset($row['awareness_place']) ? $row['awareness_place'] : ''; ?></td>
                <td><?php echo isset($row['comments']) ? $row['comments'] : ''; ?></td>
            </tr>
          <?php } } ?>
          </tbody>
        </table>
      </div>
      
    </div>
    <script type="text/javascript">
    window.print();
</script>
  </body>
</html>