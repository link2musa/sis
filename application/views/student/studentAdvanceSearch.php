<?php
$currency_symbol = $this->customlib->getSchoolCurrencyFormat();
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-user-plus"></i> <?php echo $this->lang->line('student_information'); ?> <small><?php echo $this->lang->line('student1'); ?></small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> <?php echo $this->lang->line('select_criteria'); ?></h3>
                    </div>
                    <div class="box-body">
                        <form role="form" action="<?php echo site_url('student/advancesearchvalidation') ?>" method="post" class="class_search_form">
                            <?php if ($this->session->flashdata('msg')) { ?> <div class="alert alert-success"> <?php echo $this->session->flashdata('msg') ?> </div> <?php } ?>
                              <?php echo $this->customlib->getCSRF(); ?>

                              <!------1ST row started------->
                              <div class="row">
                                  <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('academic_year'); ?> </label>
                                        <select id="academic_year" name="academic_year" class="form-control">
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                            <?php
                                            foreach ($sessionlist as $session) {

                                            ?>
                                                <option  value="<?php echo $session['id'] ?>"><?php echo $session['session'] ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('class'); ?></label>
                                        <select autofocus="" id="class_id" name="class_id" class="form-control">
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                            <?php
                                            $count = 0;
                                            foreach ($classlist as $class) {
                                            ?>
                                                <option value="<?php echo $class['id'] ?>" <?php if (set_value('class_id') == $class['id']) {
                                                    echo "selected=selected";
                                                }
                                                ?>><?php echo $class['class'] ?></option>
                                            <?php
                                                $count++;
                                            }
                                            ?>
                                        </select>
                                        <span class="text-danger" id="error_class_id"></span>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('section'); ?></label>
                                        <select id="section_id" name="section_id" class="form-control">
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1">  
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('session'); ?> </label>
                                    <select id="session" name="session" class="form-control">
                                        <option value="" <?php if (set_value('session') == '') echo "selected=selected" ?>><?php echo $this->lang->line('select'); ?></option>
                                        <option value="Morning" <?php if (set_value('session') == 'Morning') echo "selected=selected" ?>>Morning</option>
                                        <option value="Afternoon" <?php if (set_value('session') == 'Afternoon') echo "selected=selected" ?>>Afternoon</option>
                                        <option value="Evening" <?php if (set_value('session') == 'Evening') echo "selected=selected" ?>>Evening</option>
                                    </select>
                                </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('specialization'); ?> </label>
                                        <select id="specialization" name="specialization" class="form-control">
                                            <option value="-1">Select</option>
                                            <option value="None">None</option>
                                            <option value="FSC">FSC</option>
                                            <option value="IGCSE">IGCSE</option>
                                            <option value="Arts">Arts</option>
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-md-1 col-xs-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('fees') ?> <?php echo $this->lang->line('category') ?></label>
                                                <select class="form-control" rows="3" placeholder="" name="feecategory">
                                                    <option value="-1">Select</option>
                                                    <?php foreach ($feecategory as $k => $cat) {
                                                    ?>
                                                        <option value="<?php echo $cat["id"] ?>"><?php echo $cat["category_name"] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                <div class="col-md-2">
                                <label><?php echo $this->lang->line('status'); ?></label>
                                <div class="radio" style="margin-top: 2px;">
                                    <label><input class="radio-inline" type="radio" name="status" value="Regular">Regular</label>
                                    <label><input class="radio-inline" type="radio" name="status" value="Private">Private</label>
                                </div>
                              </div>

                                <div class="col-md-2 col-xs-12"> 
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('house') ?></label>
                                        <select class="form-control" rows="3" placeholder="" name="reg_batch_id">
                                            <option value="-1">Select</option>
                                            <?php foreach ($regbatchlist as $hkey => $batch) {
                                            ?>
                                                <option value="<?php echo $batch["id"] ?>"><?php echo $batch["name"] ?></option>
                                            <?php } ?>
                                        </select>
                                        
                                    </div>
                                </div>

                              </div>
                              <!------1ST row end------->

                              <!------2nd row start------->
                              <div class="row">
                                   <div class="col-md-3">
                                            <div class="form-group">
                                                <label><?php echo $this->lang->line('search_by_keyword'); ?></label>
                                        <input type="text" name="search_text" id="search_text" class="form-control" value="<?php echo set_value('search_text'); ?>"   placeholder="Search By Keyword (Student ID, Roll No, Student Name, Name in English, Father Name)">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>First Name</label>
                                        <input type="text" name="firstname" id="firstname" class="form-control" value="<?php echo set_value('firstname'); ?>"   placeholder="First Name">
                                            </div>
                                        </div>
                                        <div class="col-md-3"> 
                                            <div class="form-group">
                                                <label><?php echo $this->lang->line('middle_name'); ?></label>
                                        <input type="text" name="middlename" id="middlename" class="form-control" value="<?php echo set_value('middlename'); ?>"   placeholder="<?php echo $this->lang->line('middle_name'); ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label><?php echo $this->lang->line('last_name'); ?></label>
                                        <input type="text" name="lastname" id="lastname" class="form-control" value="<?php echo set_value('lastname'); ?>"   placeholder="<?php echo $this->lang->line('last_name'); ?>">
                                            </div>
                                        </div>
                              </div> 
                              <!------2nd row end------->

                              <!------3rd row start------->
                              <div class="row">
                                  <div class="col-md-2">
                                    <label><?php echo $this->lang->line('gender'); ?></label>
                                    <div class="radio" style="margin-top: 2px;">
                                        <label><input class="radio-inline" type="radio" name="gender" value="Male">Male</label>
                                        <label><input class="radio-inline" type="radio" name="gender" value="Female">Female</label>
                                    </div>
                                   </div> 
                                   <div class="col-md-2">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('nationality'); ?></label>

                                            <select id="my-select" class="form-control" name="nationality">
                                                <option selected value="">Select</option>
                                                <?php foreach ($countries as $country) {
                                                        ?>
                                                <option  value="<?php echo $country->country_name; ?>"><?php echo $country->country_name; ?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('religion'); ?></label>
                                            <input id="religion" name="religion" placeholder="" type="text" class="form-control" value="<?php echo set_value('religion'); ?>" />
                                            <span class="text-danger"><?php echo form_error('religion'); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('father_occupation'); ?></label>
                                            <input id="father_occupation" name="father_occupation" placeholder="" type="text" class="form-control" value="<?php echo set_value('father_occupation'); ?>" />
                                            <span class="text-danger"><?php echo form_error('father_occupation'); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('mother_occupation'); ?></label>
                                        <input id="mother_occupation" name="mother_occupation" placeholder="" type="text" class="form-control" value="<?php echo set_value('mother_occupation'); ?>" />
                                        <span class="text-danger"><?php echo form_error('mother_occupation'); ?></span>
                                    </div>
                                </div>
                                    <div class="col-md-2 col-xs-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('blood_group'); ?></label>
                                        <?php
                                        ?>
                                        <select class="form-control" rows="3" placeholder="" name="blood_group">
                                            <option value=""><?php echo $this->lang->line('select') ?></option>
                                            <?php foreach ($bloodgroup as $bgkey => $bgvalue) {
                                            ?>
                                                <option value="<?php echo $bgvalue ?>"><?php echo $bgvalue ?></option>

                                            <?php } ?>
                                        </select>

                                        <span class="text-danger"><?php echo form_error('blood_group'); ?></span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo $this->lang->line('category'); ?></label>
                                <input type="hidden" name="cat_id" id="cat_id" value="ssd">
                                <select id="category_id" name="category_id" class="form-control">
                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                    <?php foreach ($categorylist as $category) { ?>
                                        <option value="<?php echo $category['id'] ?>" <?php
                                        if (set_value('category_id') == $category['id']) {
                                           echo "selected=selected";
                                            }
                                            ?>><?php echo $category['category'] ?></option>
                                    <?php $count++;
                                    }
                                    ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('category_id'); ?></span>
                                    </div>
                                    </div>
                              </div>
                              <!------3rd row end------->

                              <!------4th row start------->
                              <div class="row">
                                  <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('admission_date'); ?> <?php echo $this->lang->line('from'); ?></label>
                                        <input id="admission_date" name="admission_date_from" placeholder="" type="text" class="form-control date" value="" />
                                        <span class="text-danger"><?php echo form_error('admission_date'); ?></span>
                                    </div>
                                  </div>
                                  <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('admission_date'); ?> <?php echo $this->lang->line('to'); ?></label>
                                        <input id="admission_date" name="admission_date_to" placeholder="" type="text" class="form-control date" value="" readonly="readonly" />
                                    </div>
                                  </div>
                              </div>
                              <!------4th row end------->
                              <div class="row">
                                  <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('date_of_birth'); ?> <?php echo $this->lang->line('from'); ?></label>
                                                        <input id="dob" name="dob_from" placeholder="" type="text" class="form-control date" value="" />
                                                        <span class="text-danger"><?php echo form_error('dob'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('date_of_birth'); ?> <?php echo $this->lang->line('to'); ?></label>
                                                        <input id="dob" name="dob_to" placeholder="" type="text" class="form-control date" value="" />
                                                        <span class="text-danger"><?php echo form_error('dob'); ?></span>
                                                    </div>
                                                </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-3">
                            <label><?php echo $this->lang->line('file_submit'); ?> </label>
                            <div class="radio" style="margin-top: 2px;">
                                <label><input class="radio-inline" type="radio" name="file_submit" value="Yes" >Yes</label>
                                <label><input class="radio-inline" type="radio" name="file_submit" value="No"  <?php   echo set_value('file_submit') == "No" ? "checked" : ""; ?>>No</label>
                            </div>
                                                </div>
                              </div>

                            <div class="row">
                                <div class="col-md-12">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <button type="submit" name="search" value="search_filter" class="btn btn-primary btn-sm pull-right checkbox-toggle"><i class="fa fa-search"></i> <?php echo $this->lang->line('search'); ?></button>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--./col-md-6-->
            </div>
            <!--./row-->
        </div>

        <?php
        //if (isset($resultlist)) {
        ?>
        <div class="nav-tabs-custom border0 navnoshadow">
            <div class="box-header ptbnull"></div>
            <ul class="nav nav-tabs">
                <!-- <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="fa fa-list"></i> <?php echo $this->lang->line('list'); ?> <?php echo $this->lang->line('view'); ?></a></li> -->
                <!-- <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"><i class="fa fa-newspaper-o"></i> <?php echo $this->lang->line('details'); ?> <?php echo $this->lang->line('view'); ?></a></li> -->
            </ul>
            <div class="tab-content">
                <div class="tab-pane active table-responsive no-padding" id="tab_1">
                    <table class="table table-striped table-bordered table-hover student-list" data-export-title="<?php echo $this->lang->line('student') . " " . $this->lang->line('list'); ?>">
                        <thead>
                            <tr>
                                 <th class="text-right noExport"><?php echo $this->lang->line('action'); ?></th>
                                <th><?php echo $this->lang->line('admission_no'); ?></th>

                                <th><?php echo $this->lang->line('student_name'); ?></th>
                                <th><?php echo $this->lang->line('class'); ?></th>
                                <?php if ($sch_setting->father_name) { ?>
                                    <th><?php echo $this->lang->line('father_name'); ?></th>
                                    <th><?php echo $this->lang->line('father_phone'); ?></th>
                                    <th><?php echo $this->lang->line('father_occupation'); ?></th>
                                    <th><?php echo $this->lang->line('mother_name'); ?></th>
                                    <th><?php echo $this->lang->line('mother_phone'); ?></th>
                                    <th><?php echo $this->lang->line('mother_occupation'); ?></th>
                                <?php } ?>
                                <th><?php echo $this->lang->line('date_of_birth'); ?></th>
                                <th><?php echo $this->lang->line('gender'); ?></th>
                                <?php if ($sch_setting->category) {
                                ?>
                                    <?php if ($sch_setting->category) { ?>
                                        <th><?php echo $this->lang->line('category'); ?></th>
                                    <?php }
                                }
                                if ($sch_setting->mobile_no) {
                                    ?>
                                    <th><?php echo $this->lang->line('mobile_no'); ?></th>
                                <?php
                                } ?>

                                <th class="text-right noExport"><?php echo $this->lang->line('action'); ?></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="tab-pane detail_view_tab" id="tab_2">
                    <?php if (empty($resultlist)) {
                    ?>
                        <div class="alert alert-info"><?php echo $this->lang->line('no_record_found'); ?></div>
                        <?php
                    } else {
                        $count = 1;
                        foreach ($resultlist as $student) {

                            if (empty($student["image"])) {
                                if ($student['gender'] == 'Female') {
                                    $image = "uploads/student_images/default_female.jpg";
                                } else {
                                    $image = "uploads/student_images/default_male.jpg";
                                }
                            } else {
                                $image = $student['image'];
                            }
                        ?>
                            <div class="carousel-row">
                                <div class="slide-row">
                                    <div id="carousel-2" class="carousel slide slide-carousel" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <a href="<?php echo base_url(); ?>student/view/<?php echo $student['id'] ?>">
                                                    <?php if ($sch_setting->student_photo) { ?><img class="img-responsive img-thumbnail width150" alt="<?php echo $student["firstname"] . " " . $student["lastname"] ?>" src="<?php echo base_url() . $image; ?>" alt="Image"><?php } ?></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-content">
                                        <h4><a href="<?php echo base_url(); ?>student/view/<?php echo $student['id'] ?>"> <?php echo $this->customlib->getFullName($student['firstname'], $student['middlename'], $student['lastname'], $sch_setting->middlename, $sch_setting->lastname); ?></a></h4>
                                        <div class="row">
                                            <div class="col-xs-6 col-md-6">
                                                <address>
                                                    <strong><b><?php echo $this->lang->line('class'); ?>: </b><?php echo $student['class'] . "(" . $student['section'] . ")" ?></strong><br>
                                                    <b><?php echo $this->lang->line('admission_no'); ?>: </b><?php echo $student['admission_no'] ?><br />
                                                    <b><?php echo $this->lang->line('date_of_birth'); ?>:
                                                        <?php if ($student["dob"] != null && $student["dob"] != '0000-00-00') {
                                                            echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($student['dob']));
                                                        } ?><br>
                                                        <b><?php echo $this->lang->line('gender'); ?>:&nbsp;</b><?php echo $student['gender'] ?><br>
                                                </address>
                                            </div>
                                            <div class="col-xs-6 col-md-6">
                                                <b><?php echo $this->lang->line('local_identification_no'); ?>:&nbsp;</b><?php echo $student['samagra_id'] ?><br>
                                                <?php if ($sch_setting->guardian_name) { ?>
                                                    <b><?php echo $this->lang->line('guardian_name'); ?>:&nbsp;</b><?php echo $student['guardian_name'] ?><br>
                                                <?php }
                                                if ($sch_setting->guardian_name) { ?>
                                                    <b><?php echo $this->lang->line('guardian_phone'); ?>: </b> <abbr title="Phone"><i class="fa fa-phone-square"></i>&nbsp;</abbr> <?php echo $student['guardian_phone'] ?><br> <?php } ?>
                                                <b><?php echo $this->lang->line('current_address'); ?>:&nbsp;</b><?php echo $student['current_address'] ?> <?php echo $student['city'] ?><br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-footer">
                                        <span class="pull-right buttons">
                                            <a href="<?php echo base_url(); ?>student/view/<?php echo $student['id'] ?>" class="btn btn-default btn-xs" data-toggle="tooltip" title="<?php echo $this->lang->line('show'); ?>">
                                                <i class="fa fa-reorder"></i>
                                            </a>
                                            <?php
                                            if ($this->rbac->hasPrivilege('student', 'can_edit')) {
                                            ?>
                                                <a href="<?php echo base_url(); ?>student/edit/<?php echo $student['id'] ?>" class="btn btn-default btn-xs" data-toggle="tooltip" title="<?php echo $this->lang->line('edit'); ?>">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            <?php
                                            }
                                            if ($this->rbac->hasPrivilege('collect_fees', 'can_add')) {
                                            ?>
                                                <a href="<?php echo base_url(); ?>studentfee/addfee/<?php echo $student['id'] ?>" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="<?php echo $this->lang->line('add_fees'); ?>">
                                                    <?php echo $currency_symbol; ?>
                                                </a>
                                            <?php } ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                    <?php
                        }
                        $count++;
                    }
                    ?>
                </div>
            </div>
        </div>
</div>
<!--./box box-primary -->
<?php
//  }
?>
</div>
</div>
</section>
</div>

<script type="text/javascript">
    function getSectionByClass(class_id, section_id) {
        if (class_id != "" && section_id != "") {
            $('#section_id').html("");
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            $.ajax({
                type: "GET",
                url: base_url + "sections/getByClass",
                data: {
                    'class_id': class_id
                },
                dataType: "json",
                success: function(data) {
                    $.each(data, function(i, obj) {
                        var sel = "";
                        if (section_id == obj.section_id) {
                            sel = "selected";
                        }
                        div_data += "<option value=" + obj.section_id + " " + sel + ">" + obj.section + "</option>";
                    });
                    $('#section_id').append(div_data);
                }
            });
        }
    }
    $(document).ready(function() {
        var class_id = $('#class_id').val();
        var section_id = '<?php echo set_value('section_id') ?>';
        getSectionByClass(class_id, section_id);
        $(document).on('change', '#class_id', function(e) {
            $('#section_id').html("");
            var class_id = $(this).val();
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            $.ajax({
                type: "GET",
                url: base_url + "sections/getByClass",
                data: {
                    'class_id': class_id
                },
                dataType: "json",
                success: function(data) {
                    $.each(data, function(i, obj) {
                        div_data += "<option value=" + obj.section_id + ">" + obj.section + "</option>";
                    });
                    $('#section_id').append(div_data);
                }
            });
        });
    });
</script>
<script>
    $(document).ready(function() {
        emptyDatatable('student-list', 'data');
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {

        $("form.class_search_form button[type=submit]").click(function() {
            $("button[type=submit]", $(this).parents("form")).removeAttr("clicked");
            $(this).attr("clicked", "true");
        });

        $(document).on('submit', '.class_search_form', function(e) {
            e.preventDefault(); // avoid to execute the actual submit of the form.
            var $this = $("button[type=submit][clicked=true]");
            var form = $(this);
            var url = form.attr('action');
            var form_data = form.serializeArray();
              // console.log(form_data); return false;
            form_data.push({
                name: 'search_type',
                value: $this.attr('value')
            });
            $.ajax({
                url: url,
                type: "POST",
                dataType: 'JSON',
                data: form_data, // serializes the form's elements.
                beforeSend: function() {
                    $('[id^=error]').html("");
                    $this.button('loading');
                    resetFields($this.attr('value'));
                },
                success: function(response) { // your success handler

                    if (!response.status) {
                        $.each(response.error, function(key, value) {
                            $('#error_' + key).html(value);
                        });
                    } else {

                        if ($.fn.DataTable.isDataTable('.student-list')) { // if exist datatable it will destrory first
                            $('.student-list').DataTable().destroy();
                        }
                        table = $('.student-list').DataTable({
                            // "scrollX": true,
                            dom: 'Bfrtip',
                            buttons: [{
                                    extend: 'copy',
                                    text: '<i class="fa fa-files-o"></i>',
                                    titleAttr: 'Copy',
                                    className: "btn-copy",
                                    title: $('.student-list').data("exportTitle"),
                                    exportOptions: {
                                        columns: ["thead th:not(.noExport)"]
                                    }
                                },
                                {
                                    extend: 'excel',
                                    text: '<i class="fa fa-file-excel-o"></i>',
                                    titleAttr: 'Excel',
                                    className: "btn-excel",
                                    title: $('.student-list').data("exportTitle"),
                                    exportOptions: {
                                        columns: ["thead th:not(.noExport)"]
                                    }
                                },
                                {
                                    extend: 'csv',
                                    text: '<i class="fa fa-file-text-o"></i>',
                                    titleAttr: 'CSV',
                                    className: "btn-csv",
                                    title: $('.student-list').data("exportTitle"),
                                    exportOptions: {
                                        columns: ["thead th:not(.noExport)"]
                                    }
                                },
                                {
                                    extend: 'pdf',
                                    text: '<i class="fa fa-file-pdf-o"></i>',
                                    titleAttr: 'PDF',
                                    className: "btn-pdf",
                                    title: $('.student-list').data("exportTitle"),
                                    exportOptions: {
                                        columns: ["thead th:not(.noExport)"]
                                    },

                                },
                                {
                                    extend: 'print',
                                    text: '<i class="fa fa-print"></i>',
                                    titleAttr: 'Print',
                                    className: "btn-print",
                                    title: $('.student-list').data("exportTitle"),
                                    customize: function(win) {

                                        $(win.document.body).find('th').addClass('display').css('text-align', 'center');
                                        $(win.document.body).find('table').addClass('display').css('font-size', '14px');
                                        $(win.document.body).find('h1').css('text-align', 'center');
                                    },
                                    exportOptions: {
                                        columns: ["thead th:not(.noExport)"]

                                    }

                                }
                            ],


                            "language": {
                                processing: '<i class="fa fa-spinner fa-spin fa-1x fa-fw"></i><span class="sr-only">Loading...</span> '
                            },
                            "pageLength": 100,
                            "processing": true,
                            "serverSide": true,
                            "ajax": {
                                "url": baseurl + "student/dtadvancestudentlist",
                                "dataSrc": 'data',
                                "type": "POST",
                                'data': response.params,
                            },
                            "drawCallback": function(settings) {

                                $('.detail_view_tab').html("").html(settings.json.student_detail_view);
                            }

                        });



                        //=======================
                    }
                },
                error: function() { // your error handler
                    $this.button('reset');
                },
                complete: function() {
                    $this.button('reset');
                }
            });

        });

    });

    function resetFields(search_type) {

        if (search_type == "search_full") {
            $('#class_id').prop('selectedIndex', 0);
            $('#section_id').find('option').not(':first').remove();
        } else if (search_type == "search_filter") {

            $('#search_text').val("");
        }
    }
</script>