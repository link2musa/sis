<div class="content-wrapper">
    <section class="content-header">
        <h1> 
            <i class="fa fa-user-plus"></i> <?php echo $this->lang->line('student_information'); ?>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                   
                    <form id="form1" action="<?php echo site_url('student/socialwork') ?>" id="employeeform" name="employeeform" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="">
                            <div class="bozero">
                                <h4 class="pagetitleh-whitebg"><?php echo $this->lang->line('student_information'); ?></h4>
                                <div class="around10">
                                    <?php if ($this->session->flashdata('msg')) { ?>
                                        <?php echo $this->session->flashdata('msg') ?>
                                    <?php } ?>
                                    <?php if (isset($error_message)) { ?>
                                        <div class="alert alert-warning"><?php echo $error_message; ?></div>
                                    <?php } ?>
                                    <?php echo $this->customlib->getCSRF(); ?>
                                   
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('roll_no'); ?></label>
                                                <input id="roll_no" name="roll_no" placeholder="" type="text" class="form-control rollnumber" value=""  />
                                                <span class="text-danger"><?php echo form_error('roll_no'); ?></span>
                                                    </div>
                                                    <div class="col-md-4" style="margin-top:23px;">
                                                       <button class="btn btn-sm btn-success syncuserdata"><i class=" fa fa-refresh"></i></button> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="fill_person_name"><?php echo $this->lang->line('fill_person_name'); ?></label>
                                                    <input id="fill_person_name" name="fill_person_name" placeholder="" type="text" class="form-control" value="<?php echo set_value('fill_person_name'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('fill_person_name'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                    <div class="form-group"> 
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('fill_form_date'); ?></label><small class="req"> *</small>
                                                        <input id="fill_form_date" name="fill_form_date" placeholder="" type="text" class="form-control date" value="<?php echo set_value('fill_form_date'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('fill_form_date'); ?></span>
                                                    </div>
                                                </div>
                                        <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('student_name'); ?></label>
                                                    <input id="std_name" name="std_name" placeholder="" type="text" class="form-control" value="<?php echo set_value('std_name'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('std_name'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputFile"> <?php echo $this->lang->line('gender'); ?></label><small class="req"> *</small>
                                                        <select class="form-control" name="gender">
                                                            <?php
                                                            foreach ($genderList as $key => $value) {
                                                            ?>
                                                                <option value="<?php echo $key; ?>" <?php
                                                                 if (set_value('gender') == $key) {
                                                                  echo "selected";
                                                                }
                                                                ?>><?php echo $value; ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="text-danger"><?php echo form_error('gender'); ?></span>
                                                    </div> 
                                                </div>
                                        <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('date_of_birth'); ?></label><small class="req"> *</small>
                                                        <input id="dob" name="dob" placeholder="" type="text" class="form-control date" value="<?php echo set_value('dob'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('dob'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('age'); ?></label>
                                                    <input id="std_name" name="age" placeholder="" type="text" class="form-control" value="<?php echo set_value('age'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('age'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('nationality'); ?></label>

                                            <select id="my-select" class="form-control" name="nationality">
                                                <option selected value="">Select</option>
                                                <?php foreach ($countries as $country) {
                                                        ?>
                                                <option  value="<?php echo $country->country_name; ?>"><?php echo $country->country_name; ?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                         <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('place_of_birth'); ?></label>
                                                    <input id="place_of_birth" name="place_of_birth" placeholder="" type="text" class="form-control" value="<?php echo set_value('place_of_birth'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('place_of_birth'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('total_siblings'); ?></label>
                                                    <input id="total_siblings" name="total_siblings" placeholder="" type="text" class="form-control" value="<?php echo set_value('total_siblings'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('total_siblings'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('total_siblings'); ?></label>
                                                    <input id="total_siblings" name="total_siblings" placeholder="" type="text" class="form-control" value="<?php echo set_value('total_siblings'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('total_siblings'); ?></span>
                                                </div>
                                            </div>
                                             <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('total_male'); ?></label>
                                                    <input id="total_male" name="total_male" placeholder="" type="text" class="form-control" value="<?php echo set_value('total_male'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('total_male'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('total_female'); ?></label>
                                                    <input id="total_female" name="total_female" placeholder="" type="text" class="form-control" value="<?php echo set_value('total_female'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('total_female'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('family_rank'); ?></label>
                                                    <input id="family_rank" name="family_rank" placeholder="" type="text" class="form-control" value="<?php echo set_value('family_rank'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('family_rank'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('permanent_home_address'); ?></label>
                                                    <input id="permanent_address" name="permanent_address" placeholder="" type="text" class="form-control" value="<?php echo set_value('permanent_address'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('permanent_address'); ?></span>
                                                </div>
                                            </div>
                                           
                                    </div>
                                   
                                    <div class="bozero">
                                        <h4 class="pagetitleh2"><?php echo $this->lang->line('fathers_information'); ?></h4>
                                        <div class="around10">
                                           <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('father_name'); ?></label>
                                                        <input id="father_name" name="father_name" placeholder="" type="text" class="form-control" value="<?php echo set_value('father_name'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('father_name'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('father_phone'); ?></label>
                                                        <input id="father_phone" name="father_phone" placeholder="" type="text" class="form-control" value="<?php echo set_value('father_phone'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('father_phone'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('address'); ?></label>
                                                        <input id="father_address" name="father_address" placeholder="" type="text" class="form-control" value="<?php echo set_value('father_address'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('father_address'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('education'); ?></label>
                                                        <input id="father_education" name="father_education" placeholder="" type="text" class="form-control" value="<?php echo set_value('father_education'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('father_education'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('working_location'); ?></label>
                                                        <input id="father_work_location" name="father_work_location" placeholder="" type="text" class="form-control" value="<?php echo set_value('father_work_location'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('father_work_location'); ?></span>
                                                    </div>
                                                </div>
                                        </div> 
                                        </div>
                                    </div>


                                      <div class="bozero">
                                        <h4 class="pagetitleh2"><?php echo $this->lang->line('mothers_information'); ?></h4>
                                        <div class="around10">
                                           <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('mother_name'); ?></label>
                                                        <input id="mother_name" name="mother_name" placeholder="" type="text" class="form-control" value="<?php echo set_value('mother_name'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_name'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('mother_phone'); ?></label>
                                                        <input id="mother_phone" name="mother_phone" placeholder="" type="text" class="form-control" value="<?php echo set_value('mother_phone'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_phone'); ?></span>
                                                    </div>
                                                </div>

                                                  <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('address'); ?></label>
                                                        <input id="mother_address" name="mother_address" placeholder="" type="text" class="form-control" value="<?php echo set_value('mother_address'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_address'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('education'); ?></label>
                                                        <input id="mother_education" name="mother_education" placeholder="" type="text" class="form-control" value="<?php echo set_value('mother_education'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_education'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('working_location'); ?></label>
                                                        <input id="mother_work_location" name="mother_work_location" placeholder="" type="text" class="form-control" value="<?php echo set_value('mother_work_location'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_work_location'); ?></span>
                                                    </div>
                                                </div>
                                        </div> 
                                        </div>
                                    </div>
                                    <div class="bozero">
                                        <h4 class="pagetitleh2"><?php echo $this->lang->line('student_social_information'); ?></h4>
                                        <div class="around10">
                                            <div class="row">
                                                 <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('parent_kinship'); ?></label>
                                                    <input id="parent_kinship" name="parent_kinship" placeholder="" type="text" class="form-control" value="<?php echo set_value('parent_kinship'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('parent_kinship'); ?></span>
                                                </div>
                                            </div>
                                               <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('type_of_home'); ?> </label><small class="req"> *</small>
                                                        <select id="type_of_home" name="type_of_home" class="form-control">
                                                            <option value="Flat"><?php echo $this->lang->line('flat'); ?></option>
                                                            <option value="Villa"><?php echo $this->lang->line('villa'); ?></option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('is_live_same_building'); ?> </label><small class="req"> *</small>
                                                        <select id="is_live_same_building" name="is_live_same_building" class="form-control">
                                                            <option value="yes"><?php echo $this->lang->line('yes'); ?></option>
                                                            <option value="no"><?php echo $this->lang->line('no'); ?></option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('child_live_with'); ?> </label><small class="req"> *</small>
                                                        <select id="child_live_with" name="child_live_with" class="form-control">
                                                            <option value="both"><?php echo $this->lang->line('both'); ?></option>
                                                            <option value="Mother"><?php echo $this->lang->line('mother'); ?></option>
                                                            <option value="Father"><?php echo $this->lang->line('father'); ?></option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                                 <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('std_diseases'); ?> </label><small class="req"> *</small>
                                                        <select id="std_diseases" name="std_disease" class="form-control">
                                                       <option value="Convulsions or seizures">Convulsions or seizures</option>
                                                       <option value="Delayed motor development">Delayed motor development</option>
                                                       <option value="Hearing Impairment">Hearing Impairment</option>
                                                       <option value="Visual Impairment">Visual Impairment</option>
                                                       <option value="Learning difficulties">Learning difficulties</option>
                                                       <option value="Developmental disabilities">Developmental disabilities</option>
                                                       <option value="Sensitivity of grace (male gender)">Sensitivity of grace (male gender)</option>
                                                       <option value="Hyperactivity">Hyperactivity</option>
                                                       <option value="Vitamin deficiency">Vitamin deficiency</option>
                                                       <option value="Physical deformities">Physical deformities</option>
                                                       <option value="Attention disorder">Attention disorder</option>
                                                       <option value="Delayed language and communication development">Delayed language and communication development</option>
                                                       <option value="Difficulties in academic achievement">Difficulties in academic achievement</option>
                                                       <option value="Behavioral problems">Behavioral problems</option>
                                                       <option value="Cognitive problems">Cognitive problems</option>
                                                       <option value="Social problems">Social problems</option>
                                                       <option value="Other problems">Other problems</option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                              <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('entered_foster_care'); ?> </label>
                                                        <input id="entered_foster_care" name="entered_foster_care" placeholder="" type="text" class="form-control" value="<?php echo set_value('entered_foster_care'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('entered_foster_care'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('kindergarten'); ?> </label>
                                                        <input id="kindergarten" name="kindergarten" placeholder="" type="text" class="form-control" value="<?php echo set_value('kindergarten'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('kindergarten'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('age_upon_entry'); ?> </label>
                                                        <input id="age_upon_entry" name="age_upon_entry" placeholder="" type="text" class="form-control" value="<?php echo set_value('age_upon_entry'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('age_upon_entry'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('pregnancy_complication'); ?> </label>
                                                        <input id="pregnancy_complication" name="pregnancy_complication" placeholder="" type="text" class="form-control" value="<?php echo set_value('pregnancy_complication'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('pregnancy_complication'); ?></span>
                                                    </div>
                                                </div>
                                                 <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('childbirth_complication'); ?> </label>
                                                        <input id="childbirth_complication" name="childbirth_complication" placeholder="" type="text" class="form-control" value="<?php echo set_value('childbirth_complication'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('childbirth_complication'); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="bozero">
                                        <h4 class="pagetitleh2"><?php echo $this->lang->line('diagnostic_results'); ?></h4>
                                        <div class="around10">
                                            <div class="row">
                                                 
                                               <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('is_child_diagnose_disease'); ?> </label><small class="req"> *</small>
                                                        <select id="is_child_diagnose_disease" name="is_child_diagnose_disease" class="form-control">
                                                            <option value="yes"><?php echo $this->lang->line('yes'); ?></option>
                                                            <option value="no"><?php echo $this->lang->line('no'); ?></option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                              
                                                 <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('result_of_diagnosis'); ?> </label>
                                                        <input id="result_of_diagnosis" name="result_of_diagnosis" placeholder="" type="text" class="form-control" value="<?php echo set_value('result_of_diagnosis'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('result_of_diagnosis'); ?></span>
                                                    </div>
                                                </div>
                                                 <div class="col-md-3">
                                                    <div class="form-group"> 
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('date_of_diagnosis'); ?></label><small class="req"> *</small>
                                                        <input id="date_of_diagnosis" name="date_of_diagnosis" placeholder="" type="text" class="form-control date" value="<?php echo set_value('date_of_diagnosis'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('date_of_diagnosis'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('child_age_at_diagnosis'); ?> </label>
                                                        <input id="child_age_at_diagnosis" name="child_age_at_diagnosis" placeholder="" type="text" class="form-control" value="<?php echo set_value('child_age_at_diagnosis'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('child_age_at_diagnosis'); ?></span>
                                                    </div>
                                                </div>
                                                 <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('diagnostic_side'); ?> </label>
                                                        <input id="diagnostic_side" name="diagnostic_side" placeholder="" type="text" class="form-control" value="<?php echo set_value('diagnostic_side'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('diagnostic_side'); ?></span>
                                                    </div>
                                                </div>
                                                 <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('diagnostic_team'); ?> </label>
                                                        <input id="diagnostic_team" name="diagnostic_team" placeholder="" type="text" class="form-control" value="<?php echo set_value('diagnostic_team'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('diagnostic_team'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('responsible_person_name'); ?> </label>
                                                        <input id="responsible_person_name" name="responsible_person_name" placeholder="" type="text" class="form-control" value="<?php echo set_value('responsible_person_name'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('responsible_person_name'); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    

                                </div>
                            </div>
                            
                           

                           

                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
</div>
</section>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click','.syncuserdata',function(e){
            e.preventDefault();
            var rollnumber = $('.rollnumber').val();
            if(rollnumber.length !=''){

                   $.ajax({
                type: "GET",
                url: baseurl + "student/getStudentSocialData",
                data: {
                    'student_id': rollnumber
                }, 
                dataType: "json",
                beforeSend: function() {
                    
                },
                success: function(data) {
                    $('input[name=father_name]').val(data.father_name);
                    $('input[name=std_name]').val(data.full_name);
                    $('input[name=dob]').val(data.dob);
                    $('input[name=father_phone]').val(data.father_phone);
                    $('input[name=mother_name]').val(data.mother_name);
                    $('input[name=mother_phone]').val(data.mother_phone);
                    $('input[name=permanent_address]').val(data.permanent_address);
                    if(data.reg_data.father_address.length !=""){
                        $('input[name=father_address]').val(data.reg_data.father_address);
                    }
                    if(data.reg_data.place_of_birth.length !=""){
                        $('input[name=place_of_birth]').val(data.reg_data.place_of_birth);
                    }
                    if(data.reg_data.fill_person_name.length !=""){
                        $('input[name=fill_person_name]').val(data.reg_data.fill_person_name);
                    }
                    if(data.reg_data.fill_form_date.length !=""){
                        $('input[name=fill_form_date]').val(data.reg_data.fill_form_date);
                    }
                    if(data.reg_data.age.length !=""){
                        $('input[name=age]').val(data.reg_data.age);
                    }
                    if(data.reg_data.nationality.length !=""){
                        $('select[name=nationality]').val(data.reg_data.nationality);
                    }
                    if(data.reg_data.total_siblings.length !=""){
                        $('input[name=total_siblings]').val(data.reg_data.total_siblings);
                    }
                    if(data.reg_data.total_male.length !=""){
                        $('input[name=total_male]').val(data.reg_data.total_male);
                    }
                    if(data.reg_data.total_female.length !=""){
                        $('input[name=total_female]').val(data.reg_data.total_female);
                    }
                    if(data.reg_data.family_rank.length !=""){
                        $('input[name=family_rank]').val(data.reg_data.family_rank);
                    }
                    if(data.reg_data.parent_kinship.length !=""){
                        $('input[name=parent_kinship]').val(data.reg_data.parent_kinship);
                    }
                    if(data.reg_data.type_of_home.length !=""){
                        $('select[name=type_of_home]').val(data.reg_data.type_of_home);
                    }
                    if(data.reg_data.is_live_same_building.length !=""){
                        $('select[name=is_live_same_building]').val(data.reg_data.is_live_same_building);
                    }
                    if(data.reg_data.child_live_with.length !=""){
                        $('select[name=child_live_with]').val(data.reg_data.child_live_with);
                    }
                    if(data.reg_data.std_disease.length !=""){
                        $('select[name=std_disease]').val(data.reg_data.std_disease);
                    }
                    if(data.reg_data.entered_foster_care.length !=""){
                        $('input[name=entered_foster_care]').val(data.reg_data.entered_foster_care);
                    }
                    if(data.reg_data.kindergarten.length !=""){
                        $('input[name=kindergarten]').val(data.reg_data.kindergarten);
                    }
                    if(data.reg_data.age_upon_entry.length !=""){
                        $('input[name=age_upon_entry]').val(data.reg_data.age_upon_entry);
                    }
                    if(data.reg_data.pregnancy_complication.length !=""){
                        $('input[name=pregnancy_complication]').val(data.reg_data.pregnancy_complication);
                    }
                    if(data.reg_data.childbirth_complication.length !=""){
                        $('input[name=childbirth_complication]').val(data.reg_data.childbirth_complication);
                    }
                    if(data.reg_data.is_child_diagnose_disease.length !=""){
                        $('select[name=is_child_diagnose_disease]').val(data.reg_data.is_child_diagnose_disease);
                    }
                    if(data.reg_data.result_of_diagnosis.length !=""){
                        $('input[name=result_of_diagnosis]').val(data.reg_data.result_of_diagnosis);
                    }
                    if(data.reg_data.date_of_diagnosis.length !=""){
                        $('input[name=date_of_diagnosis]').val(data.reg_data.date_of_diagnosis);
                    }
                    if(data.reg_data.child_age_at_diagnosis.length !=""){
                        $('input[name=child_age_at_diagnosis]').val(data.reg_data.child_age_at_diagnosis);
                    }
                    if(data.reg_data.diagnostic_side.length !=""){
                        $('input[name=diagnostic_side]').val(data.reg_data.diagnostic_side);
                    }
                    if(data.reg_data.diagnostic_team.length !=""){
                        $('input[name=diagnostic_team]').val(data.reg_data.diagnostic_team);
                    }
                    if(data.reg_data.responsible_person_name.length !=""){
                        $('input[name=responsible_person_name]').val(data.reg_data.responsible_person_name);
                    }
                    if(data.reg_data.father_education.length !=""){
                        $('input[name=father_education]').val(data.reg_data.father_education);
                    }

                    if(data.reg_data.father_work_location.length !=""){
                        $('input[name=father_work_location]').val(data.reg_data.father_work_location);
                    }
                    if(data.reg_data.mother_address.length !=""){
                        $('input[name=mother_address]').val(data.reg_data.mother_address);
                    }
                     if(data.reg_data.mother_education.length !=""){
                        $('input[name=mother_education]').val(data.reg_data.mother_education);
                    }
                    if(data.reg_data.mother_work_location.length !=""){
                        $('input[name=mother_work_location]').val(data.reg_data.mother_work_location);
                    }
                    if(data.reg_data.no_of_family_members.length !=""){
                        $('input[name=no_of_family_members]').val(data.reg_data.no_of_family_members);
                    }
                    if(data.reg_data.how_old_child.length !=""){
                        $('input[name=how_old_child]').val(data.reg_data.how_old_child);
                    }
                    if(data.reg_data.type_of_illness.length !=""){
                        $('input[name=type_of_illness]').val(data.reg_data.type_of_illness);
                    }
                    if(data.reg_data.how_old_was_std_illness.length !=""){
                        $('input[name=how_old_was_std_illness]').val(data.reg_data.how_old_was_std_illness);
                    }
                    if(data.reg_data.child_live_with.length !=""){
                        $('#child_live_with').val(data.reg_data.child_live_with).trigger("change");
                         $('.live_with_single').css("display", "block");
                    }
                    if(data.reg_data.is_previous_illness.length !=""){
                        $('#is_previous_illness').val(data.reg_data.is_previous_illness).trigger("change");
                         // $('.live_with_single').css("display", "block");
                    }
                    if(data.reg_data.live_with_one_parent.length !=""){
                        $('#live_with_one_parent').val(data.reg_data.live_with_one_parent).trigger("change");
                    }
                    if(data.reg_data.treatment_of_student.length !=""){
                        $('#treatment_of_student').val(data.reg_data.treatment_of_student).trigger("change");
                    }
                     if(data.reg_data.educational_followup.length !=""){
                        $('#educational_followup').val(data.reg_data.educational_followup).trigger("change");
                    }
                     if(data.reg_data.panadol_permission.length !=""){
                        $('#panadol_permission').val(data.reg_data.panadol_permission).trigger("change");
                    }
                },
                complete: function() {
                }
            });

            }
        })

        $('body').on('change','#child_live_with',function(e){
            e.preventDefault();
            var val = $(this).val();
            if(val == 'Mother' || val == 'Father'){
                $('.live_with_single').css("display", "block");
                $('.how_old_child_cls').css("display", "block");
            }else{
                 $('.live_with_single').css("display", "none");
                 $('.how_old_child_cls').css("display", "none");
            }
        })
         $('body').on('change','#is_previous_illness',function(e){
            e.preventDefault();
            var val = $(this).val();
            if(val == 'Yes'){
                $('.type_of_illness_cls').css("display", "block");
                $('.how_old_was_std_illness_cls').css("display", "block");
                $('.continue_illness_cls').css("display", "block");
            }else{
                 $('.type_of_illness_cls').css("display", "none");
                 $('.how_old_was_std_illness_cls').css("display", "none");
                 $('.continue_illness_cls').css("display", "none");
            }
        })
    })
    
</script>