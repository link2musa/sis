<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-user-plus"></i> <?php echo $this->lang->line('student_information'); ?>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="pull-right box-tools impbtntitle">
                        <?php if ($this->rbac->hasPrivilege('import_student', 'can_view')) { ?>
                            <a href="<?php echo site_url('student/import') ?>">
                                <button class="btn btn-primary btn-sm"><i class="fa fa-upload"></i> <?php echo $this->lang->line('import_student'); ?></button>
                            </a>
                        <?php }
                        ?>
                    </div>
                    <form id="form1" action="<?php echo site_url('student/create') ?>" id="employeeform" name="employeeform" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="">
                            <div class="bozero">
                                <h4 class="pagetitleh-whitebg"><?php echo $this->lang->line('student'); ?> <?php echo $this->lang->line('admission'); ?> </h4>
                                <div class="around10">
                                    <?php if ($this->session->flashdata('msg')) { ?>
                                        <?php echo $this->session->flashdata('msg') ?>
                                    <?php } ?>
                                    <?php if (isset($error_message)) { ?>
                                        <div class="alert alert-warning"><?php echo $error_message; ?></div>
                                    <?php } ?>
                                    <?php echo $this->customlib->getCSRF(); ?>
                                    <input type="hidden" id="std_id" name="student_id" value="0">
                                    <input type="hidden" name="sibling_name" value="<?php echo set_value('sibling_name'); ?>" id="sibling_name_next">
                                    <input type="hidden" name="sibling_id" value="<?php echo set_value('sibling_id', 0); ?>" id="sibling_id">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('roll_no'); ?></label>
                                                <input id="roll_no" name="roll_no" placeholder="" type="text" class="form-control" value="<?php echo $last_student->id + 1; ?>"  />
                                                <span class="text-danger"><?php echo form_error('roll_no'); ?></span>
                                            </div>
                                        </div>
                                        
                                        <?php if (!$adm_auto_insert) { ?>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('admission_no'); ?></label> <small class="req"> *</small>

                                                    <input autofocus="" id="admission_no" name="admission_no" placeholder="" type="text" class="form-control" value="<?php echo set_value('admission_no'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('admission_no'); ?></span>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php if ($sch_setting->roll_no) { ?>

                                        <?php } ?>

                                        <!-- <div class="col-md-3">

                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('section'); ?></label><small class="req"> *</small>
                                                <select id="section_id" name="section_id" class="form-control">
                                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                </select>
                                                <span class="text-danger"><?php echo form_error('section_id'); ?></span>
                                            </div>
                                        </div> -->

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('faculty'); ?></label> <small class="req"> *</small>
                                                <select id="faculty_id" name="faculty_id" class="form-control">
                                                    <?php
                                                foreach ($faculties as $faculty) {
                                                ?>
                                                    <option value="<?php echo $faculty['id'] ?>" <?php
                                                    if (set_value('faculty_id') == $faculty['id']) {
                                                        echo "selected=selected";
                                                    }
                                                    ?>><?php echo $faculty['name'] ?></option>
                                                <?php
                                                }
                                                   ?>
                                                </select>
                                                <span class="text-danger"><?php echo form_error('faculty_id'); ?></span>
                                            </div>
                                        </div>


                                        <!-- <div class="row"> -->
                                        <!-- <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('house'); ?> </label><small class="req"> *</small>
                                                <select id="class_grade" name="class_grade" class="form-control">
                                                    <?php
                                                    foreach ($grades as $grade) {
                                                    ?>
                                                        <option value="<?php echo $grade['id'] ?>" <?php if (set_value('class_grade') == $grade['id']) echo "selected=selected"; ?>><?php echo $grade['house_name'] ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>

                                            </div>
                                        </div> -->

                                        <!-- </div> -->
                                        <?php if ($sch_setting->category) { ?>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('category'); ?></label>
                                                    <select id="category_id" name="category_id" class="form-control">
                                                        <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                        <?php foreach ($categorylist as $category) { ?>
                                                            <option value="<?php echo $category['id'] ?>" <?php
                                                                                                            if (set_value('category_id') == $category['id']) {
                                                                                                                echo "selected=selected";
                                                                                                            }
                                                                                                            ?>><?php echo $category['category'] ?></option>
                                                        <?php $count++;
                                                        }
                                                        ?>
                                                    </select>
                                                    <span class="text-danger"><?php echo form_error('category_id'); ?></span>
                                                </div>
                                            </div>
                                        <?php }

                                        if ($sch_setting->cast) { ?>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('cast'); ?></label>
                                                    <input id="cast" name="cast" placeholder="" type="text" class="form-control" value="<?php echo set_value('cast'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('cast'); ?></span>
                                                </div>
                                            </div>
                                        <?php }
                                        if ($sch_setting->mobile_no) { ?>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('mobile_no'); ?></label>
                                                    <input id="mobileno" name="mobileno" placeholder="" type="text" class="form-control" value="<?php echo set_value('mobileno'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('mobileno'); ?></span>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="row">
                                        <?php if ($sch_setting->admission_date) { ?>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('admission_date'); ?></label>
                                                    <input id="admission_date" name="admission_date" placeholder="" type="text" class="form-control date" value="<?php echo set_value('admission_date', date($this->customlib->getSchoolDateFormat())); ?>" readonly="readonly" />
                                                    <span class="text-danger"><?php echo form_error('admission_date'); ?></span>
                                                </div>
                                            </div>
                                        <?php }

                                        ?> 
                                        <div class="col-md-3 col-xs-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('house') ?></label>
                                                <select class="form-control" rows="3" placeholder="" name="reg_batch_id">
                                                    <?php foreach ($regbatchlist as $hkey => $batch) {
                                                    ?>
                                                        <option value="<?php echo $batch["id"] ?>"><?php echo $batch["name"] ?></option>
                                                    <?php } ?>
                                                </select>
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('campus') ?></label>
                                                <select class="form-control" rows="3" placeholder="" name="campus_id">
                                                    <?php foreach ($campuses as $hkey => $campus) {
                                                    ?>
                                                        <option value="<?php echo $campus["id"] ?>"><?php echo $campus["name"] ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="text-danger"><?php echo form_error('campus_id'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('fees') ?> <?php echo $this->lang->line('category') ?></label>
                                                <select class="form-control" rows="3" placeholder="" name="feecategory">
                                                    <?php foreach ($feecategory as $k => $cat) {
                                                    ?>
                                                        <option value="<?php echo $cat["id"] ?>"><?php echo $cat["category_name"] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>


                                        <?php if ($sch_setting->student_height) { ?>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('height'); ?></label>
                                                    <?php ?>
                                                    <input type="text" name="height" class="form-control" value="<?php echo set_value('height'); ?>">
                                                    <span class="text-danger"><?php echo form_error('height'); ?></span>
                                                </div>
                                            </div>
                                        <?php }
                                        if ($sch_setting->student_weight) { ?>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('weight'); ?></label>
                                                    <?php ?>
                                                    <input type="text" name="weight" class="form-control" value="<?php echo set_value('weight'); ?>">
                                                    <span class="text-danger"><?php echo form_error('weight'); ?></span>
                                                </div>
                                            </div>
                                        <?php }
                                        if ($sch_setting->measurement_date) { ?>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('measurement_date'); ?></label>
                                                    <?php ?>
                                                    <input type="text" id="measure_date" value="<?php echo set_value('measure_date', date($this->customlib->getSchoolDateFormat())); ?>" name="measure_date" class="form-control date">
                                                    <span class="text-danger"><?php echo form_error('measure_date'); ?></span>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="col-md-3" style="display:none;">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('fees_discount'); ?></label>
                                                <input id="fees_discount" name="fees_discount" placeholder="" type="text" class="form-control" value="<?php echo set_value('fees_discount', 0); ?>" />
                                                <span class="text-danger"><?php echo form_error('fees_discount'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bozero">
                                        <h4 class="pagetitleh2"><?php echo $this->lang->line('personal_details'); ?></h4>
                                        <div class="around10">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('first_name'); ?></label><small class="req"> *</small>
                                                        <input id="firstname" name="firstname" placeholder="" type="text" class="form-control" value="<?php echo set_value('firstname'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('firstname'); ?></span>
                                                    </div>
                                                </div>
                                                <?php if ($sch_setting->middlename) { ?>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('middle_name'); ?></label>
                                                            <input id="middlename" name="middlename" placeholder="" type="text" class="form-control" value="<?php echo set_value('middlename'); ?>" />
                                                            <span class="text-danger"><?php echo form_error('middlename'); ?></span>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php if ($sch_setting->lastname) { ?>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('last_name'); ?></label>
                                                            <input id="lastname" name="lastname" placeholder="" type="text" class="form-control" value="<?php echo set_value('lastname'); ?>" />
                                                            <span class="text-danger"><?php echo form_error('lastname'); ?></span>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('eng_name'); ?></label>
                                                        <input id="eng_name" name="eng_name" placeholder="" type="text" class="form-control" value="<?php echo set_value('eng_name'); ?>" />
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('date_of_birth'); ?></label><small class="req"> *</small>
                                                        <input id="dob" name="dob" placeholder="" type="text" class="form-control date" value="<?php echo set_value('dob'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('dob'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputFile"> <?php echo $this->lang->line('gender'); ?></label><small class="req"> *</small>
                                                        <select class="form-control" name="gender">
                                                            <?php
                                                            foreach ($genderList as $key => $value) {
                                                            ?>
                                                                <option value="<?php echo $key; ?>" <?php
                                                                                                    if (set_value('gender') == $key) {
                                                                                                        echo "selected";
                                                                                                    }
                                                                                                    ?>><?php echo $value; ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="text-danger"><?php echo form_error('gender'); ?></span>
                                                    </div>
                                                </div>
                                                <?php if ($sch_setting->student_photo) { ?>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="exampleInputFile"><?php echo $this->lang->line('student') . " " . $this->lang->line('photo'); ?></label>
                                                            <div><input class="filestyle form-control" type='file' name='file' id="file" size='20' />
                                                            </div>
                                                            <span class="text-danger"><?php echo form_error('file'); ?></span>
                                                        </div>
                                                    </div>
                                                <?php
                                                } ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="bozero">
                                        <h4 class="pagetitleh2"><?php echo $this->lang->line('academic_details'); ?></h4>
                                        <div class="around10">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('academic_year'); ?> </label><small class="req"> *</small>
                                                        <select id="academic_year" name="academic_year" class="form-control">
                                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                            <?php
                                                            foreach ($sessionlist as $session) {
                                                            ?>
                                                                <option value="<?php echo $session['id'] ?>" <?php if ($session['is_active'] != "no") echo "selected=selected"; ?> <?php if (set_value('academic_year') == $session['id']) echo "selected=selected"; ?> <?php if ($session['id'] == $active_session_id){echo "selected=selected"; } ?>  ><?php echo $session['session'] ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>

                                                        <span class="text-danger"><?php echo form_error('academic_year'); ?></span>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('class'); ?></label><small class="req"> *</small>
                                                        <select id="class_id" name="class_id" class="form-control">
                                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                            <?php
                                                            foreach ($classlist as $class) {
                                                            ?>
                                                                <option value="<?php echo $class['id'] ?>" <?php
                                                                                                            if (set_value('class_id') == $class['id']) {
                                                                                                                echo "selected=selected";
                                                                                                            }
                                                                                                            ?>><?php echo $class['class'] ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="text-danger"><?php echo form_error('class_id'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('section'); ?> </label><small class="req"> *</small>
                                                        <select id="section_id" name="section_id" class="form-control">
                                                            <option value=""><?php echo $this->lang->line('section'); ?></option>
                                                            <!-- <?php
                                                                    foreach ($sections as $section) {
                                                                    ?>
                                                                <option value="<?php echo $section['id'] ?>" <?php if (set_value('section_id') == $section['id']) echo "selected=selected"; ?>><?php echo $section['name'] ?></option>
                                                            <?php
                                                                    }
                                                            ?> -->
                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('session'); ?> </label><small class="req"> *</small>
                                                        <select id="session" name="session" class="form-control">
                                                            <option value="Morning">Morning</option>
                                                            <option value="Afternoon">Afternoon</option>
                                                            <option value="Evening">Evening</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('specialization'); ?> </label>
                                                        <select id="specialization" name="specialization" class="form-control">
                                                            <option value="None">None</option>
                                                            <option value="FSC">FSC</option>
                                                            <option value="IGCSE">IGCSE</option>
                                                            <option value="Arts">Arts</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                            <label><?php echo $this->lang->line('status'); ?></label><small class="req"> *</small>
                            <div class="radio" style="margin-top: 2px;">
                                <label><input class="radio-inline" type="radio" name="status" value="Regular" checked>Regular</label>
                                <label><input class="radio-inline" type="radio" name="status" value="Private" <?php   echo set_value('status') == "Private" ? "checked" : ""; ?>>Private</label>
                            </div>
                                                    <span class="text-danger"><?php echo form_error('status'); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tshadow mb25 bozero">
                                            <h4 class="pagetitleh2">
                                                <?php echo $this->lang->line('student_login_details'); ?>
                                            </h4>

                                            <div class="row around10"> 
                                                
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('std_username'); ?></label>
                                                        <input id="std_username" name="std_username" placeholder="" type="text" class="form-control" value="<?php echo set_value('std_username'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('std_username'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('std_password'); ?></label>
                                                        <input id="std_password" name="std_password" placeholder="" type="text" class="form-control" value="<?php echo set_value('std_password'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('std_password'); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="bozero">
                                        <h4 class="pagetitleh2"><?php echo $this->lang->line('contact_details'); ?></h4>
                                        <div class="around10">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('address'); ?></label>
                                                        <input id="address" name="address" placeholder="" type="text" class="form-control" value="<?php echo set_value('address'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('address'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('contact_1'); ?></label><small class="req"> *</small>
                                                        <input id="contact_1" name="contact_1" placeholder="" type="number" class="form-control" value="<?php echo set_value('contact_1'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('contact_1'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('contact_2'); ?></label>
                                                        <input id="contact_2" name="contact_2" placeholder="" type="number" class="form-control" value="<?php echo set_value('contact_2'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('contact_2'); ?></span>
                                                    </div>
                                                </div>
                                                <?php if ($sch_setting->student_email) { ?>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('email'); ?></label>
                                                            <input id="email" name="email" placeholder="" type="email" class="form-control" value="<?php echo set_value('email'); ?>" />
                                                            <span class="text-danger"><?php echo form_error('email'); ?></span>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="col-md-3">
                            <label><?php echo $this->lang->line('file_submit'); ?> </label>
                            <div class="radio" style="margin-top: 2px;">
                                <label><input class="radio-inline" type="radio" name="file_submit" value="Yes" >Yes</label>
                                <label><input class="radio-inline" type="radio" name="file_submit" value="No" checked <?php   echo set_value('file_submit') == "No" ? "checked" : ""; ?>>No</label>
                            </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 pt25">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row"> 
                                                        <button type="button" class="btn btn-sm mysiblings anchorbtn "><i class="fa fa-plus"></i> <?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('sibling'); ?></button>
                                                        <button type="button" class="btn btn-sm add_parent_by_file anchorbtn "><i class="fa fa-plus"></i> <?php echo $this->lang->line('add_parent_by_file'); ?></button>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div id='sibling_id' class="pt6"> <span id="sibling_name" class="label label-success "><?php echo set_value('sibling_name'); ?></span></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <?php
                                        echo display_custom_fields('students');
                                        ?>
                                    </div>

                                </div>
                            </div>
                            <?php if (($sch_setting->father_name) || ($sch_setting->father_phone) || ($sch_setting->father_occupation) || ($sch_setting->father_pic) || ($sch_setting->mother_name) || ($sch_setting->mother_phone) || ($sch_setting->mother_occupation) || ($sch_setting->mother_pic) || ($sch_setting->guardian_name) || ($sch_setting->guardian_occupation) || ($sch_setting->guardian_relation) || ($sch_setting->guardian_phone) || ($sch_setting->guardian_email) || ($sch_setting->guardian_pic) || ($sch_setting->guardian_address)) { ?>
                                <div class="bozero">
                                    <h4 class="pagetitleh2"><?php echo $this->lang->line('parent_guardian_detail'); ?></h4>
                                    <div class="around10">
                                        <div class="row">
                                            <div class="col-md-3">
                                            <div class="form-group"> 
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('file_no'); ?></label>
                                                <input id="file_no" name="file_no" placeholder="" type="text" class="form-control" value="<?php echo $last_file_no->parent_id + 2; ?>"  />
                                            </div>
                                        </div>
                                            <?php if ($sch_setting->father_name) { ?>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('father_name'); ?></label>
                                                        <input id="father_name" name="father_name" placeholder="" type="text" class="form-control" value="<?php echo set_value('father_name'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('father_name'); ?></span>
                                                    </div>
                                                </div>
                                            <?php }
                                            if ($sch_setting->father_phone) { ?>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('father_phone'); ?></label>
                                                        <input id="father_phone" name="father_phone" placeholder="" type="text" class="form-control" value="<?php echo set_value('father_phone'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('father_phone'); ?></span>
                                                    </div>
                                                </div>
                                            <?php }
                                            if ($sch_setting->father_occupation) { ?>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('father_occupation'); ?></label>
                                                        <input id="father_occupation" name="father_occupation" placeholder="" type="text" class="form-control" value="<?php echo set_value('father_occupation'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('father_occupation'); ?></span>
                                                    </div>
                                                </div>
                                            <?php }
                                            if ($sch_setting->father_pic) { ?>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputFile"><?php echo $this->lang->line('father'); ?> <?php echo $this->lang->line('photo'); ?></label>
                                                        <div><input class="filestyle form-control" type='file' name='father_pic' id="file" size='20' />
                                                        </div>
                                                        <span class="text-danger"><?php echo form_error('file'); ?></span>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="row">
                                            <?php if ($sch_setting->mother_name) { ?>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('mother_name'); ?></label>
                                                        <input id="mother_name" name="mother_name" placeholder="" type="text" class="form-control" value="<?php echo set_value('mother_name'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_name'); ?></span>
                                                    </div>
                                                </div>
                                            <?php }
                                            if ($sch_setting->mother_phone) { ?>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('mother_phone'); ?></label>
                                                        <input id="mother_phone" name="mother_phone" placeholder="" type="text" class="form-control" value="<?php echo set_value('mother_phone'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_phone'); ?></span>
                                                    </div>
                                                </div>
                                            <?php }
                                            if ($sch_setting->mother_occupation) { ?>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('mother_occupation'); ?></label>
                                                        <input id="mother_occupation" name="mother_occupation" placeholder="" type="text" class="form-control" value="<?php echo set_value('mother_occupation'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_occupation'); ?></span>
                                                    </div>
                                                </div>
                                            <?php }
                                            if ($sch_setting->mother_pic) { ?>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputFile"><?php echo $this->lang->line('mother'); ?> <?php echo $this->lang->line('photo'); ?></label>
                                                        <div><input class="filestyle form-control" type='file' name='mother_pic' id="file" size='20' />
                                                        </div>
                                                        <span class="text-danger"><?php echo form_error('file'); ?></span>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <?php
                                        if ($sch_setting->guardian_name) {
                                        ?>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label><?php echo $this->lang->line('if_guardian_is'); ?><small class="req"> *</small>&nbsp;&nbsp;&nbsp;</label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="guardian_is" <?php
                                                                                                echo set_value('guardian_is') == "father" ? "checked" : "";
                                                                                                ?> value="father"> <?php echo $this->lang->line('father'); ?>
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="guardian_is" <?php
                                                                                                echo set_value('guardian_is') == "mother" ? "checked" : "";
                                                                                                ?> value="mother"> <?php echo $this->lang->line('mother'); ?>
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="guardian_is" <?php
                                                                                                echo set_value('guardian_is') == "other" ? "checked" : "";
                                                                                                ?> value="other"> <?php echo $this->lang->line('other'); ?>
                                                    </label>
                                                    <span class="text-danger"><?php echo form_error('guardian_is'); ?></span>
                                                </div>
                                            </div>
                                        <?php
                                        }
                                        ?>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <?php
                                                    if ($sch_setting->guardian_name) {
                                                    ?>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('guardian_name'); ?></label><small class="req"> *</small>
                                                                <input id="guardian_name" name="guardian_name" placeholder="" type="text" class="form-control" value="<?php echo set_value('guardian_name'); ?>" />
                                                                <span class="text-danger"><?php echo form_error('guardian_name'); ?></span>
                                                            </div>
                                                        </div>
                                                    <?php }
                                                    if ($sch_setting->guardian_relation) { ?>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('guardian_relation'); ?></label>
                                                                <input id="guardian_relation" name="guardian_relation" placeholder="" type="text" class="form-control" value="<?php echo set_value('guardian_relation'); ?>" />
                                                                <span class="text-danger"><?php echo form_error('guardian_relation'); ?></span>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="row">
                                                    <?php
                                                    if ($sch_setting->guardian_phone) {
                                                    ?>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('guardian_phone'); ?></label><small class="req"> *</small>
                                                                <input id="guardian_phone" name="guardian_phone" placeholder="" type="text" class="form-control" value="<?php echo set_value('guardian_phone'); ?>" />
                                                                <span class="text-danger"><?php echo form_error('guardian_phone'); ?></span>
                                                            </div>
                                                        </div>
                                                    <?php }
                                                    if ($sch_setting->guardian_occupation) {
                                                    ?>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('guardian_occupation'); ?></label>
                                                                <input id="guardian_occupation" name="guardian_occupation" placeholder="" type="text" class="form-control" value="<?php echo set_value('guardian_occupation'); ?>" />
                                                                <span class="text-danger"><?php echo form_error('guardian_occupation'); ?></span>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <?php if ($sch_setting->guardian_email) { ?>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('guardian_email'); ?></label>
                                                        <input id="guardian_email" name="guardian_email" placeholder="" type="text" class="form-control" value="<?php echo set_value('guardian_email'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('guardian_email'); ?></span>
                                                    </div>
                                                </div>
                                            <?php }
                                            if ($sch_setting->guardian_pic) { ?>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputFile"><?php echo $this->lang->line('guardian'); ?> <?php echo $this->lang->line('photo'); ?></label>
                                                        <div><input class="filestyle form-control" type='file' name='guardian_pic' id="file" size='20' />
                                                        </div>
                                                        <span class="text-danger"><?php echo form_error('file'); ?></span>
                                                    </div>
                                                </div>
                                            <?php }
                                            if ($sch_setting->guardian_address) { ?>
                                                <div class="col-md-6">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('guardian_address'); ?></label>
                                                    <textarea id="guardian_address" name="guardian_address" placeholder="" class="form-control" rows="2"><?php echo set_value('guardian_address'); ?></textarea>
                                                    <span class="text-danger"><?php echo form_error('guardian_address'); ?></span>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                            <div class="box-group collapsed-box">
                                <div class="panel box collapsed-box border0 mb0">
                                    <div class="addmoredetail-title">

                                        <a data-widget="collapse" data-original-title="Collapse" class="collapsed btn boxplus">
                                            <i class="fa fa-fw fa-plus"></i><?php echo $this->lang->line('additional_personal_details'); ?>
                                        </a>

                                    </div>
                                    <div class="box-body">
                                        <div class="row around10">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label><?php echo $this->lang->line('nationality'); ?></label>
                                                    <select id="my-select" class="form-control" name="nationality">
                                                        <?php foreach ($countries as $country) {
                                                                ?>
                                                        <option <?php if($country->country_name == 'Libyan'){ echo "selected"; } ?> value="<?php echo $country->country_name; ?>"><?php echo $country->country_name; ?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label><?php echo $this->lang->line('national_id'); ?></label>
                                                    <input id="national_id" name="national_id" placeholder="" type="text" class="form-control" value="<?php echo set_value('national_id'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('national_id'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label><?php echo $this->lang->line('medical_condition'); ?></label>
                                                    <input id="medical_condition" name="medical_condition" placeholder="" type="text" class="form-control" value="<?php echo set_value('medical_condition'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('medical_condition'); ?></span>
                                                </div>
                                            </div>
                                             <?php if ($sch_setting->religion) { ?>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('religion'); ?></label>
                                                            <input id="religion" name="religion" placeholder="" type="text" class="form-control" value="<?php echo set_value('religion'); ?>" />
                                                            <span class="text-danger"><?php echo form_error('religion'); ?></span>
                                                        </div>
                                                    </div>
                                                <?php }
                                                if ($sch_setting->is_blood_group) {
                                                ?>
                                                    <div class="col-md-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('blood_group'); ?></label>
                                                            <?php
                                                            ?>
                                                            <select class="form-control" rows="3" placeholder="" name="blood_group">
                                                                <option value=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php foreach ($bloodgroup as $bgkey => $bgvalue) {
                                                                ?>
                                                                    <option value="<?php echo $bgvalue ?>"><?php echo $bgvalue ?></option>

                                                                <?php } ?>
                                                            </select>

                                                            <span class="text-danger"><?php echo form_error('blood_group'); ?></span>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            <div class="col-md-3">
                                                <label><?php echo $this->lang->line('transportation'); ?></label><small class="req"> *</small>
                                                <div class="radio" style="margin-top: 2px;">
                                                    <label><input class="radio-inline" type="radio" id="transportation-yes" name="transportation" value="Yes">Yes</label>
                                                    <label><input class="radio-inline" type="radio" id="transportation-no" name="transportation" value="No">No</label>
                                                </div>
                                            </div>



                                        </div>
                                        <div class="row around10">
                                            <div id="div_driver_name" class="col-md-3">
                                                <div class="form-group">
                                                    <label><?php echo $this->lang->line('driver_name'); ?></label>
                                                    <input id="driver_name" name="driver_name" placeholder="" type="text" class="form-control" value="<?php echo set_value('driver_name'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('driver_name'); ?></span>
                                                </div>
                                            </div>
                                            <div id="div_driver_contact_no" class="col-md-3">
                                                <div class="form-group">
                                                    <label><?php echo $this->lang->line('driver_contact_no'); ?></label>
                                                    <input id="driver_contact_no" name="driver_contact_no" placeholder="" type="text" class="form-control" value="<?php echo set_value('driver_contact_no'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('driver_contact_no'); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                           

                            <div class="box-group collapsed-box">
                                <div class="panel box collapsed-box border0 mb0">
                                    <div class="addmoredetail-title">

                                        <a data-widget="collapse" data-original-title="Collapse" class="collapsed btn boxplus">
                                            <i class="fa fa-fw fa-plus"></i><?php echo $this->lang->line('add_more_details'); ?>
                                        </a>

                                    </div>
                                    <div class="box-body">
                                        <div class="mb25 bozero">
                                            <h4 class="pagetitleh2"><?php echo $this->lang->line('student'); ?> <?php echo $this->lang->line('address'); ?> <?php echo $this->lang->line('details'); ?></h4>

                                            <div class="row around10">
                                                <?php if ($sch_setting->current_address) { ?>
                                                    <div class="col-md-6">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" id="autofill_current_address" onclick="return auto_fill_guardian_address();">
                                                                <?php echo $this->lang->line('if_guardian_address_is_current_address'); ?>
                                                            </label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('current_address'); ?></label>
                                                            <textarea id="current_address" name="current_address" placeholder="" class="form-control"><?php echo set_value('current_address'); ?><?php echo set_value('current_address'); ?></textarea>
                                                            <span class="text-danger"><?php echo form_error('current_address'); ?></span>
                                                        </div>
                                                    </div>
                                                <?php }
                                                if ($sch_setting->permanent_address) { ?>
                                                    <div class="col-md-6">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" id="autofill_address" onclick="return auto_fill_address();">
                                                                <?php echo $this->lang->line('if_permanent_address_is_current_address'); ?>
                                                            </label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('permanent_address'); ?></label>
                                                            <textarea id="permanent_address" name="permanent_address" placeholder="" class="form-control"></textarea>
                                                            <span class="text-danger"><?php echo form_error('permanent_address'); ?></span>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>

                                        </div>
                                        
                                        <?php if ($sch_setting->route_list) { ?>
                                            <?php
                                            if ($this->module_lib->hasActive('transport')) {
                                            ?>
                                                <div class="tshadow mb25 bozero">
                                                    <h4 class="pagetitleh2">
                                                        <?php echo $this->lang->line('transport') . " " . $this->lang->line('details'); ?>
                                                    </h4>

                                                    <div class="row around10">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('route_list'); ?></label>
                                                                <select class="form-control" id="vehroute_id" name="vehroute_id">

                                                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                                    <?php
                                                                    foreach ($vehroutelist as $vehroute) {
                                                                    ?>
                                                                        <optgroup label=" <?php echo $vehroute->route_title; ?>">
                                                                            <?php
                                                                            $vehicles = $vehroute->vehicles;
                                                                            if (!empty($vehicles)) {
                                                                                foreach ($vehicles as $key => $value) {
                                                                            ?>

                                                                                    <option value="<?php echo $value->vec_route_id ?>" <?php echo set_select('vehroute_id', $value->vec_route_id); ?> data-fee="<?php echo $vehroute->fare; ?>">
                                                                                        <?php echo $value->vehicle_no ?>
                                                                                    </option>
                                                                            <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </optgroup>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <span class="text-danger"><?php echo form_error('transport_fees'); ?></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php
                                            if ($this->module_lib->hasActive('hostel')) {
                                            ?>
                                                <?php if ($sch_setting->hostel_id) { ?>
                                                    <div class="tshadow mb25 bozero">
                                                        <h4 class="pagetitleh2">
                                                            <?php echo $this->lang->line('hostel'); ?></label> <?php echo $this->lang->line('details'); ?></label>
                                                        </h4>

                                                        <div class="row around10">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('hostel'); ?></label>

                                                                    <select class="form-control" id="hostel_id" name="hostel_id">

                                                                        <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                                        <?php
                                                                        foreach ($hostelList as $hostel_key => $hostel_value) {
                                                                        ?>

                                                                            <option value="<?php echo $hostel_value['id'] ?>" <?php echo set_select('hostel_id', $hostel_value['id']); ?>>
                                                                                <?php echo $hostel_value['hostel_name']; ?>
                                                                            </option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                    <span class="text-danger"><?php echo form_error('hostel_id'); ?></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('room_no'); ?></label>
                                                                    <select id="hostel_room_id" name="hostel_room_id" class="form-control">
                                                                        <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                                    </select>
                                                                    <span class="text-danger"><?php echo form_error('hostel_room_id'); ?></span>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                <?php } ?> <?php }
                                                    }
                                                            ?>
                                        <div class="tshadow mb25 bozero">
                                            <h4 class="pagetitleh2"><?php echo $this->lang->line('miscellaneous_details'); ?>
                                            </h4>

                                            <div class="row around10">
                                                <?php if ($sch_setting->bank_account_no) { ?>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('bank_account_no'); ?></label>
                                                            <input id="bank_account_no" name="bank_account_no" placeholder="" type="text" class="form-control" value="<?php echo set_value('bank_account_no'); ?>" />
                                                            <span class="text-danger"><?php echo form_error('bank_account_no'); ?></span>
                                                        </div>
                                                    </div><?php }
                                                        if ($sch_setting->bank_name) { ?>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('bank_name'); ?></label>
                                                            <input id="bank_name" name="bank_name" placeholder="" type="text" class="form-control" value="<?php echo set_value('bank_name'); ?>" />
                                                            <span class="text-danger"><?php echo form_error('bank_name'); ?></span>
                                                        </div>
                                                    </div><?php }
                                                        if ($sch_setting->ifsc_code) { ?>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('ifsc_code'); ?></label>
                                                            <input id="ifsc_code" name="ifsc_code" placeholder="" type="text" class="form-control" value="<?php echo set_value('ifsc_code'); ?>" />
                                                            <span class="text-danger"><?php echo form_error('ifsc_code'); ?></span>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>

                                            <div class="row around10">
                                                <?php if ($sch_setting->national_identification_no) { ?>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">
                                                                <?php echo $this->lang->line('national_identification_no'); ?>
                                                            </label>
                                                            <input id="adhar_no" name="adhar_no" placeholder="" type="text" class="form-control" value="<?php echo set_value('adhar_no'); ?>" />
                                                            <span class="text-danger"><?php echo form_error('adhar_no'); ?></span>
                                                        </div>
                                                    </div>
                                                <?php }
                                                if ($sch_setting->local_identification_no) { ?>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">
                                                                <?php echo $this->lang->line('local_identification_no'); ?>
                                                            </label>
                                                            <input id="samagra_id" name="samagra_id" placeholder="" type="text" class="form-control" value="<?php echo set_value('samagra_id'); ?>" />
                                                            <span class="text-danger"><?php echo form_error('samagra_id'); ?></span>
                                                        </div>
                                                    </div>
                                                <?php }
                                                if ($sch_setting->rte) { ?>
                                                    <div class="col-md-4">
                                                        <label><?php echo $this->lang->line('rte'); ?></label>
                                                        <div class="radio" style="margin-top: 2px;">
                                                            <label><input class="radio-inline" type="radio" name="rte" value="Yes" <?php
                                                                                                                                    echo set_value('rte') == "yes" ? "checked" : "";
                                                                                                                                    ?>><?php echo $this->lang->line('yes'); ?></label>
                                                            <label><input class="radio-inline" checked="checked" type="radio" name="rte" value="No" <?php
                                                                                                                                                    echo set_value('rte') == "no" ? "checked" : "";
                                                                                                                                                    ?>><?php echo $this->lang->line('no'); ?></label>
                                                        </div>
                                                        <span class="text-danger"><?php echo form_error('rte'); ?></span>
                                                    </div>
                                                <?php }
                                                if ($sch_setting->previous_school_details) { ?>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('previous_school_details'); ?></label>
                                                            <textarea class="form-control" rows="3" placeholder="" name="previous_school"></textarea>
                                                            <span class="text-danger"><?php echo form_error('previous_school'); ?></span>
                                                        </div>
                                                    </div>
                                                <?php }
                                                if ($sch_setting->student_note) { ?>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('note'); ?></label>
                                                            <textarea class="form-control" rows="3" placeholder="" name="note"></textarea>
                                                            <span class="text-danger"><?php echo form_error('note'); ?></span>
                                                        </div>
                                                    </div>
                                                <?php } ?>


                                            </div>
                                        </div>
                                        <div id='upload_documents_hide_show'>
                                            <?php if ($sch_setting->upload_documents) { ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="tshadow bozero">
                                                            <h4 class="pagetitleh2"><?php echo $this->lang->line('upload_documents'); ?></h4>

                                                            <div class="row around10">
                                                                <div class="col-md-6">
                                                                    <table class="table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <th style="width: 10px">#</th>
                                                                                <th><?php echo $this->lang->line('title'); ?></th>
                                                                                <th><?php echo $this->lang->line('documents'); ?></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>1.</td>
                                                                                <td><input type="text" name='first_title' class="form-control" placeholder=""></td>
                                                                                <td>
                                                                                    <input class="filestyle form-control" type='file' name='first_doc' id="doc1">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>2.</td>
                                                                                <td><input type="text" name='second_title' class="form-control" placeholder=""></td>
                                                                                <td>
                                                                                    <input class="filestyle form-control" type='file' name='second_doc' id="doc1">
                                                                                </td>
                                                                            </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <table class="table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <th style="width: 10px">#</th>
                                                                                <th><?php echo $this->lang->line('title'); ?></th>
                                                                                <th><?php echo $this->lang->line('documents'); ?></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>3.</td>
                                                                                <td><input type="text" name='fourth_title' class="form-control" placeholder=""></td>
                                                                                <td>
                                                                                    <input class="filestyle form-control" type='file' name='fourth_doc' id="doc1">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>4.</td>
                                                                                <td><input type="text" name='fifth_title' class="form-control" placeholder=""></td>
                                                                                <td>
                                                                                    <input class="filestyle form-control" type='file' name='fifth_doc' id="doc1">
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
</div>
</section>
</div>



<div class="modal fade" id="mySiblingModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title modal_title"></h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="box-body">

                        <input type="hidden" class="form-control" id="transport_student_session_id" value="0" readonly="readonly" />
                        <div class="form-group">
                            <div class="sibling_msg">

                            </div>
                            <label for="inputEmail3" class="col-sm-2 control-label"><?php echo $this->lang->line('class'); ?></label>
                            <div class="col-sm-10">
                                <select id="sibiling_class_id" name="sibiling_class_id" class="form-control">
                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                    <?php
                                    foreach ($classlist as $class) {
                                    ?>
                                        <option value="<?php echo $class['id'] ?>" <?php
                                                                                    if (set_value('sibiling_class_id') == $class['id']) {
                                                                                        echo "selected=selected";
                                                                                    }
                                                                                    ?>><?php echo $class['class'] ?></option>
                                    <?php
                                        $count++;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label"><?php echo $this->lang->line('section'); ?></label>
                            <div class="col-sm-10">
                                <select id="sibiling_section_id" name="sibiling_section_id" class="form-control">
                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                </select>
                                <span class="text-danger" id="transport_amount_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label"><?php echo $this->lang->line('student'); ?>
                            </label>

                            <div class="col-sm-10">
                                <select id="sibiling_student_id" name="sibiling_student_id" class="form-control">
                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                </select>
                                <span class="text-danger" id="transport_amount_fine_error"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-primary add_sibling" id="load" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing"><i class="fa fa-user"></i> <?php echo $this->lang->line('add'); ?></button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="addParentByFileModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title modal_title"></h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="box-body">
 
                        <input type="hidden" class="form-control" id="transport_student_session_id" value="0" readonly="readonly" />
                        <div class="form-group">
                            <div class="sibling_msg">

                            </div>
                           
                        <div class="form-group">
                            <label  class="col-sm-2 control-label"><?php echo $this->lang->line('file_no'); ?>
                            </label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control file_no" name="" >
                                <span class="text-danger" id="transport_amount_fine_error"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-primary add_file_no" id="load" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing"><i class="fa fa-user"></i> <?php echo $this->lang->line('add'); ?></button>
            </div>
        </div>
    </div>
</div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        var date_format = '<?php echo $result = strtr($this->customlib->getSchoolDateFormat(), ['d' => 'dd', 'm' => 'mm', 'Y' => 'yyyy']) ?>';
        var class_id = $('#class_id').val();
        var section_id = '<?php echo set_value('section_id', 0) ?>';
        var hostel_id = $('#hostel_id').val();
        var hostel_room_id = '<?php echo set_value('hostel_room_id', 0) ?>';
        getHostel(hostel_id, hostel_room_id);
        getSectionByClass(class_id, section_id);

        $(document).on('change', '#class_id', function(e) {
            $('#section_id').html("");
            var class_id = $(this).val();
            getSectionByClass(class_id, 0);
        });
        $(document).on('keypress keyup', '#roll_no', function(e) {
            var roll_no = $(this).val();
            $('#admission_no').val('');
            $('#admission_no').val('INT'+roll_no);
        });


        // $('.datetime').datetimepicker({

        // });
        $(".color").colorpicker();

        $("#btnreset").click(function() {
            $("#form1")[0].reset();
        });


        $(document).on('change', '#hostel_id', function(e) {
            var hostel_id = $(this).val();
            getHostel(hostel_id, 0);

        });

        function getSectionByClass(class_id, section_id) {

            if (class_id != "") {
                $('#section_id').html("");
                var base_url = '<?php echo base_url() ?>';
                var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
                var url = "<?php
                            $userdata = $this->customlib->getUserData();
                            if (($userdata["role_id"] == 2)) {
                                echo "getClassTeacherSection";
                            } else {
                                echo "getByClass";
                            }
                            ?>";

                $.ajax({
                    type: "GET",
                    url: base_url + "sections/getByClass",
                    data: {
                        'class_id': class_id
                    },
                    dataType: "json",
                    beforeSend: function() {
                        $('#section_id').addClass('dropdownloading');
                    },
                    success: function(data) {
                        $.each(data, function(i, obj) {
                            var sel = "";
                            if (section_id == obj.section_id) {
                                sel = "selected";
                            }
                            div_data += "<option value=" + obj.section_id + " " + sel + ">" + obj.section + "</option>";
                        });
                        $('#section_id').append(div_data);
                    },
                    complete: function() {
                        $('#section_id').removeClass('dropdownloading');
                    }
                });
            }
        }


        function getHostel(hostel_id, hostel_room_id) {
            if (hostel_room_id == "") {
                hostel_room_id = 0;
            }

            if (hostel_id != "") {

                $('#hostel_room_id').html("");


                var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
                $.ajax({
                    type: "GET",
                    url: baseurl + "admin/hostelroom/getRoom",
                    data: {
                        'hostel_id': hostel_id
                    },
                    dataType: "json",
                    beforeSend: function() {
                        $('#hostel_room_id').addClass('dropdownloading');
                    },
                    success: function(data) {
                        $.each(data, function(i, obj) {
                            var sel = "";
                            if (hostel_room_id == obj.id) {
                                sel = "selected";
                            }

                            div_data += "<option value=" + obj.id + " " + sel + ">" + obj.room_no + " (" + obj.room_type + ")" + "</option>";

                        });
                        $('#hostel_room_id').append(div_data);
                    },
                    complete: function() {
                        $('#hostel_room_id').removeClass('dropdownloading');
                    }
                });
            }
        }

    });

    function auto_fill_guardian_address() {
        if ($("#autofill_current_address").is(':checked')) {
            $('#current_address').val($('#guardian_address').val());
        }
    }

    function auto_fill_address() {
        if ($("#autofill_address").is(':checked')) {
            $('#permanent_address').val($('#current_address').val());
        }
    }
    $('input:radio[name="guardian_is"]').change(
        function() {
            if ($(this).is(':checked')) {
                var value = $(this).val();
                if (value == "father") {
                    var father_relation = "<?php echo $this->lang->line('father'); ?>";
                    $('#guardian_name').val($('#father_name').val());
                    $('#guardian_phone').val($('#father_phone').val());
                    $('#guardian_occupation').val($('#father_occupation').val());
                    $('#guardian_relation').val(father_relation);
                } else if (value == "mother") {
                    var mother_relation = "<?php echo $this->lang->line('mother'); ?>";
                    $('#guardian_name').val($('#mother_name').val());
                    $('#guardian_phone').val($('#mother_phone').val());
                    $('#guardian_occupation').val($('#mother_occupation').val());
                    $('#guardian_relation').val(mother_relation);
                } else {
                    $('#guardian_name').val("");
                    $('#guardian_phone').val("");
                    $('#guardian_occupation').val("");
                    $('#guardian_relation').val("")
                }
            }
        });
</script>

<script type="text/javascript">
    $(".mysiblings").click(function() {
        $('.sibling_msg').html("");
        $('.modal_title').html('<b>' + "<?php echo $this->lang->line('sibling'); ?>" + '</b>');
        $('#mySiblingModal').modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    });
</script>
<script type="text/javascript">
    $(".add_parent_by_file").click(function() {
        $('.sibling_msg').html("");
        $('.modal_title').html('<b>' + "<?php echo $this->lang->line('add_parent_by_file'); ?>" + '</b>');
        $('#addParentByFileModal').modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    });
</script>
<script type="text/javascript">
    $(document).on('change', '#sibiling_class_id', function(e) {
        $('#sibiling_section_id').html("");
        var class_id = $(this).val();
        var base_url = '<?php echo base_url() ?>';
        var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
        $.ajax({
            type: "GET",
            url: base_url + "sections/getByClass",
            data: {
                'class_id': class_id
            },
            dataType: "json",
            success: function(data) {
                $.each(data, function(i, obj) {
                    div_data += "<option value=" + obj.section_id + ">" + obj.section + "</option>";
                });
                $('#sibiling_section_id').append(div_data);
            }
        });
    });

    $(document).on('change', '#sibiling_section_id', function(e) {
        getStudentsByClassAndSection();
    });

    function getStudentsByClassAndSection() {

        $('#sibiling_student_id').html("");
        var class_id = $('#sibiling_class_id').val();
        var section_id = $('#sibiling_section_id').val();
        var student_id = '<?php echo set_value('student_id') ?>';
        var base_url = '<?php echo base_url() ?>';
        var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
        $.ajax({
            type: "GET",
            url: base_url + "student/getByClassAndSection",
            data: {
                'class_id': class_id,
                'section_id': section_id
            },
            dataType: "json",
            success: function(data) {
                $.each(data, function(i, obj) {
                    var sel = "";
                    if (section_id == obj.section_id) {
                        sel = "selected=selected";
                    }


                    if (obj.roll_no == null) {

                        div_data += "<option value=" + obj.id + ">" + obj.full_name + "</option>";


                    } else {
                        div_data += "<option value=" + obj.id + ">" + obj.full_name + " (" + obj.roll_no + ") " + "</option>";
                    }

                });
                $('#sibiling_student_id').append(div_data);
            }
        });
    }

    $(document).on('click', '.add_sibling', function() {
        var student_id = $('#sibiling_student_id').val();
        var base_url = '<?php echo base_url() ?>';
        if (student_id.length > 0) {
            $.ajax({
                type: "GET",
                url: base_url + "student/getStudentRecordByID",
                data: {
                    'student_id': student_id
                },
                dataType: "json",
                success: function(data) {
                    $('#sibling_name').text("Sibling: " + data.full_name);
                    $('#sibling_name_next').val(data.firstname + " " + data.lastname);
                    $('#sibling_id').val(student_id);
                    $('#father_name').val(data.father_name);
                    $('#father_phone').val(data.father_phone);
                    $('#father_occupation').val(data.father_occupation);
                    $('#mother_name').val(data.mother_name);
                    $('#mother_phone').val(data.mother_phone);
                    $('#mother_occupation').val(data.mother_occupation);
                    $('#guardian_name').val(data.guardian_name);
                    $('#guardian_relation').val(data.guardian_relation);
                    $('#guardian_address').val(data.guardian_address);
                    $('#guardian_phone').val(data.guardian_phone);
                    $('#state').val(data.state);
                    $('#city').val(data.city);
                    $('#pincode').val(data.pincode);
                    $('#current_address').val(data.current_address);
                    $('#permanent_address').val(data.permanent_address);
                    $('#guardian_occupation').val(data.guardian_occupation);
                    $("input[name=guardian_is][value='" + data.guardian_is + "']").prop("checked", true);
                    $('#mySiblingModal').modal('hide');
                }
            });
        } else {
            $('.sibling_msg').html("<div class='alert alert-danger text-center'><?php echo $this->lang->line('no_student_selected') ?></div>");
        }

    });



    $(document).on('click', '.add_file_no', function() {
        var student_id = $('#sibiling_student_id').val();

        var file_no = $('.file_no').val();
        var base_url = '<?php echo base_url() ?>';
        if (file_no.length > 0) {
            $.ajax({
                type: "GET",
                url: base_url + "student/getStudentRecordByFileNo",
                data: {
                    'file_no': file_no 
                },
                dataType: "json",
                success: function(data) {
                    if(data.error){
                        $('.sibling_msg').html("<div class='alert alert-danger text-center'><?php echo $this->lang->line('no_student_selected') ?></div>");
                    }else{
                        $('#sibling_name').text("Sibling: " + data.full_name);
                        $('#sibling_name_next').val(data.firstname + " " + data.lastname);
                        $('#sibling_id').val(data.id);
                        $('#file_no').val(file_no);
                        $('#father_name').val(data.father_name);
                        $('#father_phone').val(data.father_phone);
                        $('#father_occupation').val(data.father_occupation);
                        $('#mother_name').val(data.mother_name);
                        $('#mother_phone').val(data.mother_phone);
                        $('#mother_occupation').val(data.mother_occupation);
                        $('#guardian_name').val(data.guardian_name);
                        $('#guardian_relation').val(data.guardian_relation);
                        $('#guardian_address').val(data.guardian_address);
                        $('#guardian_phone').val(data.guardian_phone);
                        $('#state').val(data.state);
                        $('#city').val(data.city); 
                        $('#pincode').val(data.pincode);
                        $('#current_address').val(data.current_address);
                        $('#permanent_address').val(data.permanent_address);
                        $('#guardian_occupation').val(data.guardian_occupation);
                        $("input[name=guardian_is][value='" + data.guardian_is + "']").prop("checked", true);
                        $('#addParentByFileModal').modal('hide');
                    }
                    
                }
            });
        } else {
            $('.sibling_msg').html("<div class='alert alert-danger text-center'><?php echo $this->lang->line('no_student_selected') ?></div>");
        }

    });


</script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/js/savemode.js"></script>