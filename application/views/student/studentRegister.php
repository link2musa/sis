<div class="content-wrapper">
    <section class="content-header">
        <h1> 
            <i class="fa fa-user-plus"></i> <?php echo $this->lang->line('student_information'); ?>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                   
                    <form id="form1" action="<?php echo site_url('student/register') ?>" id="employeeform" name="employeeform" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="">
                            <div class="bozero">
                                <h4 class="pagetitleh-whitebg"><?php echo $this->lang->line('student_information'); ?></h4>
                                <div class="around10">
                                    <?php if ($this->session->flashdata('msg')) { ?>
                                        <?php echo $this->session->flashdata('msg') ?>
                                    <?php } ?>
                                    <?php if (isset($error_message)) { ?>
                                        <div class="alert alert-warning"><?php echo $error_message; ?></div>
                                    <?php } ?>
                                    <?php echo $this->customlib->getCSRF(); ?>
                                   
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('roll_no'); ?></label>
                                                <input id="roll_no" name="roll_no" placeholder="" type="text" class="form-control rollnumber" value=""  />
                                                <span class="text-danger"><?php echo form_error('roll_no'); ?></span>
                                                    </div>
                                                    <div class="col-md-4" style="margin-top:23px;">
                                                       <button class="btn btn-sm btn-success syncuserdata"><i class=" fa fa-refresh"></i></button> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('student_name'); ?></label>
                                                    <input id="std_name" name="std_name" placeholder="" type="text" class="form-control" value="<?php echo set_value('std_name'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('std_name'); ?></span>
                                                </div>
                                            </div>
                                        <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('date_of_birth'); ?></label><small class="req"> *</small>
                                                        <input id="dob" name="dob" placeholder="" type="text" class="form-control date" value="<?php echo set_value('dob'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('dob'); ?></span>
                                                    </div>
                                                </div>
                                         <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('place_of_birth'); ?></label>
                                                    <input id="place_of_birth" name="place_of_birth" placeholder="" type="text" class="form-control" value="<?php echo set_value('place_of_birth'); ?>" />
                                                    <span class="text-danger"><?php echo form_error('place_of_birth'); ?></span>
                                                </div>
                                            </div>

                                    </div>
                                   
                                    <div class="bozero">
                                        <h4 class="pagetitleh2"><?php echo $this->lang->line('fathers_information'); ?></h4>
                                        <div class="around10">
                                           <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('father_name'); ?></label>
                                                        <input id="father_name" name="father_name" placeholder="" type="text" class="form-control" value="<?php echo set_value('father_name'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('father_name'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('father_phone'); ?></label>
                                                        <input id="father_phone" name="father_phone" placeholder="" type="text" class="form-control" value="<?php echo set_value('father_phone'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('father_phone'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('address'); ?></label>
                                                        <input id="father_address" name="father_address" placeholder="" type="text" class="form-control" value="<?php echo set_value('father_address'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('father_address'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('education'); ?></label>
                                                        <input id="father_education" name="father_education" placeholder="" type="text" class="form-control" value="<?php echo set_value('father_education'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('father_education'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('working_location'); ?></label>
                                                        <input id="father_work_location" name="father_work_location" placeholder="" type="text" class="form-control" value="<?php echo set_value('father_work_location'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('father_work_location'); ?></span>
                                                    </div>
                                                </div>
                                        </div> 
                                        </div>
                                    </div>


                                      <div class="bozero">
                                        <h4 class="pagetitleh2"><?php echo $this->lang->line('mothers_information'); ?></h4>
                                        <div class="around10">
                                           <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('mother_name'); ?></label>
                                                        <input id="mother_name" name="mother_name" placeholder="" type="text" class="form-control" value="<?php echo set_value('mother_name'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_name'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('mother_phone'); ?></label>
                                                        <input id="mother_phone" name="mother_phone" placeholder="" type="text" class="form-control" value="<?php echo set_value('mother_phone'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_phone'); ?></span>
                                                    </div>
                                                </div>

                                                  <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('address'); ?></label>
                                                        <input id="mother_address" name="mother_address" placeholder="" type="text" class="form-control" value="<?php echo set_value('mother_address'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_address'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('education'); ?></label>
                                                        <input id="mother_education" name="mother_education" placeholder="" type="text" class="form-control" value="<?php echo set_value('mother_education'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_education'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('working_location'); ?></label>
                                                        <input id="mother_work_location" name="mother_work_location" placeholder="" type="text" class="form-control" value="<?php echo set_value('mother_work_location'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_work_location'); ?></span>
                                                    </div>
                                                </div>
                                        </div> 
                                        </div>
                                    </div>
                                    <div class="bozero">
                                        <h4 class="pagetitleh2"><?php echo $this->lang->line('student_social_information'); ?></h4>
                                        <div class="around10">
                                            <div class="row">
                                                
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('no_of_family_members'); ?></label>
                                                        <input id="no_of_family_members" name="no_of_family_members" placeholder="" type="text" class="form-control" value="<?php echo set_value('no_of_family_members'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('no_of_family_members'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('child_live_with'); ?> </label><small class="req"> *</small>
                                                        <select id="child_live_with" name="child_live_with" class="form-control">
                                                            <option value="both"><?php echo $this->lang->line('both'); ?></option>
                                                            <option value="Mother"><?php echo $this->lang->line('mother'); ?></option>
                                                            <option value="Father"><?php echo $this->lang->line('father'); ?></option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 live_with_single" style="display: none;">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('live_with_one_parent'); ?></label>
                                                         <select id="live_with_one_parent" name="live_with_one_parent" class="form-control">
                                                            <option value="" selected disabled><?php echo $this->lang->line('select'); ?></option>
                                                            <option ><?php echo $this->lang->line('death_of_parent'); ?></option>
                                                            <option><?php echo $this->lang->line('illness_of_parent'); ?></option>
                                                            <option><?php echo $this->lang->line('divorce'); ?></option>
                                                            <option><?php echo $this->lang->line('polygamous_marriage'); ?></option>
                                                            <option><?php echo $this->lang->line('other_reason'); ?></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                 <div class="col-md-3 how_old_child_cls" style="display: none;">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('how_old_child'); ?></label>
                                                        <input id="how_old_child" name="how_old_child" placeholder="" type="text" class="form-control" value="<?php echo set_value('how_old_child'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('how_old_child'); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="bozero">
                                        <h4 class="pagetitleh2"><?php echo $this->lang->line('student_health_history'); ?></h4>
                                        <div class="around10">
                                            <div class="row ">
                                                <div class="col-md-3 is_previous_illness_cls">
                                                    <div class="form-group">
                                                        <label><?php echo $this->lang->line('child_illness_txt'); ?></label><small class="req"> *</small>

                                                        <select class="form-control" id="is_previous_illness" name="is_previous_illness">
                                                            <option value="No"><?php echo $this->lang->line('no'); ?></option>
                                                        <option value="Yes"><?php echo $this->lang->line('yes'); ?></option>
                                                        </select>
                                                        
                                                   
                                                </div>
                                            </div>
                                               <div class="col-md-3 type_of_illness_cls" style="display: none;">
                                                    <div class="form-group">
                                                        
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('type_of_illness'); ?></label>
                                                        <input id="type_of_illness" name="type_of_illness" placeholder="" type="text" class="form-control" value="<?php echo set_value('type_of_illness'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('type_of_illness'); ?></span>
                                                    </div>
                                                </div>
                                               <div class="col-md-3 how_old_was_std_illness_cls" style="display: none;">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('how_old_was_std_illness'); ?></label>

                                                        <input id="how_old_was_std_illness" name="how_old_was_std_illness" placeholder="" type="text" class="form-control" value="<?php echo set_value('how_old_was_std_illness'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('how_old_was_std_illness'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 continue_illness_cls" style="display: none;">
                                                    <div class="form-group">
                                                        <label><?php echo $this->lang->line('continue_illness'); ?></label><small class="req"> *</small>
                                                        <select class="form-control" name="continue_illness">
                                                            <option value="<?php echo $this->lang->line('no'); ?>"><?php echo $this->lang->line('no'); ?></option>
                                                            <option value="<?php echo $this->lang->line('yes'); ?>"><?php echo $this->lang->line('yes'); ?></option>
                                                        </select>
                                                   
                                                </div>
                                            </div>
                                            </div>
                                            <div class="row">
                                                
                                            <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label><?php echo $this->lang->line('panadol_permission'); ?></label><small class="req"> *</small>
                                                        <select class="form-control" name="panadol_permission" id="panadol_permission"> 
                                                            <option value="<?php echo $this->lang->line('no'); ?>"><?php echo $this->lang->line('no'); ?></option>
                                                            <option value="<?php echo $this->lang->line('yes'); ?>"><?php echo $this->lang->line('yes'); ?></option>
                                                        </select>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('treatment_of_student'); ?></label>
                                                         <select id="treatment_of_student" name="treatment_of_student" class="form-control">
                                                            <option value="" selected disabled><?php echo $this->lang->line('select'); ?></option>
                                                            <option value="<?php echo $this->lang->line('overly_protected'); ?>"><?php echo $this->lang->line('overly_protected'); ?></option>
                                                            <option value="<?php echo $this->lang->line('authoritative'); ?>"><?php echo $this->lang->line('authoritative'); ?></option>
                                                            <option value="<?php echo $this->lang->line('advise_punishment'); ?>"> <?php echo $this->lang->line('advise_punishment'); ?></option>
                                                            <option value="<?php echo $this->lang->line('simple_punishment'); ?>"><?php echo $this->lang->line('simple_punishment'); ?></option>
                                                            <option value="<?php echo $this->lang->line('careless'); ?>"> <?php echo $this->lang->line('careless'); ?></option>
                                                            <option value="<?php echo $this->lang->line('spoiled'); ?>"><?php echo $this->lang->line('spoiled'); ?></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                  <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('educational_followup'); ?></label>
                                                         <select id="educational_followup" name="educational_followup" class="form-control">
                                                            <option value="" selected disabled><?php echo $this->lang->line('select'); ?></option>
                                                            <option value="<?php echo $this->lang->line('never'); ?>"><?php echo $this->lang->line('never'); ?></option>
                                                            <option value="<?php echo $this->lang->line('sometimes'); ?>"><?php echo $this->lang->line('sometimes'); ?></option>
                                                            <option value="<?php echo $this->lang->line('always'); ?>"><?php echo $this->lang->line('always'); ?></option>
                                                           
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    

                                </div>
                            </div>
                            
                           

                           

                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
</div>
</section>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click','.syncuserdata',function(e){
            e.preventDefault();
            var rollnumber = $('.rollnumber').val();
            if(rollnumber.length !=''){

                   $.ajax({
                type: "GET",
                url: baseurl + "student/getStudentDetailsData",
                data: {
                    'student_id': rollnumber
                }, 
                dataType: "json",
                beforeSend: function() {
                    
                },
                success: function(data) {
                    $('input[name=father_name]').val(data.father_name);
                    $('input[name=std_name]').val(data.full_name);
                    $('input[name=dob]').val(data.dob);
                    $('input[name=father_phone]').val(data.father_phone);
                    $('input[name=mother_name]').val(data.mother_name);
                    $('input[name=mother_phone]').val(data.mother_phone);
                    if(data.reg_data.father_address.length !=""){
                        $('input[name=father_address]').val(data.reg_data.father_address);
                    }
                    if(data.reg_data.place_of_birth.length !=""){
                        $('input[name=place_of_birth]').val(data.reg_data.place_of_birth);
                    }
                    if(data.reg_data.father_education.length !=""){
                        $('input[name=father_education]').val(data.reg_data.father_education);
                    }
                    if(data.reg_data.father_work_location.length !=""){
                        $('input[name=father_work_location]').val(data.reg_data.father_work_location);
                    }
                    if(data.reg_data.mother_address.length !=""){
                        $('input[name=mother_address]').val(data.reg_data.mother_address);
                    }
                     if(data.reg_data.mother_education.length !=""){
                        $('input[name=mother_education]').val(data.reg_data.mother_education);
                    }
                    if(data.reg_data.mother_work_location.length !=""){
                        $('input[name=mother_work_location]').val(data.reg_data.mother_work_location);
                    }
                    if(data.reg_data.no_of_family_members.length !=""){
                        $('input[name=no_of_family_members]').val(data.reg_data.no_of_family_members);
                    }
                    if(data.reg_data.how_old_child.length !=""){
                        $('input[name=how_old_child]').val(data.reg_data.how_old_child);
                    }
                    if(data.reg_data.type_of_illness.length !=""){
                        $('input[name=type_of_illness]').val(data.reg_data.type_of_illness);
                    }
                    if(data.reg_data.how_old_was_std_illness.length !=""){
                        $('input[name=how_old_was_std_illness]').val(data.reg_data.how_old_was_std_illness);
                    }
                    if(data.reg_data.child_live_with.length !=""){
                        $('#child_live_with').val(data.reg_data.child_live_with).trigger("change");
                         $('.live_with_single').css("display", "block");
                    }
                    if(data.reg_data.is_previous_illness.length !=""){
                        $('#is_previous_illness').val(data.reg_data.is_previous_illness).trigger("change");
                         // $('.live_with_single').css("display", "block");
                    }
                    if(data.reg_data.live_with_one_parent.length !=""){
                        $('#live_with_one_parent').val(data.reg_data.live_with_one_parent).trigger("change");
                    }
                    if(data.reg_data.treatment_of_student.length !=""){
                        $('#treatment_of_student').val(data.reg_data.treatment_of_student).trigger("change");
                    }
                     if(data.reg_data.educational_followup.length !=""){
                        $('#educational_followup').val(data.reg_data.educational_followup).trigger("change");
                    }
                     if(data.reg_data.panadol_permission.length !=""){
                        $('#panadol_permission').val(data.reg_data.panadol_permission).trigger("change");
                    }
                },
                complete: function() {
                }
            });

            }
        })

        $('body').on('change','#child_live_with',function(e){
            e.preventDefault();
            var val = $(this).val();
            if(val == 'Mother' || val == 'Father'){
                $('.live_with_single').css("display", "block");
                $('.how_old_child_cls').css("display", "block");
            }else{
                 $('.live_with_single').css("display", "none");
                 $('.how_old_child_cls').css("display", "none");
            }
        })
         $('body').on('change','#is_previous_illness',function(e){
            e.preventDefault();
            var val = $(this).val();
            if(val == 'Yes'){
                $('.type_of_illness_cls').css("display", "block");
                $('.how_old_was_std_illness_cls').css("display", "block");
                $('.continue_illness_cls').css("display", "block");
            }else{
                 $('.type_of_illness_cls').css("display", "none");
                 $('.how_old_was_std_illness_cls').css("display", "none");
                 $('.continue_illness_cls').css("display", "none");
            }
        })
    })
    
</script>