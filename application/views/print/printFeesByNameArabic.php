<?php $currency_symbol = $this->customlib->getSchoolCurrencyFormat();?>

<?php
$description = '';
if (isJSON($feeList->amount_detail)) {
    $fee    = json_decode($feeList->amount_detail);
        // echo "<pre>"; print_r($fee); exit();
    $record = $fee->{$sub_invoice_id};
    
    if (!empty($record->received_by)) {
        echo isset($record->collected_by) ? $record->collected_by : '';
    }
    if(isset($record->description) && !empty($record->description)){
        $description = $record->description;
    }
    // echo "<pre>"; print_r($record); exit();
    // $total_amount = $total_amnt;
    // $total_blnc = $record->amount;

    foreach($fee as $row){
        $remaining = $remaining - $row->amount;
    }
    
}
$amount    = 0;
$discount  = 0;
$fine      = 0;
$total     = 0;
$grd_total = 0;


$amount = number_format($record->amount, 2, '.', '');
       
?>
<!DOCTYPE html>
<html  lang="ar">
    <head>
        <meta charset="utf-8" />
        <title><?php echo $this->lang->line('fees_receipt'); ?></title>

        <style>
            .invoice-box {
                max-width: 800px;
                margin: auto;
                padding: 30px;
                border: 1px solid #eee;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
                font-size: 20px;
                line-height: 24px;
                font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                color: #555;
            }

            .invoice-box table {
                width: 100%;
                line-height: inherit;
                text-align: left;
            }

            .invoice-box table td {
                padding: 5px;
                vertical-align: top;
                font-size: 20px;
            }

            .invoice-box table tr td:nth-child(2) {
                text-align: right;
            }

            .invoice-box table tr.top table td {
                padding-bottom: 20px;
            }

            .invoice-box table tr.top table td.title {
                font-size: 50px;
                line-height: 45px;
                color: #333;
            }

            .invoice-box table tr.information table td {
                padding-bottom: 40px;
            }

            .invoice-box table tr.heading td {
                border-bottom: 2px solid #000;
                font-weight: bold;
            }

            .invoice-box table tr.details td {
                padding-bottom: 20px;
            }

            .invoice-box table tr.item td {
                border-bottom: 1px solid #eee;
            }

            .invoice-box table tr.item.last td {
                border-bottom: none;
            }

            .invoice-box table tr.total td:nth-child(2) {
                border-top: 2px solid #eee;
                font-weight: bold;
            }

            @media only screen and (max-width: 600px) {
                .invoice-box table tr.top table td {
                    width: 100%;
                    display: block;
                    text-align: center;
                }

                .invoice-box table tr.information table td {
                    width: 100%;
                    display: block;
                    text-align: center;
                }
            }

            /** RTL **/
            .invoice-box.rtl {
                direction: rtl;
                font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            }

            .invoice-box.rtl table {
                text-align: right;
            }

            .invoice-box.rtl table tr td:nth-child(2) {
                text-align: left;
            }
        </style>
    </head>

    <body>
        <div class="invoice-box">
            <table cellpadding="0" cellspacing="0">
                <tr class="top">
                    <td colspan="2">
                        <table>
                            <tr class="heading">
                                <td></td>

                                <td></td>
                            </tr>
                            <tr>
                                <td class="title">
                                    <img src="<?php echo base_url(); ?>uploads/school_content/logo/1.png" style=" width: 120px" />
                                </td>

                                <td>
                                    مدرسة النابغة الصغير<br />
                                    السنة الدراسية <?php date('Y'); ?><br />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><h4 style="text-align: center;">إيصال إستلام</h4></td>
                </tr>
                <tr class="information">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>
                                    تاريخ الأيصال: <?php echo date('d-m-Y'); ?><br />
                                    <span dir="rtl">رقم الإيصال: <?php echo " (" . $feeList->admission_no . ")"; ?></span><br />
                                </td>

                                <td>
                                    استلمت من : <?php echo $this->customlib->getFullName($feeList->firstname, $feeList->middlename, $feeList->lastname, $sch_setting->middlename, $sch_setting->lastname); ?><br />
                                    ا<span dir="rtl">القيمة :<?php echo $currency_symbol .$amount; ?></span><br />
                                    <span dir="rtl">الدفع : <?php echo $record->payment_mode; ?></span><br />
                                    <span ><?php echo $description; ?> :ملاحظات</span><br>
    .................................<?php echo $amount; ?>........:القيمة بالحروف<br />
                                    </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="heading">
                    <td></td>

                    <td>يرجى الإحتفاظ بالإيصال.</td>
                </tr>

                <tr class="details">
                    <td>الإشتراك السنوى:<?php echo number_format($total_amnt, 2, '.', ''); ?></td>

                    <td>.......................أسم المستلم.</td>
                </tr>
                <tr class="details">
                    <td>المدفوع حتى تاريخه :<?php echo number_format($record->amount, 2, '.', ''); ?></td>

                    <td>التوقيع</td>
                </tr>

                <tr class="details">
                    <td>االمبلغ المتبقي:  <?php echo number_format($remaining, 2, '.', ''); ?></td>

                    <td></td>
                </tr>
                <tr class="heading">
                    <td></td>
                    <td></td>
                </tr>
                <tr class="details">
                    <td>عادل مخلوف<br>0928660792</td>

                    <td>العنوان  :  الماجوري - شارع الفاتح /خلف عيادة البيان للأسنان</td>
                </tr>
                
            </table>
        </div>
    </body>
</html>
