<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="content">
    <section>
        <?php echo $breadcrumb . $pagetitle; ?>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <!-- <div class="panel-heading">
                        <div class="row">
                            <div class="col-12">
                                <a href="<?= base_url('admin/exams/add_sub_exams_evaluate/'); ?>" class="btn btn-primary btn-sm m-r-5"><?= lang('new_sub_exam_eval'); ?></a>
                            </div>

                        </div>
                    </div> -->

                    <div class="panel-body">
                        <form id="form2" name="form2" class="stdform stdform2" method="POST">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered m-b-0 result_dataTable">
                                    <thead>
                                        <tr>
                                            <th width="1%"><?= lang('no'); ?></th>
                                            <th class="text-nowrap"><?= lang('id'); ?></th>
                                            <th class="text-nowrap"><?= lang('name'); ?></th>
                                            <th class="text-nowrap"><?= lang('marks'); ?></th>
                                            <th class="text-nowrap"><?= lang('absent'); ?></th>
                                            <th class="text-nowrap"><?= lang('ex_acp'); ?></th>
                                        </tr>
                                    </thead>

                                    <input type="hidden" id="max_marks" value="<?= $max_marks; ?>" />
                                    <input type="hidden" id="exam_id" value="<?= $exam_id; ?>" />
                                    <input type="hidden" id="sub_exam_id" value="<?= $sub_exam_id; ?>" />
                                    <input type="hidden" id="course_id" value="<?= $course_id; ?>" />
                                    <input type="hidden" id="sub_exams_evaluate_id" value="<?= $sub_exam_eval_id; ?>" />

                                    <tbody>
                                        <?php 
                                        $group_students = $group_students->result_array();
                                        foreach ($group_students as $key => $value): ?>

                                            <tr id="tbl_row_<?= $value['std_id']; ?>">
                                                <td class="f-s-600 text-inverse"><?= ($key + 1); ?></td>
                                                <td><?= htmlspecialchars($value['std_id'], ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td>
                                                    <?= htmlspecialchars($value['std_fname'], ENT_QUOTES, 'UTF-8'); ?>
                                                    <input type="hidden" name="update_result_ids[]" id="update_result_ids_<?= $key; ?>" value="<?= $value['id']; ?>" />
                                                    <input type="hidden" name="update_std_ids[]" id="update_std_ids_<?= $key; ?>" value="<?= $value['std_id']; ?>" />
                                                </td>
                                                <td class="center">
                                                    <input type="number" class="form-control" id="update_std_marks_<?= $key; ?>" name="update_std_marks_<?= $key; ?>" step="0.01" value="<?= $value['marks']; ?>" <?= $value["absent"] == 'Y' ? 'readonly="true"' : ''; ?> />
                                                </td>
                                                <td class="center">
                                                    <div class="checkbox checkbox-css tbl-checkbox p-0"><input type="checkbox" p="<?= $key; ?>" class="std_marks_checkbox" id="update_absent_<?= $key; ?>" name="update_absent_<?= $key; ?>" value="Y" <?= $value["absent"] == 'Y' ? 'checked="true"' : ''; ?> /><label for="update_absent_<?= $key; ?>"><?= lang('absent'); ?></label></div>
                                                </td>
                                                <td class="center">
                                                    <div class="checkbox checkbox-css tbl-checkbox p-0"><input type="checkbox" p="<?= $key; ?>" id="update_ea_<?= $key; ?>" name="update_ea_<?= $key; ?>" value="Y" onchange="changeWarnStatus(this.value);" <?= $value["ea"] == 'Y' ? 'checked="checked"' : ''; ?> /><label for="update_ea_<?= $key; ?>"><?= lang('ex_acp'); ?></label></div>
                                                </td>
                                            </tr>

                                        <?php endforeach; ?>

                                        <?php if ($group_students) { ?>

                                        <input type="hidden" name="total_update" id="total_update" value="<?= count($group_students); ?>" />
                                        <input type="hidden" name="f_type" id="f_type" value="update" />
                                        <td class="text-center" colspan="6">
                                            <input type="button" class="btn btn-primary btn-sm m-r-5" name="update" value="Update" onclick="update_std_result()" />
                                            <input type="reset" class="btn btn-default btn-sm bdr-dim" value="Reset" name="reset" />
                                        </td>

                                    <?php } ?>

                                    <?php foreach ($prev_students as $key => $value): //pre($value); ?>

                                        <tr id="tbl_row_<?= $value['std_id']; ?>">
                                            <td class="f-s-600 text-inverse"><?= ($key + 1); ?></td>
                                            <td><?= htmlspecialchars($value['std_id'], ENT_QUOTES, 'UTF-8'); ?></td>
                                            <td>
                                                <?= htmlspecialchars($value['std_fname'], ENT_QUOTES, 'UTF-8'); ?>
                                                <input type="hidden" name="add_std_id[]" id="add_std_id_<?= $key; ?>" value="<?= $value['std_id']; ?>" />
                                            </td>
                                            <td class="center">
                                                <input type="number" class="form-control" name="add_std_marks[]" id="add_std_marks_<?= $key; ?>" step="0.01" value="0" />
                                            </td>
                                            <td class="center">
                                                <div class="checkbox checkbox-css tbl-checkbox p-0"><input type="checkbox" p="<?= $key; ?>" class="std_marks_checkbox" id="add_absent_<?= $key; ?>" name="add_absent_<?= $key; ?>" value="Y" /><label for="add_absent_<?= $key; ?>"><?= lang('absent'); ?></label></div>
                                            </td>
                                            <td class="center">
                                                <div class="checkbox checkbox-css tbl-checkbox p-0"><input type="checkbox" p="<?= $key; ?>" id="add_ea_<?= $key; ?>" name="add_ea_<?= $key; ?>" value="Y" onchange="changeWarnStatus(this.value);" /><label for="add_ea_<?= $key; ?>"><?= lang('ex_acp'); ?></label></div>
                                            </td>

                                        <?php endforeach; ?>

                                        <?php if ($prev_students) { ?>

                                        <tr>
                                        <input type="hidden" name="total_add" id="total_add" value="<?= count($prev_students); ?>" />
                                        <input type="hidden" name="f_type" id="f_type" value="add" />
                                        <td class="text-center" colspan="6">
                                            <input type="button" class="btn btn-primary btn-sm m-r-5" name="save" value="Save" onclick="add_std_result();" />
                                            <input type="reset" class="btn btn-default btn-sm bdr-dim" value="Reset" name="reset" />
                                        </td>
                                        </tr>

                                    <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>