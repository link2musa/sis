<div class="maincontent">
        	<div class="maincontentinner">
            	
                <ul class="maintabmenu">
                	
                    <li class=""><a href="<?php echo site_url();?>admin/dashboard">Dashboard</a></li>
                    
                    <li class="current"><a href="#">Examinations Dashboard</a></li>
                    </ul><!--maintabmenu-->
                
                <div class="content">
                <?php if(isset($_SESSION['msg']) && $_SESSION['msg']!=''){?>
                    <div class="notification msgerror">
                        <a class="close"></a>
                         <p><?php echo $_SESSION['msg']; unset($_SESSION['msg']);?></p>
                    </div><!-- notification msgsuccess -->
               <?php }?>
                
					<ul class="widgetlist">
                                    
                      <?php if(isAllowed(array(1,3))and isAllowed2(array(1,2,3,4,5,6,7,8,9,10,11,12,13))){?>
               
                    <li class="exams"><a href="<?php echo site_url();?>admin/exams" title="Manage Examinations">Examination</a></li>
                     <?php } ?>
                      <?php if(isAllowed(array(1,3))and isAllowed2(array(1,2,4))){?>
                    
                    <li class="resetexam"><a href="<?php echo site_url();?>admin/reset_exams" title="Manage Reset Examinations">Reset Examination</a></li>
                    
                     <li class="resetexams"><a href="<?php echo site_url();?>admin/exams/std_uncredit_result_entry" title="Manage Uncredit Repeater Examinations">Uncredit Repeater Examination</a></li>    
                    <li class="resetdecision"><a href="<?php echo site_url();?>admin/exams/reset_decision" title="ManageCourse Grace Marks / Reset Decision">Course Grace Marks / Reset Decision</a></li>
                    <li class="reset"><a href="<?php echo site_url();?>admin/reset_exams/reset_decision" title="Manage Reset Course Grace Marks">Reset Course Grace Marks</a></li>
                     <?php } ?>
                
                


                  
                    </ul>
                </div><!--content-->
                
            </div><!--maincontentinner-->
            
            <div class="footer">
            	<p>Libyan Internatinal Medical University &copy; 2014. All Rights Reserved. Designed by: <a href="">IT Office</a>            </p>
            </div>
            <!--footer-->
            
        </div>