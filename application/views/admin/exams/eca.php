<script>
function fetchExamsYearBatch(id)
{
	var post_url  = site_url+'admin/exams/ajax';
	jQuery.ajax({
		type: "POST",
		url: post_url,
		data: 	{id:id,action:'fetchYearSemester',attr:'onchange=fetchEca(this.value);'},
		success: function(html){
			jQuery('#year_semester').html(html)
		}
	});
	
	jQuery.ajax({
		type: "POST",
		url: post_url,
		data: 	{id:id,action:'fetchBatches',attr:''},
		success: function(html){
			jQuery('#batch').html(html)
		}
	});
}
function fetchEca(id)
{
	var fid  = jQuery('select#faculty').val();
	var bid  = jQuery('select#batch_name').val();

	var post_url  = site_url+'admin/exams/ajax';
	jQuery.ajax({
		type: "POST",
		url: post_url,
		data: 	{id:id,faculty_id:fid,bid:bid,action:'eca',attr:''},
		success: function(html){
			jQuery('#eca_data').html(html)
		}
	});
}


function ecaSubmit()
{
	var fid  = jQuery('select#faculty').val();
	var bid  = jQuery('select#batch_name').val();
	var sid	 = jQuery('select#year_semester').val();
	
	var eca_text 	= document.querySelectorAll("#form2 input[name='eca_text[]']");
	var desc		= document.querySelectorAll("#form2 input[name='desc[]']");
	var std_id		= document.querySelectorAll("#form2 input[name='std_id[]']");
	var eca_id		= document.querySelectorAll("#form2 input[name='eca_id[]']");
	
	var eca_arr		=  new Array();
	var des_arr 	=  new Array();
	var std_arr		=  new Array();
	var eca_id_arr	=  new Array();
	
	for (var i = 0; i < std_id.length; i++) {
		eca_arr[i] 	 = eca_text[i].value;
		des_arr[i] 	 = desc[i].value;
		std_arr[i] 	 = std_id[i].value;
		eca_id_arr[i]= eca_id[i].value;
	}
	var jEca 		= JSON.parse(JSON.stringify(eca_arr));
	var jDesc		= JSON.parse(JSON.stringify(des_arr));
	var jStd		= JSON.parse(JSON.stringify(std_arr));
	var jEcaId		= JSON.parse(JSON.stringify(eca_id_arr));
	
	var post_url  = site_url+'admin/exams/ajax';
	jQuery.ajax({
			type: "POST",
			url: post_url,
			data: 	{eca:jEca,desc:jDesc,std_ids:jStd,fid:fid,bid:bid,yid:sid,eca_id:jEcaId,action:'eca_add'},
			success: function(html){
				jQuery('#message').html(html)
			}
	});
		
}


</script>

<div class="maincontent noright">
        	<div class="maincontentinner">
            	
                <ul class="maintabmenu">
                	<li class=""><a href="<?php echo site_url()?>admin/dashboard">Dashboard</a></li>
                    <li class="current"><a href="#">ECA</a></li>
                </ul><!--maintabmenu-->

                
                <div class="content">
                <div id="message"></div>
                
                
                 
                      <form id="form2" class="stdform stdform2" method="post" action="">
                         <div class="contenttitle radiusbottom0">
                			<h2 class="table"><span>Extra Curricular Activities</span></h2>
                		</div><!--contenttitle-->	
                 <p>
                        	<label>Select a Faculty</label>
                            <span class="field" id="faculty">
                            <?php $where = getFacultyIds();?>
                            <?php echo show_dropdown('faculty','faculty','faculty_name','faculty_id',set_value('faculty_id'),"Select Faculty","onchange=fetchExamsYearBatch(this.value);","isActive='Y'$where");?>
                            </span>
                        </p>
                        
                          
                         <p>
                        	<label>Select Academic Year</label>
                            <span class="field" id="batch">
                            <select>
                            	<option value='0'>Select Academic Year</option>
                            </select>
							</span>
                        </p>
                        
                         <p>
                        	<label>Select Year/Semester</label>
                            <span class="field" id="year_semester">
                           	<select>
                            	<option value='0'>Select Year/Semester</option>
                            </select>
                            </span>
                        </p>
                <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
                    <colgroup>
                       <col class="con0" />
                        <col class="con1" />
                      
                    </colgroup>
                    <thead>
                        <tr>
                            <th class="head0"  >Admission No</th>
                            <th class="head1"  >Name</th>
                            <th class="head0"  >ECA</th>
                            <th class="head1"  >Notes</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th class="head0"  >Admission No</th>
                            <th class="head1"  >Name</th>
                            <th class="head0"  >ECA</th>
                            <th class="head1"  >Notes</th>
                        </tr>
                    </tfoot>
                     <tbody id="eca_data">
                     </tbody>
                     </table>
           		
                        </form>
                        
                </div>
          </div>
</div>
                
                