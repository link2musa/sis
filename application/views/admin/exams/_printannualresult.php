<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Report Card</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <style type="text/css">
    	@media print {
        .pagebreak { page-break-before: always; } /* page-break-after works, as well */
        body {-webkit-print-color-adjust: exact;}
    }
        body {
  margin: 0;
  padding: 0;
  background-color: #fafafa;
  font: 12pt "Tahoma";
}
* {
  box-sizing: border-box;
  -moz-box-sizing: border-box;
}
.courses_total_r td{
  background-color: #ADD8E6 !important;
  font-family: 'Roboto';
  font-weight: bold;
}
.courses_heading th{
  background-color: #ADD8E6 !important;
  font-family: 'Roboto';
  font-weight: bold;
}
/* .page {
  width: 21cm;
  min-height: 29.7cm;
  padding: 1cm;
  margin: 1cm auto;
  border: 1px #d3d3d3 solid;
  border-radius: 3px;
  background: white;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
} */
.subpage {
  padding: 2mm;
  height: 330mm;
}
.namecard {
  padding: 2mm;
  border-radius: 2mm;
  font-size: 13px;
  font-weight: bold;
}
.finalresultcard {
    /* padding: 2mm; */
    height: 8mm;
}
.namecard-inside {
  display: block;
  padding-right: 20px;
}
p {
  margin: 1mm;
  display: inline-block;
}
.imagescss {
  justify-content: space-between;
  display: flex;
}
.post-container{
    margin: 20px 20px 0 0;  
    width:100%;
    overflow:hidden;
}
.post-thumb{
        margin-left: 10px;
    margin-right: 20px;
}
.post-thumb img {
    float: left;
    clear:left;
    width:50px;
    height:50px;
}

.post-title {
  font-family: 'Roboto';
  font-weight: 700;
    margin-left:10px;
    text-align: center;
    margin: 20px;
}
.post-title h4{
     font-family: 'Roboto';
  font-weight: 700;
    font-size: 20px;
}
.post-title h5{
  font-family: 'Roboto';
    font-size: 20px;
    font-weight: 600;
    color: #3ba8ff;
    line-height: 0px;
}
.post-title h6{
  font-family: 'Roboto';
    font-size: 20px;
    line-height: 0px;
    font-weight: 600;
    color: #92bb92;
}
.post-content {
    float:right;
}

.parent{
    -moz-column-count: 2;
    -moz-column-gap: 0%;
    -webkit-column-count: 2;
    -webkit-column-gap: 0%;
    column-count: 2;
    column-gap: 0%;
}
.footer_left{
    float: left;
    font-weight: bold;
    margin-right: 20px;
}
.footer_info_left{
    float: left;
    margin-right: 20px;
}
@page {
  size: A4;
  margin: 0;
}
@media print {
  .page {
    margin: 0;
    border: initial;
    border-radius: initial;
    width: initial;
    min-height: initial;
    box-shadow: initial;
    background: initial;
    page-break-after: always;
  }
}
@media print {
  body,
  page {
    margin: 0;
    box-shadow: 0;
  }
}
table,
th,
td {
      border: 3px #009688 solid;
    font: 12pt "Tahoma";
    text-align: center;
    border-collapse: collapse;
    padding: 3px;
}
.graduation_table,
.graduation_table th,
.graduation_table td{
      border: none;
    font: 12pt "Tahoma";
    text-align: center;
    border-collapse: collapse;
    padding: 3px;
}
.graduation_table{
    float: left;
}
.graduation_table td{
    width: 150px;
    font-size: 14px;
}
.signature{
    float: left;
}
.signature img{
    width: 200px;
}

    </style>

  

</head>
<body>
 <div class="subpage">
 	<?php echo $html_data; ?>
   </div>
</body>
</html>