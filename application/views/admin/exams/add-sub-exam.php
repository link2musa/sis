<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="content">
    <section>
        <?php echo $breadcrumb . $pagetitle; ?>

        <div class="row">
            <div class="col-md-12">
                <?php echo form_open(current_url(), array('id' => 'add-sub-exam-form', 'class' => 'form-control-with-bg form-wizard', 'data-parsley-validate' => 'true', 'autocomplete' => 'off')); ?>

                    <div id="wizard-one-tab">
                        <ul>
                            <li class="col-12">
                                <a href="#step-1">
                                    <span class="info text-ellipsis">
                                        <?= lang('details'); ?>
                                        <small class="text-ellipsis"><?= lang('name').', '.lang('gen_absence_warn').', '.lang('fail_if_absent').' ...'; ?></small>
                                    </span>
                                </a>
                            </li>
                        </ul>

                        <div>
                            <div id="step-1" class="p-b-15">
                                <fieldset>
                                    <div class="row">
                                        <div class="col-xl-8 offset-xl-1 col-lg-10 p-b-15">

                                            <!-- Name -->
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label text-lg-right"><?= lang('name'); ?> <span class="text-danger">*</span></label>

                                                <div class="col-lg-8">
                                                    <input type="text" class="form-control" id="sub_exam_name" name="sub_exam_name" value="<?= ($this->input->post('sub_exam_name')) ? $this->input->post('sub_exam_name') : (isset($sub_exam) ? $sub_exam['sub_exam_name'] : ''); ?>" data-parsley-required="true">

                                                    <span class="text-danger"><?= form_error('sub_exam_name'); ?></span>
                                                </div>
                                            </div>

                                            <!-- Gen Absence Warn -->
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label text-lg-right"><?= lang('gen_absence_warn'); ?></label>

                                                <?php $gen_absence_warn = ($this->input->post('absent_warn')) ? $this->input->post('absent_warn') : (isset($sub_exam) ? $sub_exam['absent_warn'] : 'N'); ?>

                                                <div class="col-lg-8">
                                                    <div class="radio radio-css radio-inline">
                                                        <input type="radio" id="absent_warn_yes" name="absent_warn" value="Y" <?= ($gen_absence_warn == 'Y') ? 'checked' : ''; ?> />
                                                        <label for="absent_warn_yes">Yes</label>
                                                    </div>

                                                    <div class="radio radio-css radio-inline">
                                                        <input type="radio" id="absent_warn_no" name="absent_warn" value="N" <?= ($gen_absence_warn == 'N' OR empty($gen_absence_warn)) ? 'checked' : ''; ?> />
                                                        <label for="absent_warn_no">No</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Fail if Absent -->
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label text-lg-right"><?= lang('fail_if_absent'); ?></label>

                                                <?php $fail_absent = ($this->input->post('fail_absent')) ? $this->input->post('fail_absent') : (isset($sub_exam) ? $sub_exam['fail_absent'] : 'N'); ?>

                                                <div class="col-lg-8">
                                                    <div class="radio radio-css radio-inline">
                                                        <input type="radio" id="fail_absent_yes" name="fail_absent" value="Y" <?= ($fail_absent == 'Y') ? 'checked' : ''; ?> />
                                                        <label for="fail_absent_yes">Yes</label>
                                                    </div>

                                                    <div class="radio radio-css radio-inline">
                                                        <input type="radio" id="fail_absent_no" name="fail_absent" value="N" <?= ($fail_absent == 'N' OR empty($fail_absent)) ? 'checked' : ''; ?> />
                                                        <label for="fail_absent_no">No</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-12 panel-footer text-right p-t-15 p-b-0">
                                            <?= anchor('admin/exams/sub_exams/'.$exam_id, lang('cancel'), array('class' => 'btn btn-default bdr-dim m-r-10')); ?>

                                            <?= form_button(array('type' => 'submit', 'class' => 'btn btn-success', 'content' => lang('save'))); ?>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </section>
</div>