<style type="text/css">
    @media print
    {
        .no-print, .no-print *
        {
            display: none !important;
        }
    } 
</style> 
<div class="content-wrapper" style="min-height: 946px;">  
    <section class="content-header">
        <h1>
            <i class="fa fa-mortar-board"></i> <?php echo $this->lang->line('academics'); ?></h1>
    </section>
    <!-- Main content -->
    <section class="content"> 
        <div class="row">
            <?php
            if ($this->rbac->hasPrivilege('subject', 'can_add')) {
                ?>         
                <div class="col-md-4">          
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo $this->lang->line('search'); ?> <?php echo $this->lang->line('exam'); ?></h3>
                        </div>
                        <form  action="<?php echo site_url('admin/exams/index') ?>"  id="assignsubject" name="assignsubject" method="post" accept-charset="utf-8">
                            <div class="box-body">
                                <?php if ($this->session->flashdata('msg')) { ?>
                                    <?php echo $this->session->flashdata('msg') ?>
                                <?php } ?>     
                                <?php // echo $this->customlib->getCSRF(); ?>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('faculty'); ?></label> <small class="req"> *</small>
                                                <select id="faculty_id" name="faculty_id" class="form-control">
                                                    <?php
                                                foreach ($faculties as $faculty) {
                                                ?>
                                                    <option value="<?php echo $faculty['id'] ?>" <?php
                                                    if (set_value('faculty_id') == $faculty['id']) {
                                                        echo "selected=selected";
                                                    }
                                                    ?>><?php echo $faculty['name'] ?></option>
                                                <?php
                                                }
                                                   ?>
                                                </select>
                                                <span class="text-danger"><?php echo form_error('faculty_id'); ?></span>
                                            </div>

                                            <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('academic_year'); ?> </label><small class="req"> *</small>
                                                        <select id="academic_year" name="academic_year" class="form-control">
                                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                            <?php
                                                            foreach ($sessionlist as $session) {
                                                            ?>
                                                                <option value="<?php echo $session['id'] ?>" <?php if ($session['is_active'] != "no") echo "selected=selected"; ?> <?php if (set_value('academic_year') == $session['id']) echo "selected=selected"; ?> <?php if ($session['id'] == $active_session_id){echo "selected=selected"; } ?>  ><?php echo $session['session'] ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>

                                                         <span class="text-danger"><?php echo form_error('academic_year'); ?></span>
                                                    </div>

                                                   <div class="form-group">
                                                <label><?php echo $this->lang->line('class'); ?></label> <small class="req"> *</small>
                                                <select autofocus="" id="class_id" name="class_id" class="form-control" >
                                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                    <?php
                                                    $count = 0;
                                                    foreach ($classlist as $class) {
                                                        ?>
                                                        <option value="<?php echo $class['id'] ?>" <?php if (set_value('class_id') == $class['id']) {
                                                        echo "selected=selected";
                                                    }
                                                    ?>><?php echo $class['class'] ?></option>
                                                        <?php
                                                    $count++;
                                                    }
                                                    ?>
                                                </select> 
                                                 <span class="text-danger"><?php echo form_error('class_id'); ?></span>
                                            </div>
                                            <div class="form-group">
                                        <label><?php echo $this->lang->line('section'); ?></label>
                                        <select id="section_id" name="section_id" class="form-control">
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                        </select>
                                    </div>
                                          <div class="form-group">
                                            <label><?php echo $this->lang->line('courses'); ?></label><small class="req"> *</small>
                                            <select autofocus="" id="subject_id" name="subject_id" class="form-control" >
                                                
                                            </select>
                                            <span class="text-danger"><?php echo form_error('subject_id'); ?></span>
                                        </div>
                        
                        

                            </div>
                            <!-- <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right"><?php echo $this->lang->line('search'); ?></button>
                            </div> -->
                        </form>
                    </div>
                </div>
            <?php } ?>
            <div class="col-md-<?php
            if ($this->rbac->hasPrivilege('subject', 'can_add')) {
                echo "8";
            } else {
                echo "12";
            }
            ?>">  
            <!-- <a href="<?php echo base_url(); ?>admin/exams/add" class="btn btn-success">New Examinations</a> 
                      -->
                      <a tabindex="-1" class="btn btn-primary btn-sm" href="#" id="examModalButton"> <?php echo $this->lang->line('new') . " " . $this->lang->line('exam') ?></a>
                <div class="box box-primary" id="sublist">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix"><?php echo $this->lang->line('exam'); ?> <?php echo $this->lang->line('list'); ?></h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label"><?php echo $this->lang->line('subject_list'); ?></div>
                            <div id="list_tbl"></div>
                        </div>
                    </div>
                </div>
            </div> 

        </div> 
    </section>
</div>
<div class="modal fade" id="examModal">
    <div class="modal-dialog modal-lg">
        <form id="formadd" action="<?php echo site_url('admin/exams/add'); ?>" method="POST">
            <div class="modal-content"> 
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo $this->lang->line('exam'); ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="exam_id" value="0">
                    <div class="row">
                       <div class="col-md-4">
                           <div class="form-group">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line('faculty'); ?></label> <small class="req"> *</small>
                            <select id="addexam_faculty_id" name="faculty_id" class="form-control">
                                <?php
                            foreach ($faculties as $faculty) {
                            ?>
                                <option value="<?php echo $faculty['id'] ?>" <?php
                                if (set_value('faculty_id') == $faculty['id']) {
                                    echo "selected=selected";
                                }
                                ?>><?php echo $faculty['name'] ?></option>
                            <?php
                            }
                               ?>
                            </select>
                            <span class="text-danger"><?php echo form_error('faculty_id'); ?></span>
                        </div>
                       </div>
                       <div class="col-md-4">
                           <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo $this->lang->line('academic_year'); ?> </label><small class="req"> *</small>
                                <select id="addexam_academic_year" name="academic_year" class="form-control">
                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                    <?php
                                    foreach ($sessionlist as $session) {
                                    ?>
                                        <option value="<?php echo $session['id'] ?>" <?php if ($session['is_active'] != "no") echo "selected=selected"; ?> <?php if (set_value('academic_year') == $session['id']) echo "selected=selected"; ?> <?php if ($session['id'] == $active_session_id){echo "selected=selected"; } ?>  ><?php echo $session['session'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>

                                 <span class="text-danger"><?php echo form_error('academic_year'); ?></span>
                            </div>

                       </div>
                       <div class="col-md-4">
                           <div class="form-group">
                            <label><?php echo $this->lang->line('class'); ?></label>
                            <select autofocus="" id="addexam_class_id" name="class_id" class="form-control">
                                <option value=""><?php echo $this->lang->line('select'); ?></option>
                                <?php
                                $count = 0;
                                foreach ($classlist as $class) {
                                ?>
                                    <option value="<?php echo $class['id'] ?>" <?php if (set_value('class_id') == $class['id']) {
                                        echo "selected=selected";
                                    }
                                    ?>><?php echo $class['class'] ?></option>
                                <?php
                                    $count++;
                                }
                                ?>
                            </select>
                            <span class="text-danger"><?php echo form_error('class_id'); ?></span>
                        </div>
                       </div>
                       <div class="col-md-4">
                           <div class="form-group">
                            <label><?php echo $this->lang->line('section'); ?></label>
                            <select id="addexam_section_id" name="section_id" class="form-control">
                                <option value=""><?php echo $this->lang->line('select'); ?></option>
                            </select>
                        </div>
                       </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label><?php echo $this->lang->line('courses'); ?></label><small class="req"> *</small>
                            <select autofocus="" id="addexam_subject_id" name="subject_id" class="form-control" >
                                
                            </select>
                            <span class="text-danger"><?php echo form_error('subject_id'); ?></span>
                        </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label><?= lang('exam_name'); ?> <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="exam_name" name="exam_name" value="<?= ($this->input->post('exam_name')) ? $this->input->post('exam_name') : (isset($exam) ? $exam['exam_name'] : ''); ?>" data-parsley="true" />
                                <span class="text-danger"><?php echo form_error('exam_name'); ?></span> 
                        </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label ><?php echo $this->lang->line('start_exam_date') ?> <span class="text-danger">*</span></label>
                                <input type="text" class="form-control datetime" id="start_date" name="exam_start_date"  data-parsley-group="step-1" data-parsley-required="true"  value="<?= ($this->input->post('exam_start_date')) ? $this->input->post('exam_start_date') : (isset($exam) ? $exam['exam_start_date'] : ''); ?>" placeholder="Select Start Date/Time" />
                            </div>
                        </div>
                        <!-- <div class="col-md-4">
                            <div class="form-group">
                            <label >Exam End Date/Time <span class="text-danger">*</span></label>
                                <input type="text" class="form-control datetime" id="end_date" name="exam_end_date"  value="<?= ($this->input->post('exam_end_date')) ? $this->input->post('exam_end_date') : (isset($exam) ? $exam['exam_end_date'] : ''); ?>" placeholder="Select End Date/Time" />
                        </div>
                        </div> -->
                        <div class="col-md-4">
                            <div class="form-group">
                            <label ><?php echo $this->lang->line('max_marks') ?> <span class="text-danger">*</span></label>
                                <input type="number" class="form-control" id="max_marks" name="max_marks"  data-parsley-maxlength="5" min="0" value="<?= ($this->input->post('max_marks')) ? $this->input->post('max_marks') : (isset($exam) ? $exam['max_marks'] : 0); ?>" />
                             </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><?php echo $this->lang->line('min_marks') ?></label>  
                                    <input type="text" class="form-control" id="min_marks" name="min_marks" value="<?= ($this->input->post('min_marks')) ? $this->input->post('min_marks') : (isset($exam) ? $exam['min_marks'] : 'NO'); ?>" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label ><?php echo $this->lang->line('fail_min_marks') ?></label>
                             <select class="form-control" name="is_fail">
                                 <option value="0"><?php echo $this->lang->line('no') ?></option>
                                 <option value="1"><?php echo $this->lang->line('yes') ?></option>
                             </select>   
                                
                           </div>
                        </div>
                        
                                            
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="load" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Saving..."><?php echo $this->lang->line('save') ?></button>                   
                </div>
            </div>
            </div>
        </form>
    </div>
</div>



<div class="modal fade" id="examEditModal">
    <div class="modal-dialog modal-lg">
        <form id="formadd" action="<?php echo site_url('admin/exams/update_exam'); ?>" method="POST">
            <div class="modal-content"> 
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo $this->lang->line('exam'); ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="exam_id" id="exam_id" value="0">
                    <div class="row">
                       <div class="col-md-4">
                           <div class="form-group">
                            <label for="exampleInputEmail1"><?php echo $this->lang->line('faculty'); ?></label> <small class="req"> *</small>
                            <select id="addexam_faculty_id" name="faculty_id" class="form-control" disabled="true">
                                <?php
                            foreach ($faculties as $faculty) {
                            ?>
                                <option value="<?php echo $faculty['id'] ?>" <?php
                                if (set_value('faculty_id') == $faculty['id']) {
                                    echo "selected=selected";
                                }
                                ?>><?php echo $faculty['name'] ?></option>
                            <?php
                            }
                               ?>
                            </select>
                            <span class="text-danger"><?php echo form_error('faculty_id'); ?></span>
                        </div>
                       </div>
                       <div class="col-md-4">
                           <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo $this->lang->line('academic_year'); ?> </label><small class="req"> *</small>
                                <select id="addexam_academic_year" name="academic_year" class="form-control" disabled="true">
                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                    <?php
                                    foreach ($sessionlist as $session) {
                                    ?>
                                        <option value="<?php echo $session['id'] ?>" <?php if ($session['is_active'] != "no") echo "selected=selected"; ?> <?php if (set_value('academic_year') == $session['id']) echo "selected=selected"; ?> <?php if ($session['id'] == $active_session_id){echo "selected=selected"; } ?>  ><?php echo $session['session'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>

                                 <span class="text-danger"><?php echo form_error('academic_year'); ?></span>
                            </div>

                       </div>
                       <div class="col-md-4">
                           <div class="form-group">
                            <label><?php echo $this->lang->line('class'); ?></label>
                            <select autofocus="" id="addexam_class_id" name="class_id" class="form-control" disabled="true">
                                <option value=""><?php echo $this->lang->line('select'); ?></option>
                                <?php
                                $count = 0;
                                foreach ($classlist as $class) {
                                ?>
                                    <option value="<?php echo $class['id'] ?>" <?php if (set_value('class_id') == $class['id']) {
                                        echo "selected=selected";
                                    }
                                    ?>><?php echo $class['class'] ?></option>
                                <?php
                                    $count++;
                                }
                                ?>
                            </select>
                            <span class="text-danger"><?php echo form_error('class_id'); ?></span>
                        </div>
                       </div>
                       <div class="col-md-4">
                           <div class="form-group">
                            <label><?php echo $this->lang->line('section'); ?></label>
                            <select id="addexam_section_id" name="section_id" class="form-control" disabled="true">
                                <option value=""><?php echo $this->lang->line('select'); ?></option>
                            </select>
                        </div>
                       </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label><?php echo $this->lang->line('courses'); ?></label><small class="req"> *</small>
                            <select autofocus="" id="addexam_subject_id" name="subject_id" class="form-control" disabled="">
                                
                            </select>
                            <span class="text-danger"><?php echo form_error('subject_id'); ?></span>
                        </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label><?= lang('exam_name'); ?> <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="exam_name" name="exam_name" value="<?= ($this->input->post('exam_name')) ? $this->input->post('exam_name') : (isset($exam) ? $exam['exam_name'] : ''); ?>" data-parsley="true" />
                                <span class="text-danger"><?php echo form_error('exam_name'); ?></span> 
                        </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label ><?php echo $this->lang->line('start_exam_date') ?> <span class="text-danger">*</span></label>
                                <input type="text" class="form-control datetime" id="start_date" name="exam_start_date"  data-parsley-group="step-1" data-parsley-required="true"  value="<?= ($this->input->post('exam_start_date')) ? $this->input->post('exam_start_date') : (isset($exam) ? $exam['exam_start_date'] : ''); ?>" placeholder="Select Start Date/Time" />
                            </div>
                        </div>
                        <!-- <div class="col-md-4">
                            <div class="form-group">
                            <label >Exam End Date/Time <span class="text-danger">*</span></label>
                                <input type="text" class="form-control datetime" id="end_date" name="exam_end_date"  value="<?= ($this->input->post('exam_end_date')) ? $this->input->post('exam_end_date') : (isset($exam) ? $exam['exam_end_date'] : ''); ?>" placeholder="Select End Date/Time" />
                        </div>
                        </div> -->
                        <div class="col-md-4">
                            <div class="form-group">
                            <label ><?php echo $this->lang->line('max_marks') ?> <span class="text-danger">*</span></label>
                                <input type="number" class="form-control" id="max_marks" name="max_marks"  data-parsley-maxlength="5" min="0" value="<?= ($this->input->post('max_marks')) ? $this->input->post('max_marks') : (isset($exam) ? $exam['max_marks'] : 0); ?>" />
                             </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><?php echo $this->lang->line('min_marks') ?></label>  
                                    <input type="text" class="form-control" id="min_marks" name="min_marks" value="<?= ($this->input->post('min_marks')) ? $this->input->post('min_marks') : (isset($exam) ? $exam['min_marks'] : 'NO'); ?>" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label ><?php echo $this->lang->line('fail_min_marks') ?></label>
                             <select class="form-control" name="is_fail" id="is_fail">
                                 <option value="0"><?php echo $this->lang->line('no') ?></option>
                                 <option value="1"><?php echo $this->lang->line('yes') ?></option>
                             </select>   
                                
                           </div>
                        </div>
                        
                                            
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="load" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Saving..."><?php echo $this->lang->line('save') ?></button>                   
                </div>
            </div>
            </div>
        </form>
    </div>
</div>
<!-- Modal -->
<div id="subjectModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title subjectmodal_header"></h4>
            </div>
            <div class="modal-body">
                <form role="form" id="searchStudentForm" action="<?php echo site_url('admin/exams/subjectstudent') ?>" method="post" class="mb10">

                    <input type="hidden" name="subject_id" value="0" class="subject_id">
                    <input type="hidden" name="teachersubject_id" value="0" class="teachersubject_id">
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <input type="hidden" name="class_id" id="search_class_id">
                        </div>

                        <div class="col-sm-4">
                            <input type="hidden" name="section_id" id="search_section_id">
                        </div>

                        <div class="col-sm-4">
                            <input type="hidden" name="session_id" id="search_session_id">
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="submit" name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle"><i class="fa fa-search"></i> <?php echo $this->lang->line('search'); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="examheight100 relative">
                    <div id="examfade"></div>
                    <div id="exammodal">
                        <img id="loader" src="<?php echo base_url() ?>/backend/images/loading_blue.gif" />
                    </div>
                    <div class="marksEntryForm">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.datetime').datetimepicker();
        $('#examModalButton').click(function () {
            $('#examModal').modal({
                backdrop: 'static',
                keyboard: false
            });
        });
        $(document).ready(function(){

            $('body').on('click','.examEditModalButton',function(e){
                e.preventDefault();
                var exam = $(this).data('exam');

                var base_url = '<?php echo base_url() ?>';
           
            $.ajax({
                type: "GET",
                url: base_url + "admin/exams/get_exam_data",
                data: {
                    'exam': exam
                },
                dataType: "json",
                success: function(data) {
                   $('#examEditModal #exam_id').val(exam);
                   $('#examEditModal #exam_name').val('');
                   $('#examEditModal #exam_name').val(data.exam_name);

                   $('#examEditModal #start_date').val('');
                   $('#examEditModal #start_date').val(data.start_date);

                   $('#examEditModal #max_marks').val('');
                   $('#examEditModal #max_marks').val(data.max_marks);

                   $('#examEditModal #min_marks').val('');
                   $('#examEditModal #min_marks').val(data.min_marks);

                   $("#examEditModal #is_fail").val(data.is_fail).change();
                   $("#examEditModal #start_date").val(data.exam_start_date);
                   $("#examEditModal #addexam_academic_year").val(data.batch_id).change();

                   $('#examEditModal').modal({
                    backdrop: 'static',
                    keyboard: false
                   });

                }
            });

                
            })
        })
        
        $('#examModal').on('hidden.bs.modal', function () {
            reset_exm_form();
            $("span[id$='_error']").html("");
        });
         $('#examEditModal').on('hidden.bs.modal', function () {
            reset_exm_form();
            $("span[id$='_error']").html("");
        });
        $('#subjectModal').on('shown.bs.modal', function (e) {
        var subject_id = $(e.relatedTarget).data('subject_id');
         var subject_name = $(e.relatedTarget).data('subject_name');
         var class_id = $(e.relatedTarget).data('calss');
         var section_id = $(e.relatedTarget).data('section_id');

         var session_id = $(e.relatedTarget).data('session_id');

        //  var teachersubject_id = $(e.relatedTarget).data('teachersubject_id');
        $('.subjectmodal_header').html("").html(subject_name);
         $('.marksEntryForm').html("");
         $('.subject_id').val("").val(subject_id);
         $('#search_class_id').val("").val(class_id);
         $('#search_section_id').val("").val(section_id);
         $('#search_session_id').val("").val(session_id);
        // $('.teachersubject_id').val("").val(teachersubject_id);
         $(e.currentTarget).find('input[name="subject_name"]').val(subject_name);
        // var current_session = $('#current_session').val();
        // $('#session_id option[value="'+current_session+'"]').prop("selected", true);

    })
        $("form#searchStudentForm").on('submit', (function (e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var $this = form.find("button[type=submit]:focus");
        var url = form.attr('action');
        $.ajax({
            url: url,
            type: "POST",
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $this.button('loading');
                    $('#examfade,#exammodal').css({'display': 'block'});
            },
            success: function (res)
            {
    $('#examfade,#exammodal').css({'display': 'none'});

    if (res.status == "0") {
        $('.marksEntryForm').html('');
                            var message = "";
                            $.each(res.error, function (index, value) {

                                message += value;
                            });
                            errorMsg(message);
                        }else{
                          $('.marksEntryForm').html(res.page);

                $('.marksEntryForm').find('.dropify').dropify();
  
                        }
            },
            error: function (xhr) { // if error occured
                alert("Error occured.please try again");
                $this.button('reset');
                    $('#examfade,#exammodal').css({'display': 'none'});
            },
            complete: function () {
                $this.button('reset');
                    $('#examfade,#exammodal').css({'display': 'none'});
            }

        });
    }
    ));
     function getExamSectionByClass(class_id) {

        if (class_id != "" && section_id != "") {
            $('#section_id').html("");
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            $.ajax({
                type: "GET",
                url: base_url + "sections/getByClass",
                data: {
                    'class_id': class_id
                },
                dataType: "json",
                success: function(data) {
                    $.each(data, function(i, obj) {
                        var sel = "";
                        if (section_id == obj.section_id) {
                            sel = "selected";
                        }
                        div_data += "<option value=" + obj.section_id + " " + sel + ">" + obj.section + "</option>";
                    });
                    $('#section_id').append(div_data);
                }
            });
        }
    } 
    function getSectionByClass(class_id, section_id) {

        if (class_id != "" && section_id != "") {
            $('#section_id').html("");
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            $.ajax({
                type: "GET",
                url: base_url + "sections/getByClass",
                data: {
                    'class_id': class_id
                },
                dataType: "json",
                success: function(data) {
                    $.each(data, function(i, obj) {
                        var sel = "";
                        if (section_id == obj.section_id) {
                            sel = "selected";
                        }
                        div_data += "<option value=" + obj.section_id + " " + sel + ">" + obj.section + "</option>";
                    });
                    $('#section_id').append(div_data);
                }
            });
        }
    } 
    $(document).ready(function() {
        var class_id = $('#addexam_class_id').val();
        var section_id = '<?php echo set_value('section_id') ?>';
        // getSectionByClass(class_id, section_id);
        $(document).on('change', '#addexam_class_id', function(e) {
            $('#addexam_section_id').html("");
            var class_id = $(this).val();
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            $.ajax({
                type: "GET",
                url: base_url + "sections/getByClass",
                data: {
                    'class_id': class_id
                },
                dataType: "json",
                success: function(data) {
                    $.each(data, function(i, obj) {
                        div_data += "<option value=" + obj.section_id + ">" + obj.section + "</option>";
                    });
                    $('#addexam_section_id').append(div_data);
                    
                }
            });

        });
        $('body').on('change','#addexam_class_id',function(){
            getaddcoursesrec();
        })
        $('body').on('change','#addexam_academic_year',function(){
            getaddcoursesrec();
        })
        $('body').on('change','#addexam_faculty_id',function(){
            getaddcoursesrec();
        })
        
    });

    function getaddcoursesrec(){
        var faculty_id = $( "#faculty_id option:selected" ).val();
        var academic_year = $( "#addexam_academic_year option:selected" ).val();
        var class_id = $( "#addexam_class_id option:selected" ).val();
        $('#addexam_subject_id').html("");
         var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            $.ajax({
                type: "GET",
                url: base_url + "admin/exams/getCourses",
                data: {
                    'faculty_id': faculty_id,
                    'academic_year': academic_year,
                    'class_id': class_id
                },
                dataType: "json",
                success: function(response) {

                    $.each(response, function(i, obj) {
                        div_data += "<option value=" + obj.subject_id + ">" + obj.name + "</option>";
                    });
                    $('#addexam_subject_id').append(div_data);
                }
            });
    }
    function getalistcoursesrec(){
        var faculty_id = $( "#faculty_id option:selected" ).val();
        var academic_year = $( "#academic_year option:selected" ).val();
        var class_id = $( "#class_id option:selected" ).val();
        $('#subject_id').html("");

         var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            $.ajax({
                type: "GET",
                url: base_url + "admin/exams/getCourses",
                data: {
                    'faculty_id': faculty_id,
                    'academic_year': academic_year,
                    'class_id': class_id
                },
                dataType: "json",
                success: function(response) {

                    $.each(response, function(i, obj) {
                        div_data += "<option value=" + obj.subject_id + ">" + obj.name + "</option>";
                    });
                    $('#subject_id').append(div_data);
                }
            });
    }
    $(document).ready(function () {
        var class_id = $('#class_id').val();
        var section_id = '<?php echo set_value('section_id') ?>';
        $("#btnreset").click(function () {
            $("#form1")[0].reset();
        });
        $('body').on('change','#class_id',function(){
            getalistcoursesrec();
            var class_id = $( "#class_id option:selected" ).val();
            getSectionByClass(class_id)
        })
        $('body').on('change','#academic_year',function(){
            getalistcoursesrec();
        })
        $('body').on('change','#faculty_id',function(){
            getalistcoursesrec();
            
        })
        $('body').on('change','#subject_id',function(){
            getindexdata();
        })
         
    });

   
    function getindexdata(){

        var faculty_id = $( "#faculty_id option:selected" ).val();
        var academic_year = $( "#academic_year option:selected" ).val();
        var class_id = $( "#class_id option:selected" ).val();
        var subject_id = $( "#subject_id option:selected" ).val();
        var section_id = $( "#section_id option:selected" ).val();   

         $('#list_tbl').html("");
         var base_url = '<?php echo base_url() ?>';
            $.ajax({
                type: "POST",
                url: base_url + "admin/exams/get_index_data",
                data: {
                    'faculty_id': faculty_id,
                    'academic_year': academic_year,
                    'class_id': class_id,
                    'subject_id': subject_id,
                    'section_id': section_id
                },
                dataType: "html",
                success: function(response) {
                    console.log(response);
                    $('#list_tbl').html(response);
                }
            });

    }
</script>

<script type="text/javascript">
    var base_url = '<?php echo base_url() ?>';
    function printDiv(elem) {
        Popup(jQuery(elem).html());
    }


    function Popup(data)
    {

        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({"position": "absolute", "top": "-1000000px"});
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html>');
        frameDoc.document.write('<head>');
        frameDoc.document.write('<title></title>');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/bootstrap/css/bootstrap.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/font-awesome.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/ionicons.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/AdminLTE.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/skins/_all-skins.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/iCheck/flat/blue.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/morris/morris.css">');


        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/datepicker/datepicker3.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/daterangepicker/daterangepicker-bs3.css">');
        frameDoc.document.write('</head>');
        frameDoc.document.write('<body>');
        frameDoc.document.write(data);
        frameDoc.document.write('</body>');
        frameDoc.document.write('</html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);


        return true;
    }
</script>