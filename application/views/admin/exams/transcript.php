<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="content">
    <section>
        <?php echo $breadcrumb . $pagetitle; ?>

        <div class="row">
            <div class="col-md-12">
                <?php echo form_open(current_url(), array('id' => 'add-exam-form', 'class' => 'form-control-with-bg form-wizard', 'data-parsley-validate' => 'true', 'autocomplete' => 'off')); ?>

                    <div id="wizard-one-tab">
                        <ul>
                            <li class="col-12">
                                <a href="#step-1">
                                    <span class="info text-ellipsis">
                                        <?= lang('details'); ?>
                                        <small class="text-ellipsis"><?= lang('faculty').', '.lang('academic_year').', '.lang('year_semester').', '.lang('examination_name').' ...'; ?></small>
                                    </span>
                                </a>
                            </li>
                        </ul>

                        <div>
                            <div id="step-1" class="p-b-15">
                                <fieldset>
                                    <div class="row">
                                        <div class="col-xl-8 offset-xl-1 col-lg-10 p-b-15">

                                            <!-- Faculty -->
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label text-lg-right"><?= lang('faculty'); ?> <span class="text-danger">*</span></label>

                                                <div class="col-lg-8">
                                                    <select class="form-control select2" id="rep-faculty-id" name="faculty_id" <?php if($update_page) {?> disabled="readonly" <?php } else { ?> data-parsley-required="true" data-value="add-exam" <?php } ?>>
                                                        <option value="">Select Faculty</option>

                                                        <?php
                                                            $selected = ($this->input->post('faculty_id')) ? $this->input->post('faculty_id') : ((isset($exam)) ? $exam['faculty_id'] : '');

                                                            foreach($faculties as $key => $value)
                                                            {
                                                        ?>

                                                            <option value="<?= $value['faculty_id']; ?>" <?= ($selected == $value['faculty_id']) ? 'selected' : ''; ?>><?= $value['faculty_name']; ?></option>

                                                        <?php } ?>
                                                    </select>

                                                    <span class="text-danger"><?= form_error('faculty_id'); ?></span>
                                                </div>
                                            </div>

                                            <!-- Academic Year -->
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label text-lg-right"><?= lang('academic_year'); ?> <span class="text-danger">*</span></label>

                                                <div class="col-lg-8">
                                                    <select class="form-control select2" id="rep-academic-year" name="batch_id" <?php if($update_page) {?> disabled="readonly" <?php } else { ?> data-parsley-required="true" data-value="add-exam" <?php } ?> data-value="add-exam">

                                                        <option value=""><?= $update_page ? getFieldValueById('batches', 'batch_name', 'batch_id', $exam['batch_id']) : 'Select Academic Year'; ?></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- Year/Semester -->
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label text-lg-right"><?= lang('year_semester'); ?> <span class="text-danger">*</span></label>

                                                <div class="col-lg-8">
                                                    <select class="form-control select2" id="rep-year-semester" name="year_id" <?php if($update_page) {?> disabled="readonly" <?php } else { ?> data-parsley-required="true" data-value="add-exam" <?php } ?> data-value="add-exam">
                                                        <option value=""><?= $update_page ? getFieldValueById('year_semester', 'year_semester', 'semester_id', $exam['year_id']) : 'Select Semester'; ?></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- Course -->
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label text-lg-right"><?= lang('course'); ?> <span class="text-danger">*</span></label>

                                                <div class="col-lg-8">
                                                    <select class="form-control select2" id="rep-course-id" name="course_id" <?php if($update_page) {?> disabled="readonly" <?php } else { ?> data-parsley-required="true" data-value="add-exam" <?php } ?>>
                                                        <option value=""><?= $update_page ? getFieldValueById('courses', 'course_name', 'course_id', $exam['course_id']) : 'Select Course'; ?></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- Exam Name -->
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label text-lg-right"><?= lang('exam_name'); ?> <span class="text-danger">*</span></label>

                                                <div class="col-lg-8">
                                                    <input type="text" class="form-control" id="exam_name" name="exam_name" value="<?= ($this->input->post('exam_name')) ? $this->input->post('exam_name') : (isset($exam) ? $exam['exam_name'] : ''); ?>" data-parsley="true" />
                                                </div>
                                            </div>

                                            <!-- Sub Exam -->
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label text-lg-right"><?= lang('sub_exam'); ?> <span class="text-danger">*</span></label>

                                                <?php $sub_exams = ($this->input->post('sub_exams')) ? $this->input->post('sub_exams') : (isset($exam) ? $exam['sub_exams'] : 'Y'); ?>

                                                <div class="col-lg-8">
                                                    <div class="radio radio-css radio-inline">
                                                        <input type="radio" id="Yes" name="sub_exams" value="Y" <?= ($sub_exams == 'Y' OR empty($sub_exams)) ? 'checked' : ''; ?> <?= $update_page ? 'disabled="true"' : '' ?> />
                                                        <label for="Yes">Yes</label>
                                                    </div>

                                                    <div class="radio radio-css radio-inline">
                                                        <input type="radio" id="No" name="sub_exams" value="N" <?= ($sub_exams == 'N') ? 'checked' : ''; ?> <?= $update_page ? 'disabled="true"' : '' ?> />
                                                        <label for="No">No</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="<?= ($sub_exams == 'Y' OR empty($sub_exams)) ? '' : 'display-none'; ?>" id="sub_exams_y">

                                                <!-- Sub Exam Maximum Marks -->
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label text-lg-right"><?= lang('sub_exam_max_marks'); ?> <span class="text-danger">*</span></label>

                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" id="max_weightage" name="max_weightage" <?php if($sub_exams == 'Y') { ?> data-parsley-group="step-1" data-parsley-required="true" <?php } ?> data-parsley-maxlength="5" min="0" value="<?= ($this->input->post('max_weightage')) ? $this->input->post('max_weightage') : (isset($exam) ? $exam['max_weightage'] : 0); ?>" />
                                                    </div>
                                                </div>

                                                <!-- Sub Exam Minimum Marks -->
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label text-lg-right"><?= lang('sub_exam_min_marks'); ?></label>

                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="min_weightage" name="min_weightage" value="<?= ($this->input->post('min_weightage')) ? $this->input->post('min_weightage') : (isset($exam) ? $exam['min_weightage'] : 'NO'); ?>" />
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="<?= ($sub_exams == 'N') ? '' : 'display-none'; ?>" id="sub_exams_n">

                                                <!-- Exam Start Date/Time -->
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label text-lg-right"><?= lang('exam_start_date/time'); ?> <span class="text-danger">*</span></label>

                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control datetimepicker" id="start_date" name="exam_start_date" <?php if($sub_exams == 'N') { ?> data-parsley-group="step-1" data-parsley-required="true" <?php } ?> value="<?= ($this->input->post('exam_start_date')) ? $this->input->post('exam_start_date') : (isset($exam) ? $exam['exam_start_date'] : ''); ?>" placeholder="Select Start Date/Time" />
                                                    </div>
                                                </div>

                                                <!-- Exam End Date/Time -->
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label text-lg-right"><?= lang('exam_end_date/time'); ?> <span class="text-danger">*</span></label>

                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control datetimepicker" id="end_date" name="exam_end_date" <?php if($sub_exams == 'N') { ?> data-parsley-group="step-1" data-parsley-required="true" <?php } ?> value="<?= ($this->input->post('exam_end_date')) ? $this->input->post('exam_end_date') : (isset($exam) ? $exam['exam_end_date'] : ''); ?>" placeholder="Select End Date/Time" />
                                                    </div>
                                                </div>

                                                <!-- Max Marks -->
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label text-lg-right"><?= lang('maximum_marks'); ?> <span class="text-danger">*</span></label>

                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" id="max_marks" name="max_marks" <?php if($sub_exams == 'N') { ?> data-parsley-group="step-1" data-parsley-required="true" <?php } ?> data-parsley-maxlength="5" min="0" value="<?= ($this->input->post('max_marks')) ? $this->input->post('max_marks') : (isset($exam) ? $exam['max_marks'] : 0); ?>" />
                                                    </div>
                                                </div>

                                                <!-- Min Marks -->
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label text-lg-right"><?= lang('minimum_marks'); ?></label>

                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="min_marks" name="min_marks" value="<?= ($this->input->post('min_marks')) ? $this->input->post('min_marks') : (isset($exam) ? $exam['min_marks'] : 'NO'); ?>" />
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="col-12 panel-footer text-right p-t-15 p-b-0">
                                            <?= anchor('admin/exams/index', lang('cancel'), array('class' => 'btn btn-default bdr-dim m-r-10')); ?>

                                            <?= form_button(array('type' => 'submit', 'class' => 'btn btn-success', 'content' => lang('save'))); ?>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </section>
</div>