<script>
	jQuery(function() {
		jQuery( "#exam_end_date" ).datetimepicker({format:'Y-m-d H:i'});
		jQuery( "#exam_start_date" ).datetimepicker({format:'Y-m-d H:i'});
	});
	
	function doconfirm()
{
    job=confirm("Are you sure to Update this exam?");
    if(job!=true)
    {
        return false;
    }
}
</script>

<?php $row	=	$detail->row();
//pre($row);
?>
<div class="maincontent noright">
        	<div class="maincontentinner">
            	
                <ul class="maintabmenu">
                	<li class=""><a href="<?php echo site_url()?>admin/dashboard">Dashboard</a></li>
                    <li class=""><a href="<?php echo site_url()?>admin/exams/">Examination</a></li>
                    <li class="current"><a href="#">Edit Examination</a></li>
                    
                </ul><!--maintabmenu-->

                
                <div class="content">
                <?php if(isset($_SESSION['msg']) && $_SESSION['msg']!=''){?>
                    <div class="notification msgsuccess">
                        <a class="close"></a>
                         <p><?php echo $_SESSION['msg']; unset($_SESSION['msg']);?></p>
                    </div><!-- notification msgsuccess -->
                    
				<?php }?>
                <?php if(validation_errors()!=''){?>
                            <?php  echo validation_errors('<div class="notification msgerror"><a class="close"></a><p>','</p></div>');?>
		        <?php }?>
                 
                      <form id="form2" class="stdform stdform2" method="post" action="">
                         <div class="contenttitle radiusbottom0">
                			<h2 class="table"><span>New Exam</span></h2>
                		</div><!--contenttitle-->	
                 <p>
                        	<label>Select a Faculty</label>
                            <span class="field" id="faculty">
                            	<?php $where = getFacultyIds();?>
								<?php echo show_dropdown('faculty','faculty','faculty_name','faculty_id',$row->faculty_id,"Select Faculty","onchange=fetchExamsYearBatch(this.value);","isActive='Y'$where");?>
                            </span>
                        </p>
                        <input id="currentdt" name="currentdt" size="30" type="hidden" value="<?php echo date('Y-m-d H:i:s'); ?>" >
                         <p>
                        	<label>Select Academic Year</label>
                            <span class="field" id="batch">
                            <?php
								echo show_dropdown('batches','batch_name','batch_name','batch_id',$row->batch_id,"Select Batch",'fetchExamCourses(this.value);',"isActive='Y'");
							?>
							</span>
                        </p>
                    
                        
                         <p>
                        	<label>Select Year/Semester</label>
                            <span class="field" id="year_semester">
                          	<?php 
								echo show_dropdown('year_semester','year_semester','year_semester','semester_id',$row->year_id,"Select Year/Semester","","isActive='Y'");?>
                            </span>
                        </p>
                         
                        
                         <p>
                        	<label>Select Course</label>
                             <span class="field" id="course">
                           <?php
						   		echo	$this->exams_model->examCourses($row->faculty_id,$row->year_id,$row->batch_id,'',$row->course_id);
						   ?>
                            </span>
                        </p>
                       
                        <p>
                        	<label>*Exam Name</label>
                            <span class="field" id="">
                            	<input type="text" name="exam_name" value="<?php if(validation_errors()!='') echo set_value('exam_name'); else echo ($row->exam_name);?>"  />
                            </span>
                        </p>
                        
                        <p>
                      <label>Allow users</label>
                            <span class="field" id="allow_users">
                            	<?php echo show_dropdown('tbl_sub_exams_admin','sub_exam_admin','title','id',validation_errors()!=''?set_value('sub_exam_admin'):$row->sub_exam_admin);?>
                            </span>
                        </p>
                          <p class="hide">
                        	<label>Sub Exam</label>
                            <span class="field" id="faculty">
                            		 <input id="" name="sub_exams"  type="radio" value="Y" <?php  $row->sub_exams=='Y' ? print 'checked="checked"' : ''; ?> onclick="hideMainExam();">Yes &nbsp;&nbsp;
                           			 <input id="" name="sub_exams"  type="radio" value="N" <?php  $row->sub_exams=='N' ? print 'checked="checked"' : ''; ?>onclick="showMainExam();">No
                           
                            </span>
                        </p>
                        <div id="main-exams" class="<?php echo ($this->input->post('sub_exams')=='N' || $row->sub_exams=='N'?'show': 'hide');?>">
                       
                         <div class="contenttitle radiusbottom0">
                			<h2 class="table"><span>Main Exam</span></h2>
                		</div><!--contenttitle-->	
                        
                         <p>
                        	<label>*Exam Start Date/Time</label>
                            <span class="field" id="faculty">
                            	<input type="text" id="exam_start_date" name="exam_start_date" value="<?php if(validation_errors()!='') echo set_value('exam_start_date'); else echo (@$row->exam_start_date);?>"  />
                            </span>
                        </p>
                          <p>
                        	<label>*Exam End Date/Time</label>
                            <span class="field" id="faculty">
                            	<input type="text" name="exam_end_date" id="exam_end_date" value="<?php if(validation_errors()!='') echo set_value('exam_end_date'); else echo (@$row->exam_end_date);?>"  />
                            </span>
                        </p>
                        <p>
                        	<label>*Maximum Marks</label>
                            <span class="field" id="faculty">
                            	<input type="text" name="main_max_marks" value="<?php if(validation_errors()!='') echo set_value('main_max_marks'); else echo (@$row->max_marks);?>"  />
                            </span>
                        </p>
                         <p>
                        	<label>Minimum Marks</label>
                            <span class="field" id="faculty">
                            	<input type="text" name="main_min_marks" value="<?php if(validation_errors()!='') echo set_value('main_min_marks','NO'); else echo (@$row->min_marks);?>"  />
                            </span>
                        </p>
                            </div>
                        <div id="sub-exams" class="<?php echo ($this->input->post('sub_exams')=='Y' || $row->sub_exams=='Y'?'show': 'hide');?>">
                          <div class="contenttitle radiusbottom0">
                			<h2 class="table"><span>Sub Exam Weightage</span></h2>
                		</div><!--contenttitle-->	
                         <p>
                        	<label>*Sub Exam Maximum Marks</label>
                            <span class="field" id="faculty">
                            	<input type="text" name="sub_max_marks" value="<?php if(validation_errors()!='') echo set_value('sub_max_marks'); else echo (@$row->max_weightage);?>"  />
                                <input type="hidden" name="sub_exams_id" value="<?php @$row->sub_exams_id?>" />
                            </span>
                        </p>
                         <p>
                        	<label>Sub Exam Minimum Marks</label>
                            <span class="field" id="faculty">
                            	<input type="text" name="sub_min_marks" value="<?php if(validation_errors()!='') echo set_value('sub_min_marks','NO'); else  if($row->min_weightage!=0) echo $row->min_weightage; else echo 'NO';?>"  />
                            </span>
                        </p>
<?php /*?>                        <p>
                        	<label>General Absence Warning</label>
                            <span class="field" id="faculty">
                            	<input type="radio" name="absent_warning" value="Y"  /> Yes
                                <input type="radio" name="absent_warning" value="N"  checked="checked"/> No
                            </span>
                        </p>
                         <p>
                        	<label>Fail if Absence in any Sub exam</label>
                            <span class="field" id="faculty">
                            	<input type="radio" name="absent_fail" value="Y"  /> Yes
                                <input type="radio" name="absent_fail" value="N" checked="checked" /> No
                            </span>
                        </p>
<?php */?>                       </div>
                       
                  <p>
                       <span class="field">
                          <input type="submit" value="Update Exam"   onclick="return doconfirm();" name="submit" />
                          <input type="reset" value="Reset" name="reset" />
                      </span>
                  </p>
                    </form>
                    
                    <br clear="all" />
                    
                
                    
                </div><!--content-->
                
            </div><!--maincontentinner-->
            
            <div class="footer">
            	<p>Libyan Internatinal Medical University &copy; 2014. All Rights Reserved. Designed by: <a href="">IT Office</a></p>
            </div><!--footer-->
            
        </div>