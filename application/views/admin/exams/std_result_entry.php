<style type="text/css">
    @media print
    {
        .no-print, .no-print *
        {
            display: none !important;
        }
    }
    .checkbox label, .radio label{
        padding-left: 0px;
    }
    .table_entry{
    	padding: 30px;
    }
</style> 
<div class="content-wrapper" style="min-height: 946px;">  
    <section class="content-header">
        <h1>
            <i class="fa fa-mortar-board"></i> <?php echo $this->lang->line('academics'); ?></h1>
    </section>
    <!-- Main content -->
    <section class="content table_entry">
        <div class="row">
             <table  class="stdtable table table-bordered table-striped table-bordered">
                    <colgroup>
                       <col class="con0" />
                        <col class="con1" />
                      
                    </colgroup>
                    <thead>
                        <tr>
                                                <th class="head0"  >S.No</th>
                        <th class="head1"  >Student ID</th>

                            <th class="head0"  >Name</th>
                            <th class="head1"  >Marks</th>
                            <th class="head0"  >Remarks (Absent)</th>
                            <th class="head1"  >Remarks (Excuse Accepted)</th>
                           
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                                                <th class="head0"  >S.No</th>
                        <th class="head1"  >Student ID</th>

                           <th class="head0"  >Name</th>
                            <th class="head1"  >Marks</th>
                            <th class="head0"  >Remarks (Absent)</th>
                            <th class="head1"  >Remarks (Ea)</th>
                        </tr>
                    </tfoot>
                    <form id="form2" class="stdform stdform2" method="post" action="">
                   <input type="hidden" id="max_marks" value="<?php echo $max_marks ?>" />                           
                    <input type="hidden" id="exam_id" value="<?php echo $exam_id ?>" />
                    <input type="hidden" id="course_id" value="<?php echo $course_id ?>" />
                    <tbody>
                    <?php
					if($stdResults->num_rows()>0){
						$i=0;
						 foreach($stdResults->result_array() as $row){
							 	 
				 
								
	 
						?>
                        <tr>
                         <td class="center" ><?php echo ($i+1)?></td>
                        <td class="center" ><?php echo $row['admission_no']?></td>
                       
                                 <td class="center" >
								 	<?php echo $row['firstname']?> <?php echo $row['middlename']?> <?php echo $row['lastname']?>
                                    <input type="hidden" name="update_result_ids[]" id="update_result_ids_<?php echo $i?>" value="<?php echo $row['id']?>" />
                                    <input type="hidden" name="update_std_ids[]" id="update_std_ids_<?php echo $i?>" value="<?php echo $row['std_id']?>" />
                                 </td>
                                 <td class="center" >
                                 <input type="text" class="form-control" id="update_std_marks_<?php echo $i?>" name="update_std_marks_<?php echo $i?>" value="<?php echo $row['marks']?>" <?php echo ($row["absent"]=='Y'?'readonly="readonly"':'');?>  /></td>
                                 <td class="center" ><input type="checkbox" p="<?php echo $i?>" class="std_marks_checkbox" id="update_absent_<?php echo $i?>" name="update_absent_<?php echo $i?>" value="Y"
                               <?php echo ($row["absent"]=='Y'?'checked="checked"':'');?> 
                                  /> Absent</td>
                                   <td class="center" ><input type="checkbox" p="<?php echo $i?>"  id="update_ea_<?php echo $i?>" name="update_ea_<?php echo $i?>" value="Y"
                               <?php echo ($row["ea"]=='Y'?'checked="checked"':'');?> 
                                 /> E.A</td>
                            </tr>

                 
                        <?php
				 
						$i++;
						}?>
                        <tr class="stdform">
                          	<td>&nbsp; </td>
                            <td class="right">
                            <input type="hidden" name="f_type" id="f_type" value="update" />
                            <input type="hidden" name="total_update" id="total_update" value="<?php echo $i?>" />
                            <input type="button" value="Update" class="btn btn-success" name="update" onclick="update_std_result();" />
                            <input type="reset" value="Reset" class="btn btn-info" name="reset" />
                            </td>
                            <td class="left">&nbsp;</td>
                            </td>
                          </tr>
                          <?php
					} if($results->num_rows()>0){
						$i=1;
						 foreach($results->result_array() as $row){
							  
							 
				?>
                        <tr>
                         <td class="center" ><?php echo $i?></td>
                        <td class="center" ><?php echo $row['admission_no']?></td>
                       
                                 <td class="center" >
								 	<?php echo $row['firstname']?> <?php echo $row['middlename']?> <?php echo $row['lastname']?>
                                    <input type="hidden" name="add_std_id[]" id="add_std_id_<?php echo $i?>" value="<?php echo $row['id']?>" />
                                 </td>
                                 <td class="center" >
                                 <input type="text" class="form-control" name="add_std_marks[]" id="add_std_marks_<?php echo $i?>" value="0" />
                                 </td>
                                 <td class="center" ><input type="checkbox" p="<?php echo $i?>" class="std_marks_checkbox" id="add_absent_<?php echo $i?>" name="add_absent_<?php echo $i?>" value="Y" /> Absent</td>
                                 <td class="center" ><input type="checkbox" p="<?php echo $i?>" id="add_ea_<?php echo $i?>" name="add_ea_<?php echo $i?>" value="Y"   /> E.A</td>
                            </tr>

                        <?php
				$i++; 
						
						}?>
                                                  <tr class="stdform">
                          	<td>&nbsp; </td>
                            <td class="right" id="result_add_buttons">
                            <input type="hidden" name="f_type" id="f_type" value="add" />
                            <input type="hidden" name="total_add" id="total_add" value="<?php echo $i?>" />
                            <input type="button" class="btn btn-success" value="Save" name="submit" onclick="add_std_result();" />
                            <input type="reset" class="btn btn-default" value="Reset" name="reset" />
                            </td>
                            <td class="left">&nbsp;</td>
                            </td>
                          </tr>

						<?php
					}else{?>
                    		<tr> <td colspan="3"> No Record Found </td> </tr>
                    <?php } ?>
                          
                    </tbody>
                    </form>
                </table>
 
        </div> 
    </section>
</div>
<script >
function add_std_result()
{
	var exam_id	 		= jQuery('#exam_id').val();
	var course_id  		= jQuery('#course_id').val();
	var total_add  		= jQuery('#total_add').val();
	var max_marks  		= jQuery('#max_marks').val();
	
	var std_id   =  new Array();
	var marks    =  new Array();
	var absent   =  new Array();
	var ea       =  new Array();
	
	for (i = 0; i < total_add; i++) {
		std_id[i] 	= parseInt(jQuery('#add_std_id_'+i+'').val());
		marks[i] 	= parseFloat(jQuery('#add_std_marks_'+i+'').val());
		if(marks[i]>max_marks){
			alert('Marks should not be greater then('+max_marks+')');
			jQuery('#add_std_id_'+i+'').focus();
			return false;
		}
		
		if (jQuery('#add_absent_'+i).is(':checked')) {
					absent[i] = 'Y';
		}else{
					absent[i] = 'N';
		}
	
	
		if (jQuery('#add_ea_'+i).is(':checked')) {
					ea[i] = 'Y';
		}else{
					ea[i] = 'N';
		}

	}

		var jMarks  = JSON.parse(JSON.stringify(marks));
		var jStdId  = JSON.parse(JSON.stringify(std_id));
		var jAbsent = JSON.parse(JSON.stringify(absent));
		var jEa     = JSON.parse(JSON.stringify(ea));
		
		form2.submit.disabled = true;
        form2.submit.value = "Saving, Please wait...";
		var site_url = '<?php echo base_url() ?>';
		var post_url  = site_url+'admin/exams/ajax';
		jQuery.ajax({
			type: "POST",
			url: post_url,
			data: 	{exam_id:exam_id,course_id:course_id,std_id:jStdId,marks:jMarks,absent:jAbsent,ea:jEa,action:'std_result_add'},
			success: function(html){
				jQuery('#result_add_buttons').hide('slow');
				jQuery('.hide').show('slow');
				jQuery('#message').html(html)
				
	 			
			}
		});

	
}
function update_std_result()
{	
	var exam_id	 		= jQuery('#exam_id').val();
	var course_id  		= jQuery('#course_id').val();
	var total_update  	= jQuery('#total_update').val();
	var max_marks  		= jQuery('#max_marks').val();
	
	var std_id =  new Array();
	var marks  =  new Array();
	var absent =  new Array();
	var ea     =  new Array();
	var ids    =  new Array();

	for (var i = 0; i < total_update; i++) {
		
		std_id[i] 	= parseInt(jQuery('#update_std_ids_'+i+'').val());
		marks[i] 	= parseFloat(jQuery('#update_std_marks_'+i+'').val());
		if(marks[i]>max_marks){
			alert('Marks should not be greater then('+max_marks+')');
			jQuery('#update_std_ids_'+i+'').focus();
			return false;
		}
		ids[i] 		= parseInt(jQuery('#update_result_ids_'+i).val());
		
		

		if (jQuery('#update_absent_'+i).is(':checked')) {
					absent[i] = 'Y';
		}else{
					absent[i] = 'N';
		}
		if (jQuery('#update_ea_'+i).is(':checked')) {
					ea[i] = 'Y';
		}else{
					ea[i] = 'N';
		}
		
}

		var jMarks  = JSON.parse(JSON.stringify(marks));
		var jStdId  = JSON.parse(JSON.stringify(std_id));
		var jAbsent = JSON.parse(JSON.stringify(absent));
		var jEa     = JSON.parse(JSON.stringify(ea));
		var ids 	= JSON.parse(JSON.stringify(ids));
		
		form2.update.disabled = true;
        form2.update.value = "Updating, Please wait...";
		var site_url = '<?php echo base_url() ?>';
		var post_url  = site_url+'admin/exams/ajax';
		
		jQuery.ajax({
			type: "POST",
			url: post_url,
			data: 	{ids:ids,exam_id:exam_id,course_id:course_id,std_id:jStdId,marks:jMarks,absent:jAbsent,ea:jEa,action:'std_result_update'},
			success: function(html){
				jQuery('.hide').show('slow');
				jQuery('#message').html(html)
				
			}
		});
	
}

</script>
<script type="text/javascript">

    function getSectionByClass(class_id, section_id) {
        if (class_id != "" && section_id != "") {
            $('#section_id').html("");
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            $.ajax({
                type: "GET",
                url: base_url + "sections/getByClass",
                data: {
                    'class_id': class_id
                },
                dataType: "json",
                success: function(data) {
                    $.each(data, function(i, obj) {
                        var sel = "";
                        if (section_id == obj.section_id) {
                            sel = "selected";
                        }
                        div_data += "<option value=" + obj.section_id + " " + sel + ">" + obj.section + "</option>";
                    });
                    $('#section_id').append(div_data);
                }
            });
        }
    } 
    $(document).ready(function() {
        var class_id = $('#class_id').val();
        var section_id = '<?php echo set_value('section_id') ?>';
        getSectionByClass(class_id, section_id);
        $(document).on('change', '#class_id', function(e) {
            $('#section_id').html("");
            var class_id = $(this).val();
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            $.ajax({
                type: "GET",
                url: base_url + "sections/getByClass",
                data: {
                    'class_id': class_id
                },
                dataType: "json",
                success: function(data) {
                    $.each(data, function(i, obj) {
                        div_data += "<option value=" + obj.section_id + ">" + obj.section + "</option>";
                    });
                    $('#section_id').append(div_data);
                    
                }
            });

        });
        $('body').on('change','#class_id',function(){
            getcoursesrec();
        })
        $('body').on('change','#academic_year',function(){
            getcoursesrec();
        })
        $('body').on('change','#faculty_id',function(){
            getcoursesrec();
        })
    });

    function getcoursesrec(){
        var faculty_id = $( "#faculty_id option:selected" ).val();
        var academic_year = $( "#academic_year option:selected" ).val();
        var class_id = $( "#class_id option:selected" ).val();
        $('#subject_id').html("");
         var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            $.ajax({
                type: "GET",
                url: base_url + "admin/exams/getCourses",
                data: {
                    'faculty_id': faculty_id,
                    'academic_year': academic_year,
                    'class_id': class_id
                },
                dataType: "json",
                success: function(response) {

                    $.each(response, function(i, obj) {
                        div_data += "<option value=" + obj.subject_id + ">" + obj.name + "</option>";
                    });
                    $('#subject_id').append(div_data);
                    getcoursesdata();
                }
            });
    }

    $(document).on('click', 'input[name=sub_exams]', function(e) {

        var value = this.value;

        if (value == 'Y') {

            $('#sub_exams_y').show('slow');
            $('#sub_exams_n').hide('slow');

            $('#max_marks').removeAttr('data-parsley-required');
            $('#exam_end_date').removeAttr('data-parsley-required');
            $('#max_weightage').attr('data-parsley-required', 'true');
            $('#exam_start_date').removeAttr('data-parsley-required');

        } else if (value == 'N') {

            $('#sub_exams_y').hide('slow');
            $('#sub_exams_n').show('slow');

            $('#max_marks').attr('data-parsley-required', 'true');
            $('#exam_end_date').attr('data-parsley-required', 'true');
            $('#max_weightage').removeAttr('data-parsley-required');
            $('#exam_start_date').attr('data-parsley-required', 'true');

        }
    });
    if($('input[name="sub_exams"]').is(':checked')){
    $('input[name="sub_exams"]:checked').trigger('click');
}
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#btnreset").click(function () {
            $("#form1")[0].reset();
        });
    });
</script>

<script type="text/javascript">
    var base_url = '<?php echo base_url() ?>';
    function printDiv(elem) {
        Popup(jQuery(elem).html());
    }

    function Popup(data)
    {

        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({"position": "absolute", "top": "-1000000px"});
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html>');
        frameDoc.document.write('<head>');
        frameDoc.document.write('<title></title>');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/bootstrap/css/bootstrap.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/font-awesome.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/ionicons.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/AdminLTE.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/skins/_all-skins.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/iCheck/flat/blue.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/morris/morris.css">');


        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/datepicker/datepicker3.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/daterangepicker/daterangepicker-bs3.css">');
        frameDoc.document.write('</head>');
        frameDoc.document.write('<body>');
        frameDoc.document.write(data);
        frameDoc.document.write('</body>');
        frameDoc.document.write('</html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);


        return true;
    }
</script>