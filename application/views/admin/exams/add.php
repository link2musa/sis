<style type="text/css">
    @media print
    {
        .no-print, .no-print *
        {
            display: none !important;
        }
    }
    .checkbox label, .radio label{
        padding-left: 0px;
    }
</style> 
<div class="content-wrapper" style="min-height: 946px;">  
    <section class="content-header">
        <h1>
            <i class="fa fa-mortar-board"></i> <?php echo $this->lang->line('academics'); ?></h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php
            if ($this->rbac->hasPrivilege('subject', 'can_add')) {
                ?>        
                <div class="col-md-6">          
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo $this->lang->line('add_course'); ?></h3>
                        </div>
                        <form  action="<?php echo site_url('admin/exams/add') ?>"  id="assignsubject" name="assignsubject" method="post" accept-charset="utf-8">
                            <div class="box-body">
                                <?php if ($this->session->flashdata('msg')) { ?>
                                    <?php echo $this->session->flashdata('msg') ?>
                                <?php } ?>     
                                <?php // echo $this->customlib->getCSRF(); ?>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('faculty'); ?></label> <small class="req"> *</small>
                                                <select id="faculty_id" name="faculty_id" class="form-control">
                                                    <?php
                                                foreach ($faculties as $faculty) {
                                                ?>
                                                    <option value="<?php echo $faculty['id'] ?>" <?php
                                                    if (set_value('faculty_id') == $faculty['id']) {
                                                        echo "selected=selected";
                                                    }
                                                    ?>><?php echo $faculty['name'] ?></option>
                                                <?php
                                                }
                                                   ?>
                                                </select>
                                                <span class="text-danger"><?php echo form_error('faculty_id'); ?></span>
                                            </div>

                                            <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('academic_year'); ?> </label><small class="req"> *</small>
                                                        <select id="academic_year" name="academic_year" class="form-control">
                                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                            <?php
                                                            foreach ($sessionlist as $session) {
                                                            ?>
                                                                <option value="<?php echo $session['id'] ?>" <?php if ($session['is_active'] != "no") echo "selected=selected"; ?> <?php if (set_value('academic_year') == $session['id']) echo "selected=selected"; ?> <?php if ($session['id'] == $active_session_id){echo "selected=selected"; } ?>  ><?php echo $session['session'] ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>

                                                         <span class="text-danger"><?php echo form_error('academic_year'); ?></span>
                                                    </div>

                                                   <div class="form-group">
                                        <label><?php echo $this->lang->line('class'); ?></label>
                                        <select autofocus="" id="class_id" name="class_id" class="form-control">
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                            <?php
                                            $count = 0;
                                            foreach ($classlist as $class) {
                                            ?>
                                                <option value="<?php echo $class['id'] ?>" <?php if (set_value('class_id') == $class['id']) {
                                                    echo "selected=selected";
                                                }
                                                ?>><?php echo $class['class'] ?></option>
                                            <?php
                                                $count++;
                                            }
                                            ?>
                                        </select>
                                        <span class="text-danger"><?php echo form_error('class_id'); ?></span>
                                    </div>
                                            <div class="form-group">
                                        <label><?php echo $this->lang->line('section'); ?></label>
                                        <select id="section_id" name="section_id" class="form-control">
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                        </select>
                                    </div>
                                            <div class="form-group">
                            <label><?php echo $this->lang->line('courses'); ?></label><small class="req"> *</small>
                            <select autofocus="" id="subject_id" name="subject_id" class="form-control" >
                                
                            </select>
                            <span class="text-danger"><?php echo form_error('subject_id'); ?></span>
                        </div>

                        <!-- Exam Name -->
                                        <div class="form-group">
                                            <label><?= lang('exam_name'); ?> <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="exam_name" name="exam_name" value="<?= ($this->input->post('exam_name')) ? $this->input->post('exam_name') : (isset($exam) ? $exam['exam_name'] : ''); ?>" data-parsley="true" />
                                                <span class="text-danger"><?php echo form_error('exam_name'); ?></span> 
                                        </div>

                                        <!-- Sub Exam -->
                                        <div class="form-group">
                                            <label >Sub Exam <span class="text-danger">*</span></label>

                                            <?php $sub_exams = ($this->input->post('sub_exams')) ? $this->input->post('sub_exams') : (isset($exam) ? $exam['sub_exams'] : 'Y'); ?>

                                                <div class="radio radio-css radio-inline">
                                                    <input type="radio" id="Yes" name="sub_exams" value="Y" <?= ($sub_exams == 'Y' or empty($sub_exams)) ? 'checked' : ''; ?>  />
                                                    <label for="Yes">Yes</label>
                                                </div>

                                                <div class="radio radio-css radio-inline" style="margin-top: 10px;">
                                                    <input type="radio" id="No" name="sub_exams" value="N" <?= ($sub_exams == 'N') ? 'checked' : ''; ?>  />
                                                    <label for="No">No</label>
                                                </div>
                                        </div>

                                        <div class="<?= ($sub_exams == 'Y' or empty($sub_exams)) ? '' : 'display-none'; ?>" id="sub_exams_y">

                                            <!-- Sub Exam Maximum Marks -->
                                            <div class="form-group">
                                                <label >Sub Exam Maximum Marks <span class="text-danger">*</span></label>
                                                    <input type="number" class="form-control" id="max_weightage" name="max_weightage" <?php if ($sub_exams == 'Y') { ?> data-parsley-group="step-1" data-parsley-required="true" <?php } ?> data-parsley-maxlength="5" min="0" value="<?= ($this->input->post('max_weightage')) ? $this->input->post('max_weightage') : (isset($exam) ? $exam['max_weightage'] : 0); ?>" />
                                            </div>

                                            <!-- Sub Exam Minimum Marks -->
                                            <div class="form-group">
                                                <label >Sub Exam Minimum Marks</label>
                                                    <input type="text" class="form-control" id="min_weightage" name="min_weightage" value="<?= ($this->input->post('min_weightage')) ? $this->input->post('min_weightage') : (isset($exam) ? $exam['min_weightage'] : 'NO'); ?>" />
                                            </div>

                                        </div>

                                        <div class="<?= ($sub_exams == 'N') ? '' : 'display-none'; ?>" id="sub_exams_n">

                                            <!-- Exam Start Date/Time -->
                                            <div class="form-group">
                                                <label >Exam Start Date/Time <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control date" id="start_date" name="exam_start_date" <?php if ($sub_exams == 'N') { ?> data-parsley-group="step-1" data-parsley-required="true" <?php } ?> value="<?= ($this->input->post('exam_start_date')) ? $this->input->post('exam_start_date') : (isset($exam) ? $exam['exam_start_date'] : ''); ?>" placeholder="Select Start Date/Time" />
                                            </div>

                                            <!-- Exam End Date/Time -->
                                            <div class="form-group">
                                                <label >Exam End Date/Time <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control date" id="end_date" name="exam_end_date" <?php if ($sub_exams == 'N') { ?> data-parsley-group="step-1" data-parsley-required="true" <?php } ?> value="<?= ($this->input->post('exam_end_date')) ? $this->input->post('exam_end_date') : (isset($exam) ? $exam['exam_end_date'] : ''); ?>" placeholder="Select End Date/Time" />
                                            </div>

                                            <!-- Max Marks -->
                                            <div class="form-group">
                                                <label >Maximum Marks <span class="text-danger">*</span></label>
                                                    <input type="number" class="form-control" id="max_marks" name="max_marks" <?php if ($sub_exams == 'N') { ?> data-parsley-group="step-1" data-parsley-required="true" <?php } ?> data-parsley-maxlength="5" min="0" value="<?= ($this->input->post('max_marks')) ? $this->input->post('max_marks') : (isset($exam) ? $exam['max_marks'] : 0); ?>" />
                                            </div>

                                            <!-- Min Marks -->
                                            <div class="form-group">
                                                <label>Minimum Marks</label>
                                                    <input type="text" class="form-control" id="min_marks" name="min_marks" value="<?= ($this->input->post('min_marks')) ? $this->input->post('min_marks') : (isset($exam) ? $exam['min_marks'] : 'NO'); ?>" />
                                            </div>
                                    

                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></button> 
                            </div>
                        </form>
                    </div>
                </div>
            <?php } ?>
 
        </div> 
    </section>
</div>
<script type="text/javascript">
    function getSectionByClass(class_id, section_id) {
        if (class_id != "" && section_id != "") {
            $('#section_id').html("");
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            $.ajax({
                type: "GET",
                url: base_url + "sections/getByClass",
                data: {
                    'class_id': class_id
                },
                dataType: "json",
                success: function(data) {
                    $.each(data, function(i, obj) {
                        var sel = "";
                        if (section_id == obj.section_id) {
                            sel = "selected";
                        }
                        div_data += "<option value=" + obj.section_id + " " + sel + ">" + obj.section + "</option>";
                    });
                    $('#section_id').append(div_data);
                }
            });
        }
    } 
    $(document).ready(function() {
        var class_id = $('#class_id').val();
        var section_id = '<?php echo set_value('section_id') ?>';
        getSectionByClass(class_id, section_id);
        $(document).on('change', '#class_id', function(e) {
            $('#section_id').html("");
            var class_id = $(this).val();
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            $.ajax({
                type: "GET",
                url: base_url + "sections/getByClass",
                data: {
                    'class_id': class_id
                },
                dataType: "json",
                success: function(data) {
                    $.each(data, function(i, obj) {
                        div_data += "<option value=" + obj.section_id + ">" + obj.section + "</option>";
                    });
                    $('#section_id').append(div_data);
                    
                }
            });

        });
        $('body').on('change','#class_id',function(){
            getcoursesrec();
        })
        $('body').on('change','#academic_year',function(){
            getcoursesrec();
        })
        $('body').on('change','#faculty_id',function(){
            getcoursesrec();
        })
    });

    function getcoursesrec(){
        var faculty_id = $( "#faculty_id option:selected" ).val();
        var academic_year = $( "#academic_year option:selected" ).val();
        var class_id = $( "#class_id option:selected" ).val();
        $('#subject_id').html("");
         var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            $.ajax({
                type: "GET",
                url: base_url + "admin/exams/getCourses",
                data: {
                    'faculty_id': faculty_id,
                    'academic_year': academic_year,
                    'class_id': class_id
                },
                dataType: "json",
                success: function(response) {

                    $.each(response, function(i, obj) {
                        div_data += "<option value=" + obj.subject_id + ">" + obj.name + "</option>";
                    });
                    $('#subject_id').append(div_data);
                    getcoursesdata();
                }
            });
    }

    $(document).on('click', 'input[name=sub_exams]', function(e) {

        var value = this.value;

        if (value == 'Y') {

            $('#sub_exams_y').show('slow');
            $('#sub_exams_n').hide('slow');

            $('#max_marks').removeAttr('data-parsley-required');
            $('#exam_end_date').removeAttr('data-parsley-required');
            $('#max_weightage').attr('data-parsley-required', 'true');
            $('#exam_start_date').removeAttr('data-parsley-required');

        } else if (value == 'N') {

            $('#sub_exams_y').hide('slow');
            $('#sub_exams_n').show('slow');

            $('#max_marks').attr('data-parsley-required', 'true');
            $('#exam_end_date').attr('data-parsley-required', 'true');
            $('#max_weightage').removeAttr('data-parsley-required');
            $('#exam_start_date').attr('data-parsley-required', 'true');

        }
    });
    if($('input[name="sub_exams"]').is(':checked')){
    $('input[name="sub_exams"]:checked').trigger('click');
}
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#btnreset").click(function () {
            $("#form1")[0].reset();
        });
    });
</script>

<script type="text/javascript">
    var base_url = '<?php echo base_url() ?>';
    function printDiv(elem) {
        Popup(jQuery(elem).html());
    }

    function Popup(data)
    {

        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({"position": "absolute", "top": "-1000000px"});
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html>');
        frameDoc.document.write('<head>');
        frameDoc.document.write('<title></title>');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/bootstrap/css/bootstrap.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/font-awesome.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/ionicons.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/AdminLTE.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/skins/_all-skins.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/iCheck/flat/blue.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/morris/morris.css">');


        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/datepicker/datepicker3.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/daterangepicker/daterangepicker-bs3.css">');
        frameDoc.document.write('</head>');
        frameDoc.document.write('<body>');
        frameDoc.document.write(data);
        frameDoc.document.write('</body>');
        frameDoc.document.write('</html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);


        return true;
    }
</script>