<!DOCTYPE html>
<html  dir="rtl" lang="ar">
    <head>
        <meta charset="utf-8" />
        <title>Result Report</title>

        <style>
            @media print {
        .pagebreak { page-break-before: always; } /* page-break-after works, as well */
    }
            .invoice-box { 
                max-width: 800px;
                margin: auto;
                padding: 30px;
                border: 1px solid #eee;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
                font-size: 12px;
                line-height: 24px;
                font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                color: #555;
            }

            .invoice-box table {
                width: 100%;
                line-height: inherit;
                text-align: left;
            }

            .invoice-box table td {
                padding: 2px;
                vertical-align: top;
            }
            @media only screen and (max-width: 600px) {
                .invoice-box table tr.top table td {
                    width: 100%;
                    display: block;
                    text-align: center;
                }

                .invoice-box table tr.information table td {
                    width: 100%;
                    display: block;
                    text-align: center;
                }
            }

            /** RTL **/
            .invoice-box.rtl {
                direction: rtl;
                font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            }

            .invoice-box.rtl table {
                text-align: right;
                border: 1px solid #ccc;
                border-collapse: collapse;
            }
            .invoice-box.rtl table td,.invoice-box.rtl table th {
                border: 1px solid #ccc;
            }

            .invoice-box.rtl table tr td:nth-child(2) {
                text-align: left;
            }
            .main_head{
                margin-top: 5px;
                display: block;
                overflow: hidden;
            }
            .main_head h3{
                text-align: center;
                font-size: 14px;
                display: block;
            }
            .foot .left{
                float: left;
            }
            .foot .right{
                float: right;
                font-size: 10px;
            }
            .std_detail .left{
                float: left;
                font-size: 10px;
            }
            .std_detail .right{
                float: right;
            }
            .invoice-box th{
                font-size: 10px;
            }
            .foot{
                display: flow-root;
            }
            .invoice-box img{
                width: 84px; float: left;
            }
        </style> 
    </head>

    <body>
        <div class="invoice-box rtl">
            
            <?php echo $html_data; ?>
            
        </div>
    </body>
</html>