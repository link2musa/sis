<style type="text/css">
    @media print
    {
        .no-print, .no-print *
        {
            display: none !important;
        }
    }
</style> 
<div class="content-wrapper" style="min-height: 946px;">  
    <section class="content-header">
        <h1>
            <i class="fa fa-mortar-board"></i> <?php echo $this->lang->line('academics'); ?></h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php
            if ($this->rbac->hasPrivilege('subject', 'can_add')) {
                ?>        
                <div class="col-md-4">          
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo $this->lang->line('add_course'); ?></h3>
                        </div>
                        <form  action="<?php echo site_url('admin/subject/assign_subject') ?>"  id="assignsubject" name="assignsubject" method="post" accept-charset="utf-8">
                            <div class="box-body">
                                <?php if ($this->session->flashdata('msg')) { ?>
                                    <?php echo $this->session->flashdata('msg') ?>
                                <?php } ?>     
                                <?php // echo $this->customlib->getCSRF(); ?>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('faculty'); ?></label> <small class="req"> *</small>
                                                <select id="faculty_id" name="faculty_id" class="form-control">
                                                    <?php
                                                foreach ($faculties as $faculty) {
                                                ?>
                                                    <option value="<?php echo $faculty['id'] ?>" <?php
                                                    if (set_value('faculty_id') == $faculty['id']) {
                                                        echo "selected=selected";
                                                    }
                                                    ?>><?php echo $faculty['name'] ?></option>
                                                <?php
                                                }
                                                   ?>
                                                </select>
                                                <span class="text-danger"><?php echo form_error('faculty_id'); ?></span>
                                            </div>

                                            <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('academic_year'); ?> </label><small class="req"> *</small>
                                                        <select id="academic_year" name="academic_year" class="form-control">
                                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                            <?php
                                                            foreach ($sessionlist as $session) {
                                                            ?>
                                                                <option value="<?php echo $session['id'] ?>" <?php if ($session['is_active'] != "no") echo "selected=selected"; ?> <?php if (set_value('academic_year') == $session['id']) echo "selected=selected"; ?> <?php if ($session['id'] == $active_session_id){echo "selected=selected"; } ?>  ><?php echo $session['session'] ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>

                                                         <span class="text-danger"><?php echo form_error('academic_year'); ?></span>
                                                    </div>

                                                   <div class="form-group">
                                                <label><?php echo $this->lang->line('class'); ?></label> <small class="req"> *</small>
                                                <select autofocus="" id="class_id" name="class_id" class="form-control" >
                                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                    <?php
                                                    $count = 0;
                                                    foreach ($classlist as $class) {
                                                        ?>
                                                        <option value="<?php echo $class['id'] ?>" <?php if (set_value('class_id') == $class['id']) {
                                                        echo "selected=selected";
                                                    }
                                                    ?>><?php echo $class['class'] ?></option>
                                                        <?php
                                                    $count++;
                                                    }
                                                    ?>
                                                </select> 
                                                 <span class="text-danger"><?php echo form_error('class_id'); ?></span>
                                            </div>
                                            <div class="form-group">
                            <label><?php echo $this->lang->line('courses'); ?></label><small class="req"> *</small>
                            <select autofocus="" id="subject_id" name="subject_id" class="form-control" >
                                <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                            <?php
                            foreach ($subjectlist as $subject) {
                                $sub_code=($subject['code'] != "") ? " (".$subject['code'].")":"";
                                ?>
                                     

                                                                <option value="<?php echo $subject['id'] ?>" <?php
                            if (set_value('subject_id') == $subject['id']) {
                                    echo "selected=selected";
                                }
                                ?>><?php echo $subject['name'].$sub_code; ?></option>
                                                                        <?php
                            } 
                            ?>
                            </select>
                            <span class="text-danger"><?php echo form_error('subject_id'); ?></span>
                        </div>
                        <div class="form-group">
                                    <label for="teacher"><?php echo $this->lang->line('teachers'); ?><small class="req"> *</small></label>
                                    <select class="form-control" name="teacher" id="teacher">
                                        <option value=""><?php echo $this->lang->line('select') ?></option>
                                        <?php
                                        if (!empty($staff_list)) {
                                            foreach ($staff_list as $staff_key => $staff_value) {
                                                ?>
                                                <option value="<?php echo $staff_value['id']; ?>"><?php echo $staff_value["name"] . " " . $staff_value["surname"] . " (" . $staff_value['employee_id'] . ")"; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('teacher'); ?></span>
                                </div>
                        

                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            <?php } ?>
            <div class="col-md-<?php
            if ($this->rbac->hasPrivilege('subject', 'can_add')) {
                echo "8";
            } else {
                echo "12";
            }
            ?>">            
                <div class="box box-primary" id="sublist">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix"><?php echo $this->lang->line('course_list'); ?></h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label"><?php echo $this->lang->line('subject_list'); ?></div>
                            <table class="table table-striped table-bordered table-hover example">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->lang->line('faculty'); ?></th>
                                        <th><?php echo $this->lang->line('academic_year'); ?></th>
                                        <th><?php echo $this->lang->line('class'); ?></th>
                                        <th><?php echo $this->lang->line('courses'); ?></th>
                                        <th><?php echo $this->lang->line('teachers'); ?></th>
                                        <th class="text-right no-print"><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $count = 1;
                                    foreach ($assignedtlist as $row) {
                                        ?>
                                        <tr>
                                            <td> <?php echo $row['faculty_name'] ?></td>
                                            <td><?php echo $row['session'] ?></td>
                                            <td><?php echo $row['class'] ?></td>
                                            <td><?php echo $row['subject_name'] ?></td>
                                            <td><?php echo $row['staff_name'].' '.$row['surname'].' ('.$row['employee_id'].')' ?></td>
                                            <td class="mailbox-date pull-right no-print">
                                                <?php
                                                if ($this->rbac->hasPrivilege('subject', 'can_edit')) {
                                                    ?>
                                                    <a data-placement="left" href="<?php echo base_url(); ?>admin/subject/editassign/<?php echo $row['id'] ?>" class="btn btn-default btn-xs"  data-toggle="tooltip" title="<?php echo $this->lang->line('edit'); ?>">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <?php
                                                }
                                                if ($this->rbac->hasPrivilege('subject', 'can_delete')) {
                                                    ?>
                                                    <a data-placement="left" href="<?php echo base_url(); ?>admin/subject/deleteassign/<?php echo $row['id'] ?>"class="btn btn-default btn-xs"  data-toggle="tooltip" title="<?php echo $this->lang->line('delete'); ?>" onclick="return confirm('<?php echo $this->lang->line('delete_confirm') ?>');">
                                                        <i class="fa fa-remove"></i>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    $count++;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> 

        </div> 
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#btnreset").click(function () {
            $("#form1")[0].reset();
        });
    });
</script>

<script type="text/javascript">
    var base_url = '<?php echo base_url() ?>';
    function printDiv(elem) {
        Popup(jQuery(elem).html());
    }

    function Popup(data)
    {

        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({"position": "absolute", "top": "-1000000px"});
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html>');
        frameDoc.document.write('<head>');
        frameDoc.document.write('<title></title>');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/bootstrap/css/bootstrap.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/font-awesome.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/ionicons.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/AdminLTE.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/skins/_all-skins.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/iCheck/flat/blue.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/morris/morris.css">');


        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/datepicker/datepicker3.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/daterangepicker/daterangepicker-bs3.css">');
        frameDoc.document.write('</head>');
        frameDoc.document.write('<body>');
        frameDoc.document.write(data);
        frameDoc.document.write('</body>');
        frameDoc.document.write('</html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);


        return true;
    }
</script>