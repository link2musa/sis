<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-ioxhost"></i> <?php echo $this->lang->line('front_office'); ?></h1>
    </section>
    <section class="content">
        <div class="row">
            <?php if ($this->rbac->hasPrivilege('social_awareness', 'can_add')) {?>
                <div class="col-md-4">
                    <!-- Horizontal Form -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('social_awareness'); ?></h3>
                        </div><!-- /.box-header -->
                        <form id="form1" action="<?php echo site_url('admin/visitors/social_awareness') ?>"   method="post" accept-charset="utf-8" enctype="multipart/form-data" >
                            <div class="box-body">
                                <?php echo $this->session->flashdata('msg') ?>
                               
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="pwd"><?php echo $this->lang->line('date'); ?></label><input type="text" id="date" class="form-control date" value="<?php echo set_value('date', date($this->customlib->getSchoolDateFormat())); ?>"  name="date" readonly="">
                                        <span class="text-danger"><?php echo form_error('date'); ?></span>
                                    </div>
                                </div>
                                    <div class="form-group">  
                                        <label><?php echo $this->lang->line('class'); ?></label>
                                        <select autofocus="" id="class_id" name="class_id" class="form-control select2 visitor_class">
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                            <?php
                                            $count = 0;
                                            foreach ($classlist as $class) {
                                            ?>
                                                <option value="<?php echo $class['id'] ?>" <?php if (set_value('class_id') == $class['id']) {
                                                    echo "selected=selected";
                                                }
                                                ?>><?php echo $class['class'] ?></option>
                                            <?php
                                                $count++;
                                            }
                                            ?>
                                        </select>
                                        <span class="text-danger" id="error_class_id"></span>
                                    </div>
                                    <div class="form-group">  
                                        <label><?php echo $this->lang->line('awareness_type'); ?></label>
                                        <select autofocus="" id="class_id" name="awareness_type" class="form-control select2 ">
                                            <option value=""><?php echo $this->lang->line('awareness_type'); ?></option>
                                            <?php
                                            $count = 0;
                                            foreach ($awareness_types as $aware) {
                                            ?>
                                                <option value="<?php echo $aware['id'] ?>" <?php if (set_value('aware_id') == $aware['id']) {
                                                    echo "selected=selected";
                                                }
                                                ?>><?php echo $aware['awareness'] ?></option>
                                            <?php
                                                $count++;
                                            }
                                            ?>
                                        </select>
                                        <span class="text-danger" id="error_class_id"></span>
                                    </div>
                                <div class="form-group">
                                    <label for="pwd"><?php echo $this->lang->line('awareness_place'); ?></label>
                                    <input type="text" class="form-control" value="<?php echo set_value('awareness_place'); ?>" name="awareness_place">

                                </div>
                               
                                <div class="form-group">
                                    <label for="pwd"><?php echo $this->lang->line('comment'); ?></label>
                                    <textarea class="form-control" id="description" name="comment" name="comment" rows="3"><?php echo set_value('comment'); ?></textarea>
                                    <span class="text-danger"><?php echo form_error('date'); ?></span>
                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></button>
                            </div>
                        </form>
                    </div>

                </div><!--/.col (right) -->
                <!-- left column -->
            <?php }?>

            <div class="col-md-<?php
if ($this->rbac->hasPrivilege('visitor_book', 'can_add')) {
    echo "8";
} else {
    echo "12";
}
?>">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix"><?php echo $this->lang->line('social_awareness'); ?> <?php echo $this->lang->line('list'); ?></h3>
                        <div class="box-tools pull-right">
                        </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="download_label"><?php echo $this->lang->line('social_awareness'); ?> <?php echo $this->lang->line('list'); ?></div>
                        <div class="mailbox-messages table-responsive">
                            <table class="table table-hover table-striped table-bordered example">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->lang->line('date'); ?></th>
                                        <th><?php echo $this->lang->line('class'); ?></th>
                                        <th><?php echo $this->lang->line('awareness_type'); ?></th>
                                        <th><?php echo $this->lang->line('awareness_place'); ?></th>
                                        <th><?php echo $this->lang->line('comment'); ?></th>
                                        <th class="text-right"><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
if (empty($visitor_list)) {
    ?>

                                        <?php
} else {
    foreach ($visitor_list as $key => $value) { 

        ?>
                                            <tr>
                                               <td class="mailbox-name"> <?php echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($value['date'])); ?></td>
                                                <td class="mailbox-name"><?php echo $value['class']; ?></td>
                                                
                                                <td class="mailbox-name"><?php echo $value['awareness_type']; ?> </td>
                                                <td class="mailbox-name"> <?php echo $value['awareness_place']; ?></td>
                                                <td class="mailbox-name"> <?php echo $value['comments']; ?></td>
                                                <td class="mailbox-date pull-right white-space-nowrap">
                                                   
                                                       <!--  <a data-placement="left" href="<?php echo base_url(); ?>admin/visitors/edit/<?php echo $value['id']; ?>" class="btn btn-default btn-xs" data-toggle="tooltip" title="<?php echo $this->lang->line('edit'); ?>">
                                                            <i class="fa fa-pencil"></i>
                                                        </a> -->

                                                        <a data-placement="left" href="<?php echo base_url(); ?>admin/visitors/delete_social_awareness/<?php echo $value['id']; ?>" class="btn btn-default btn-xs" data-toggle="tooltip" title="<?php echo $this->lang->line('delete'); ?>" onclick="return confirm('<?php echo $this->lang->line('delete_confirm') ?>');" data-original-title="<?php echo $this->lang->line('delete'); ?>">
                                                                <i class="fa fa-remove"></i>
                                                            </a>
                                                </td>

                                            </tr>
                                            <?php
}
}
?>

                                </tbody>
                            </table><!-- /.table -->
                        </div><!-- /.mail-box-messages -->
                    </div><!-- /.box-body -->
                </div>
            </div><!--/.col (left) col-8 end-->
            <!-- right column -->
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<!-- new END -->
<div id="visitordetails" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog2 modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $this->lang->line('details'); ?></h4>
            </div>
            <div class="modal-body" id="getdetails">

            </div>
        </div>
    </div> 
</div>
</div><!-- /.content-wrapper -->
<link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/timepicker/bootstrap-timepicker.min.css">
<script src="<?php echo base_url(); ?>backend/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script type="text/javascript">
     $(document).ready(function () {
        $('.select2').select2();
 
       
    });
</script>
<script type="text/javascript">

                                                                $(function () {

                                                                    $(".timepicker").timepicker({

                                                                    });
                                                                });

                                                                function getRecord(id) {
                                                                    $.ajax({
                                                                        url: '<?php echo base_url(); ?>admin/visitors/details/' + id,
                                                                        success: function (result) {

                                                                            $('#getdetails').html(result);
                                                                        }

                                                                    });
                                                                }

</script>
