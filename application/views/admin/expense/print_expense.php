<!DOCTYPE html>
<html>
<head>
	<title>Receipt</title>
	<style type="text/css">
		#invoice-POS{
  padding:2mm;
  margin: 0 auto;
  width: 100mm;
  background: #FFF;
  border: 1px solid #ddd;
  font-family: sans-serif;
::selection {background: #f31544; color: #FFF;}
::moz-selection {background: #f31544; color: #FFF;}
}
h1{
  font-size: 1.5em;
  color: #222;
}
h2{font-size: .9em; font-family: sans-serif;}
h3{
  font-size: 1.2em;
  font-weight: 300;
  line-height: 2em;
}
p{
  font-size: .7em;
  color: #666;
  line-height: 1.2em;
}
 
#top, #mid,#bot{ /* Targets all id with 'col-' */
  border-bottom: 1px solid #EEE;
}

#top{min-height: 100px;}
#mid{min-height: 80px;} 
#bot{ min-height: 50px;}

.clientlogo{
  float: left;
	height: 60px;
	width: 60px;
	background: url(http://michaeltruong.ca/images/client.jpg) no-repeat;
	background-size: 60px 60px;
  border-radius: 50px;
}
.info{
  display: block;
  //float:left;
  margin-left: 0;
}
.title{
  float: right;
}
.title p{text-align: right;} 
table{
  width: 100%;
  border-collapse: collapse;
  border: 1px solid #ddd;
}
td{
  padding: 5px 0 5px 15px;
  border: 1px solid #EEE
}
th{
  border: 1px solid #cdcbcb;
}
.tabletitle{
  font-size: 10px;
  background: #EEE;
}
.service{border-bottom: 1px solid #EEE;}
.item{width: 24mm;}
.itemtext{font-size: .6em;}

#legalcopy{
  margin-top: 5mm;
}

.voucher_lbl{
	background-color: #ccdef5;
    border: 2px solid #7ca5d9;
}
.voucher_lbl h2{
	color: #1a6ad1;
}
@media print {
body {-webkit-print-color-adjust: exact;}
}
.left {float:left; text-align: center;}
.right {float:right; text-align: center;}
.clear {clear:both}

.left h2,.right h2{
	line-height: 5px;
}
.approval_info h2{
	font-size: 10px;
}
.info h3{
	font-size: 10px;
	line-height: 5px;
}
.signature_info{
	margin-top: 20px;
}

	</style>


</head>
<body>

  <div id="invoice-POS">
    
    <center id="top">
      <div class="logo"></div>
      <div class="info"> 
        <h2>PAKISTAN EMBASSY SCHOOL & COLLEGE BENGHAZI LIBYA</h2>
      </div><!--End Info-->
      <div class="info voucher_lbl"> 
        <h2>PAYMENT VOUCHER</h2>
      </div><!--End Info-->
    </center><!--End InvoiceTop-->
    
    <div id="mid">
      <div class="info">
        <div class="row">
        	<div class="left clear">
        		<h2>Date</h2>
        		<h2><?php echo date('d-M-y',strtotime($expense['date'])); ?></h2>
        	</div>
        	<div class="right">
        		<h2>Voucher No</h2>
        		<h2><?php echo isset($expense['id']) ? $expense['id'] : '';  ?></h2>
        	</div>
        </div>
      </div>
    </div><!--End Invoice Mid-->
    
    <div id="">

					<div id="table">
						<table >
							<thead>
								<tr class="tabletitle">
								<th class="item"><h2>Expense Head</h2></th>
								<th class="item"><h2>PARTICULARS / DETAILS</h2></th>
								<th class="item"><h2>Debit</h2></th>
								<th class="item"><h2>Invoice No</h2></th>
								<th class="item"><h2>Payment Mode</h2></th>
								</tr>
							</thead>

							<tr class="service" >
								<td class="tableitem">
									<b class="itemtext">Payee Ref.</b>
									<p><?php echo isset($expense['exp_category']) ? $expense['exp_category'] : '';  ?></p>
								</td>
								<td class="tableitem">
									<p class="itemtext"><?php echo isset($expense['name']) ? $expense['name'] : '';  ?></p>
								</td>
								<td class="tableitem">
									<p class="itemtext"><?php echo isset($expense['amount']) ? $expense['amount'] : '';  ?><?php echo isset($expense['currency']) ? $expense['currency'] : '';  ?></p>
								</td>
								<td class="tableitem">
									<p class="itemtext"><?php echo isset($expense['invoice_no']) ? $expense['invoice_no'] : '';  ?></p>
								</td>
								<td class="tableitem">
									<p class="itemtext"><?php echo isset($expense['mode_name']) ? $expense['mode_name'] : '';  ?></p>
								</td>
							</tr>
							<tr class="service">
								<td class="tableitem" >
									
								</td>
								<td class="tableitem" colspan="4">
									<p class="itemtext"><?php echo isset($expense['converted']) ? $expense['converted'] : '';  ?></p>
								</td>
							</tr>
							<tr class="service">
								<td class="tableitem">
									<b class="itemtext"></b>
									<p></p>
								</td>
								<td class="tableitem">
									<p class="itemtext">Total</p>
								</td>
								<td class="tableitem">
									<p class="itemtext"><?php echo isset($expense['amount']) ? $expense['amount'] : '';  ?><?php echo isset($expense['currency']) ? $expense['currency'] : '';  ?></p>
								</td>
								<td class="tableitem">
									<p class="itemtext"></p>
								</td>
								<td class="tableitem">
									<p class="itemtext"></p>
								</td>
							</tr>
							<tr class="service">
								<td class="tableitem" >
									<b class="itemtext">Description</b>
								</td>
								<td class="tableitem" colspan="4">
									<p class="itemtext"><?php echo isset($expense['note']) ? $expense['note'] : '';  ?></p>
								</td>
							</tr>

						</table>
					</div><!--End Table-->
					<div class="approval_info">
						<div class="row">
			        	<div class="left clear">
			        		<h2>Approved By: <?php echo isset($expense['auth_name']) ? $expense['auth_name'] : '';  ?></h2>
			        	</div>
			        	<div class="right">
			        		<h2>Approval Ref. <?php echo isset($expense['approval_ref']) ? $expense['approval_ref'] : '';  ?></h2>
			        	</div>
			          </div>
		           </div>
		           <br><br>
					<div id="mid" class="signature_info">
				      <div class="info">
				        <div class="row">
				        	<div class="left clear">
				        		<h2><?php echo isset($sign[0]['name']) ? $sign[0]['name'] : '';  ?></h2>
				        		<h3><?php echo isset($sign[0]['designation']) ? $sign[0]['designation'] : '';  ?></h3>
				        		<h3><?php echo isset($sign[0]['third_line']) ? $sign[0]['third_line'] : '';  ?></h3>
				        	</div>
				        	<div class="right">
				        		<h2><?php echo isset($sign[1]['name']) ? $sign[1]['name'] : '';  ?></h2>
				        		<h3><?php echo isset($sign[1]['designation']) ? $sign[1]['designation'] : '';  ?></h3>
				        		<h3><?php echo isset($sign[1]['third_line']) ? $sign[1]['third_line'] : '';  ?></h3>
				        	</div>
				        </div>
				      </div>

				    </div><!--End Invoice Mid-->
				    <p>Print Date: <?php echo date('d-M-y'); ?></p>
				</div><!--End InvoiceBot-->
  </div><!--End Invoice-->
  <script type="text/javascript">
  	window.print();
  </script>
</body>
</html>