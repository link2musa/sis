<?php
$currency_symbol = $this->customlib->getSchoolCurrencyFormat();
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header"> 
        <h1>
            <i class="fa fa-credit-card"></i> <?php echo $this->lang->line('expenses'); ?> <small><?php echo $this->lang->line('student_fee'); ?></small></h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php
            if ($this->rbac->hasPrivilege('expense', 'can_add') || $this->rbac->hasPrivilege('expense', 'can_edit')) {
                ?>
                <div class="col-md-4">
                    <!-- Horizontal Form -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo $this->lang->line('edit_expense'); ?></h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->

                        <form action="<?php echo site_url("admin/expense/edit/" . $id) ?>"  id="employeeform" name="employeeform" method="post" accept-charset="utf-8"  enctype="multipart/form-data">
                            <div class="box-body">

                                <?php if ($this->session->flashdata('msg')) { ?>
                                    <?php echo $this->session->flashdata('msg') ?>
                                <?php } ?>
                                <?php
                                if (isset($error_message)) {
                                    echo "<div class='alert alert-danger'>" . $error_message . "</div>";
                                }
                                ?>   
                                <?php echo $this->customlib->getCSRF(); ?>                       
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('expense_head'); ?></label><small class="req"> *</small>
                                    <select autofocus="" id="exp_head_id" name="exp_head_id" class="form-control select2" >
                                        <option value=""><?php echo $this->lang->line('select'); ?></option>
                                        <?php
                                        foreach ($expheadlist as $exphead) {
                                            ?>
                                            <option value="<?php echo $exphead['id'] ?>"<?php
                                            if ($expense['exp_head_id'] == $exphead['id']) {
                                                echo "selected =selected";
                                            }
                                            ?>><?php echo $exphead['exp_category'] ?></option>
                                                    <?php
                                                    $count++;
                                                }
                                                ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('exp_head_id'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('name'); ?></label><small class="req"> *</small>
                                    <input id="name" name="name" placeholder="" type="text" class="form-control"  value="<?php echo set_value('name', $expense['name']); ?>" />
                                    <span class="text-danger"><?php echo form_error('name'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('date'); ?></label><small class="req"> *</small>
                                    <input id="date" name="date" placeholder="" type="text" class="form-control date"  value="<?php echo set_value('date', date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($expense['date']))); ?>" readonly="readonly" />
                                    <span class="text-danger"><?php echo form_error('date'); ?></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('amount'); ?></label><small class="req"> *</small>
                                        <input id="amount" name="amount" placeholder="" type="text" class="form-control"  value="<?php echo set_value('amount', $expense['amount']); ?>" />
                                        <span class="text-danger"><?php echo form_error('amount'); ?></span>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('currency'); ?></label>

                                            <select autofocus="" id="curr_id" name="curr_id" class="form-control" >
                                                <?php
                                                foreach ($currencies as $currency) {
                                                    ?>
                                                    <option value="<?php echo $currency['curr_id'] ?>"<?php
                                                    if ($currency['curr_id'] == $expense['curr_id']) {
                                                        echo "selected =selected";
                                                    }
                                                    ?>><?php echo $currency['currency'] ?></option>

                                                    <?php
                                                    $count++;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('payment_mode'); ?></label>

                                    <select autofocus="" id="mode_id" name="mode_id" class="form-control" >
                                        <?php
                                        foreach ($paymentmodes as $paymentmode) {
                                            ?>
                                            <option value="<?php echo $paymentmode['mode_id'] ?>" <?php
                                            if ($expense['mode_id'] == $paymentmode['mode_id']) {
                                                echo "selected =selected";
                                            }
                                            ?> ><?php echo $paymentmode['mode_name'] ?></option>

                                            <?php
                                            $count++;
                                        }
                                        ?>
                                    </select>
                                     <span class="text text-danger limit_error"><b><?php echo $this->lang->line('max_expense_limit'); ?></b>: <span class="ava_limit">0</span></span><br>
                                    <span class="text text-danger remaining_error"><b><?php echo $this->lang->line('remaining_expense_limit'); ?></b>: <span class="ava_remaining">0</span></span>
                                </div>
                                 <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('invoice_no'); ?></label>
                                    <input id="invoice_no" name="invoice_no" placeholder="" type="text" class="form-control"  value="<?php echo set_value('invoice_no', $expense['invoice_no']); ?>" />
                                    <span class="text-danger"><?php echo form_error('invoice_no'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('attach_invoice'); ?></label>
                                    <input id="attach_invoice" name="attach_invoice" placeholder="" type="file" class="filestyle form-control"  value="<?php echo set_value('attach_invoice'); ?>" />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('approval_auth'); ?></label>

                                    <select autofocus="" id="auth_id" name="auth_id" class="form-control" >
                                        <option value=""><?php echo $this->lang->line('select'); ?></option>
                                        <?php
                                        foreach ($approval_auths as $approval_auth) {
                                            ?>
                                            <option value="<?php echo $approval_auth['auth_id'] ?>"<?php
                                            if ($expense['auth_id'] == $approval_auth['auth_id']) {
                                                echo "selected =selected";
                                            }
                                            ?>><?php echo $approval_auth['auth_name'] ?></option>

                                            <?php
                                            $count++;
                                        }
                                        ?>
                                    </select>
                                     <span class="text text-danger auth_limit_error"><b><?php echo $this->lang->line('max_expense_limit'); ?></b>: <span class="ava_auth_limit">0</span></span><br>
                                    <span class="text text-danger remaining_auth_error"><b><?php echo $this->lang->line('remaining_expense_limit'); ?></b>: <span class="ava_auth_remaining">0</span></span>
                                </div>
                                

                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('approval_ref'); ?></label>
                                    <input id="approval_ref" name="approval_ref" placeholder="" type="text" class="form-control" value="<?php echo set_value('approval_ref', $expense['approval_ref']); ?>"  />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('attach_approval'); ?></label>
                                    <input id="attach_approval" name="attach_approval" placeholder="" type="file" class="filestyle form-control"  value="<?php echo set_value('attach_approval'); ?>" />
                                </div>
                                
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('attach_document'); ?></label>
                                    <input id="documents" name="documents" placeholder="" type="file" class="filestyle form-control"  value="<?php echo set_value('documents'); ?>" />
                                    <span class="text-danger"><?php echo form_error('documents'); ?></span>
                                </div>

                                
                                

                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('description'); ?></label>
                                    <textarea class="form-control" id="description" name="description" placeholder="" rows="3" placeholder="Enter ..."><?php echo set_value('description'); ?><?php echo set_value('description', $expense['note']) ?></textarea>
                                    <span class="text-danger"><?php echo form_error('description'); ?></span>
                                </div>
                            </div><!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></button>
                            </div>
                        </form>
                    </div>

                </div><!--/.col (right) -->
                <!-- left column -->
            <?php } ?>
            <div class="col-md-<?php
            if ($this->rbac->hasPrivilege('expense', 'can_add') || $this->rbac->hasPrivilege('expense', 'can_edit')) {
                echo "8";
            } else {
                echo "12";
            }
            ?>">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix"><?php echo $this->lang->line('expense_list'); ?></h3>
                        <div class="box-tools pull-right">
                        </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="mailbox-messages table-responsive">
                            <div class="download_label"><?php echo $this->lang->line('expense_list'); ?></div>
                            <table class="table table-striped table-bordered table-hover expense-list" data-export-title="<?php echo $this->lang->line('expense_list'); ?>">
                                <thead>
                                    <tr>
                                            <th><?php echo $this->lang->line('voucher_no'); ?>
                                            </th>
                                             <th><?php echo $this->lang->line('expense_head'); ?>
                                            </th>
                                            <th><?php echo $this->lang->line('name'); ?>
                                            </th>
                                            <th><?php echo $this->lang->line('date'); ?>
                                            </th>
                                            <th><?php echo $this->lang->line('amount'); ?>
                                            </th>
                                             <th class="noExport"><?php echo $this->lang->line('download'); ?>
                                            </th>
                                            <th class="text-right noExport"><?php echo $this->lang->line('action'); ?></th>
                                            
                                        </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table><!-- /.table -->



                        </div><!-- /.mail-box-messages -->
                    </div><!-- /.box-body -->
                </div>
            </div><!--/.col (left) -->
            <!-- right column -->

        </div>
        <div class="row">
            <div class="col-md-12">
            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    ( function ( $ ) {
    'use strict';
    $(document).ready(function () {
        initDatatable('expense-list','admin/expense/getexpenselist',[],[],100);
    });
} ( jQuery ) )
</script>
<script type="text/javascript">
     $(document).ready(function () {
        $('.select2').select2();

    });
     $('.limit_error').hide();
     $('.auth_limit_error').hide();
     $('.remaining_error').hide();
     $('.remaining_auth_error').hide();
     $('.remaining_error').hide();
       $(document).on('change', '#exp_head_id', function (e) {

        var exp_head_id = $(this).val();
        // $('#div_avail').hide();
        availableLimit(exp_head_id);

    });


       function availableLimit(exp_head_id) {

        $('.ava_limit').html(0);
        if (exp_head_id != "" && exp_head_id != null) {
            var date = $('.date').val();
            $.ajax({
                type: "POST",
                url: base_url + "admin/expense/availableLimit",
                data: {'exp_head_id': exp_head_id,'date':date},
                dataType: "json",
                beforeSend: function () {
                    $('.limit_error').show();
                    $('.remaining_error').show();
                    $('.ava_limit').empty().html('Loading...');
                    $('.ava_remaining').empty().html('Loading...');
                },
                success: function (data) {
                    $('.ava_limit').html(data.exp_monthlylimit);
                    $('.ava_remaining').html(data.remaining);
                },
                complete: function () {
                }

            });
        } else {
            $('.limit_error').hide();
            $('.remaining_error').hide();
        }
    }

    $(document).on('change', '#auth_id', function (e) {

        var auth_id = $(this).val();
        availableAuthLimit(auth_id);

    });
    function availableAuthLimit(auth_id) {

        $('.ava_auth_limit').html(0);
        if (auth_id != "" && auth_id != null) {
            var date = $('.date').val();
            $.ajax({
                type: "POST",
                url: base_url + "admin/expense/availableAuthLimit",
                data: {'auth_id': auth_id,'date':date},
                dataType: "json",
                beforeSend: function () {
                    $('.auth_limit_error').show();
                    $('.remaining_auth_error').show();
                    $('.ava_auth_limit').empty().html('Loading...');
                    $('.ava_auth_remaining').empty().html('Loading...');
                },
                success: function (data) {
                    $('.ava_auth_limit').html(data.auth_monthlylimit);
                    $('.ava_auth_remaining').html(data.remaining);
                },
                complete: function () {
                }

            });
        } else {
            $('.auth_limit_error').hide();
            $('.remaining_auth_error').hide();
        }
    }

    $('.date').change(function() { 
    var exp_head_id = $('#exp_head_id').find(":selected").val();
    var auth_id = $('#auth_id').find(":selected").val();
    if(exp_head_id != null){
        availableLimit(exp_head_id);
    }
    if(auth_id != null){
        availableAuthLimit(auth_id);
    }
});
    </script>